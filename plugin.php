<?php
/**
 * Plugin Name: Panther-selfservice-block
 * Plugin URI: https://pantherapplications.com
 * Description: This plugin supplies gutenberg blocks for building a selfservice portal for Pantheradmin.
 * Author: PantherApplications
 * Author URI: https://pantherapplications.com
 * Version: 1.0.3
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once plugin_dir_path(__FILE__) . "pawpplugin_api.php";

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';

require_once plugin_dir_path( __FILE__ ) . 'fetchMedia.php';
add_action('wp_ajax_fetch_media', 'fetchMedia');


// ********************************************************************************************************************************************************************
// *------------------------------------------------------------------  SETTINGS PAGE  -------------------------------------------------------------------------------*
// ********************************************************************************************************************************************************************
function psb_add_settings_page() {
    add_options_page( 'Panther Selfservice Blocks page	', 'Panther Selfservice Blocks', 'manage_options', 'panther_selfservice_blocks', 'psb_render_plugin_settings_page' );
}
add_action( 'admin_menu', 'psb_add_settings_page' );

function psb_render_plugin_settings_page() {
    ?>
    <h2>Pantheradmin blocks Settings</h2>
    <form action="options.php" method="post">
        <?php
        settings_fields( 'psb_plugin_options' );
        do_settings_sections( 'psb_example_plugin' ); ?>
		<input type="button" id="fetch_media_button" value="Hent logoer til TV-kanaler" class="button button-primary" onClick="fetchMedia();">
		<br><br>
        <input id="submit_btn" name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( 'Save' ); ?>" />
    </form>
	<script>
		function fetchMedia(){
			jQuery.ajax({
			type: "GET",
			url: "/wp-admin/admin-ajax.php",
			data: {
				action: 'fetch_media',
			},
			success: function (output) {
				alert(output.substring(0, output.length-1));
			}
			});
		}
	</script>
    <?php
}

function psb_register_settings() {
    register_setting( 'psb_plugin_options', 'psb_plugin_options', 'psb_example_plugin_options_validate' );

	// General settings
	add_settings_section( 'general_settings', 'General settings', 'psb_plugin_general_section_text', 'psb_example_plugin' );
	// add_settings_field( 'psb_plugin_setting_general_name', 'Name', 'psb_plugin_setting_general_name', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_title', 'Title', 'psb_plugin_setting_general_title', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_cvr', 'Cvr', 'psb_plugin_setting_general_cvr', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_email', 'Email', 'psb_plugin_setting_general_email', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_contact_email', 'Contact email', 'psb_plugin_setting_general_contact_email', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_website', 'Website', 'psb_plugin_setting_general_website', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_phone', 'Phone', 'psb_plugin_setting_general_phone', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_address', 'Address', 'psb_plugin_setting_general_address', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_zip_city', 'Zip / City', 'psb_plugin_setting_general_zip_city', 'psb_example_plugin', 'general_settings' );

	// //Checkboxes
	// add_settings_field( 'psb_plugin_setting_general_variable_start_date', 'Variable start-/change-date', 'psb_plugin_setting_general_variable_start_date', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_member_can_init_move', 'Member can start move', 'psb_plugin_setting_general_member_can_init_move', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_choose_address_on_move', 'Choose address when moving', 'psb_plugin_setting_general_choose_address_on_move', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_extended_logs_on_status_page', 'Show extended logs on status page', 'psb_plugin_setting_general_extended_logs_on_status_page', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_settle_receivable', 'Settle receivable', 'psb_plugin_setting_general_settle_receivable', 'psb_example_plugin', 'general_settings' );
	// add_settings_field( 'psb_plugin_setting_general_limit_products_to_delivery_address_available', 'Limit products to whats available on delivery address', 'psb_plugin_setting_general_limit_products_to_delivery_address_available', 'psb_example_plugin', 'general_settings' );
    add_settings_field( 'psb_update_contact_too', 'Update contact info too when updating phone and email', 'psb_update_contact_too', 'psb_example_plugin', 'general_settings' );
    add_settings_field( 'psb_disable_css', 'Disable included CSS', 'psb_disable_css', 'psb_example_plugin', 'general_settings' );


    add_settings_section( 'password_rules', 'Password rules', 'psb_plugin_password_rules_section_text', 'psb_example_plugin' );
    add_settings_field( 'psb_password_rules_length', 'Minimum length for password', 'psb_password_rules_length', 'psb_example_plugin', 'password_rules' );
    add_settings_field( 'psb_use_otp', 'Use one time pass code', 'psb_use_otp', 'psb_example_plugin', 'password_rules' );
    add_settings_field( 'psb_password_rules_chars', 'Character requirements for password', 'psb_password_rules_chars', 'psb_example_plugin', 'password_rules' );

	// Api settings section
	add_settings_section( 'api_settings', 'API connection settings', 'psb_plugin_api_section_text', 'psb_example_plugin' );
	// Api settings fields
    add_settings_field( 'psb_plugin_setting_api_key', 'API Key', 'psb_plugin_setting_api_key', 'psb_example_plugin', 'api_settings' );
	add_settings_field( 'psb_plugin_setting_api_url', 'API Url', 'psb_plugin_setting_api_url', 'psb_example_plugin', 'api_settings' );

	// Navigation settings section
	add_settings_section( 'navi_settings', 'Navigation settings', 'psb_plugin_navi_section_text', 'psb_example_plugin' );
	// Navigation settings fields
	add_settings_field( 'psb_plugin_setting_login_page', 'Login page', 'psb_plugin_setting_login_page', 'psb_example_plugin', 'navi_settings' );
	add_settings_field( 'psb_plugin_setting_customerinfo_page', 'Customer info page', 'psb_plugin_setting_customerinfo_page', 'psb_example_plugin', 'navi_settings' );
	add_settings_field( 'psb_plugin_setting_customerinvoices_page', 'Customer invoices page', 'psb_plugin_setting_customerinvoices_page', 'psb_example_plugin', 'navi_settings' );
	add_settings_field( 'psb_plugin_setting_customerproducts_page', 'Customer products page', 'psb_plugin_setting_customerproducts_page', 'psb_example_plugin', 'navi_settings' );
	add_settings_field( 'psb_plugin_setting_customercommunication_page', 'Customer communication page', 'psb_plugin_setting_customercommunication_page', 'psb_example_plugin', 'navi_settings' );

	add_settings_field( 'psb_plugin_setting_internet_page', 'Internet page', 'psb_plugin_setting_internet_page', 'psb_example_plugin', 'navi_settings' );
	add_settings_field( 'psb_plugin_setting_wifisettings_page', 'WiFi settings page', 'psb_plugin_setting_wifisettings_page', 'psb_example_plugin', 'navi_settings' );
	add_settings_field( 'psb_plugin_setting_additional_hidden_menus', 'Additional Menu points', 'psb_plugin_setting_additional_hidden_menus', 'psb_example_plugin', 'navi_settings' );

	// Magiclink email setup section
	add_settings_section( 'magiclink_settings', 'Magiclink settings', 'psb_plugin_magiclink_section_text', 'psb_example_plugin' );
	// Magiclink email setup fields
	add_settings_field( 'psb_plugin_setting_magiclink_subject', 'Email emne', 'psb_plugin_setting_magiclink_subject', 'psb_example_plugin', 'magiclink_settings' );
	add_settings_field( 'psb_plugin_setting_magiclink_template', 'Email skabelon', 'psb_plugin_setting_magiclink_template', 'psb_example_plugin', 'magiclink_settings' );

	// Payment settings section
	add_settings_section( 'payment_settings', 'Payment settings (only for customer signup page)', 'psb_plugin_payment_section_text', 'psb_example_plugin' );
	// Payment settings fields
	// add_settings_field( 'psb_plugin_setting_payment_cardpay_providers', 'Betalingskort-aftaler', 'psb_plugin_setting_payment_cardpay_providers', 'psb_example_plugin', 'payment_settings' );
	// add_settings_field( 'psb_plugin_setting_payment_buy_with_no_card', 'Buy without any registered creditcard (add to next bill) ', 'psb_plugin_setting_payment_buy_with_no_card', 'psb_example_plugin', 'payment_settings' );
	// add_settings_field( 'psb_plugin_setting_payment_subscribe_cardpay', 'Subscribe with credit card', 'psb_plugin_setting_payment_subscribe_cardpay', 'psb_example_plugin', 'payment_settings' );
	// add_settings_field( 'psb_plugin_setting_payment_subscribe_cardpay_pay_only_debtdue', 'Subscribe with creditcard, pay only current debt due.', 'psb_plugin_setting_payment_subscribe_cardpay_pay_only_debtdue', 'psb_example_plugin', 'payment_settings' );
	// add_settings_field( 'psb_plugin_setting_payment_subscribe_cardpay_standard', 'Subscribe with creditcard standard', 'psb_plugin_setting_payment_subscribe_cardpay_standard', 'psb_example_plugin', 'payment_settings' );
	// add_settings_field( 'psb_plugin_setting_payment_subscribe_betalingservice', 'Subscribe with betalingsservice', 'psb_plugin_setting_payment_subscribe_betalingservice', 'psb_example_plugin', 'payment_settings' );
	add_settings_field( 'psb_plugin_setting_payment_payment_tos_file', 'Payment Terms of service', 'psb_plugin_setting_payment_payment_tos_file', 'psb_example_plugin', 'payment_settings' );
	add_settings_field( 'psb_plugin_setting_payment_gateway', 'Payment Gateway', 'psb_plugin_setting_payment_gateway', 'psb_example_plugin', 'payment_settings' );
	add_settings_field( 'psb_plugin_setting_merchant_id', 'Merchant ID', 'psb_plugin_setting_merchant_id', 'psb_example_plugin', 'payment_settings' );
	add_settings_field( 'psb_plugin_setting_currency', 'Currency', 'psb_plugin_setting_currency', 'psb_example_plugin', 'payment_settings' );
	add_settings_field( 'psb_plugin_setting_payment_hash', 'Webhook/callback hash', 'psb_plugin_setting_payment_hash', 'psb_example_plugin', 'payment_settings' );
	add_settings_field( 'psb_plugin_setting_payment_api_user', 'Payment API user', 'psb_plugin_setting_payment_api_user', 'psb_example_plugin', 'payment_settings' );
	add_settings_field( 'psb_plugin_setting_payment_api_key', 'Payment API key', 'psb_plugin_setting_payment_api_key', 'psb_example_plugin', 'payment_settings' );
	add_settings_field( 'psb_plugin_setting_payment_window_user', 'Payment window user', 'psb_plugin_setting_payment_window_user', 'psb_example_plugin', 'payment_settings' );
	add_settings_field( 'psb_plugin_setting_payment_window_key', 'Payment_window key', 'psb_plugin_setting_payment_window_key', 'psb_example_plugin', 'payment_settings' );

	// Payment settings section
	// add_settings_section( 'subscription_settings', 'Subscription settings', 'psb_plugin_subscription_section_text', 'psb_example_plugin' );
	// // Payment settings fields
	// add_settings_field( 'psb_plugin_setting_subscription_on_hold_link', 'Show "Put subscription on hold" link', 'psb_plugin_setting_subscription_on_hold_link', 'psb_example_plugin', 'subscription_settings' );
	// add_settings_field( 'psb_plugin_setting_subscription_subscription_cancellation_steps', 'Member termination notice', 'psb_plugin_setting_subscription_subscription_cancellation_steps', 'psb_example_plugin', 'subscription_settings' );
	// add_settings_field( 'psb_plugin_setting_subscription_termination_office_only', 'Only able to terminate through contacting office', 'psb_plugin_setting_subscription_termination_office_only', 'psb_example_plugin', 'subscription_settings' );

	// Social login settings section
	add_settings_section( 'sso_settings', 'Social login settings', 'psb_plugin_sso_section_text', 'psb_example_plugin' );
	// Social login fields
	add_settings_field( 'psb_plugin_setting_facebook_client_id', 'Facebook Client Id', 'psb_plugin_setting_facebook_client_id', 'psb_example_plugin', 'sso_settings' );
	add_settings_field( 'psb_plugin_setting_google_client_id', 'Google Client Id', 'psb_plugin_setting_google_client_id', 'psb_example_plugin', 'sso_settings' );
	add_settings_field( 'psb_plugin_setting_apple_client_id', 'Apple Client Id', 'psb_plugin_setting_apple_client_id', 'psb_example_plugin', 'sso_settings' );
	add_settings_field( 'psb_plugin_setting_apple_redirect_url', 'Apple Redirect Url', 'psb_plugin_setting_apple_redirect_url', 'psb_example_plugin', 'sso_settings' );


}
add_action( 'admin_init', 'psb_register_settings' );

function psb_example_plugin_options_validate( $input ) {
	// API KEY
    if ( !preg_match( '/^[a-z0-9-_\.~]{64}$/i', trim($input['api_key'])) ) {
		$options = get_option( 'psb_plugin_options' );
		$input['api_key'] = $options['api_key'];
		add_settings_error('psb_plugin_setting_api_key', 'Wrong format','API key must be exactly 64 characters long', 'error');
    }
	// API URL
	// if ( !preg_match( '/((https:\/\/)|(http:\/\/))[[:ascii:]]{1,}/i', trim($input['api_url'])) && !preg_match( '/localhost\:[0-9]{1,}|localhost/i', trim($input['api_url']))) {
	// 	$options = get_option( 'psb_plugin_options' );
	// 	$input['api_url'] = $options['api_url'];
	// 	add_settings_error('psb_plugin_setting_api_url', 'Wrong format','API url must be an url', 'error');
    // }
	return $input;
}

// General settings
function psb_plugin_general_section_text() {
    echo '<p></p>';
}

function psb_plugin_setting_general_name() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_name',$options) ? $options['general_name'] : '';
    echo "<input style='width: 250px'  id='psb_plugin_setting_general_name' name='psb_plugin_options[general_name]' type='text' value='".$current_val."' placeholder=\"Name\" />";
}
function psb_plugin_setting_general_title() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_title',$options) ? $options['general_title'] : '';
    echo "<input style='width: 250px'  id='psb_plugin_setting_general_title' name='psb_plugin_options[general_title]' type='text' value='".$current_val."' placeholder=\"Title\" />";
}
function psb_plugin_setting_general_cvr() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_cvr',$options) ? $options['general_cvr'] : '';
    echo "<input style='width: 250px'  id='psb_plugin_setting_general_cvr' name='psb_plugin_options[general_cvr]' type='text' value='".$current_val."' placeholder=\"Cvr\" />";
}
function psb_plugin_setting_general_email() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_email',$options) ? $options['general_email'] : '';
    echo "<input style='width: 250px'  id='psb_plugin_setting_general_email' name='psb_plugin_options[general_email]' type='text' value='".$current_val."' placeholder=\"Email\" />";
}
function psb_plugin_setting_general_contact_email() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_contact_email',$options) ? $options['general_contact_email'] : '';
    echo "<input style='width: 250px'  id='psb_plugin_setting_general_contact_email' name='psb_plugin_options[general_contact_email]' type='text' value='".$current_val."' placeholder=\"Contact email\" />";
}
function psb_plugin_setting_general_website() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_website',$options) ? $options['general_website'] : '';
    echo "<input style='width: 250px'  id='psb_plugin_setting_general_website' name='psb_plugin_options[general_website]' type='text' value='".$current_val."' placeholder=\"Website\" />";
}
function psb_plugin_setting_general_phone() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_phone',$options) ? $options['general_phone'] : '';
    echo "<input style='width: 250px'  id='psb_plugin_setting_general_phone' name='psb_plugin_options[general_phone]' type='text' value='".$current_val."' placeholder=\"Phone\" />";
}
function psb_plugin_setting_general_address() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_address',$options) ? $options['general_address'] : '';
    echo "<input style='width: 250px'  id='psb_plugin_setting_general_address' name='psb_plugin_options[general_address]' type='text' value='".$current_val."' placeholder=\"Address\" />";
}
function psb_plugin_setting_general_zip_city() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_zip_city',$options) ? $options['general_zip_city'] : '';
    echo "<input style='width: 250px'  id='psb_plugin_setting_general_zip_city' name='psb_plugin_options[general_zip_city]' type='text' value='".$current_val."' placeholder=\"Zip / City\" />";
}
//Checkboxes
function psb_plugin_setting_general_variable_start_date() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_variable_start_date',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_general_variable_start_date' name='psb_plugin_options[general_variable_start_date]' type='checkbox' $current_val/>";
}
function psb_plugin_setting_general_member_can_init_move() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_variable_member_can_init_move',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_general_variable_member_can_init_move' name='psb_plugin_options[general_variable_member_can_init_move]' type='checkbox' $current_val/>";
}
function psb_plugin_setting_general_choose_address_on_move() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_variable_choose_address_on_move',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_general_variable_choose_address_on_move' name='psb_plugin_options[general_variable_choose_address_on_move]' type='checkbox' $current_val/>";
}
function psb_plugin_setting_general_extended_logs_on_status_page() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_variable_extended_logs_on_status_page',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_general_variable_extended_logs_on_status_page' name='psb_plugin_options[general_variable_extended_logs_on_status_page]' type='checkbox' $current_val/>";
}
function psb_plugin_setting_general_settle_receivable() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_settle_receivable',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_general_settle_receivable' name='psb_plugin_options[general_settle_receivable]' type='checkbox' $current_val/>";
}
function psb_plugin_setting_general_limit_products_to_delivery_address_available() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('general_limit_products_to_delivery_address_available',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_general_limit_products_to_delivery_address_available' name='psb_plugin_options[general_limit_products_to_delivery_address_available]' type='checkbox' $current_val/>";
}
function psb_update_contact_too() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('update_contact_too',$options) ? 'CHECKED' : '';
    echo "<input id='psb_update_contact_too' name='psb_plugin_options[update_contact_too]' type='checkbox' $current_val/>";
}
function psb_disable_css() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('psb_disable_css',$options) ? 'CHECKED' : '';
    echo "<input id='psb_disable_css' name='psb_plugin_options[psb_disable_css]' type='checkbox' $current_val/>";
}

// Password rules section
function psb_plugin_password_rules_section_text() {
	echo '<p>Password rules</p>';
}
function psb_password_rules_length() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('password_rules_length',$options) ? $options["password_rules_length"] : 6;
    echo "<input id='psb_password_rules_length' name='psb_plugin_options[password_rules_length]' min='6' max='128' type='number' value='$current_val'/>";
}
function psb_use_otp() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('psb_use_otp',$options) ? 'CHECKED' : '';;
    echo "<input id='psb_use_otp' name='psb_plugin_options[psb_use_otp]' type='checkbox' $current_val/>";
}

function psb_password_rules_chars() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('password_rules_chars',$options) ? $options["password_rules_chars"] : 'none';
    echo sprintf("<div><input id='psb_password_rules_chars_no_req' name='psb_plugin_options[password_rules_chars]' type='radio' value='none' %s >None - No requirement</input></div>", $current_val == "none" ? "checked" : "");
    echo sprintf("<div><input id='psb_password_rules_chars_low_req' name='psb_plugin_options[password_rules_chars]' type='radio' value='low' %s>Low - Alphanumeric (min. 1 Uppercase + 1 lowercase)</input></div>", $current_val == "low" ? "checked" : "");
    echo sprintf("<div><input id='psb_password_rules_chars_med_req' name='psb_plugin_options[password_rules_chars]' type='radio' value='medium' %s>Medium - Alphanumeric (min. 1 Uppercase + 1 lowercase + 1 number)</input></div>", $current_val == "medium" ? "checked" : "");
    echo sprintf("<div><input id='psb_password_rules_chars_high_req' name='psb_plugin_options[password_rules_chars]' type='radio' value='high' %s>High - Alphanumeric + special characters (min. 1 Uppercase + 1 lowercase + 1 number + 1 special char.) </input><span title='Includes: ! # $ & \ ( ) * + , - . / : ; < = > ? @ [ \ ] ^ _ { | } ~' class='fa fa-question-circle'></span></div>", $current_val == "high" ? "checked" : "");
}



// API settings
function psb_plugin_api_section_text() {
	echo '<p>Insert API key and url to connect to PantherAdmin	</p>';
}

function psb_plugin_setting_api_key() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('api_key',$options) ? $options['api_key'] : '';
    echo "<input style='width: 500px' id='psb_plugin_setting_api_key' name='psb_plugin_options[api_key]' type='text' value='".$current_val."' placeholder=\"Api key\" />";
}

function psb_plugin_setting_api_url() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('api_url',$options) ? $options['api_url'] : '';
    echo "<input style='width: 350px' id='psb_plugin_setting_api_url' name='psb_plugin_options[api_url]' type='text' value='".$current_val."' placeholder=\"https://pantheradmin.com\"/>";
}

// Navigation settings
function psb_plugin_navi_section_text() {
	echo '<p>Define what pages the different blocks are placed on, and "menu item id" is for the dynamic menu to work.</p>';
}

function psb_plugin_setting_login_page(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('login_page',$options) ? $options['login_page'] : ["page"=>"","menu_item_id"=>""];
	echo "<label for='psb_plugin_setting_login_page'> Page: </label>
	<input style='width: 250px' id='psb_plugin_setting_login_page' name='psb_plugin_options[login_page][page]' type='text' value='".$current_val['page']."' placeholder=\"login_page\"/>
	<label for='psb_plugin_setting_menu_items_login'>Menu item id: </label>
	<input style='width: 100px' id='psb_plugin_setting_menu_items_login' name='psb_plugin_options[login_page][menu_item_id]' type='number' value='".$current_val['menu_item_id']. 	"' placeholder=\"menu_item_login\"/>";
}

function psb_plugin_setting_customerinfo_page(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('customerinfo_page',$options) ? $options['customerinfo_page'] : ["page"=>"","menu_item_id"=>""];
	echo "<label for='psb_plugin_setting_customerinfo_page'> Page: </label>
	<input style='width: 250px' id='psb_plugin_setting_customerinfo_page' name='psb_plugin_options[customerinfo_page][page]' type='text' value='".$current_val['page']."' placeholder=\"customerinfo_page\"/>
	<label for='psb_plugin_setting_menu_items_customerpage'>Menu item id: </label>
	<input style='width: 100px' id='psb_plugin_setting_menu_items_customerpage' name='psb_plugin_options[customerinfo_page][menu_item_id]' type='number' value='".$current_val['menu_item_id']. 	"' placeholder=\"menu_item_customerinfo\"/>";
}

function psb_plugin_setting_customerinvoices_page(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('customerinvoices_page',$options) ? $options['customerinvoices_page'] : ["page"=>"","menu_item_id"=>""];
	echo "<label for='psb_plugin_setting_customerinvoices_page'> Page: </label>
	<input style='width: 250px' id='psb_plugin_setting_customerinvoices_page' name='psb_plugin_options[customerinvoices_page][page]' type='text' value='".$current_val['page']."' placeholder=\"customerinvoices_page\"/>
	<label for='psb_plugin_setting_menu_items_customerinvoices'>Menu item id: </label>
	<input style='width: 100px' id='psb_plugin_setting_menu_items_customerinvoices' name='psb_plugin_options[customerinvoices_page][menu_item_id]' type='number' value='".$current_val['menu_item_id']. 	"' placeholder=\"menu_item_customer_invoices\"/>";
}

function psb_plugin_setting_customerproducts_page(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('customerproducts_page',$options) ? $options['customerproducts_page'] : ["page"=>"","menu_item_id"=>""];
	echo "<label for='psb_plugin_setting_customerproducts_page'> Page: </label>
	<input style='width: 250px' id='psb_plugin_setting_customerproducts_page' name='psb_plugin_options[customerproducts_page][page]' type='text' value='".$current_val['page']."' placeholder=\"customerproducts_page\"/>
	<label for='psb_plugin_setting_menu_items_customerproducts'>Menu item id: </label>
	<input style='width: 100px' id='psb_plugin_setting_menu_items_customerproducts' name='psb_plugin_options[customerproducts_page][menu_item_id]' type='number' value='".$current_val['menu_item_id']. 	"' placeholder=\"menu_item_customer_products\"/>";
}

function psb_plugin_setting_internet_page(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('internet_page',$options) ? $options['internet_page'] : ["page"=>"","menu_item_id"=>""];
	echo "<label for='psb_plugin_setting_internet_page'> Page: </label>
	<input style='width: 250px' id='psb_plugin_setting_internet_page' name='psb_plugin_options[internet_page][page]' type='text' value='".$current_val['page']."' placeholder=\"internet_page\"/>
	<label for='psb_plugin_setting_menu_items_internet'>Menu item id: </label>
	<input style='width: 100px' id='psb_plugin_setting_menu_items_internet' name='psb_plugin_options[internet_page][menu_item_id]' type='number' value='".$current_val['menu_item_id']. 	"' placeholder=\"menu_item_internet\"/>";
}

function psb_plugin_setting_wifisettings_page(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('wifisettings_page',$options) ? $options['wifisettings_page'] : ["page"=>"","menu_item_id"=>""];
	echo "<label for='psb_plugin_setting_wifisettings_page'> Page: </label>
	<input style='width: 250px' id='psb_plugin_setting_wifisettings_page' name='psb_plugin_options[wifisettings_page][page]' type='text' value='".$current_val['page']."' placeholder=\"wifisettings_page\"/>
	<label for='psb_plugin_setting_menu_items_wifisettings'>Menu item id: </label>
	<input style='width: 100px' id='psb_plugin_setting_menu_items_wifisettings' name='psb_plugin_options[wifisettings_page][menu_item_id]' type='number' value='".$current_val['menu_item_id']. 	"' placeholder=\"menu_item_wifisettings\"/>";
}

function psb_plugin_setting_customercommunication_page(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('customercommunication_page',$options) ? $options['customercommunication_page'] : ["page"=>"","menu_item_id"=>""];
	echo "<label for='psb_plugin_setting_customercommunication_page'> Page: </label>
	<input style='width: 250px' id='psb_plugin_setting_customercommunication_page' name='psb_plugin_options[customercommunication_page][page]' type='text' value='".$current_val['page']."' placeholder=\"customercommunication_page\"/>
	<label for='psb_plugin_setting_menu_items_customercommunication'>Menu item id: </label>
	<input style='width: 100px' id='psb_plugin_setting_menu_items_customercommunication' name='psb_plugin_options[customercommunication_page][menu_item_id]' type='number' value='".$current_val['menu_item_id']. 	"' placeholder=\"menu_item_customer_communication\"/>";
}

function psb_plugin_setting_additional_hidden_menus(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('additional_hidden_menus',$options) ? $options['additional_hidden_menus'] : "";
	echo "<label for='psb_plugin_setting_additional_hidden_menus'>Menu items to hide when not loggin in</label>
	<input style='width: 250px' id='psb_plugin_setting_additional_hidden_menus' name='psb_plugin_options[additional_hidden_menus]' type='text' value='".$current_val."' placeholder=\"123,456,123\"/>";
}

//Magiclink settings
function psb_plugin_magiclink_section_text() {
	echo '<p></p>';
}

function psb_plugin_setting_magiclink_subject(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('magiclink_subject',$options) ? $options['magiclink_subject'] : '';
	echo "<input style='width: 350px' id='psb_plugin_setting_magiclink_subject' name='psb_plugin_options[magiclink_subject]' type='text' value='".$current_val."' placeholder=\"Email emne\"/>";
}

function psb_plugin_setting_magiclink_template(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('magiclink_template',$options) ? $options['magiclink_template'] : '';
	$content = $current_val;
	$editor_id = 'magiclink_template_editor';
	echo '<p>Remember to include following link somewhere in the email. It is the link used to login to selfservice by the customer/member.</p><p>'.get_rest_url().'pantheradmin/v1/magic-login?token={.magiclinktoken.}</p>';
	wp_editor( $content, $editor_id, $settings = array('textarea_name'=>'psb_plugin_options[magiclink_template]',
	'textarea_rows'=>15,
	'editor_css'=>'<style>#wp-magiclink_template_editor-editor-container{max-width: 750px;}#wp-magiclink_template_editor-editor-tools{max-width: 750px;}</style>',
	'wpautop'=>false
));
}

// Payment settings
function psb_plugin_payment_section_text() {
	echo '<p></p>';
}

function psb_plugin_setting_payment_cardpay_providers(){
	$options = get_option( 'psb_plugin_options' );
	$current_val_dk = !empty($options) && array_key_exists('payment_cardpay_providers_dankort',$options) ? 'CHECKED' : '';
	$current_val_edk = !empty($options) && array_key_exists('payment_cardpay_providers_edankort',$options) ? 'CHECKED' : '';
	$current_val_mp = !empty($options) && array_key_exists('payment_cardpay_providers_mobilepay',$options) ? 'CHECKED' : '';
	$current_val_ic = !empty($options) && array_key_exists('payment_cardpay_providers_internationalcards',$options) ? 'CHECKED' : '';
	echo "
	<table>
	<tbody>
	<tr>
	<td><label for='psb_plugin_setting_payment_cardpay_providers_dankort'>Dankort</label></td>
	<td><input id='psb_plugin_setting_payment_cardpay_providers_dankort' name='psb_plugin_options[payment_cardpay_providers_dankort]' type='checkbox' value='".$current_val_dk."'/></td>
	</tr>
	<tr>
	<td><label for='psb_plugin_setting_payment_cardpay_providers_dankort'>eDankort</label></td>
	<td><input id='psb_plugin_setting_payment_cardpay_providers_edankort' name='psb_plugin_options[payment_cardpay_providers_edankort]' type='checkbox' value='".$current_val_edk."'/></td>
	</tr>
	<tr>
	<td><label for='psb_plugin_setting_payment_cardpay_providers_mobilepay'>Mobilepay</label></td>
	<td><input id='psb_plugin_setting_payment_cardpay_providers_mobilepay' name='psb_plugin_options[payment_cardpay_providers_mobilepay]' type='checkbox' value='".$current_val_mp."'/></td>
	</tr>
	<tr>
	<td><label for='psb_plugin_setting_payment_cardpay_providers_internationalcards'>International Cards</label></td>
	<td><input id='psb_plugin_setting_payment_cardpay_providers_internationalcards' name='psb_plugin_options[payment_cardpay_providers_internationalcards]' type='checkbox' value='".$current_val_ic."'/></td>
	</tr>
	</tbody>
	</table>
	";
}
function psb_plugin_setting_payment_buy_with_no_card() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('payment_buy_with_no_card',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_payment_buy_with_no_card' name='psb_plugin_options[payment_buy_with_no_card]' type='checkbox' $current_val/>";
}
function psb_plugin_setting_payment_subscribe_cardpay() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('payment_subscribe_cardpay',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_payment_subscribe_cardpay' name='psb_plugin_options[payment_subscribe_cardpay]' type='checkbox' $current_val/>";
}
function psb_plugin_setting_payment_subscribe_cardpay_pay_only_debtdue() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('payment_subscribe_cardpay_pay_only_debtdue',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_payment_subscribe_cardpay_pay_only_debtdue' name='psb_plugin_options[payment_subscribe_cardpay_pay_only_debtdue]' type='checkbox' $current_val/>";
}
function psb_plugin_setting_payment_subscribe_cardpay_standard() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('payment_subscribe_cardpay_standard',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_payment_subscribe_cardpay_standard' name='psb_plugin_options[payment_subscribe_cardpay_standard]' type='checkbox' $current_val />";
}
function psb_plugin_setting_payment_subscribe_betalingservice() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('payment_subscribe_betalingservice',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_payment_subscribe_betalingservice' name='psb_plugin_options[payment_subscribe_betalingservice]' type='checkbox' $current_val/>";
}

function psb_plugin_setting_payment_payment_tos_file(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('payment_payment_tos',$options) ? $options['payment_payment_tos'] : '';
	echo "<input style='width: 100px' id='psb_plugin_setting_payment_payment_tos' name='psb_plugin_options[payment_payment_tos]' placeholder=\"url to document\" value=\"".$current_val."\"/>";
}

//Payment gateway settings

function psb_plugin_setting_payment_gateway(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('payment_gateway',$options) ? $options['payment_gateway'] : '';
	echo "<select id='psb_plugin_setting_payment_gateway' name='psb_plugin_options[payment_gateway]'>";
	echo "<option value='' ".(($current_val == "") ? "SELECTED" : "").">-- Vælg --</option>";
	echo "<option value='Epay' ".(($current_val == "Epay") ? "SELECTED" : "").">Epay</option>";
	echo "<option value='Quickpay' ".(($current_val == "Quickpay") ? "SELECTED" : "").">Quickpay</option>";
	echo "<option value='Reepay' ".(($current_val == "Reepay") ? "SELECTED" : "").">Reepay</option>";
	echo "</select>";
}

function psb_plugin_setting_merchant_id(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('merchant_id',$options) ? $options['merchant_id'] : '';
	echo "<input style='width: 100px' id='psb_plugin_setting_merchant_id' name='psb_plugin_options[merchant_id]' value=\"".$current_val."\"/>";
}

function psb_plugin_setting_currency(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('currency',$options) ? $options['currency'] : '';
	echo "<input style='width: 100px' id='psb_plugin_setting_currency' name='psb_plugin_options[currency]' value=\"".$current_val."\"/>";
}

function psb_plugin_setting_payment_hash(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('payment_gateway_hash',$options) ? $options['payment_gateway_hash'] : '';
	echo "<input style='width: 250px' id='psb_plugin_setting_payment_hash' name='psb_plugin_options[payment_gateway_hash]' value=\"".$current_val."\"/>";
}

function psb_plugin_setting_payment_api_user(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('payment_api_user',$options) ? $options['payment_api_user'] : '';
	echo "<input style='width: 250px' id='psb_plugin_setting_payment_api_user' name='psb_plugin_options[payment_api_user]' value=\"".$current_val."\"/>";
}

function psb_plugin_setting_payment_api_key(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('payment_api_key',$options) ? $options['payment_api_key'] : '';
	echo "<input style='width: 250px' id='psb_plugin_setting_payment_api_key' name='psb_plugin_options[payment_api_key]' value=\"".$current_val."\"/>";
}

function psb_plugin_setting_payment_window_user(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('payment_window_user',$options) ? $options['payment_window_user'] : '';
	echo "<input style='width: 250px' id='psb_plugin_setting_payment_window_user' name='psb_plugin_options[payment_window_user]' value=\"".$current_val."\"/>";
}

function psb_plugin_setting_payment_window_key(){
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('payment_window_key',$options) ? $options['payment_window_key'] : '';
	echo "<input style='width: 250px' id='psb_plugin_setting_payment_window_key' name='psb_plugin_options[payment_window_key]' value=\"".$current_val."\"/>";
}

// Subscription settings
function psb_plugin_subscription_section_text() {
	echo '<p></p>';
}

function psb_plugin_setting_subscription_on_hold_link() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('subscription_on_hold_link',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_subscription_on_hold_link' name='psb_plugin_options[subscription_on_hold_link]' type='checkbox' $current_val/>";
}
function psb_plugin_setting_subscription_subscription_cancellation_steps() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('subscription_subscription_cancellation_steps',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_subscription_subscription_cancellation_steps' name='psb_plugin_options[subscription_subscription_cancellation_steps]' type='checkbox' $current_val/>";
}
function psb_plugin_setting_subscription_termination_office_only() {
    $options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('subscription_termination_office_only',$options) ? 'CHECKED' : '';
    echo "<input id='psb_plugin_setting_subscription_termination_office_only' name='psb_plugin_options[subscription_termination_office_only]' type='checkbox' $current_val/>";
}

// Social login settings
function psb_plugin_sso_section_text() {
	echo '<p>Insert credentials from the proper Developer/API site.	</p>';
}

function psb_plugin_setting_facebook_client_id() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('facebook_client_id',$options) ? $options['facebook_client_id'] : '';
	echo "<input style='width: 500px' id='psb_plugin_setting_facebook_client_id' name='psb_plugin_options[facebook_client_id]' type='text' value='".$current_val."' placeholder=\"Facebook Client Id\" />";
}

function psb_plugin_setting_google_client_id() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('google_client_id',$options) ? $options['google_client_id'] : '';
	echo "<input style='width: 500px' id='psb_plugin_setting_google_client_id' name='psb_plugin_options[google_client_id]' type='text' value='".$current_val."' placeholder=\"Google Client Id\" />";
}

function psb_plugin_setting_apple_client_id() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('apple_client_id',$options) ? $options['apple_client_id'] : '';
	echo "<input style='width: 500px' id='psb_plugin_setting_apple_client_id' name='psb_plugin_options[apple_client_id]' type='text' value='".$current_val."' placeholder=\"Apple Client Id\" />";
}

function psb_plugin_setting_apple_redirect_url() {
	$options = get_option( 'psb_plugin_options' );
	$current_val = !empty($options) && array_key_exists('apple_redirect_url',$options) ? $options['apple_redirect_url'] : '';
	echo "<input style='width: 500px' id='psb_plugin_setting_apple_redirect_url' name='psb_plugin_options[apple_redirect_url]' type='text' value='".$current_val."' placeholder=\"Apple Redirect Url\" />";
}
