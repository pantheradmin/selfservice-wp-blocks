<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
require_once(plugin_dir_path(__FILE__).'/paymentGateways.php');
require_once(plugin_dir_path(__FILE__).'/CustomDB.php');

add_action('rest_api_init', 'registerRoutes');
//session_start();

function apiError($code, $message, $status){
    return new WP_Error( $code, $message, array( 'status' => $status ));
}

function registerRoutes(){

    register_rest_route( 'pantheradmin/v1', '/login',array(
        'methods'  => 'POST',
        'callback' => 'login',
        'permission_callback'=> 'checkNonce'
    ));

    register_rest_route( 'pantheradmin/v1', '/otp-activate',array(
        'methods'  => 'POST',
        'callback' => 'otpActivate',
        'permission_callback'=> 'checkNonce'
    ));

    register_rest_route( 'pantheradmin/v1', '/magic-login',array(
        'methods'  => 'GET',
        'callback' => 'magiclogin',
        'permission_callback'=> 'checkNonce'
    ));

    register_rest_route( 'pantheradmin/v1', '/magiclink',array(
        'methods'  => 'POST',
        'callback' => 'magiclink',
        'permission_callback'=> 'checkNonce'
    ));

    register_rest_route( 'pantheradmin/v1', '/logout',array(
        'methods'  => 'POST',
        'callback' => 'logout',
        'permission_callback'=> 'checkNonce'
    ));

    register_rest_route( 'pantheradmin/v1', '/forgot-password',array(
        'methods'  => 'POST',
        'callback' => 'forgotPassword',
        'permission_callback'=> 'checkNonce'
    ));

	register_rest_route( 'pantheradmin/v1', '/googlelogin',array(
		'methods'  => 'POST',
		'callback' => 'googlelogin',
		'permission_callback'=> 'checkNonce'
	));

	register_rest_route( 'pantheradmin/v1', '/facebooklogin',array(
		'methods'  => 'POST',
		'callback' => 'facebooklogin',
		'permission_callback'=> 'checkNonce'
	));

	register_rest_route( 'pantheradmin/v1', '/applelogin',array(
		'methods'  => 'POST',
		'callback' => 'applelogin',
		'permission_callback'=> 'checkNonce'
	));

	register_rest_route( 'pantheradmin/v1', '/attachfacebook',array(
		'methods'  => 'POST',
		'callback' => 'attachfacebook',
		'permission_callback'=> 'checkNonce'
	));

	register_rest_route( 'pantheradmin/v1', '/attachgoogle',array(
		'methods'  => 'POST',
		'callback' => 'attachgoogle',
		'permission_callback'=> 'checkNonce'
	));

	register_rest_route( 'pantheradmin/v1', '/attachapple',array(
		'methods'  => 'POST',
		'callback' => 'attachapple',
		'permission_callback'=> 'checkNonce'
	));

	register_rest_route( 'pantheradmin/v1', '/removesocial',array(
		'methods'  => 'DELETE',
		'callback' => 'removesocial',
		'permission_callback'=> 'checkNonce'
	));

	// Customer endpoints
    register_rest_route( 'pantheradmin/v1', '/customer',array(
        'methods'  => 'GET',
        'callback' => 'getCustomerInfo',
        'permission_callback'=> 'isUserLoggedIn',
    ));

    register_rest_route( 'pantheradmin/v1', '/customer',array(
        'methods'  => 'PUT',
        'callback' => 'updateCustomerInfo',
        'permission_callback'=> 'isUserLoggedIn',
        'validate_callback' => function($request){
            // Here you can validate the data received before further processing, return apiError if not valid, true if valid
            return true;
        }
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/password',array(
        'methods'  => 'POST',
        'callback' => 'updateCustomerPassword',
        'permission_callback'=> 'isUserLoggedIn',
        'validate_callback' => function($request){
            // Here you can validate the data received before further processing, return apiError if not valid, true if valid
            return $request->get_param('new_password') == $request->get_param('new2_password') ?: apiError(400, 'Mismatch between new password and confimation', "Mismatched passwords");
        }
    ));

    register_rest_route( 'pantheradmin/v1', '/change-password',array(
        'methods'  => 'POST',
        'callback' => 'newCustomerPassword',
        'permission_callback'=> 'checkNonce',
        'validate_callback' => function($request){
            // Here you can validate the data received before further processing, return apiError if not valid, true if valid
            return $request->get_param('new_password1') == $request->get_param('new_password2') ?: apiError(400, 'Mismatch between new password and confirmation', "Mismatched passwords");
        }
    ));

    // Bullguard endpoints
    register_rest_route( 'pantheradmin/v1', '/customer/bullguard/order',array(
        'methods'  => 'POST',
        'callback' => 'orderBullguard',
        'permission_callback'=> 'isUserLoggedIn',
    ));


    register_rest_route( 'pantheradmin/v1', '/customer/bullguard/cancel',array(
        'methods'  => 'DELETE',
        'callback' => 'cancelBullguard',
        'permission_callback'=> 'isUserLoggedIn',
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/bullguard',array(
        'methods'  => 'GET',
        'callback' => 'getBullguard',
        'permission_callback'=> 'isUserLoggedIn',
    ));

    // Fsecure endpoints
    register_rest_route( 'pantheradmin/v1', '/customer/fsecure/order',array(
        'methods'  => 'POST',
        'callback' => 'orderFSecure',
        'permission_callback'=> 'isUserLoggedIn',
    ));


    register_rest_route( 'pantheradmin/v1', '/customer/fsecure/cancel',array(
        'methods'  => 'DELETE',
        'callback' => 'cancelFSecure',
        'permission_callback'=> 'isUserLoggedIn',
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/fsecure',array(
        'methods'  => 'GET',
        'callback' => 'getFSecure',
        'permission_callback'=> 'isUserLoggedIn',
    ));
    //Ordering different stuff
    register_rest_route( 'pantheradmin/v1', '/customer/order',array(
        'methods'  => 'POST',
        'callback' => 'genericOrder',
        'permission_callback'=> 'isUserLoggedIn',
    ));
    
    // Invoice endpoints
    register_rest_route( 'pantheradmin/v1', '/customer/invoices',array(
        'methods'  => 'GET',
        'callback' => 'getCustomerInvoices',
        'permission_callback'=> 'isUserLoggedIn'
    ));
    
    register_rest_route( 'pantheradmin/v1', '/customer/invoices/(?P<invoiceId>\d+)/pdf',array(
        'methods'  => 'GET',
        'callback' => 'getCustomerInvoiceAsPdf',
        'permission_callback'=> 'isUserLoggedIn',
        'args'=> [
            'id'=> [
                'validate_callback' => function($param, $request, $key) {
                    return is_numeric( $param );
                }
                ]
            ]
        )
    );

    // Product endpoints
    register_rest_route( 'pantheradmin/v1', '/customer/products',array(
        'methods'  => 'GET',
        'callback' => 'getCustomerProducts',
        'permission_callback'=> 'isUserLoggedIn'
    ));
    
    register_rest_route( 'pantheradmin/v1', '/customer/available-products',array(
        'methods'  => 'GET',
        'callback' => 'getAvailableProducts',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/products/type',array(
        'methods'  => 'GET',
        'callback' => 'getCustomerProductByIdent',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/products/type',array(
        'methods'  => 'POST',
        'callback' => 'addCustomerProductByIdent',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/products/type',array(
        'methods'  => 'DELETE',
        'callback' => 'removeCustomerProductByIdent',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/products/(?P<productId>\d+)/internet-information',array(
        'methods'  => 'GET',
        'callback' => 'getInternetInformation',
        'permission_callback'=> 'isUserLoggedIn',
        'args'=> [
            'id'=> [
                'validate_callback' => function($param, $request, $key) {
                    return is_numeric( $param );
                }
                ]
                ]
                )
    );

    register_rest_route( 'pantheradmin/v1', '/customer/cpe/(?P<mac>[a-z0-9]+)/modemsettings',array(
        'methods'  => 'GET',
        'callback' => 'getModemSettings',
        'permission_callback'=> 'isUserLoggedIn',
        'args'=> [
            'id'=> [
                'validate_callback' => function($param, $request, $key) {
                    return (strlen( $param ) > 0);
                }
                ]
            ]
        )
    );

    register_rest_route( 'pantheradmin/v1', '/customer/cpe/(?P<mac>[a-z0-9]+)/modemsettings',array(
        'methods'  => 'POST',
        'callback' => 'setModemSettings',
        'permission_callback'=> 'isUserLoggedIn',
        'args'=> [
            'id'=> [
                'validate_callback' => function($param, $request, $key) {
                    return (strlen( $param ) > 0);
                }
                ]
            ]
        )
    );

    register_rest_route( 'pantheradmin/v1', '/wifi-devices',array(
        'methods'  => 'GET',
        'callback' => 'getWifiDevices',
        'permission_callback'=> 'isUserLoggedIn',
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/products/type',array(
        'methods'  => 'GET',
        'callback' => 'getCustomerProductByIdent',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/products/type/access',array(
        'methods'  => 'GET',
        'callback' => 'hasCustomerAccessToProduct',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/products/type',array(
        'methods'  => 'GET',
        'callback' => 'getCustomerProductByIdent',
        'permission_callback'=> 'isUserLoggedIn'
    ));


    register_rest_route( 'pantheradmin/v1', '/customer/products/type',array(
        'methods'  => 'DELETE',
        'callback' => 'removeCustomerProductByIdent',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/product-configs',array(
        'methods'  => 'GET',
        'callback' => 'getCustomerProductsAvailable',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/prchannels',array(
        'methods'  => 'GET',
        'callback' => 'getProviderChannels',
        'permission_callback'=> 'checkNonce',
    ));

    register_rest_route( 'pantheradmin/v1', '/channels/(?P<channelId>\d+)/logo',array(
        'methods'  => 'GET',
        'callback' => 'getChannelLogos',
        'permission_callback'=> 'checkNonce',
    ));

    // Tickets endpoints
    register_rest_route( 'pantheradmin/v1', '/customer/tickets',array(
        'methods'  => 'GET',
        'callback' => 'getCustomerTickets',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/tickets/(?P<ticketId>\d+)/pdf',array(
        'methods'  => 'GET',
        'callback' => 'getCustomerTicketAsPdf',
        'permission_callback'=> 'isUserLoggedIn',
        'args'=> [
            'ticketId'=> [
                'validate_callback' => function($param, $request, $key) {
                    return is_numeric( $param );
                }
                ]
            ]
        )
    );


    // Payment endpoints
    register_rest_route( 'pantheradmin/v1', '/paymentgateway',array(
        'methods'  => 'GET',
        'callback' => 'getPaymentGatewayInfo',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/paymentgateway/link',array(
        'methods'  => 'GET',
        'callback' => 'paymentGatewayLink',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/paymentgateway/callback',array(
        'methods'  => 'POST',
        'callback' => 'paymentGatewayCallback',
        'permission_callback'=> '__return_true'
    ));

    register_rest_route( 'pantheradmin/v1', '/paymentgateway/callback',array(
        'methods'  => 'GET',
        'callback' => 'paymentGatewayCallback',
        'permission_callback'=> '__return_true'
    ));

    // BS A2A PISP
    register_rest_route( 'pantheradmin/v1', '/paymentgateway/bs/pisp/register',array(
        'methods'  => 'GET',
        'callback' => 'BsPispRegister',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/paymentgateway/bs/pisp/callback',array(
        'methods'  => 'GET',
        'callback' => 'BsPispCallback',
        'permission_callback'=> 'checkNonce'
    ));

    register_rest_route( 'pantheradmin/v1', '/paymentgateway/bs/pisp/cancel',array(
        'methods'  => 'GET',
        'callback' => 'BsPispCancel',
        'permission_callback'=> 'checkNonce'
    ));

    // BS Mandate

    register_rest_route( 'pantheradmin/v1', '/paymentgateway/bsmandate/init',array(
        'methods'  => 'POST',
        'callback' => 'BsMandateInit',
        'permission_callback'=> 'checkNonce'
    ));

    register_rest_route( 'pantheradmin/v1', '/paymentgateway/bsmandate/status',array(
        'methods'  => 'GET',
        'callback' => 'BsMandateStatus',
        'permission_callback'=> 'checkNonce'
    ));

    // Technician
    register_rest_route( 'pantheradmin/v1', '/products/technician-required',array(
        'methods'  => 'GET',
        'callback' => 'technicianRequired',
        'permission_callback'=> 'checkNonce'
    ));

    register_rest_route( 'pantheradmin/v1', '/products/(?P<customerProductId>\d+)/technician/available-dates',array(
        'methods'  => 'POST',
        'callback' => 'technicianAvailableDates',
        'permission_callback'=> 'checkNonce'
    ));

    register_rest_route( 'pantheradmin/v1', '/products/(?P<customerProductId>\d+)/technician/reschedule',array(
        'methods'  => 'POST',
        'callback' => 'technicianReschedule',
        'permission_callback'=> 'checkNonce'
    ));

    register_rest_route( 'pantheradmin/v1', '/products/(?P<customerProductId>\d+)/technician/reschedule/(?P<ticketId>[\w\W]+)',array(
        'methods'  => 'GET',
        'callback' => 'technicianResceduleStatus',
        'permission_callback'=> 'checkNonce'
    ));

    register_rest_route( 'pantheradmin/v1', '/signup/address/(?P<dawaId>[\w-]+)/product-configs',array(
        'methods'  => 'POST',
        'callback' => 'getAddressProductsAvailable',
        'permission_callback'=> 'checkNonce'
        )
    );

    register_rest_route( 'pantheradmin/v1', '/signup/paymentgatewaylink',array(
        'methods'  => 'POST',
        'callback' => 'signupPaymentGatewayLink',
        'permission_callback'=> '__return_true'
    ));

    //Start process
    register_rest_route( 'pantheradmin/v1', '/submit-signup',array(
		'methods'  => 'POST',
		'callback' => 'submitSignup',
		'permission_callback'=> '__return_true'
	));

    register_rest_route( 'pantheradmin/v1', '/customer/email',array(
        'methods'  => 'GET',
        'callback' => 'getEmails',
        'permission_callback'=> 'isUserLoggedIn',
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/email',array(
        'methods'  => 'PATCH',
        'callback' => 'updateEmail',
        'permission_callback'=> 'isUserLoggedIn',
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/email',array(
        'methods'  => 'POST',
        'callback' => 'createEmail',
        'permission_callback'=> 'isUserLoggedIn',
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/email/(?P<emailId>\d+)',array(
        'methods'  => 'DELETE',
        'callback' => 'deleteEmail',
        'permission_callback'=> 'isUserLoggedIn',
    ));

    register_rest_route( 'pantheradmin/v1', '/sendsbbu',array(
        'methods'  => 'POST',
        'callback' => 'sendSbbu',
        'permission_callback'=> 'checkNonce',
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/webtv',array(
        'methods'  => 'GET',
        'callback' => 'getWebtv',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/webtv',array(
        'methods'  => 'POST',
        'callback' => 'activateWebtv',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/webtv',array(
        'methods'  => 'PUT',
        'callback' => 'updateWebtv',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/webtv/devices',array(
        'methods'  => 'GET',
        'callback' => 'getWebtvDevices',
        'permission_callback'=> 'isUserLoggedIn'
    ));

    register_rest_route( 'pantheradmin/v1', '/customer/webtv/devices/(?P<deviceId>[\w]+)',array(
        'methods'  => 'DELETE',
        'callback' => 'removeWebtvDevice',
        'permission_callback'=> 'isUserLoggedIn'
    ));

}

function isUserLoggedIn(WP_REST_Request $request){
    checkNonce();
    if(!empty($request['token'])){
        return true;
    }
    return false;
}

function checkNonce()
{
    if (isset($_REQUEST['_wpnonce']) && wp_verify_nonce($_REQUEST['_wpnonce'], 'wp_rest')) {
        return true;
    } else {
        return new WP_Error('rest_cookie_invalid_nonce', 'Cookie nonce is invalid', array('status' => 401));
    }
}

function redirect($page){
    // Create the response object
    $response = new WP_REST_Response([]);
    // Add a custom status code
    $response->set_status( 302 );
    // Add a custom header
    $response->header( 'Location', get_home_url()."/".$page);
    return $response;
}

function goBack(){
    return header("Location: ".$_SERVER['HTTP_REFERER']);
}

function callAPI($func, $headers, $data=[], $method = 'GET'){
    $options = get_option('psb_plugin_options');
    $api_url = $options['api_url'];
    $api_token = $options['api_key'];

    //Debug
    //ob_start();  
    //$out = fopen('php://output', 'w');

    $header_array = array_merge(array('Authorization: Bearer '.$api_token, "Content-Type: application/json"), $headers);
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $api_url.$func,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_HTTPHEADER => $header_array,
        CURLOPT_FAILONERROR => true,
        CURLOPT_POSTFIELDS => json_encode($data)
    ));
    //Debug
    //curl_setopt($curl, CURLOPT_VERBOSE, true);  
    //curl_setopt($curl, CURLOPT_STDERR, $out);

    //Debug
    //fclose($out);  
    //$debug = ob_get_clean();
    //error_log($debug);
    
    $response = curl_exec($curl);
   

    //[22] => 'CURLE_HTTP_RETURNED_ERROR',
    if (curl_errno($curl) == 22){
        $error_message = curl_error($curl);
        $error_code = str_replace(array("The requested URL returned error: ", " Error", "\""), "", $error_message);
        if(is_numeric($error_code) && ($error_code > 399) && ($error_code < 600))
            $response = $error_code;
    }
    
    curl_close($curl);
    return $response;
}

function httpCall($destination, $headers, $data=[], $method = 'GET'){
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $destination,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_FAILONERROR => true,
        CURLOPT_POSTFIELDS => $data
    ));
    
    $response = curl_exec($curl);   

    //[22] => 'CURLE_HTTP_RETURNED_ERROR',
    if (curl_errno($curl) == 22){
        $error_message = curl_error($curl);
        $error_code = str_replace(array("The requested URL returned error: ", " Error", "\""), "", $error_message);
        if(is_numeric($error_code) && ($error_code > 399) && ($error_code < 600))
            $response = $error_code;
    }
    
    curl_close($curl);
    return $response;
}

function login(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    if (empty($request->get_params()['username']) || empty($request->get_params()['password'])) {
        return redirect($options['login_page']['page'] . '?e=Udfyld kundenummer/kodeord');
    } else {
        $result = callAPI('/api/v2/selfservice/login', [], array(
            'user' => $request->get_params()['username'],
            'pass' => $request->get_params()['password'],
            'useOtp' => $options['psb_use_otp']
        ), "POST");
        $result = json_decode($result, true);
        $responseBody = $result;
        $responseBody['tokenExpire'] = time() + 900;

        if (empty($result['authtoken'])) {
            return redirect($options['login_page']['page'] . '?e=Forkert kundenummer/kodeord');
        }

        //useOtp
        if(!$options['psb_use_otp']){
            // get customer to set info
            $result = callAPI(
                '/api/v2/selfservice',
                array(
                    'customer-token: ' . $result['authtoken'],
                )
            );
            $result = json_decode($result, true);
            $responseBody['isMaster'] = $result['isMaster'] == 1 ? true : false;
            $responseBody['redirectTo'] = get_home_url() . "/" . $options['customerinfo_page']['page'];
        }

        $response = new WP_REST_Response(json_encode($responseBody));
        // Add a custom status code
        $response->set_status(200);
        return $response;
    }
}

function otpActivate(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $result = callAPI('/api/v2/selfservice/activateotplogin', [], array(
        'authtoken' => $request->get_params()['authtoken'],
        'otp' => $request->get_params()['otp']
    ), "POST");
    $result = json_decode($result, true);
    if (empty($result['authtoken'])) {
        return redirect($options['login_page']['page'] . '?e=Login fejlet');
    }

    $responseBody = $result;
    $responseBody['tokenExpire'] = time() + 900;

    // get customer to set info
    $result = callAPI(
        '/api/v2/selfservice',
        array(
            'customer-token: ' . $result['authtoken'],
        )
    );
    $result = json_decode($result, true);
    $responseBody['isMaster'] = $result['isMaster'] == 1 ? true : false;
    $responseBody['redirectTo'] = get_home_url() . "/" . $options['customerinfo_page']['page'];

    $response = new WP_REST_Response(json_encode($responseBody));
    // Add a custom status code
    $response->set_status(200);
    return $response;

}

function magiclink(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $email = $request->get_params()['magiclink-email'];
    if(empty($email)) {
        return goBack();
    } else {
        $email_body = '<html><body>'.$options['magiclink_template'].'</body></html>';
        $result = callAPI('/api/v2/selfservice/magiclink',[],array('username'=>$email,'emailSubject'=>$options['magiclink_subject'], 'emailTemplate'=>$email_body),"POST");
        $result = json_decode($result,true);
        if(array_key_exists('error_msg',$result)) {
            return redirect($options['login_page']['page']."?e=Der er sket en fejl.&msg=".$result['error_msg']);
        }
        if(array_key_exists('status',$result) && $result['status'] == 'ok') {
            return redirect($options['login_page']['page']."?reset_msg=Link til gendannelse er blevet sendt, det virker i 24 timer.");
        } else {
        }
    }
}

function magicLogin(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $token = $request->get_params()['token'];
    $result = callAPI('/api/v2/selfservice',array(
        'customer-token: '.$token,
        )
    );
    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }

    return redirect($options['login_page']['page'].'?token='.$token);
}

function logout(WP_REST_Request $request){
    $body = $request->get_json_params();
    $options = get_option('psb_plugin_options');
    return redirect($options['login_page']['page']);
}

function forgotPassword(WP_REST_Request $request){
    $email = $request->get_params()['email'];
    $options = get_option('psb_plugin_options');
    if(empty($email)) {
        return goBack();
    } else {
        $result = callAPI('/api/v2/selfservice/reset-password',array(),array('email'=>$email, "sourceurl"=>$_SERVER['HTTP_ORIGIN']),"POST");
        $result = json_decode($result,true);
        if(array_key_exists('error_msg',$result)) {
            return redirect($options['login_page']['page']."?e=Ikke istand til at gendanne kodeord via web, kontakt venligst kundeservice.&msg=".$result['error_msg']);
        }
        return redirect($options['login_page']['page']."?reset_msg=Link til gendannelse er blevet sendt, det virker i 24 timer.");
    }
}

function googlelogin(WP_REST_Request $request){
	$options = get_option('psb_plugin_options');
	if(empty($request->get_params()['credential'])) {
		return redirect($options['login_page']['page'].'?e=Forkert kundenummer/kodeord');
	} else if(isset($request->get_params()['justValidate'])){
		$result = callAPI('/api/v2/selfservice/login/google', [], array(
			'credential' => $request->get_params()['credential'],
			'justValidate' => true
		), "POST");

		$response = new WP_REST_Response($result);
		// Add a custom status code
		$response->set_status(200);
		return $response;
	} else {
		$result = callAPI('/api/v2/selfservice/login/google', [], array(
			'credential' => $request->get_params()['credential']
		), "POST");

		$result = json_decode($result, true);
		$responseBody = $result;
		$responseBody['tokenExpire'] = time() + 900;

		if (empty($result['authtoken'])) {
			return redirect($options['login_page']['page'] . '?e=Forkert kundenummer/kodeord');
		}
		// get customer to set info
		$result = callAPI('/api/v2/selfservice', array(
				'customer-token: ' . $result['authtoken'],
			)
		);
		$result = json_decode($result, true);
		$responseBody['isMaster'] = $result['isMaster'] == 1 ? true : false;
		$responseBody['redirectTo'] = get_home_url() . "/" . $options['customerinfo_page']['page'];

		$response = new WP_REST_Response(json_encode($responseBody));
		// Add a custom status code
		$response->set_status(200);
		return $response;
	}
}

function facebooklogin(WP_REST_Request $request){
	$options = get_option('psb_plugin_options');
	if(empty($request->get_params()['credential']) || empty($request->get_params()['email'])) {
		return redirect($options['login_page']['page'].'?e=Forkert kundenummer/kodeord');
	} else {
		$result = callAPI('/api/v2/selfservice/login/facebook', [], array(
			'email' => $request->get_params()['email'],
			'credential' => $request->get_params()['credential']
		), "POST");

		$result = json_decode($result, true);
		$responseBody = $result;
		$responseBody['tokenExpire'] = time() + 900;

		if (empty($result['authtoken'])) {
			return redirect($options['login_page']['page'] . '?e=Forkert kundenummer/kodeord');
		}
		// get customer to set info
		$result = callAPI('/api/v2/selfservice', array(
				'customer-token: ' . $result['authtoken'],
			)
		);
		$result = json_decode($result, true);
		$responseBody['isMaster'] = $result['isMaster'] == 1 ? true : false;
		$responseBody['redirectTo'] = get_home_url() . "/" . $options['customerinfo_page']['page'];

		$response = new WP_REST_Response(json_encode($responseBody));
		// Add a custom status code
		$response->set_status(200);
		return $response;
	}
}

function applelogin(WP_REST_Request $request){
	$options = get_option('psb_plugin_options');
	if(empty($request->get_params()['credential'])) {
		return redirect($options['login_page']['page'].'?e=Forkert kundenummer/kodeord');
	} else if(isset($request->get_params()['justValidate'])){
		$result = callAPI('/api/v2/selfservice/login/apple', [], array(
			'credential' => $request->get_params()['credential'],
			'justValidate' => true
		), "POST");

		$response = new WP_REST_Response($result);
		// Add a custom status code
		$response->set_status(200);
		return $response;

	} else {
		$result = callAPI('/api/v2/selfservice/login/apple', [], array(
			'credential' => $request->get_params()['credential']
		), "POST");

		$result = json_decode($result, true);
		$responseBody = $result;
		$responseBody['tokenExpire'] = time() + 900;

		if (empty($result['authtoken'])) {
			return redirect($options['login_page']['page'] . '?e=Forkert kundenummer/kodeord');
		}
		// get customer to set info
		$result = callAPI('/api/v2/selfservice', array(
				'customer-token: ' . $result['authtoken'],
			)
		);
		$result = json_decode($result, true);
		$responseBody['isMaster'] = $result['isMaster'] == 1 ? true : false;
		$responseBody['redirectTo'] = get_home_url() . "/" . $options['customerinfo_page']['page'];

		$response = new WP_REST_Response(json_encode($responseBody));
		// Add a custom status code
		$response->set_status(200);
		return $response;
	}
}

function attachgoogle(WP_REST_Request $request){
	$options = get_option('psb_plugin_options');
	if(empty($request->get_params()['googleident'])) {
		$response = new WP_REST_Response(json_encode("{'fejl':'fejl'}"));
		$response->set_status(400);
		return $response;
	}

	$result = callAPI('/api/v2/selfservice', array('customer-token: '.$request['token'],), array(
		'googleident' => $request->get_params()['googleident']
	), "PATCH");

	$result = json_decode($result, true);
	$response = new WP_REST_Response(json_encode($result));
	// Add a custom status code
	$response->set_status(200);
	return $response;
}

function attachfacebook(WP_REST_Request $request){
	$options = get_option('psb_plugin_options');
	if(empty($request->get_params()['facebookident'])) {
		$response = new WP_REST_Response(json_encode("{'fejl':'fejl'}"));
		$response->set_status(400);
		return $response;
	}

	$result = callAPI('/api/v2/selfservice', array('customer-token: '.$request['token'],), array(
		'facebookident' => $request->get_params()['facebookident']
	), "PATCH");

	$result = json_decode($result, true);
	$response = new WP_REST_Response(json_encode($result));
	// Add a custom status code
	$response->set_status(200);
	return $response;
}

function attachapple(WP_REST_Request $request){
	$options = get_option('psb_plugin_options');
	if(empty($request->get_params()['appleident'])) {
		$response = new WP_REST_Response(json_encode("{'fejl':'fejl'}"));
		$response->set_status(400);
		return $response;
	}

	$result = callAPI('/api/v2/selfservice', array('customer-token: '.$request['token'],), array(
		'appleident' => $request->get_params()['appleident']
	), "PATCH");

	$result = json_decode($result, true);
	$response = new WP_REST_Response(json_encode($result));
	// Add a custom status code
	$response->set_status(200);
	return $response;
}

function removesocial(WP_REST_Request $request){
	$options = get_option('psb_plugin_options');
	if(empty($request->get_params()['socialType'])) {
		$response = new WP_REST_Response(json_encode("{'fejl':'fejl'}"));
		$response->set_status(400);
		return $response;
	}

	$result = callAPI('/api/v2/selfservice/socialident', array('customer-token: '.$request['token'],), array(
		'socialType' => $request->get_params()['socialType']
	), "DELETE");

	$result = json_decode($result, true);
	$response = new WP_REST_Response(json_encode($result));
	// Add a custom status code
	$response->set_status(200);
	return $response;
}


function getCustomerInfo(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $result = callAPI('/api/v2/selfservice?dont-include-customattributes=true',array(
        'customer-token: '.$request['token'],
        )
    );
    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }

    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function getProviderChannels(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $result = callAPI('/api/v2/selfservice/tv/prchannels',array());
    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }

    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}


function getChannelLogos(WP_REST_Request $request){
    $chnId = $request['channelId'];
    $result = callAPI('/api/v2/selfservice/tv/channels/'.$chnId.'/logo-file',array());
    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }

    header('Content-type: application/pdf');
    header('Content-Disposition: inline; filename=tmp.pdf');
    header('Content-Transfer-Encoding: binary');
    header('Accept-Ranges: bytes');
    echo $result;
    exit();

}

function updateCustomerInfo(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $body = $request->get_params();
    $customer_info = [];
    foreach($body as $key => $val){
        switch($key){
            case "phone":
                $customer_info["phone"] = $body["phone"];
                break;
            case "mobile":
                $customer_info["mobile"] = $body["mobile"];
                break;
            case "email":
                $customer_info["email"] = $body["email"];
                break;
            case "company":
                $customer_info["company"] = $body['company'];
                break;
            case "cvrnum";
                $customer_info["vatNo"] = $body['cvrnum'];
                break;
            case "billingPeriod":
                $customer_info["paymentCyclus"] = $body["billingPeriod"];
                break;
            case "invoiceBy":
                $customer_info["invoiceby"] = $body["invoiceBy"];
                break;
        }
    }

    $updateContactToo = $options['update_contact_too']?"true":"false";
    $result = callAPI('/api/v2/selfservice?update-contact-too='.$updateContactToo,array('customer-token: '.$request['token'],),$customer_info, "PATCH");
    return $result;
}

function updateCustomerPassword(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $oldPass = $request->get_param('old_password');
    $newPass = $request->get_param('new_password');
    if( !verifyPasswordRequirements($newPass) ){
        return apiError('An error occured', 'Opfylder ikke kravene til kodeord', 400);
    }
    $customer_info = ["old-password"=>$oldPass,"new-password"=>$newPass];
    $result = callAPI('/api/v2/selfservice',array('customer-token: '.$request['token'],),
    $customer_info,"PATCH");
    if(strpos($result, "error_msg")){
        return apiError("An error occured", $result, 400);
    }
    return $result;
}

function newCustomerPassword(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $token = $request->get_param('prtoken');
    $pass = $request->get_param('new_password1');
    if( !verifyPasswordRequirements($pass) ){
        return apiError('An error occured', 'Opfylder ikke kravene til kodeord', 400);
        // return redirect(get_site_url().$options['login_page']['page']."?prtoken=$token&e=Opfylder ikke kravene til kodeord");
    }
    $result = callAPI('/api/v2/selfservice/change-password',array(),
                     ["token" => $token, "pass" => $pass],"POST");

    return redirect($options['login_page']['page']."?success=Kodeord nulstillet");
}

function getCustomerInvoices(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $result = callAPI('/api/v2/selfservice/invoices',array(
        'customer-token: '.$request['token'],
        )
    );

    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function getCustomerInvoiceAsPdf(WP_REST_Request $request){

    $invoice_id = $request['invoiceId'];
    $result = callAPI('/api/v2/selfservice/invoices/'.$invoice_id.'/pdf',array(
        'customer-token: '.$request['token'],
        )
    );
    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }
    header('Content-type: application/pdf');
    header('Content-Disposition: inline; filename=document.pdf');
    header('Content-Transfer-Encoding: binary');
    header('Accept-Ranges: bytes');
    echo $result;
}

function getInternetInformation(WP_REST_Request $request){
    $productId = $request['productId'];
    $result = callAPI('/api/v2/selfservice/products/'.$productId.'/internet-information',array(
        'customer-token: '.$request['token'],
        )
    );
    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }
    $response = new WP_REST_Response($result);   
    $response->set_status( 200 );
    return $response;
}

function getAddressProductsAvailable(WP_REST_Request $request){
    $dawaId = $request['dawaId'];
    $webUrl = $request['wurl'];

    if(strlen($webUrl)){
        $res = httpCall($webUrl.$dawaId,array(),array(),'GET');
        $res = json_decode($res, true);
        //$result = json_encode($result[0]["installations"][0]["products"]);
        $result = Array();
        foreach($res as $re){
            foreach($re["installations"] as $re2){
                foreach($re2["products"] as $rpkey => $rpcontent){
                    $result[$rpkey] = $rpcontent;
                }
            }
        }
        $result = json_encode($result);
    }else{
        $result = callAPI('/api/v2/selfservice/signup/address/'.$dawaId.'/product-configs',array());
    }
    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }
    $response = new WP_REST_Response($result);

    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function getCustomerProductsAvailable(WP_REST_Request $request){
    $result = callAPI('/api/v2/selfservice/product-configurations',array(
        'customer-token: '.$request['token'],
        )
    );

    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }
    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}


function getModemSettings(WP_REST_Request $request){
    $mac = $request['mac'];
    $result = callAPI('/api/v2/selfservice/cpe/'.$mac.'/modemsettings',array(
        'customer-token: '.$request['token'],
        )
    );
    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }
    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function setModemSettings(WP_REST_Request $request) {
    $mac = $request['mac'];
    $modemData['modemSettings'] = $request->get_params();
    //We do not need mac and token
    unset($modemData['modemSettings']['mac']);
    unset($modemData['modemSettings']['token']);
    //$modemData["modemSettings"]["wifidefaults"] = "custom";
    //print_r($modemData);
    //exit();
    $result = callAPI('/api/v2/selfservice/cpe/'.$mac.'/modemsettings',array(
        'customer-token: '.$request['token']
    ), $modemData, "PATCH");

    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    $options = get_option('psb_plugin_options');
    return redirect($options['internet_page']['page']);

}

function getWifiDevices(WP_REST_Request $request){
    $result = callAPI('/api/v2/selfservice/wifi-devices',array(
        'customer-token: '.$request['token'],
        )
    );
    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }
    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function getCustomerProducts(WP_REST_Request $request){
    if($request['is-master'] == 'true') {
        $result = callAPI('/api/v2/selfservice/subcustomers/products?dont-include-customattributes=true',array(
            'customer-token: '.$request['token'],
            )
        );
    } else{
        $result = callAPI('/api/v2/selfservice/products?dont-include-customattributes=true',array(
            'customer-token: '.$request['token'],
            )
        );
    }
    if($request["productids"] != ""){
        $parsed = json_decode($result,true);
        $filtered = [];
        foreach($parsed as $product){
            if(in_array($product["productConf"]["id"], json_decode($request["productids"], true))){
                $filtered[] = $product;
            }
        }
        $result = json_encode($filtered);
    }
    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function getPaymentGatewayInfo(WP_REST_Request $request) {
    $result = callAPI('/api/v2/selfservice/paymentgateway',array(
        'customer-token: '.$request['token'],
        )
    );
    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function paymentGatewayCallback(WP_REST_Request $request) {
    $callbackData = array_merge($request->get_params(), $request->get_query_params());

    $response = new WP_REST_Response();
    if(!validateCallback($callbackData)){
        $response->set_status(400);
        return $response;
    }

    //callbackData[orderdata] is from Epay. JSON in a url param
    if(isset($callbackData["orderdata"])){
        $callbackData["orderdata"] = json_decode($callbackData["orderdata"], true);
    }

    // Tell pantheradmin about the callback
    if($request['token']){
        $result = callAPI('/api/v2/selfservice/paymentgateway/callback',array(
            'customer-token: '.$request['token'],
        ), json_encode($callbackData), "POST");
    }else if(isset($callbackData["variables"]["payloadId"]) || isset($callbackData["orderdata"]["payloadId"])){
        //Callbacks generated from signup have no tokens. Reepay signup callbacks will not be caught here though. 
        $payloadId = isset($callbackData["variables"]["payloadId"]) ? $callbackData["variables"]["payloadId"] : $callbackData["orderdata"]["payloadId"];
        $cdb = new CustomDB();
        $payload = json_decode($cdb->getPaymentData($payloadId), true);

        if(isset($callbackData["variables"])){
            $callbackData["variables"] = array_merge($callbackData["variables"], $payload);
            $cdb->updatePaymentData($callbackData["variables"], $payloadId);
        }

        if(isset($callbackData["orderdata"])){
            $callbackData["orderdata"] = array_merge($callbackData["orderdata"], $payload);
            $cdb->updatePaymentData($callbackData["orderdata"], $payloadId);
        }

        if(isset($payload["apiTarget"])){
            $target = $payload["apiTarget"];
            //Signup not heading for Pantheradmin
            $result = httpCall($target,array(), array("data" => json_encode($callbackData)),'POST');
        }else{
            //Signup heading for Pantheradmin
            $result = callAPI('/api/v2/selfservice/signup/submit-signup',array(), $callbackData,'POST');  
        }
        if(!is_numeric($result)){
            $cdb->deletePaymentData($payloadId);
        }
    }else{
        //Reepay callbacks dont have a token
        //Could be a Reepay signup not heading for Pantheradmin
        $rp = new Reepay();
        $payload = $rp->callbackToRemote($callbackData);
        if(is_array($payload)){        
            //Signup heading for Pantheradmin
            $result = callAPI('/api/v2/selfservice/paymentgateway/reepayCallback',array(), json_encode($payload), "POST");
        }

        if(!is_numeric($result)){
            $cdb = new CustomDB();
            $cdb->deletePaymentData($payload["payloadId"]);
        }
    }
    $http_status = (is_numeric($result) && ($result > 399) && ($result < 600)) ? $result : 200;
    // Return 200 OK to payment gateway
    // Add a custom status code
    $response->set_status( $http_status );
    return $response;
}

function validateCallback($cData){
    $options = get_option('psb_plugin_options');
    if(!$options["payment_gateway"] || !$options["payment_gateway_hash"]){
        return true;
    }

    switch($options["payment_gateway"]){
        case "Epay":
            return validateEpay($cData);
        case "Quickpay":
            return validateQuickpay($cData);
        case "Reepay":
            return validateReepay($cData);
        default:
            return true;
    }
}

function validateQuickpay($cData){
    $options = get_option('psb_plugin_options');
    $privateKey = $options["payment_gateway_hash"];
    $request_body = file_get_contents("php://input");
    $checksum = hash_hmac("sha256", $request_body, $privateKey);

    return (($checksum == $_SERVER["HTTP_QUICKPAY_CHECKSUM_SHA256"]) && ($cData["accepted"] == 1));
}

function validateEpay($cData){
    $options = get_option('psb_plugin_options');
    $md5Key = $options["payment_gateway_hash"];
    $var = "";

    foreach ($cData as $key => $value){
        if($key != "hash"){
            $var .= $value;
        }
    }

    $genstamp = md5($var . $md5Key);

    if($genstamp != $cData["hash"]){
        error_log("Callback denied");
        return false;
    }else{
        error_log("Callback accepted");
        return true;  
    }  
}

function validateReepay($cData){
    $options = get_option('psb_plugin_options');
    $privateKey = $options["payment_gateway_hash"];
    $data = $cData["timestamp"].$cData["id"];
    $checksum = hash_hmac("sha256", $data, $privateKey, false);
    return hash_equals($checksum, $cData["signature"]);
}

function paymentGatewayLink(WP_REST_Request $request) {
    $data = array(
        "acceptUrl"=>$_SERVER['HTTP_REFERER'].'?acceptedPayment=1&invoice-id='.$request['invoiceId'].'&amount='.$request['amount'],
        "cancelUrl"=>$_SERVER['HTTP_REFERER'].'?canceledpayment=1',
        "callbackUrl"=>get_home_url()."/wp-json/pantheradmin/v1/paymentgateway/callback?token={{customertoken}}",
        "amount"=>$request['amount'],
        "invoiceId"=>$request['invoiceId'],
        "subscription"=>$request['subscription'],
        'paymentMethods'=>$request['paymentMethods']
    );

    if($request['subscription'] == true) {
        $data["acceptUrl"] = $_SERVER['HTTP_REFERER'];
        $data["cancelUrl"] = $_SERVER['HTTP_REFERER'];
    }

    $result = callAPI('/api/v2/selfservice/paymentgatewaylink',array('customer-token: '.$request['token']), $data, "POST");

    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function signupPaymentGatewayLink(WP_REST_Request $request) {
    $options = get_option('psb_plugin_options');

    $req_a = $request->get_params();
    $data = array(
        "acceptUrl"=>"https://".$_SERVER['SERVER_NAME']."/".$req_a["acceptUrl"],
        "cancelUrl"=>"https://".$_SERVER['SERVER_NAME']."/".$req_a["cancelUrl"],
        "callbackUrl"=>get_home_url()."/wp-json/pantheradmin/v1/paymentgateway/callback",
        "amount"=>$req_a['amount'],
        "subscription"=>$req_a['subscription'] ? 1 : 0,
        'paymentMethods'=>$req_a['paymentMethods']
    );
    unset($req_a["acceptUrl"]);
    unset($req_a["cancelUrl"]);
    unset($req_a['amount']);
    unset($req_a['subscription']);
    unset($req_a['paymentMethods']);

    //Put req_a in db and set id in data
    //$data["data"] = $req_a;
    $cdb = new CustomDB();
    $data["data"]["dawaid"] = $req_a["dawaid"];
    $data["data"]["payloadId"] = $cdb->savePaymentData($req_a);

    if(isset($req_a["apiTarget"])){
        switch($options["payment_gateway"]){
            case "Epay": $pg = new Epay();break;
            case "Reepay": $pg = new Reepay();break;
            case "Quickpay": $pg = new Quickpay();break;
        }

        $result = $pg->getLink($data["amount"], $data["subscription"], array("acceptUrl" => $data["acceptUrl"], "cancelUrl" => $data["cancelUrl"], "callbackUrl" => $data["callbackUrl"]), $data["data"]);
    }else{
        //Get link from Pantheradmin
        $result = callAPI('/api/v2/selfservice/paymentgateway/signuppaymentgatewaylink',array(), $data, "POST");
    }
    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function orderBullguard(WP_REST_Request $request) {
    $result = callAPI('/api/v2/selfservice/bullguard/order',array(
        'customer-token: '.$request['token'],
    ), null, "POST");

    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function cancelBullguard(WP_REST_Request $request) {
    $result = callAPI('/api/v2/selfservice/bullguard/cancel',array(
        'customer-token: '.$request['token'],
    ), null, "DELETE");

    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function getBullguard(WP_REST_Request $request) {
    $result = callAPI('/api/v2/selfservice/bullguard',array(
        'customer-token: '.$request['token'],
    ), null);
    $response = new WP_REST_Response($result);
    if(array_key_exists('error_msg',json_decode($result, true))){
        $response->set_status( 404 );
    } else {
        $response->set_status( 200 );
    }
    return $response;
}

function orderFSecure(WP_REST_Request $request) {
    $result = callAPI('/api/v2/selfservice/fsecure/order',array(
        'customer-token: '.$request['token'],
    ), null, "POST");

    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function cancelFSecure(WP_REST_Request $request) {
    $result = callAPI('/api/v2/selfservice/fsecure/cancel',array(
        'customer-token: '.$request['token'],
    ), null, "DELETE");

    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function getFSecure(WP_REST_Request $request) {
    $result = callAPI('/api/v2/selfservice/fsecure',array(
        'customer-token: '.$request['token'],
    ), null);
    $response = new WP_REST_Response($result);
    if(array_key_exists('error_msg',json_decode($result, true))){
        $response->set_status( 404 );
    } else {
        $response->set_status( 200 );
    }
    return $response;
}

function getCustomerTickets(WP_REST_Request $request) {
    $response = new WP_REST_Response();
    if(!$request['tickettypes']){
        $response->set_status( 400 );
        return $response;
    }
    $types =  explode(',', $request['tickettypes']);
    // explode ud i uri encoded array
    $typesEncoded = [];
    foreach($types as $type){
        $typesEncoded[] = sprintf('notetype[]=%s', $type);
    }
    $result = callAPI('/api/v2/selfservice/tickets?'.utf8_encode(implode('&',$typesEncoded)),array(
        'customer-token: '.$request['token'],
    ), null);
    if(array_key_exists('error_msg',json_decode($result, true))){
        $response->set_status( 404 );
    } else {
        $response->set_status( 200 );
    }
    $response = new WP_REST_Response($result);
    return $response;
}

function getCustomerTicketAsPdf(WP_REST_Request $request){
    $ticketId = $request['ticketId'];
    $result = callAPI('/api/v2/selfservice/tickets/'.$ticketId.'/pdf',array(
        'customer-token: '.$request['token'],
        )
    );
    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }
    header('Content-type: application/pdf');
    header('Content-Disposition: inline; filename='.$ticketId.'.pdf');
    header('Content-Transfer-Encoding: binary');
    header('Accept-Ranges: bytes');
    echo $result;
    exit();
}

function BsPispRegister(WP_REST_Request $request){
    // $baseUrl = ($_SERVER["HTTPS"] ? "https://":"http://") . $_SERVER["HTTP_HOST"];
    $baseUrl = get_rest_url()."pantheradmin/v1";
    // Amount is only accepted in format 1234.56
    $amount = $request['amount']/100;
    $amount = $amount;
    $amount = fmod($amount, 1) == 0 ? $amount.'.00' : (string)$amount;

    $invoiceId = isset($request['invoice']) && !empty($request['invoice']) ? '?invoice='.$request['invoice'] : '';

    $postdata = [
        "callBackUrl"=> $baseUrl+'/paymentgateway/bs/pisp/callback'.$invoiceId,
        "cancelPaymentCallBackUrl"=>$baseUrl+'/paymentgateway/bs/pisp/cancel',
        "isInstant"=>true,
        "amount"=>$amount
    ];

    $result = callAPI('/api/v2/selfservice/betalingsservice/pisp/register',['customer-token: '.$request['token'],],$postdata,'POST');

    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }

    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function BsPispCallback(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $uuid = $request['uuid'];
    $invoiceId = $request['invoice'];
    $result = callAPI('/api/v2/selfservice/betalingsservice/pisp/callback',array(
        'customer-token: '.$request['token'],
    ),
    $postdata = ['uuid'=>$uuid, 'invoiceId'=>$invoiceId], 'POST'
    );
    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }

    return redirect($options['customerinvoices_page']['page']);
}

function BsPispCancel(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $uuid = $request['uuid'];
    $result = callAPI('/api/v2/selfservice/betalingsservice/pisp/cancel',array(
        'customer-token: '.$request['token'],
    ),
    $postdata = ['uuid'=>$uuid], 'POST'
    );

    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }
    return redirect($options['customerinvoices_page']['page']);
}

function BsMandateInit(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $result = callAPI('/api/v2/selfservice/betalingsservice/mandate/activate',array(
        'customer-token: '.$request['token'],
    ),
    $postdata = ['nationalid'=>$request['cpr'], 'regno'=>$request['regno'], 'accountno'=>$request['accountno'], 'datatype'=>"DELEGATED"], 'POST'
    );

    if(empty($result) | array_key_exists('error_msg', json_decode($result, true))) {
        return apiError('An error occured', 'An error occured, please try again. Message : '.json_decode($result, true)['error_msg'], 401);
    }
    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
    // return redirect($options['customerinvoices_page']['page']);
}

function BsMandateStatus(WP_REST_Request $request){
    $options = get_option('psb_plugin_options');
    $result = callAPI('/api/v2/selfservice/betalingsservice/mandate/check-allowed',array(
        'customer-token: '.$request['token'],
    ),
    );

    if(empty($result)) {
        return apiError('An error occured', 'An error occured, please try again later', 401);
    }
     $processedResult = json_decode($result, true);
    if($processedResult['active'] == 1){
        $processedResult['active'] = 0;
    }
    $response = new WP_REST_Response(json_encode($processedResult));
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

// Product endpoints

function getAvailableProducts(WP_REST_Request $request) {
    $result = callAPI('/api/v2/selfservice/product-configurations',array(
        'customer-token: '.$request['token'],
    ), null);
    $decodedResult = json_decode($result);
    // If productids is present, only return products asked for, if available
    if(!empty($request['productids'])){
        $filteredProducts = [];
        foreach($decodedResult as $product){
            if(in_array($product->id, json_decode($request['productids'], true))){
                $filteredProducts[] = $product;
            }       
        }
        $result = json_encode($filteredProducts);
    }
    $response = new WP_REST_Response($result);
    if(array_key_exists('error_msg',json_decode($result, true))){
        $response->set_status( 404 );
    } else {
        $response->set_status( 200 );
    }
    return $response;
}




function getCustomerProductByIdent(WP_REST_Request $request) {
    $result = callAPI('/api/v2/selfservice/products/addon/type?ident='.$request['ident'],array(
        'customer-token: '.$request['token'],
    ), null);
    $response = new WP_REST_Response($result);
    if(array_key_exists('error_msg',json_decode($result, true))){
        $response->set_status( 404 );
    } else {
        $response->set_status( 200 );
    }
    return $response;
}

function hasCustomerAccessToProduct(WP_REST_Request $request) {
    $result = callAPI('/api/v2/selfservice/products/addon/type/access?ident='.$request['ident'],array(
        'customer-token: '.$request['token'],
    ), null);
    $response = new WP_REST_Response($result);
    if(array_key_exists('error_msg',json_decode($result, true))){
        $response->set_status( 404 );
    } else {
        $response->set_status( 200 );
    }
    return $response;
}

function addCustomerProductByIdent(WP_REST_Request $request) {
    if(empty($request['ident']) && empty($request['pcid'])){
        $response = new WP_REST_Response("Error");
        $response->set_status( 400 );
        return $response;
    }
    $result = callAPI('/api/v2/selfservice/products/addon/type',array(
        'customer-token: '.$request['token'],
    ), $request->get_params(),'POST');
    $response = new WP_REST_Response($result);
    if(json_decode($result, true) == "process started"){
        $response->set_status( 200 );
    } else {
        $response->set_status( 400 );
    }
    return $response;
}

function removeCustomerProductByIdent(WP_REST_Request $request) {
    $result = callAPI('/api/v2/selfservice/products/addon/type',array(
        'customer-token: '.$request['token'],
    ), $request->get_params(),'DELETE');
    $response = new WP_REST_Response($result);
    if(json_decode($result, true) == "process started"){
        $response->set_status( 200 );
    } else {
        $response->set_status( 400 );
    }
    return $response;
}

/** Technician endpoints functions */

/** PA endpoints
     * $auth_group->post('/products/{customerproductId}/technician/availabledates','API\Controller\SelfserviceController::getTechnicianGetAvailableDates');
     * $auth_group->post('/products/{customerproductId}/technician/reschedule','API\Controller\SelfserviceController::orderTechnician');
     * $auth_group->get('/products/{customerproductId}/technician/reschedule/{ticketId}/status','API\Controller\SelfserviceController::getTechnicianOrderStatus');
     */
function technicianRequired(WP_REST_Request $request){
    $result = callAPI('/api/v2/selfservice/products',array(
        'customer-token: '.$request['token'],
    ), ['ident'=>$request['ident']],'GET');
    $parsedResult = json_decode($result, true);
    $techDate = '';
    $productId = null;
    if($parsedResult){
        foreach($parsedResult as $product){
            if(!isset($product['customAttributes']['technicianrequired'])){break;}
            if($product['customAttributes']['technicianrequired'] == 'yes' || $product['customAttributes']['technicianrequired'] == 'y' || $product['customAttributes']['technicianrequired'] == 'ja' || $product['customAttributes']['technicianrequired'] == 'j'){
                $techDate = $product['customAttributes']['executiondate'];
                $productId = $product['id'];
            }
        }
    }

    $response = new WP_REST_Response(json_encode(["techRequired"=>!empty($techDate), "techDate"=>$techDate, "productId"=>$productId]));
    return $response;
}

function technicianAvailableDates(WP_REST_Request $request) {
    $customerproductId = $request['customerProductId'];
    $preferredDate = $request['perferred-date'];
    $dt = new DateTime($preferredDate);
    $formattedDate = $dt->format(DateTime::ATOM);

    $result = callAPI('/api/v2/selfservice/products/'.$customerproductId.'/technician/availabledates',array(
        'customer-token: '.$request['token'],
    ),['optimalDate'=>$formattedDate],'POST');
    $response = new WP_REST_Response($result);
    if(json_decode($result)){
        $response->set_status( 200 );
    } else {
        $response->set_status( 400 );
    }
    return $response;
}

function technicianReschedule(WP_REST_Request $request) {
    $customerproductId = $request['customerProductId'];
    $result = callAPI('/api/v2/selfservice/products/'.$customerproductId.'/technician/reschedule',array(
        'customer-token: '.$request['token'],
    ), ['from'=>$request['from'],'to'=>$request['to']],'POST');
    $response = new WP_REST_Response($result);
    if(json_decode($result)){
        $response->set_status( 200 );
    } else {
        $response->set_status( 400 );
    }
    return $response;
}

function technicianResceduleStatus(WP_REST_Request $request) {
    $ticketId = $request['ticketId'];
    $customerproductId = $request['customerProductId'];
    $result = callAPI('/api/v2/selfservice/products/'.$customerproductId.'/technician/reschedule/'.$ticketId.'/status',array(
        'customer-token: '.$request['token'],
    ),null,'GET');
    $response = new WP_REST_Response($result);
    if(json_decode($result)){
        $response->set_status( 200 );
    } else {
        $response->set_status( 400 );
    }
    return $response;
}

function verifyPasswordRequirements(string $password) : bool{
    $options = get_option('psb_plugin_options');
    $length = $options["password_rules_length"];
    $securityLevel = $options["password_rules_chars"];
    $isValid = true;
    switch($securityLevel){
        case "none":
            $isValid = strlen($password) >= $length;
            break;
        case "low":
            $isValid = strlen($password) >= $length && preg_grep("/[A-ZÆØÅ]{1,}/", [$password]) && preg_grep("/[a-zæøå]{1,}/", [$password]);
            break;
        case "medium":
            $isValid = strlen($password) >= $length && preg_grep("/[A-ZÆØÅ]{1,}/", [$password]) && preg_grep("/[a-zæøå]{1,}/", [$password]) && preg_grep("/[0-9]{1,}/", [$password]);
            break;
        case "high":
            $isValid = strlen($password) >= $length && preg_grep("/[A-ZÆØÅ]{1,}/", [$password]) && preg_grep("/[a-zæøå]{1,}/", [$password]) && preg_grep("/[0-9]{1,}/", [$password]) && preg_grep("/[\!\(\)\-\.\?\:\;\~\`\@\#\$\%\^\*\+\=]{1,}/", [$password]);
            break;
    }

    return $isValid;
}

function submitSignup(WP_REST_Request $request) {
    $body = json_decode($request->get_body());
    error_log("REQUEST: ".var_export($body, true));

    if($body->apiTarget){
        $result = httpCall($body->apiTarget,array(), array("data" => json_encode($body)),'POST');
    }else{
        $result = callAPI('/api/v2/selfservice/signup/submit-signup',array(), json_encode($body),'POST');
    }
    $response = new WP_REST_Response($result);
    if(is_numeric($result)){
        $response->set_status( $result );
    } else {
        $response->set_status( 200 );
    }

    return $response;
}

function genericOrder(WP_REST_Request $request) {
    $result = callAPI('/api/v2/selfservice/order',array('customer-token: '.$request['token']), json_decode($request->get_body()), "POST");
    $response = new WP_REST_Response($result);
    // Add a custom status code
    $response->set_status( 200 );
    return $response;
}

function getEmails(WP_REST_Request $request){
    $result = callAPI('/api/v2/selfservice/email',array('customer-token: '.$request['token']), null, "GET");
    $response = new WP_REST_Response($result);
    if(json_decode($result)){
        $response->set_status( 200 );
    } else {
        $response->set_status( 400 );
    }
    return $response;
}

function updateEmail(WP_REST_Request $request){
    $result = callAPI('/api/v2/selfservice/email',array('customer-token: '.$request['token']), json_decode($request->get_body()), "PATCH");
    $response = new WP_REST_Response($result);
    if(json_decode($result)){
        $response->set_status( 200 );
    } else {
        $response->set_status( 400 );
    }
    return $response;
}

function createEmail(WP_REST_Request $request){
    $result = callAPI('/api/v2/selfservice/email',array('customer-token: '.$request['token']), json_decode($request->get_body()), "POST");
    $response = new WP_REST_Response($result);
    if(json_decode($result)){
        $response->set_status( 200 );
    } else {
        $response->set_status( 400 );
    }
    return $response;
}

function deleteEmail(WP_REST_Request $request){
    $result = callAPI('/api/v2/selfservice/email/'.$request['emailId'],array('customer-token: '.$request['token']), null, "DELETE");
    $response = new WP_REST_Response($result);
    if(json_decode($result)){
        $response->set_status( 200 );
    } else {
        $response->set_status( 400 );
    }
    return $response;
    }

function sendSbbu(WP_REST_Request $request) {
    $options = get_option('psb_plugin_options');
    $body = $request->get_params();
    $token = $body["customer-token"];
    unset($body["customer-token"]);
    if(isset($body["confirm"])){
        $body["confirm"] = true;
    } else {
        $body["confirm"] = false;
    }
    if(isset($body["nomarketing"])){
        $body["nomarketing"] = true;
    } else {
        $body["nomarketing"] = false;
    }
    $result = callAPI('/api/v2/selfservice/sbbu',array('customer-token: '.$token), $body, "POST");
    $responseBody['redirectTo'] = get_home_url()."/".$options['customerinfo_page']['page'];
    $response = new WP_REST_Response($responseBody);
    if(json_decode($result)){
        $response->set_status( 200 );
    } else {
        $response->set_status( 400 );
    }
    return $response;
}
    
function getWebtv(WP_REST_Request $request){
    $result = callAPI('/api/v2/selfservice/playmaker',array('customer-token: '.$request['token']), null, "GET");
    $response = new WP_REST_Response($result);
    if(json_decode($result)){
        $response->set_status( 200 );
    } else {
        $response->set_status( 400 );
    }
    return $response;
}

function activateWebtv(WP_REST_Request $request) {
    $body = $request->get_params();
    $token = $body["token"];

    $data = array();
    if(isset($body["login"])){
        $data["login"] = $body["login"];
    } 
    
    if(isset($body["pass"])){
        $data["pass"] = $body["pass"];
    }
    
    $result = callAPI('/api/v2/selfservice/playmaker',array('customer-token: '.$token), $data, "POST");

    $response = new WP_REST_Response($result);
    if(is_numeric($result)){
        $response->set_status( $result );
    } else {
        $response->set_status( 200 );
    }
    return $response;
}

function updateWebtv(WP_REST_Request $request) {
    $body = $request->get_params();
    $result = callAPI('/api/v2/selfservice/playmaker',array('customer-token: '.$body['token']), $body, "PUT");

    $response = new WP_REST_Response($result);
    if(is_numeric($result)){
        $response->set_status( $result );
    } else {
        $response->set_status( 200 );
    }
    return $response;
}


function getWebtvDevices(WP_REST_Request $request){
    $result = callAPI('/api/v2/selfservice/playmaker/devices',array('customer-token: '.$request['token']), null, "GET");
    $response = new WP_REST_Response($result);
    if(json_decode($result)){
        $response->set_status( 200 );
    } else {
        $response->set_status( 400 );
    }
    return $response;
}

function removeWebtvDevice(WP_REST_Request $request){
    $result = callAPI('/api/v2/selfservice/playmaker/devices/'.$request['deviceId'],array('customer-token: '.$request['token']), null, "DELETE");
    $response = new WP_REST_Response($result);
    if(is_numeric($result)){
        $response->set_status( $result );
    } else {
        $response->set_status( 200 );
    }
    return $response;
}

?>
