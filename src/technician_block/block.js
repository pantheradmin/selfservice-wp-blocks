/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    InnerBlocks,
	useBlockProps,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
	CheckboxControl,
	FontSizePicker,
    SelectControl,
    Dashicon,
    Tooltip,
    TextControl
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-technician-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Technician-block' ), // Block title.
	icon: 'groups', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		fontSize: {type: 'int', default: 16}
		
	},

	edit: (props) => {
		
		return (

			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
					
				</BlockControls>


				<div class="container" style={{'font-size': props.attributes.fontSize}}>
					<div> Du har en tekniker-tid d. 01 Jan. 2022 i tidsrummet 8-16</div>
					<div><button type="button">Ombook</button></div>
				</div>

				<div class="modal" id="technician-modal">
					<div class="modal-background"></div>
					<div class="modal-content">
						<div class="box">
							<div id="technician-modal-title"></div>
							<div id="technician-modal-content"></div>
							<div id="technician-modal-footer"></div>
						</div>
						<button class="modal-close is-large" aria-label="close"></button>
					</div>
				</div>

			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div class="container" id="technician-container" style={{'font-size': props.attributes.fontSize}}>
				<div id="technician-precursor">{}</div>
				<div id="technician-current-time-text"></div>
				<div><button id="technician-open-modal-button" type="button" style="display:none">Ombook</button></div>
				<div class="modal" id="technician-modal">
					<div class="modal-background"></div>
					<div class="modal-content">
						<div class="box">
							<div id="technician-modal-title">Ombook tekniker</div>
							<div id="technician-modal-content">
								<form id="preferred-date-form">
									<p>Vælg ønsket dato</p>
									<input type="hidden" id="token" name="token" value=""></input>
									<input id="perferred-date" name="perferred-date" type="date"></input>
									<button id="perferred-date-check-btn">Tjek</button>
								</form>
								<form id="technician-time-form" style="display: none">
									<p>Vælg tilgængelig teknikertid</p>
									<select id="technician_datetime"disabled></select>
									<button id="technician_datetime-btn">Vælg</button>
								</form>
							</div>
							<div id="technician-modal-footer"></div>
						</div>
						<button class="modal-close is-large" aria-label="close"></button>
					</div>
				</div>
			</div>
		);
	}
});