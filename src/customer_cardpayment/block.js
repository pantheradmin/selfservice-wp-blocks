/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
	CheckboxControl,
    SelectControl,
	FontSizePicker,
    Dashicon,
    Tooltip,
    TextControl
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-cardpayment-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Customer Cardpayment and debt.' ), // Block title.
	icon: 'money-alt', // Block icon from Dashicons â https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category â Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block â CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		debtDueMessage: {type: 'string', default: 'Du kan ikke tilmelde dig automatisk kortbetaling da der er et udestående på kontoen. Betal dette først.'},
		subscribeBsMessage: {type: 'string', default: 'Klik på nedenstående knap for at gå videre til tilmeldingen.'},
		subscribeButtonText: {type: 'string', default: 'Gå til kort tilmelding.'},
		subscribeChangeCardButtonText: {type: 'string', default: 'Tilmeld nyt betalingskort.'},
		subscribeEmailButtonText: {type: 'string', default: 'Faktura via email.'},
		subscribeMPButtonText: {type: 'string', default: 'Gå til Mobilepay abb. tilmelding.'},
		subscribeBSMandateButtonText: {type: 'string', default: 'Gå til Betalingsservice tilmelding'},
		payDebtButtonText: {type: 'string', default: 'Betal udestående.'},
		currentPaymentMethod: {type: 'string', default: 'Du er pt. tilmeldt'},
		cardpayInfo: {type:"string", default:"Klik på nedenstående knap for at gå til tilmeldingen."},
		emailpayInfo: {type:"string", default:"Klik på nedenstående knap for at tilmelde faktura via email."},
		mobilepayInfo: {type:"string", default:"Klik på nedenstående knap for at gå til tilmeldingen."},
		bsMandateInfo: {type:"string", default:"Udfyld nedenstående og tryk på knappen for at tilmelde."},
		fontSize: {type: 'int', default: 16},
        showInvoiceByEmailButton: {type: 'boolean', default: true},
		showA2AButton: {type: 'boolean', default: false},
		showBSMandateButton: {type: 'boolean', default: false},
		showMPButton: {type: 'boolean', default: false},
		showMPSubButton: {type: 'boolean', default: false},
	},

	edit: ( props ) => {
		// Creates a <p class='wp-block-cgb-block-panther-selfservice-block'></p>.
		return (

			

			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						<PanelRow>
							<TextareaControl
								label={__(
									"Message shown if there is debt"
								)}
								value={props.attributes.debtDueMessage}
								onChange={ function(value){
									props.setAttributes( { debtDueMessage: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Message shown if there is no debt"
								)}
								value={props.attributes.subscribeBsMessage}
								onChange={ function(value){
									props.setAttributes( { subscribeBsMessage: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Text shown on 'subcribe with card' button"
								)}
								value={props.attributes.subscribeButtonText}
								onChange={ function(value){
									props.setAttributes( { subscribeButtonText: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Text shown on 'subcribe with card' button when already using card as payment solution"
								)}
								value={props.attributes.subscribeChangeCardButtonText}
								onChange={ function(value){
									props.setAttributes( { subscribeChangeCardButtonText: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Text shown on 'invoice by email' button"
								)}
								value={props.attributes.subscribeEmailButtonText}
								onChange={ function(value){
									props.setAttributes( { subscribeEmailButtonText: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Text shown on 'subcribe with mobilepay' button"
								)}
								value={props.attributes.subscribeMPButtonText}
								onChange={ function(value){
									props.setAttributes( { subscribeMPButtonText: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Text shown on 'subcribe with BS Mandate' button "
								)}
								value={props.attributes.subscribeBSMandateButtonText}
								onChange={ function(value){
									props.setAttributes( { subscribeBSMandateButtonText: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Text shown on \"subscribe with card\" pop up"
								)}
								value={props.attributes.cardpayInfo}
								onChange={ function(value){
									props.setAttributes( { cardpayInfo: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Text shown on \"subscribe invoice via email\" pop up"
								)}
								value={props.attributes.emailpayInfo}
								onChange={ function(value){
									props.setAttributes( { emailpayInfo: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Text shown on \"subscribe with mobilepay\" pop up"
								)}
								value={props.attributes.mobilepayInfo}
								onChange={ function(value){
									props.setAttributes( { mobilepayInfo: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Text shown on \"subscribe with bs mandate\" pop up"
								)}
								value={props.attributes.bsMandateInfo}
								onChange={ function(value){
									props.setAttributes( { bsMandateInfo: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Text shown on button if there is debt"
								)}
								value={props.attributes.payDebtButtonText}
								onChange={ function(value){
									props.setAttributes( { payDebtButtonText: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Text shown before current payment-method."
								)}
								value={props.attributes.currentPaymentMethod}
								onChange={ function(value){
									props.setAttributes( { currentPaymentMethod: value } );
								}}
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Payment settings">
                    <PanelRow>
						<CheckboxControl
								label='Show "Invoice by email" payment option'
								help="Check the box if you want to show the button"
								checked={ props.attributes.showInvoiceByEmailButton }
								onChange={ function(value){
									props.setAttributes( { showInvoiceByEmailButton: value } );
								} }
							/>
						</PanelRow>
                        <PanelRow>
						<CheckboxControl
								label="Show A2A / PISP payment option"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showA2AButton }
								onChange={ function(value){
									props.setAttributes( { showA2AButton: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
						<CheckboxControl
								label="Show BS subcription option"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showBSMandateButton }
								onChange={ function(value){
									props.setAttributes( { showBSMandateButton: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
						<CheckboxControl
								label="Show Mobilepay payment option"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showMPButton }
								onChange={ function(value){
									props.setAttributes( { showMPButton: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
						<CheckboxControl
								label="Show Mobilepay Subscription option"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showMPSubButton }
								onChange={ function(value){
									props.setAttributes( { showMPSubButton: value } );
								} }
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				


				<div id="cardpay_container" className={ props.className } data-attributes={JSON.stringify(props.attributes)} style={{'font-size': props.attributes.fontSize}}>
					<div class="container">
						<div id="pa_invoice_debt_due_container">
							<div id="pa_invoices_debt_due_body">
									<div>{props.attributes.subscribeBsMessage}</div>
									<button id="subscription-modal-subscribe-button">{props.attributes.subscribeButtonText}</button>
							</div>								
						</div>

									
						<div class="modal" id="payment-modal">
							<div class="modal-background"></div>
							<div class="modal-content">
								<div class="box">
									<div id="payment-modal-title"></div>
									<div id="payment-modal-content"></div>
									<div id="payment-modal-footer"></div>
								</div>
								<button class="modal-close is-large" aria-label="close"></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div id="cardpay_container" className={ props.className } data-attributes={JSON.stringify(props.attributes)} style={{'font-size': props.attributes.fontSize}}>
				<div class="container">
					<div id="pa_invoice_debt_due_container">
						<h4 id="pa_invoices_debt_due_title" class="title is-5">{props.attributes.bsTitle}</h4>
						<div id="pa_invoices_debt_due_body"></div>
					</div>
				</div>

				<div class="modal" id="subscription-payment-modal">
					<div class="modal-background"></div>
					<div class="modal-content">
						<div class="box">
							<div id="subscription-payment-modal-title"></div>
							<div id="subscription-payment-modal-content"></div>
							<div id="subscription-payment-modal-footer"></div>
						</div>
						<button class="modal-close is-large" aria-label="close"></button>
					</div>
				</div>
			</div>

			
		);
	
} });
