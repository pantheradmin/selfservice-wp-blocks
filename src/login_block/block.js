/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { useState } from '@wordpress/element';
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
	CheckboxControl,
    SelectControl,
    Dashicon,
    Tooltip,
    TextControl,
	FontSizePicker
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Login' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		title: {type: 'string', default: 'Login'},
		usernameField: {type: 'string', default: 'Brugernavn'},
		passwordField: {type: 'string', default: 'Adgangskode'},
		newpasswordField1: {type: 'string', default: 'Ny adgangskode'},
		newpasswordField2: {type: 'string', default: 'Gentag ny adgangskode'},
		loginButtonText: {type: 'string', default: 'Log ind'},
		savePWButtonText: {type: 'string', default: 'Gem'},
		showFacebookButton: {type: 'boolean', default: true},
		showGoogleButton: {type: 'boolean', default: true},
		showAppleButton: {type: 'boolean', default: true},
		facebookButtonText: {type: 'string', default: 'Fortsæt med Facebook'},
		googleButtonText: {type: 'string', default: 'Fortsæt med Google'},
		appleButtonText: {type: 'string', default: 'Fortsæt med Apple'},
		forgotPassText: {type: 'string', default: 'Har du glemt dit kodeord?'},
        sendOtpButtonText: {type: 'string', default: 'Send'},
        otpText: {type: 'string', default: 'Indtast her koden du har modtaget på sms og tryk send.'},
		fontSize: {type: 'int', default: 16}
	},

	edit: ( props ) => {
		// Creates a <p class='wp-block-cgb-block-panther-selfservice-block'></p>.
		function updateTitle( newTitle ) {
			props.setAttributes( { title: newTitle } );
		}
		function updateUsername( newUsernameField ) {
			props.setAttributes( { usernameField: newUsernameField } );
		}
		function updatePassword( newpasswordField ) {
			props.setAttributes( { passwordField: newpasswordField } );
		}
		function updatePassword1( newpasswordField1 ) {
			props.setAttributes( { newpasswordField1: newpasswordField1 } );
		}
		function updatePassword2( newpasswordField2 ) {
			props.setAttributes( { newpasswordField2: newpasswordField2 } );
		}
		function updateSavePWButtonText( newsavePWButtonText ) {
			props.setAttributes( { savePWButtonText: savePWButtonText } );
		}
		function updateLoginButtonText( newLoginButtonText ) {
			props.setAttributes( { loginButtonText: newLoginButtonText } );
		}
		function updateFacebookButtonText( newFacebookButtonText ) {
			props.setAttributes( { facebookButtonText: newFacebookButtonText } );
		}
		function updateGoogleButtonText( newGoogleButtonText ) {
			props.setAttributes( { googleButtonText: newGoogleButtonText } );
		}
		function updateAppleButtonText( newAppleButtonText ) {
			props.setAttributes( { appleButtonText: newAppleButtonText } );
		}
		function updateForgotPassText( newForgotPassText ) {
			props.setAttributes( { forgotPassText: newForgotPassText } );
		}
		function updatesendOtpButtonText( newsendOtpButtonText ) {
			props.setAttributes( { sendOtpButtonText: newsendOtpButtonText } );
		}
		function updateotpText( newotpText ) {
			props.setAttributes( { otpText: newotpText } );
		}


		return (
			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						<PanelRow>
							<TextControl
								label={__(
									"Title"
								)}
								value={props.attributes.title}
								onChange={updateTitle}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Username/Email"
								)}
								value={props.attributes.usernameField}
								onChange={updateUsername}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Password"
								)}
								value={props.attributes.passwordField}
								onChange={updatePassword}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"New Password"
								)}
								value={props.attributes.newpasswordField1}
								onChange={updatePassword1}
							/>
						</PanelRow>
                        <PanelRow>
							<TextControl
								label={__(
									"New Password 2"
								)}
								value={props.attributes.newpasswordField2}
								onChange={updatePassword2}
							/>
						</PanelRow>

						<PanelRow>
							<TextControl
								label={__(
									"Login button text"
								)}
								value={props.attributes.loginButtonText}
								onChange={updateLoginButtonText}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Save password button text"
								)}
								value={props.attributes.savePWButtonText}
								onChange={updateSavePWButtonText}
							/>
						</PanelRow>


						<PanelRow>
							<TextControl
								label={__(
									"Send one time passcode button text"
								)}
								value={props.attributes.sendOtpButtonText}
								onChange={updatesendOtpButtonText}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"One time passcode description"
								)}
								value={props.attributes.otpText}
								onChange={updateotpText}
							/>
						</PanelRow>



						<PanelRow>
							<CheckboxControl
								label="Activate Facebook button"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showFacebookButton }
								onChange={ function(value){
									props.setAttributes( { showFacebookButton: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Activate Google button"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showGoogleButton }
								onChange={ function(value){
									props.setAttributes( { showGoogleButton: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Activate Apple button"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showAppleButton }
								onChange={ function(value){
									props.setAttributes( { showAppleButton: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Facebook button text"
								)}
								value={props.attributes.facebookButtonText}
								onChange={updateFacebookButtonText}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Google button text"
								)}
								value={props.attributes.googleButtonText}
								onChange={updateGoogleButtonText}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Apple button text"
								)}
								value={props.attributes.appleButtonText}
								onChange={updateAppleButtonText}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Forgot password text"
								)}
								value={props.attributes.forgotPassText}
								onChange={updateForgotPassText}
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>

				</BlockControls>


				<section>
				<div class="content center-column" style={{'font-size': props.attributes.fontSize}}>
					<h2 class="title"> {props.attributes.title} </h2>

					<div class="columns is-multiline">
						<div id="login-container">
							<form id="login_form" method="POST" action={cgbGlobal.resturl + "/login?_wpnonce="+cgbGlobal.nonce}>
								<input type="hidden" value="login" name="api_function"/>

									<div class="column">
										<label class="label">{props.attributes.usernameField}</label>
										<input class="input" id="pawpplugin_login_user" type="text" placeholder={props.attributes.usernameField} name="username"/>
									</div>

									<div class="column">
										<label class="label">{props.attributes.passwordField}</label>
										<input class="input" id="pawpplugin_login_pass" type="password" placeholder={props.attributes.passwordField} name="password"/>
										<a id="forgot-pass-link" class="link right">{props.attributes.forgotPassText}</a>
									</div>
								<div className="column columns is-mobile is-centered">
									<button className="column is-half"
											type="submit">{props.attributes.loginButtonText}</button>
								</div>
								{props.attributes.showFacebookButton &&
								<div className="column columns is-centered">
									<button id="facebookbutton" className="column is-full-mobile" type="button">
										<div className="level is-mobile">
											<div className="level-left">
												<span className="icon is-small level-item"><i
													className="fab fa-facebook-f"/></span>
											</div>
											<span className="level-item">{props.attributes.facebookButtonText}</span>
										</div>
									</button>
								</div>}
								{props.attributes.showGoogleButton &&
								<div className="column columns is-centered">
									<button id="googlebutton" className="column is-full-mobile" type="button">
										<div className="level is-mobile">
											<div className="level-left">
												<span className="icon is-small level-item"><i
													className="fab fa-google"/></span>
											</div>
											<span className="level-item">{props.attributes.googleButtonText}</span>
										</div>
									</button>
								</div>}
								{props.attributes.showAppleButton &&
								<div className="column columns is-centered">
									<button id="applebutton" className="column is-full-mobile" type="button">
										<div className="level is-mobile">
											<div className="level-left">
												<span className="icon is-small level-item"><i className="fab fa-apple"/></span>
											</div>
											<span className="level-item">{props.attributes.appleButtonText}</span>
										</div>
									</button>
								</div>}
							</form>
						</div>

					<div id="forgot-pass-container" class="column columns is-hidden">
						<form id="reset_password_form" method="POST" action={ cgbGlobal.resturl+"/forgot-password?_wpnonce="+cgbGlobal.nonce}>
								<div class="column">
									<label class="label">Email</label>
									<input id="pawpplugin_forgot_pass_email" name="email" type="text" placeholder="Email"></input>
								</div>
								 <div class="column">
									 <button type="submit">Indsend</button>
								 </div>
						</form>
					</div>

					</div>
				</div>
				</section>
			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {

		return (

			<div className={ props.className } style={{'font-size': props.attributes.fontSize}}>
				<section>
				<div class="content center-column">
					<h2 class="title"> {props.attributes.title} </h2>
					<div class="columns is-multiline">
						<div id="login-container">
							<form id="login_form" method="POST" action={cgbGlobal.restUrl+"/login?_wpnonce="+cgbGlobal.nonce}>
								<input type="hidden" value="login" name="api_function"/>
									<div id="login_message"></div>
									<div class="column">
										<label class="label">{props.attributes.usernameField}</label>
										<input class="input" id="pawpplugin_login_user" type="text" placeholder={props.attributes.usernameField} name="username"/>
									</div>

									<div class="column">
										<label class="label">{props.attributes.passwordField}</label>
										<input class="input" id="pawpplugin_login_pass" type="password" placeholder={props.attributes.passwordField} name="password"/>
										<a id="forgot-pass-link" class="link center">{props.attributes.forgotPassText}</a>
									</div>

									<div id="login_error"></div>

								<div className="column columns is-mobile is-centered">
									<button id="login_form_btn" className="column is-full-mobile is-half"
											type="button">{props.attributes.loginButtonText}</button>
								</div>
								{props.attributes.showFacebookButton &&
								<div className="column columns is-centered">
									<button id="facebookbutton" className="column is-full-mobile" type="button">
										<div className="level is-mobile">
											<div className="level-left">
												<span className="icon is-small level-item"><i
													className="fab fa-facebook-f"></i></span>
											</div>
											<span className="level-item">{props.attributes.facebookButtonText}</span>
										</div>
									</button>
								</div>}
								{props.attributes.showGoogleButton &&
								<div className="column columns is-centered">
									<button id="googlebutton" className="column is-full-mobile" type="button">
										<div className="level is-mobile">
											<div className="level-left">
												<span className="icon is-small level-item"><i
													className="fab fa-google"></i></span>
											</div>
											<span className="level-item">{props.attributes.googleButtonText}</span>
										</div>
									</button>
								</div>}
								{props.attributes.showAppleButton &&
								<div className="column columns is-centered">
									<button id="applebutton" className="column is-full-mobile" type="button">
										<div className="level is-mobile">
											<div className="level-left">
												<span className="icon is-small level-item"><i className="fab fa-apple"/></span>
											</div>
											<span className="level-item">{props.attributes.appleButtonText}</span>
										</div>
									</button>
								</div>}
								<a id="magiclink-link" class="link	center">Login uden kodeord</a>
							</form>
						</div>
                        
                        <div id="pr-container" style={{display:'none'}}>
                            <div class="column">
                                <label class="label">{props.attributes.newpasswordField1}</label>
                                <div class="flex flex-direction-row"><input class="input" id="pawpplugin_newpass1" type="password" placeholder={props.attributes.newpasswordField1} name="newpass1" /><span style="border: none !important"id="pass1_tooltip" class="has-tooltip-left has-tooltip-right-desktop has-tooltip-multiline"><i class="fa fa-question-circle"></i></span></div>
                            </div>

                            <div class="column">
                                <label class="label">{props.attributes.newpasswordField2}</label>
                                <div class="flex flex-direction-row"><input class="input" id="pawpplugin_newpass2" type="password" placeholder={props.attributes.newpasswordField2} name="newpass2"/><span style="border: none !important"id="pass2_tooltip" class="has-tooltip-left has-tooltip-right-desktop has-tooltip-multiline" ><i class="fa fa-question-circle"></i></span></div>
                            </div>
                            <div className="column columns is-mobile is-centered">
                                <button id="pr_btn" className="column is-full-mobile is-half" type="button">{props.attributes.savePWButtonText}</button>
                            </div>
                        </div>

                        <div id="otp-container" style={{display:'none'}}>
                            <input id="otp-authtoken" name="authtoken" type="hidden"/>
                            <div class="column">
                                <label class="label">{props.attributes.otpText}</label>
                                <div class="flex flex-direction-row"><input class="input" id="pawpplugin_otp" type="text" name="otp" /></div>
                            </div>

                            <div className="column columns is-mobile is-centered">
                                <button id="otp_btn" className="column is-full-mobile is-half" type="button">{props.attributes.sendOtpButtonText}</button>
                            </div>
                        </div>

					<div id="forgot-pass-container" class="column columns is-hidden">
                                <form id="reset_password_form" method="POST" action={cgbGlobal.restUrl + "/forgot-password?_wpnonce=" + cgbGlobal.nonce}>
								<div class="column">
									<label class="label">Email</label>
									<input id="pawpplugin_forgot_pass_email" name="email" type="text" placeholder="Email"></input>
								</div>
								 <div class="column">
									 <button id="forgot-pass-send-button" type="submit" disabled >Indsend</button>
								 </div>
								 <a class="link right"id="forgot-pass-goback">Gå tilbage til login</a>
						</form>
					</div>
					<div id="magiclink-container" class="column columns is-hidden">
						<form id="magiclink-form" method="POST" action={cgbGlobal.restUrl+"/magiclink?_wpnonce"+cgbGlobal.nonce}>
							<div class="column">
								<label for="magiclink-email" class="label">Email</label>
								<input type="text" name="magiclink-email" id="pawpplugin-magiclink-email" placeholder="Email"></input>
							</div>
							<div class="column">
								<button id="magiclink_send_button" type="submit">Send email</button>
							</div>
							<a class="link right"id="magiclink-goback">Gå tilbage til alm. login</a>
						</form>
					</div>


					</div>
				</div>
				</section>
			</div>

		);
	},
} );
