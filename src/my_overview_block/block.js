/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component, useState } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
    SelectControl,
	CheckboxControl,
	FontSizePicker,
    Dashicon,
    Tooltip,
    TextControl,
	ToolsPanel,
	ToolsPanelItem
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-my-overview-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'My Overview' ), // Block title.
	icon: 'businessperson', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category → Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block → CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		nameRow: {type: 'boolean', default: true},
		provisioningModels: { type: "string"},
		productIdents: { type: "string"},
		hideDecimals: {type: "boolean", default: false},
		fontSize: {type: 'int', default: 16},
	},

	edit: (props) => {
		
		return (
			
			<div classname={ props.className }>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						<PanelRow>
							<TextControl
								label="List what provisioning-model's to look for, comma-separated"
								value={props.attributes.provisioningModels}
								onChange={ function(value){
									props.setAttributes({ provisioningModels: value})
								}}/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label="List what product-idenfications's to look for, comma-separated"
								help="Will also look for partials, etc. 'Bredbånd' would find all product with 'Bredbånd' in product identification"
								value={props.attributes.productIdents}
								onChange={ function(value){
									props.setAttributes({ productIdents: value})
								}}/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Hide decimals on all prices"
								checked={props.attributes.hideDecimals}
								onChange={ function(value){
									props.setAttributes({ hideDecimals: value})
								}}/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
				
				</BlockControls>

				{/* here starts the edit block itself*/}
				<div className={'pa-my-overview container'} style={{'font-size': props.attributes.fontSize}}>
					<div className='pa-my-overview-title'>Mit Abonnement</div>
					<div className='pa-my-overview-address'>Adresse:<span>Paradisæblevej 111, Andeby</span></div>
					<div className='pa-my-overview-customernumber'>Kundenummer:<span>123456789</span></div>
					<div className='pa-my-overview-product'>Produktnavn:<span>Bredbånd 1000/1000Mbit</span></div>
					<div className='pa-my-overview-product'>Navn:<span>Anders And</span></div>
					<div className='pa-my-overview-product'>Abonnementspris pr. måned:<span>249,- kr</span></div>
					<div className='pa-my-overview-product'>Abonnementspris pr. kvartal:<span>747,- kr</span></div>
				</div>		
			</div>
			
		);	
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div className={'pa-my-overview container'} style={{'font-size': props.attributes.fontSize}} data-decimals={props.attributes.hideDecimals} data-provmodels={props.attributes.provisioningModels} data-productidents={props.attributes.productIdents}>
					<div className='pa-my-overview-title columns'>
						<div className='column is-half is-offset-one-quarter'>Mit abonnement</div>
					</div>
					<div className='pa-my-overview-address columns'>
						<div className='column is-offset-1 is-3'>Adresse:</div>
						<div className='column is-offset-1 is-3 value'></div>
					</div>
					<div className='pa-my-overview-customernumber columns'>
						<div className='column is-offset-1 is-3'>Kundenummer:</div>
						<div className='column is-offset-1 is-3 value'></div>
					</div>
					<div className='pa-my-overview-product columns'>
						<div className='column is-offset-1 is-3'>Produktnavn:</div>
						<div className='column is-offset-1 is-3 value'></div>
					</div>
					<div className='pa-my-overview-name columns'>
						<div className='column is-offset-1 is-3'>Navn:</div>
						<div className='column is-offset-1 is-3 value'></div>
					</div>
					<div className='pa-my-overview-monthly columns'>
						<div className='column is-offset-1 is-3'>Abonnementspris pr. måned:</div>
						<div className='column is-offset-1 is-3 value'></div>
					</div>
					<div className='pa-my-overview-quarterly columns'>
						<div className='column is-offset-1 is-3'>Abonnementspris pr. kvartal:</div>
						<div className='column is-offset-1 is-3 value'></div>
					</div>
			</div>
			
		);	
 }

});
