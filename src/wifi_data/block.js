/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component, useState } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
    SelectControl,
	CheckboxControl,
	FontSizePicker,
    Dashicon,
    Tooltip,
    TextControl,
	ToolsPanel,
	ToolsPanelItem
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-wifi-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'WiFi Data' ), // Block title.
	icon: 'businessperson', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-wifi-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
        hiddenByDefault: {type: 'boolean', default: false},
        
        //Access Points
        aHeader: {type: 'string', default: "Access Points"},

		showAName: {type: 'boolean', default: false},
		aNameAdvanced: {type: 'boolean', default: false},
		aNameLabel: {type: 'string', default: "Navn/MAC"},
        aNameClass: {type: 'string', default: ""},
        aNameStyle: {type: 'CSSProperties', default: {}},

		showAIP: {type: 'boolean', default: false},
		aIPAdvanced: {type: 'boolean', default: true},
		aIPLabel: {type: 'string', default: "IP adresse"},
        aIPClass: {type: 'string', default: "hideme"},
        aIPStyle: {type: 'CSSProperties', default: {display: 'none'}},

		showAModel: {type: 'boolean', default: false},
		aModelAdvanced: {type: 'boolean', default: false},
		aModelLabel: {type: 'string', default: "Model"},
        aModelClass: {type: 'string', default: ""},
        aModelStyle: {type: 'CSSProperties', default: {}},

		showAVersion: {type: 'boolean', default: false},
		aVersionAdvanced: {type: 'boolean', default: true},
		aVersionLabel: {type: 'string', default: "Version"},
        aVersionClass: {type: 'string', default: "hideme"},
        aVersionStyle: {type: 'CSSProperties', default: {display: 'none'}},

		showASSID2: {type: 'boolean', default: false},
		aSSID2Advanced: {type: 'boolean', default: false},
		aSSID2Label: {type: 'string', default: "SSID 2.4G"},
        aSSID2Class: {type: 'string', default: ""},
        aSSID2Style: {type: 'CSSProperties', default: {}},

		showAChannel2: {type: 'boolean', default: false},
		aChannel2Advanced: {type: 'boolean', default: true},
		aChannel2Label: {type: 'string', default: "Kanal 2.4G"},
        aChannel2Class: {type: 'string', default: "hideme"},
        aChannel2Style: {type: 'CSSProperties', default: {display: 'none'}},

		showASSID5: {type: 'boolean', default: false},
		aSSID5Advanced: {type: 'boolean', default: false},
		aSSID5Label: {type: 'string', default: "SSID 5G"},
        aSSID5Class: {type: 'string', default: ""},
        aSSID5Style: {type: 'CSSProperties', default: {}},

		showAChannel5: {type: 'boolean', default: false},
		aChannel5Advanced: {type: 'boolean', default: true},
		aChannel5Label: {type: 'string', default: "Kanal 5G"},
        aChannel5Class: {type: 'string', default: "hideme"},
        aChannel5Style: {type: 'CSSProperties', default: {display: 'none'}},

 		showAUpTime: {type: 'boolean', default: false},
		aUpTimeAdvanced: {type: 'boolean', default: true},
		aUpTimeLabel: {type: 'string', default: "Oppetid"},
        aUpTimeClass: {type: 'string', default: "hideme"},
        aUpTimeStyle: {type: 'CSSProperties', default: {display: 'none'}},

 		showAStatus: {type: 'boolean', default: false},
		aStatusAdvanced: {type: 'boolean', default: false},
		aStatusLabel: {type: 'string', default: "Status"},
        aStatusClass: {type: 'string', default: ""},
        aStatusStyle: {type: 'CSSProperties', default: {}},

        //Clients
        cHeader: {type: 'string', default: "Klienter"},
        
 		showCMAC: {type: 'boolean', default: false},
		cMACAdvanced: {type: 'boolean', default: true},
		cMACLabel: {type: 'string', default: "MAC"},
        cMACClass: {type: 'string', default: "hideme"},
        cMACStyle: {type: 'CSSProperties', default: {display: 'none'}},

 		showCHardware: {type: 'boolean', default: false},
		cHardwareAdvanced: {type: 'boolean', default: false},
		cHardwareLabel: {type: 'string', default: "Hardware"},
        cHardwareClass: {type: 'string', default: ""},
        cHardwareStyle: {type: 'CSSProperties', default: {}},

 		showCName: {type: 'boolean', default: false},
		cNameAdvanced: {type: 'boolean', default: false},
		cNameLabel: {type: 'string', default: "Navn"},
        cNameClass: {type: 'string', default: ""},
        cNameStyle: {type: 'CSSProperties', default: {}},

 		showCIP: {type: 'boolean', default: false},
		cIPAdvanced: {type: 'boolean', default: true},
		cIPLabel: {type: 'string', default: "IP adresse"},
        cIPClass: {type: 'string', default: "hideme"},
        cIPStyle: {type: 'CSSProperties', default: {display: 'none'}},

 		showCAccessPoint: {type: 'boolean', default: false},
		cAccessPointAdvanced: {type: 'boolean', default: false},
		cAccessPointLabel: {type: 'string', default: "Access Point"},
        cAccessPointClass: {type: 'string', default: ""},
        cAccessPointStyle: {type: 'CSSProperties', default: {}},

 		showCRadio: {type: 'boolean', default: false},
		cRadioAdvanced: {type: 'boolean', default: false},
		cRadioLabel: {type: 'string', default: "Radio"},
        cRadioClass: {type: 'string', default: ""},
        cRadioStyle: {type: 'CSSProperties', default: {}},

 		showCRXRate: {type: 'boolean', default: false},
		cRXRateAdvanced: {type: 'boolean', default: true},
		cRXRateLabel: {type: 'string', default: "RX rate"},
        cRXRateClass: {type: 'string', default: "hideme"},
        cRXRateStyle: {type: 'CSSProperties', default: {display: 'none'}},

 		showCTXRate: {type: 'boolean', default: false},
		cTXRateAdvanced: {type: 'boolean', default: true},
		cTXRateLabel: {type: 'string', default: "TX rate"},
        cTXRateClass: {type: 'string', default: "hideme"},
        cTXRateStyle: {type: 'CSSProperties', default: {display: 'none'}},

 		showCSignal: {type: 'boolean', default: false},
		cSignalAdvanced: {type: 'boolean', default: false},
		cSignalLabel: {type: 'string', default: "Signal"},
        cSignalClass: {type: 'string', default: ""},
        cSignalStyle: {type: 'CSSProperties', default: {}},

 		showCDown: {type: 'boolean', default: false},
		cDownAdvanced: {type: 'boolean', default: true},
		cDownLabel: {type: 'string', default: "Down"},
        cDownClass: {type: 'string', default: "hideme"},
        cDownStyle: {type: 'CSSProperties', default: {display: 'none'}},

 		showCUp: {type: 'boolean', default: false},
		cUpAdvanced: {type: 'boolean', default: true},
		cUpLabel: {type: 'string', default: "Up"},
        cUpClass: {type: 'string', default: "hideme"},
        cUpStyle: {type: 'CSSProperties', default: {display: 'none'}},

		fontSize: {type: 'int', default: 16}
	},

	edit: (props) => {
		
		return (
			<div classname={ props.className }>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="Access Point settings">
                        <PanelRow>
                        {/************************** Navn/MAC *****************************/}
							<CheckboxControl
								label="Skjul block som standard"
								help="Check the box if you want to initially hide the entire block"
								checked={ props.attributes.hiddenByDefault }
								onChange={ function(value){
									props.setAttributes( { hiddenByDefault: value } );
								} }
							/>
						</PanelRow>
                    	<PanelRow>
                        {/************************** Overskrift *****************************/}
							<TextControl
								label={__(
									"Access Points overskrift"
								)}
								value={props.attributes.aHeader}
								onChange={ function(value){
									props.setAttributes( { aHeader: value } );
								}}
							/>
						</PanelRow>

						<PanelRow>
                        {/************************** Navn/MAC *****************************/}
							<CheckboxControl
								label="Navn/MAC"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showAName }
								onChange={ function(value){
									props.setAttributes( { showAName: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.aNameLabel}
								onChange={ function(value){
									props.setAttributes( { aNameLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.aNameAdvanced }
								onChange={ function(value){
									props.setAttributes( { aNameAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { aNameClass: 'hideme' } );
                                        props.setAttributes( { aNameStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { aNameClass: '' } );
                                        props.setAttributes( { aNameStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
                        <PanelRow>
                        {/************************** IP adresse *****************************/}
							<CheckboxControl
								label="IP adresse"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showAIP }
								onChange={ function(value){
									props.setAttributes( { showAIP: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.aIPLabel}
								onChange={ function(value){
									props.setAttributes( { aIPLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.aIPAdvanced }
								onChange={ function(value){
									props.setAttributes( { aIPAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { aIPClass: 'hideme' } );
                                        props.setAttributes( { aIPStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { aIPClass: '' } );
                                        props.setAttributes( { aIPStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
                        <PanelRow>
                        {/************************** Model *****************************/}
							<CheckboxControl
								label="Model"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showAModel }
								onChange={ function(value){
									props.setAttributes( { showAModel: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.aModelLabel}
								onChange={ function(value){
									props.setAttributes( { aModelLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.aModelAdvanced }
								onChange={ function(value){
									props.setAttributes( { aModelAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { aModelClass: 'hideme' } );
                                        props.setAttributes( { aModelStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { aModelClass: '' } );
                                        props.setAttributes( { aModelStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
                        <PanelRow>
                        {/************************** Version *****************************/}
							<CheckboxControl
								label="Version"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showAVersion }
								onChange={ function(value){
									props.setAttributes( { showAVersion: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.aVersionLabel}
								onChange={ function(value){
									props.setAttributes( { aVersionLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.aVersionAdvanced }
								onChange={ function(value){
									props.setAttributes( { aVersionAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { aVersionClass: 'hideme' } );
                                        props.setAttributes( { aVersionStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { aVersionClass: '' } );
                                        props.setAttributes( { aVersionStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
                        <PanelRow>
                        {/************************** SSID 2.4G *****************************/}
							<CheckboxControl
								label="SSID 2.4G"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showASSID2 }
								onChange={ function(value){
									props.setAttributes( { showASSID2: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.aSSID2Label}
								onChange={ function(value){
									props.setAttributes( { aSSID2Label: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.aSSID2Advanced }
								onChange={ function(value){
									props.setAttributes( { aSSID2Advanced: value } );
                                    if(value){
                                        props.setAttributes( { aSSID2Class: 'hideme' } );
                                        props.setAttributes( { aSSID2Style: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { aSSID2Class: '' } );
                                        props.setAttributes( { aSSID2Style: {} } );
                                    }
								} }
							/>
						</PanelRow>
                        <PanelRow>
                        {/************************** Kanal 2.4G *****************************/}
							<CheckboxControl
								label="Kanal 2.4G"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showAChannel2 }
								onChange={ function(value){
									props.setAttributes( { showAChannel2: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.aChannel2Label}
								onChange={ function(value){
									props.setAttributes( { aChannel2Label: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.aChannel2Advanced }
								onChange={ function(value){
									props.setAttributes( { aChannel2Advanced: value } );
                                    if(value){
                                        props.setAttributes( { aChannel2Class: 'hideme' } );
                                        props.setAttributes( { aChannel2Style: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { aChannel2Class: '' } );
                                        props.setAttributes( { aChannel2Style: {} } );
                                    }
								} }
							/>
						</PanelRow>
                        <PanelRow>
                        {/************************** SSID 5G *****************************/}
							<CheckboxControl
								label="SSID 5G"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showASSID5 }
								onChange={ function(value){
									props.setAttributes( { showASSID5: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.aSSID5Label}
								onChange={ function(value){
									props.setAttributes( { aSSID5Label: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.aSSID5Advanced }
								onChange={ function(value){
									props.setAttributes( { aSSID5Advanced: value } );
                                    if(value){
                                        props.setAttributes( { aSSID5Class: 'hideme' } );
                                        props.setAttributes( { aSSID5Style: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { aSSID5Class: '' } );
                                        props.setAttributes( { aSSID5Style: {} } );
                                    }
								} }
							/>
						</PanelRow>
                        <PanelRow>
                        {/************************** Kanal 5G *****************************/}
							<CheckboxControl
								label="Kanal 5G"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showAChannel5 }
								onChange={ function(value){
									props.setAttributes( { showAChannel5: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.aChannel5Label}
								onChange={ function(value){
									props.setAttributes( { aChannel5Label: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.aChannel5Advanced }
								onChange={ function(value){
									props.setAttributes( { aChannel5Advanced: value } );
                                    if(value){
                                        props.setAttributes( { aChannel5Class: 'hideme' } );
                                        props.setAttributes( { aChannel5Style: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { aChannel5Class: '' } );
                                        props.setAttributes( { aChannel5Style: {} } );
                                    }
								} }
							/>
						</PanelRow>
                        <PanelRow>
                        {/************************** Oppetid *****************************/}
							<CheckboxControl
								label="Oppetid"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showAUpTime }
								onChange={ function(value){
									props.setAttributes( { showAUpTime: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.aUpTimeLabel}
								onChange={ function(value){
									props.setAttributes( { aUpTimeLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.aUpTimeAdvanced }
								onChange={ function(value){
									props.setAttributes( { aUpTimeAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { aUpTimeClass: 'hideme' } );
                                        props.setAttributes( { aUpTimeStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { aUpTimeClass: '' } );
                                        props.setAttributes( { aUpTimeStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
                        <PanelRow>
                        {/************************** Status *****************************/}
							<CheckboxControl
								label="Status"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showAStatus }
								onChange={ function(value){
									props.setAttributes( { showAStatus: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.aStatusLabel}
								onChange={ function(value){
									props.setAttributes( { aStatusLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.aStatusAdvanced }
								onChange={ function(value){
                                    if(value){
                                        props.setAttributes( { aStatusClass: 'hideme' } );
                                        props.setAttributes( { aStatusStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { aStatusClass: '' } );
                                        props.setAttributes( { aStatusStyle: {} } );
                                    }
									props.setAttributes( { aStatusAdvanced: value } );
								} }
							/>
						</PanelRow>
					</PanelBody>
                    <PanelBody title="Clients settings">
                        <PanelRow>
                        {/************************** Overskrift *****************************/}
							<TextControl
								label={__(
									"Clients overskrift"
								)}
								value={props.attributes.cHeader}
								onChange={ function(value){
									props.setAttributes( { cHeader: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
                        {/************************** MAC *****************************/}
							<CheckboxControl
								label="MAC"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showCMAC }
								onChange={ function(value){
									props.setAttributes( { showCMAC: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.cMACLabel}
								onChange={ function(value){
									props.setAttributes( { cMACLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.cMACAdvanced }
								onChange={ function(value){
									props.setAttributes( { cMACAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { cMACClass: 'hideme' } );
                                        props.setAttributes( { cMACStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { cMACClass: '' } );
                                        props.setAttributes( { cMACStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
						<PanelRow>
                        {/************************** Hardware *****************************/}
							<CheckboxControl
								label="Hardware"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showCHardware }
								onChange={ function(value){
									props.setAttributes( { showCHardware: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.cHardwareLabel}
								onChange={ function(value){
									props.setAttributes( { cHardwareLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.cHardwareAdvanced }
								onChange={ function(value){
									props.setAttributes( { cHardwareAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { cHardwareClass: 'hideme' } );
                                        props.setAttributes( { cHardwareStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { cHardwareClass: '' } );
                                        props.setAttributes( { cHardwareStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
						<PanelRow>
                        {/************************** Navn *****************************/}
							<CheckboxControl
								label="Navn"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showCName }
								onChange={ function(value){
									props.setAttributes( { showCName: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.cNameLabel}
								onChange={ function(value){
									props.setAttributes( { cNameLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.cNameAdvanced }
								onChange={ function(value){
									props.setAttributes( { cNameAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { cNameClass: 'hideme' } );
                                        props.setAttributes( { cNameStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { cNameClass: '' } );
                                        props.setAttributes( { cNameStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
						<PanelRow>
                        {/************************** IP adresse *****************************/}
							<CheckboxControl
								label="IP adresse"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showCIP }
								onChange={ function(value){
									props.setAttributes( { showCIP: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.cIPLabel}
								onChange={ function(value){
									props.setAttributes( { cIPLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.cIPAdvanced }
								onChange={ function(value){
									props.setAttributes( { cIPAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { cIPClass: 'hideme' } );
                                        props.setAttributes( { cIPStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { cIPClass: '' } );
                                        props.setAttributes( { cIPStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
						<PanelRow>
                        {/************************** Access Point *****************************/}
							<CheckboxControl
								label="Access Point"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showCAccessPoint }
								onChange={ function(value){
									props.setAttributes( { showCAccessPoint: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.cAccessPointLabel}
								onChange={ function(value){
									props.setAttributes( { cAccessPointLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.cAccessPointAdvanced }
								onChange={ function(value){
									props.setAttributes( { cAccessPointAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { cAccessPointClass: 'hideme' } );
                                        props.setAttributes( { cAccessPointStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { cAccessPointClass: '' } );
                                        props.setAttributes( { cAccessPointStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
						<PanelRow>
                        {/************************** Radio *****************************/}
							<CheckboxControl
								label="Radio"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showCRadio }
								onChange={ function(value){
									props.setAttributes( { showCRadio: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.cRadioLabel}
								onChange={ function(value){
									props.setAttributes( { cRadioLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.cRadioAdvanced }
								onChange={ function(value){
									props.setAttributes( { cRadioAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { cRadioClass: 'hideme' } );
                                        props.setAttributes( { cRadioStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { cRadioClass: '' } );
                                        props.setAttributes( { cRadioStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
						<PanelRow>
                        {/************************** RX Rate *****************************/}
							<CheckboxControl
								label="RX Rate"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showCRXRate }
								onChange={ function(value){
									props.setAttributes( { showCRXRate: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.cRXRateLabel}
								onChange={ function(value){
									props.setAttributes( { cRXRateLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.cRXRateAdvanced }
								onChange={ function(value){
									props.setAttributes( { cRXRateAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { cRXRateClass: 'hideme' } );
                                        props.setAttributes( { cRXRateStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { cRXRateClass: '' } );
                                        props.setAttributes( { cRXRateStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
						<PanelRow>
                        {/************************** TX Rate *****************************/}
							<CheckboxControl
								label="TX Rate"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showCTXRate }
								onChange={ function(value){
									props.setAttributes( { showCTXRate: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.cTXRateLabel}
								onChange={ function(value){
									props.setAttributes( { cTXRateLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.cTXRateAdvanced }
								onChange={ function(value){
									props.setAttributes( { cTXRateAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { cTXRateClass: 'hideme' } );
                                        props.setAttributes( { cTXRateStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { cTXRateClass: '' } );
                                        props.setAttributes( { cTXRateStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
						<PanelRow>
                        {/************************** Signal *****************************/}
							<CheckboxControl
								label="Signal"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showCSignal }
								onChange={ function(value){
									props.setAttributes( { showCSignal: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.cSignalLabel}
								onChange={ function(value){
									props.setAttributes( { cSignalLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.cSignalAdvanced }
								onChange={ function(value){
									props.setAttributes( { cSignalAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { cSignalClass: 'hideme' } );
                                        props.setAttributes( { cSignalStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { cSignalClass: '' } );
                                        props.setAttributes( { cSignalStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
						<PanelRow>
                        {/************************** Down *****************************/}
							<CheckboxControl
								label="Down"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showCDown }
								onChange={ function(value){
									props.setAttributes( { showCDown: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.cSignalLabel}
								onChange={ function(value){
									props.setAttributes( { cDownLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.cDownAdvanced }
								onChange={ function(value){
									props.setAttributes( { cDownAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { cDownClass: 'hideme' } );
                                        props.setAttributes( { cDownStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { cDownClass: '' } );
                                        props.setAttributes( { cDownStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
						<PanelRow>
                        {/************************** Up *****************************/}
							<CheckboxControl
								label="Up"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showCUp }
								onChange={ function(value){
									props.setAttributes( { showCUp: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.cUpLabel}
								onChange={ function(value){
									props.setAttributes( { cUpLabel: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<CheckboxControl
								label="Avanceret"
								help="Check the box if the field is advanced"
								checked={ props.attributes.cUpAdvanced }
								onChange={ function(value){
									props.setAttributes( { cUpAdvanced: value } );
                                    if(value){
                                        props.setAttributes( { cUpClass: 'hideme' } );
                                        props.setAttributes( { cUpStyle: {display: 'none'} } );
                                    }else{
                                        props.setAttributes( { cUpClass: '' } );
                                        props.setAttributes( { cUpStyle: {} } );
                                    }
								} }
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
				
				</BlockControls>

				{/* here starts the edit block itself*/}
				<div id="wifi-data-container" class="content" style={{'font-size': props.attributes.fontSize},{'display': props.attributes.hiddenByDefault ? 'none' : 'block'}}>
						<div class="container">
							<div class="column table-container">
                                {props.attributes.aHeader}<button class="toggle-visibility-button">Avanceret</button>
								<table id="accesspoints_table" class="table is-fullwidth">
									<thead>
										<tr>
                                            {props.attributes.showAName ? <th>{props.attributes.aNameLabel}</th> : ''}
                                            {props.attributes.showAIP ? <th>{props.attributes.aIPLabel}</th> : ''}
                                            {props.attributes.showAModel ? <th>{props.attributes.aModelLabel}</th> : ''}
                                            {props.attributes.showAVersion ? <th>{props.attributes.aVersionLabel}</th> : ''}
                                            {props.attributes.showASSID2 ? <th>{props.attributes.aSSID2Label}</th> : ''}
                                            {props.attributes.showAChannel2 ? <th>{props.attributes.aChannel2Label}</th> : ''}
                                            {props.attributes.showASSID5 ? <th>{props.attributes.aSSID5Label}</th> : ''}
                                            {props.attributes.showAChannel5 ? <th>{props.attributes.aChannel5Label}</th> : ''}
                                            {props.attributes.showAUpTime ? <th>{props.attributes.aUpTimeLabel}</th> : ''}
                                            {props.attributes.showAStatus ? <th>{props.attributes.aStatusLabel}</th> : ''}
										</tr>
									</thead>
									<tbody>
										<tr>
											{props.attributes.showAName ? <td>AP navn</td> : ''}
                                            {props.attributes.showAIP ? <th>1.2.3.4</th> : ''}
                                            {props.attributes.showAModel ? <th>Modem model</th> : ''}
                                            {props.attributes.showAVersion ? <th>1.2</th> : ''}
                                            {props.attributes.showASSID2 ? <th>bruger1</th> : ''}
                                            {props.attributes.showAChannel2 ? <th>3</th> : ''}
                                            {props.attributes.showASSID5 ? <th>bruger1</th> : ''}
                                            {props.attributes.showAChannel5 ? <th>4</th> : ''}
                                            {props.attributes.showAUpTime ? <th>10 sec</th> : ''}
                                            {props.attributes.showAStatus ? <th>Grøn</th> : ''}
										</tr>
										<tr>
											{props.attributes.showAName ? <td>AP navn 2</td> : ''}
                                            {props.attributes.showAIP ? <th>1.2.3.5</th> : ''}
                                            {props.attributes.showAModel ? <th>Modem model2</th> : ''}
                                            {props.attributes.showAVersion ? <th>1.2</th> : ''}
                                            {props.attributes.showASSID2 ? <th>bruger2</th> : ''}
                                            {props.attributes.showAChannel2 ? <th>3</th> : ''}
                                            {props.attributes.showASSID5 ? <th>bruger2</th> : ''}
                                            {props.attributes.showAChannel5 ? <th>4</th> : ''}
                                            {props.attributes.showAUpTime ? <th>8 sec</th> : ''}
                                            {props.attributes.showAStatus ? <th>Grøn</th> : ''}
										</tr>
									</tbody>
								</table>
							</div>
						</div>
                        <div class="container">
							<div class="column table-container">
                                {props.attributes.cHeader}
								<table id="clients_table" class="table is-fullwidth">
									<thead>
										<tr>
                                            {props.attributes.showCMAC ? <th>{props.attributes.cMACLabel}</th> : ''}
                                            {props.attributes.showCHardware ? <th>{props.attributes.cHardwareLabel}</th> : ''}
                                            {props.attributes.showCName ? <th>{props.attributes.cNameLabel}</th> : ''}
                                            {props.attributes.showCIP ? <th>{props.attributes.cIPLabel}</th> : ''}
                                            {props.attributes.showCAccessPoint ? <th>{props.attributes.cAccessPointLabel}</th> : ''}
                                            {props.attributes.showCRadio ? <th>{props.attributes.cRadioLabel}</th> : ''}
                                            {props.attributes.showCRXRate ? <th>{props.attributes.cRXRateLabel}</th> : ''}
                                            {props.attributes.showCTXRate ? <th>{props.attributes.cTXRateLabel}</th> : ''}
                                            {props.attributes.showCSignal ? <th>{props.attributes.cSignalLabel}</th> : ''}
                                            {props.attributes.showCDown ? <th>{props.attributes.cDownLabel}</th> : ''}
                                            {props.attributes.showCUp ? <th>{props.attributes.cUpLabel}</th> : ''}
										</tr>
									</thead>
									<tbody>
										<tr>
                                            {props.attributes.showCMAC ? <th>123:456:abc:def</th> : ''}
                                            {props.attributes.showCHardware ? <th>Telefon</th> : ''}
                                            {props.attributes.showCName ? <th>T1</th> : ''}
                                            {props.attributes.showCIP ? <th>2.3.4.5</th> : ''}
                                            {props.attributes.showCAccessPoint ? <th>AP navn 2</th> : ''}
                                            {props.attributes.showCRadio ? <th>c</th> : ''}
                                            {props.attributes.showCRXRate ? <th>3</th> : ''}
                                            {props.attributes.showCTXRate ? <th>4</th> : ''}
                                            {props.attributes.showCSignal ? <th>12</th> : ''}
                                            {props.attributes.showCDown ? <th>101</th> : ''}
                                            {props.attributes.showCUp ? <th>102</th> : ''}
										</tr>
									</tbody>
								</table>
							</div>
						</div>
				</div>		
			</div>
			
		);	
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {
		return (
			<div id="wifi-data-container" className={ props.className } data-attributes={JSON.stringify(props.attributes)} style={{'font-size': props.attributes.fontSize},{'display': props.attributes.hiddenByDefault ? 'none' : 'block'}}>
                <div id="ap-container" class="container">
                    <span id="ap_table_header">{props.attributes.aHeader}</span><span id="ap_wifi_toggle_advanced" class="dashicons dashicons-admin-generic"></span>
                    <div class="table-container">    
                        <table id="accesspoints_table" class="table">
                            <thead>
                                <tr>
                                    {props.attributes.showAName ? <th class={props.attributes.aNameClass} style={props.attributes.aNameStyle}>{props.attributes.aNameLabel}</th> : ''}
                                    {props.attributes.showAIP ? <th class={props.attributes.aIPClass} style={props.attributes.aIPStyle}>{props.attributes.aIPLabel}</th> : ''}
                                    {props.attributes.showAModel ? <th class={props.attributes.aModelClass} style={props.attributes.aModelStyle}>{props.attributes.aModelLabel}</th> : ''}
                                    {props.attributes.showAVersion ? <th class={props.attributes.aVersionClass} style={props.attributes.aVersionStyle}>{props.attributes.aVersionLabel}</th> : ''}
                                    {props.attributes.showASSID2 ? <th class={props.attributes.aSSID2Class} style={props.attributes.aSSID2Style}>{props.attributes.aSSID2Label}</th> : ''}
                                    {props.attributes.showAChannel2 ? <th class={props.attributes.aChannel2Class} style={props.attributes.aChannel2Style}>{props.attributes.aChannel2Label}</th> : ''}
                                    {props.attributes.showASSID5 ? <th class={props.attributes.aSSID5Class} style={props.attributes.aSSID5Style}>{props.attributes.aSSID5Label}</th> : ''}
                                    {props.attributes.showAChannel5 ? <th class={props.attributes.aChannel5Class} style={props.attributes.aChannel5Style}>{props.attributes.aChannel5Label}</th> : ''}
                                    {props.attributes.showAUpTime ? <th class={props.attributes.aUpTimeClass} style={props.attributes.aUpTimeStyle}>{props.attributes.aUpTimeLabel}</th> : ''}
                                    {props.attributes.showAStatus ? <th class={props.attributes.aStatusClass} style={props.attributes.aStatusStyle}>{props.attributes.aStatusLabel}</th> : ''}
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="cl-container" class="container">
                    <span id="cl_table_header">{props.attributes.cHeader}</span>
                    <div class="table-container">    
                        <table id="clients_table" class="table">
                            <thead>
                                <tr>
                                    {props.attributes.showCMAC ? <th class={props.attributes.cMACClass} style={props.attributes.cMACStyle}>{props.attributes.cMACLabel}</th> : ''}
                                    {props.attributes.showCHardware ? <th class={props.attributes.cHardwareClass} style={props.attributes.cHardwareStyle}>{props.attributes.cHardwareLabel}</th> : ''}
                                    {props.attributes.showCName ? <th class={props.attributes.cNameClass} style={props.attributes.cNameStyle}>{props.attributes.cNameLabel}</th> : ''}
                                    {props.attributes.showCIP ? <th class={props.attributes.cIPClass} style={props.attributes.cIPStyle}>{props.attributes.cIPLabel}</th> : ''}
                                    {props.attributes.showCAccessPoint ? <th class={props.attributes.cAccessPointClass} style={props.attributes.cAccessPointStyle}>{props.attributes.cAccessPointLabel}</th> : ''}
                                    {props.attributes.showCRadio ? <th class={props.attributes.cRadioClass} style={props.attributes.cRadioStyle}>{props.attributes.cRadioLabel}</th> : ''}
                                    {props.attributes.showCRXRate ? <th class={props.attributes.cRXRateClass} style={props.attributes.cRXRateStyle}>{props.attributes.cRXRateLabel}</th> : ''}
                                    {props.attributes.showCTXRate ? <th class={props.attributes.cTXRateClass} style={props.attributes.cTXRateStyle}>{props.attributes.cTXRateLabel}</th> : ''}
                                    {props.attributes.showCSignal ? <th class={props.attributes.cSignalClass} style={props.attributes.cSignalStyle}>{props.attributes.cSignalLabel}</th> : ''}
                                    {props.attributes.showCDown ? <th class={props.attributes.cDownClass} style={props.attributes.cDownStyle}>{props.attributes.cDownLabel}</th> : ''}
                                    {props.attributes.showCUp ? <th class={props.attributes.cUpClass} style={props.attributes.cUpStyle}>{props.attributes.cUpLabel}</th> : ''}
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>                        
			</div>
		);
 }

});