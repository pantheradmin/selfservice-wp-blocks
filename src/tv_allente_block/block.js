/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    InnerBlocks,
	useBlockProps,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
	MediaUpload,
	MediaUploadCheck,
	CheckboxControl,
	FontSizePicker,
    SelectControl,
    Dashicon,
    Tooltip,
    TextControl
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const ALLOWED_MEDIA_TYPES = [ 'image' ];

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-allente-tv-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Allente-tv-block' ), // Block title.
	icon: 'groups', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		fontSize: {type: 'int', default: 16},

		showShowContentButtons: {type: "boolean", default: false},

		packet1ProductName: {type: "string", default: "Streaming Basic"},
		packet1ProductIdent: {type: "string"},
		packet1Content: {type:"string", default:""},
		packet1Content2: {type:"string", default:""},
		packet1ContentImage: {type:"string", default:""},
		packet1Price: {type:"string", default:""},
		packet1Price2: {type:"string", default:""},
		packet1PriceDiscountAmount: {type:"string", default:""},
		packet1PriceDiscountAmount2: {type:"string", default:""},
		packet1PriceWithDiscount: {type:"string", default:""},
		packet1PriceWithDiscount2: {type:"string", default:""},
		packet1Commitment: {type:"string", default:""},
		
		packet2ProductName: {type: "string", default: "Streaming Standard"},
		packet2ProductIdent: {type: "string"},
		packet2Content: {type:"string", default:""},
		packet2Content2: {type:"string", default:""},
		packet2ContentImage: {type:"string", default:""},
		packet2Price: {type:"string", default:""},
		packet2Price2: {type:"string", default:""},
		packet2PriceDiscountAmount: {type:"string", default:""},
		packet2PriceDiscountAmount2: {type:"string", default:""},
		packet2PriceWithDiscount: {type:"string", default:""},
		packet2PriceWithDiscount2: {type:"string", default:""},
		packet2Commitment: {type:"string", default:""},
		
		packet3ProductName: {type: "string", default: "Streaming Premium"},
		packet3ProductIdent: {type: "string"},
		packet3Content: {type:"string", default:""},
		packet3Content2: {type:"string", default:""},
		packet3ContentImage: {type:"string", default:""},
		packet3Price: {type:"string", default:""},
		packet3Price2: {type:"string", default:""},
		packet3PriceDiscountAmount: {type:"string", default:""},
		packet3PriceDiscountAmount2: {type:"string", default:""},
		packet3PriceWithDiscount: {type:"string", default:""},
		packet3PriceWithDiscount2: {type:"string", default:""},
		packet3Commitment: {type:"string", default:""},

		currentProductInfoText: {type: "string"},
		changeproductbtntext: {type: "string", default: "Skift pakke"},
		step1BeforeText: {type: "string"},
		step1BeforeText2: {type: "string"},
		step1AfterText: {type: "string"},
		showReadMoreAboutTvButton: {type: "boolean", default: false},
		readMoreAboutTvButtonLink: {type: "string"},
		step2InfoText: {type: "string"},
		settopboxtext: {type: "string", default: "Ønsker du en TV boks? 59,-/mdr i 24 mdr."},
		receiptInfoText: {type: "string"}

		
	},

	edit: (props) => {
		
		return (

			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="Packet settings">
						<PanelRow>
						<CheckboxControl
								label="Show 'Show content' button"
								help="Check the box if you want to show the buttons"
								checked={ props.attributes.showShowContentButtons }
								onChange={ function(value){
									props.setAttributes( { showShowContentButtons: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 1 Product Name"}
								value={props.attributes.packet1ProductName}
								onChange={ function(value){
									props.setAttributes( { packet1ProductName: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 1 Product Ident"}
								value={props.attributes.packet1ProductIdent}
								onChange={ function(value){
									props.setAttributes( { packet1ProductIdent: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 1 content"}
								value={props.attributes.packet1Content}
								onChange={ function(value){
									props.setAttributes( { packet1Content: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 1 content,second row"}
								value={props.attributes.packet1Content2}
								onChange={ function(value){
									props.setAttributes( { packet1Content2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 1 content image"}
								value={props.attributes.packet1ContentImage}
								onChange={ function(value){
									props.setAttributes( { packet1ContentImage: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 1 price"}
								value={props.attributes.packet1Price}
								onChange={ function(value){
									props.setAttributes( { packet1Price: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 1 price, second row"}
								value={props.attributes.packet1Price2}
								onChange={ function(value){
									props.setAttributes( { packet1Price2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 1 discount"}
								value={props.attributes.packet1PriceDiscountAmount}
								onChange={ function(value){
									props.setAttributes( { packet1PriceDiscountAmount: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 1 discount, second row"}
								value={props.attributes.packet1PriceDiscountAmount2}
								onChange={ function(value){
									props.setAttributes( { packet1PriceDiscountAmount2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 1 price with discount"}
								value={props.attributes.packet1PriceWithDiscount}
								onChange={ function(value){
									props.setAttributes( { packet1PriceWithDiscount: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 1 price with discount, second row"}
								value={props.attributes.packet1PriceWithDiscount2}
								onChange={ function(value){
									props.setAttributes( { packet1PriceWithDiscount2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 1 Commitment"}
								value={props.attributes.packet1Commitment}
								onChange={ function(value){
									props.setAttributes( { packet1Commitment: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 2 Product Name"}
								value={props.attributes.packet2ProductName}
								onChange={ function(value){
									props.setAttributes( { packet2ProductName: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 2 Product Ident"}
								value={props.attributes.packet2ProductIdent}
								onChange={ function(value){
									props.setAttributes( { packet2ProductIdent: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 2 content"}
								value={props.attributes.packet2Content}
								onChange={ function(value){
									props.setAttributes( { packet2Content: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 2 content,second row"}
								value={props.attributes.packet2Content2}
								onChange={ function(value){
									props.setAttributes( { packet2Content2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 2 content image"}
								value={props.attributes.packet2ContentImage}
								onChange={ function(value){
									props.setAttributes( { packet2ContentImage: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 2 price"}
								value={props.attributes.packet2Price}
								onChange={ function(value){
									props.setAttributes( { packet2Price: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 2 price, second row"}
								value={props.attributes.packet2Price2}
								onChange={ function(value){
									props.setAttributes( { packet2Price2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 2 discount"}
								value={props.attributes.packet2PriceDiscountAmount}
								onChange={ function(value){
									props.setAttributes( { packet2PriceDiscountAmount: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 2 discount, second row"}
								value={props.attributes.packet2PriceDiscountAmount2}
								onChange={ function(value){
									props.setAttributes( { packet2PriceDiscountAmount2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 2 price with discount"}
								value={props.attributes.packet2PriceWithDiscount}
								onChange={ function(value){
									props.setAttributes( { packet2PriceWithDiscount: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 2 price with discount, second row"}
								value={props.attributes.packet2PriceWithDiscount2}
								onChange={ function(value){
									props.setAttributes( { packet2PriceWithDiscount2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 2 Commitment"}
								value={props.attributes.packet2Commitment}
								onChange={ function(value){
									props.setAttributes( { packet2Commitment: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 3 Product Name"}
								value={props.attributes.packet3ProductName}
								onChange={ function(value){
									props.setAttributes( { packet3ProductName: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 3 Product Ident"}
								value={props.attributes.packet3ProductIdent}
								onChange={ function(value){
									props.setAttributes( { packet3ProductIdent: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 3 content"}
								value={props.attributes.packet3Content}
								onChange={ function(value){
									props.setAttributes( { packet3Content: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 3 content,second row"}
								value={props.attributes.packet3Content2}
								onChange={ function(value){
									props.setAttributes( { packet3Content2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 3 content image"}
								value={props.attributes.packet3ContentImage}
								onChange={ function(value){
									props.setAttributes( { packet3ContentImage: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 3 price"}
								value={props.attributes.packet3Price}
								onChange={ function(value){
									props.setAttributes( { packet3Price: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 3 price, second row"}
								value={props.attributes.packet3Price2}
								onChange={ function(value){
									props.setAttributes( { packet3Price2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 3 discount"}
								value={props.attributes.packet3PriceDiscountAmount}
								onChange={ function(value){
									props.setAttributes( { packet3PriceDiscountAmount: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 3 discount, second row"}
								value={props.attributes.packet3PriceDiscountAmount2}
								onChange={ function(value){
									props.setAttributes( { packet3PriceDiscountAmount2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 3 price with discount"}
								value={props.attributes.packet3PriceWithDiscount}
								onChange={ function(value){
									props.setAttributes( { packet3PriceWithDiscount: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 3 price with discount, second row"}
								value={props.attributes.packet3PriceWithDiscount2}
								onChange={ function(value){
									props.setAttributes( { packet3PriceWithDiscount2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Packet 3 Commitment"}
								value={props.attributes.packet3Commitment}
								onChange={ function(value){
									props.setAttributes( { packet3Commitment: value } );
								}}
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Current product settings">
							<PanelRow>
								<TextareaControl
									label={"Extra information, shown below current product info"}
									value={props.attributes.currentProductInfoText}
									onChange={ function(value){
										props.setAttributes( { currentProductInfoText: value } );
									}}
								/>
							</PanelRow>
							<PanelRow>
								<TextControl
									label={"Text shown on change product button"}
									value={props.attributes.changeproductbtntext}
									onChange={ function(value){
										props.setAttributes( { changeproductbtntext: value } );
									}}
								/>
							</PanelRow>
					</PanelBody>
					<PanelBody title="Step1 settings">
						<PanelRow>
							<TextareaControl
								label={"Text shown before product-selection"}
								value={props.attributes.step1BeforeText}
								onChange={ function(value){
									props.setAttributes( { step1BeforeText: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={"Text shown before product-selection, second row"}
								value={props.attributes.step1BeforeText2}
								onChange={ function(value){
									props.setAttributes( { step1BeforeText2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
						<CheckboxControl
								label="Show 'Read about tv' button"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showReadMoreAboutTvButton }
								onChange={ function(value){
									props.setAttributes( { showReadMoreAboutTvButton: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={"Read more about tv link"}
								value={props.attributes.readMoreAboutTvButtonLink}
								onChange={ function(value){
									props.setAttributes( { readMoreAboutTvButtonLink: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={"Text shown after product-selection"}
								value={props.attributes.step1AfterText}
								onChange={ function(value){
									props.setAttributes( { step1AfterText: value } );
								}}
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Step2 settings">
							<TextareaControl
								label={"Mandatory text, binding, other info, etc."}
								value={props.attributes.step2InfoText}
								onChange={ function(value){
									props.setAttributes( { step2InfoText: value } );
								}}
							/>
							<TextControl
								label={"Text before Settop box checkbox"}
								value={props.attributes.settopboxtext}
								onChange={ function(value){
									props.setAttributes( { settopboxtext: value } );
								}}
							/>
					</PanelBody>
					<PanelBody title="Receipt view setttings">
							<TextareaControl
								label={"Text before \"receipt\""}
								value={props.attributes.receiptInfoText}
								onChange={ function(value){
									props.setAttributes( { receiptInfoText: value } );
								}}
							/>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
					
				</BlockControls>

				<div className="container" style={{'font-size': props.attributes.fontSize}}>
				<div className="allente_tv_container">
					<h3>Current product</h3>
					<div className='allente-current-product-container' data-packages={JSON.stringify(props.attributes)}>
						<div id="allente-current-product-info-container" className='is-flex is-flex-direction-column is-align-items-center'>
							<div>Du har følgende tv-pakke</div>
							<div id="allente-current-product-name">{props.attributes.packet3ProductName}</div>
							<div id="allente-current-product-content">{props.attributes.packet3Content}</div>
							<div className={"columns is-centered"} id="allente-current-product-content-image"><img src={props.attributes.packet3ContentImage} className={"column is-half"}></img></div>
							<div id="allente-current-product-price">
								<div>Abonnement</div>
								<div id="allente-current-product-price-final">{props.attributes.packet3PriceWithDiscount}</div>
								<div id="allente-current-product-price-final2">{props.attributes.packet3PriceWithDiscount2}</div>
								<div id="allente-current-product-price-discount">{props.attributes.packet3PriceDiscountAmount}</div>
								<div id="allente-current-product-price-discount2">{props.attributes.packet3PriceDiscountAmount2}</div>
								<div id="allente-current-product-price-normal">{props.attributes.packet3Price}</div>
								<div id="allente-current-product-price-normal2">{props.attributes.packet3Price2}</div>
							</div>
							<div><button id="allente-current-product-change-product">{props.attributes.changeproductbtntext}</button></div>
						</div>
						<div className='is-flex is-flex-direction-column is-align-items-center' id="allente-current-product-infotext">{props.attributes.currentProductInfoText}</div>
					</div>
					<h3>Step 1</h3>
					<div className="allente_frame_step_1">
						<div className='step1-intro-text'>
							{props.attributes.step1BeforeText}
						</div>
						<div className='step1-intro-text2'>
							{props.attributes.step1BeforeText2}
						</div>
						<div className="columns allente_product_list">
							<div className="column is-3-widescreen is-4-tablet is-offset-1-desktop">
								<div className="card">
									<div className="product-name is-flex is-justify-content-center">{props.attributes.packet1ProductName}</div>
									<div className="product-text is-flex is-align-items-center is-flex-direction-column">
										<div className="block product-content">{props.attributes.packet1Content}</div>
										<div className="block product-content2">{props.attributes.packet1Content2}</div>
										<div className="block product-content-image"><img src={props.attributes.packet1ContentImage}></img></div>
										<div className="block" ><button id="package-1-view-content" style={props.attributes.showShowContentButtons ? {}:{'display': 'none'}}>Se indhold</button></div>
										<div className="block product-final-price">{props.attributes.packet1PriceWithDiscount}</div>
										<div className="block product-final-price2">{props.attributes.packet1PriceWithDiscount2}</div>
										<div className="block product-discount">{props.attributes.packet1PriceDiscountAmount}</div>
										<div className="block product-discount2">{props.attributes.packet1PriceDiscountAmount2}</div>
										<div className="block product-price">{props.attributes.packet1Price}</div>
										<div className="block product-price2">{props.attributes.packet1Price2}</div>
										<button className="product-order-button" data-tvpacket={'1'} data-content={props.attributes.packet1Content} data-contentimage={props.attributes.packet1ContentImage} data-tvpacketprice={props.attributes.packet1Price} data-tvpacketdiscount={props.attributes.packet1PriceDiscountAmount} data-tvpacketfinalprice={props.attributes.packet1PriceWithDiscount} data-tvpacketcommitment={props.attributes.packet1Commitment}>Bestil</button>
									</div>
								</div>
							</div>

							<div className="column is-3-widescreen is-4-tablet">
								<div className="card">
									<div className="product-name is-flex is-justify-content-center">{props.attributes.packet2ProductName}</div>
									<div className="product-text is-flex is-align-items-center is-flex-direction-column">
										<div className="block product-content">{props.attributes.packet2Content}</div>
										<div className="block product-content2">{props.attributes.packet2Content2}</div>
										<div className="block product-content-image"><img src={props.attributes.packet2ContentImage}></img></div>
										<div className="block" ><button id="package-2-view-content" style={props.attributes.showShowContentButtons ? {}:{'display': 'none'}}>Se indhold</button></div>
										<div className="block product-final-price">{props.attributes.packet2PriceWithDiscount}</div>
										<div className="block product-final-price2">{props.attributes.packet2PriceWithDiscount2}</div>
										<div className="block product-discount">{props.attributes.packet2PriceDiscountAmount}</div>
										<div className="block product-discount2">{props.attributes.packet2PriceDiscountAmount2}</div>
										<div className="block product-price">{props.attributes.packet2Price}</div>
										<div className="block product-price2">{props.attributes.packet2Price2}</div>
										<button className="product-order-button" data-tvpacket={'2'} data-content={props.attributes.packet2Content} data-contentimage={props.attributes.packet2ContentImage} data-tvpacketprice={props.attributes.packet2Price} data-tvpacketdiscount={props.attributes.packet2PriceDiscountAmount} data-tvpacketfinalprice={props.attributes.packet2PriceWithDiscount} data-tvpacketcommitment={props.attributes.packet2Commitment} >Bestil</button>
									</div>
								</div>
							</div>

							<div className="column is-3-widescreen is-4-tablet">
								<div className="card">
									<div className="product-name is-flex is-justify-content-center">{props.attributes.packet3ProductName}</div>
									<div className="product-text is-flex is-align-items-center is-flex-direction-column">
										<div className="block product-content">{props.attributes.packet3Content}</div>
										<div className="block product-content2">{props.attributes.packet3Content2}</div>
										<div className="block product-content-image"><img src={props.attributes.packet3ContentImage}></img></div>
										<div className="block" ><button id="package-3-view-content" style={props.attributes.showShowContentButtons ? {}:{'display': 'none'}}>Se indhold</button></div>
										<div className="block product-final-price">{props.attributes.packet3PriceWithDiscount}</div>
										<div className="block product-final-price2">{props.attributes.packet3PriceWithDiscount2}</div>
										<div className="block product-discount">{props.attributes.packet3PriceDiscountAmount}</div>
										<div className="block product-discount2">{props.attributes.packet3PriceDiscountAmount2}</div>
										<div className="block product-price">{props.attributes.packet3Price}</div>
										<div className="block product-price2">{props.attributes.packet3Price2}</div>
										<button className="product-order-button" data-tvpacket={'3'} data-content={props.attributes.packet3Content} data-contentimage={props.attributes.packet3ContentImage} data-tvpacketprice={props.attributes.packet3Price} data-tvpacketdiscount={props.attributes.packet3PriceDiscountAmount}  data-tvpacketcommitment={props.attributes.packet3Commitment} >Bestil</button>
									</div>
								</div>
							</div>
						</div>
						<div style={props.attributes.showReadMoreAboutTvButton ? {}:{'display': 'none'}}><a href={props.attributes.readMoreAboutTvButtonLink}><button id="read-more-tv-button">Læs mere om tv</button></a></div>
						<div className="step1-outro-text">
						{props.attributes.step1AfterText}
						</div>
					</div>
					<h3>Step 2</h3>
					<div class="allente_frame_step_2 is-flex is-justify-content-center is-flex-direction-column">
						<div className="title is-4">Bestillingsoverblik</div>
						<div className='columns is-mobile'>
							<div className='column is-offset-4 is-2-tablet is-half-mobile' id="allente-chosen-product">{props.attributes.packet3ProductName}</div><button className="column is-3-tablet is-2-widescreen is-half-mobile" id="allente-change-product">Vælg anden pakke</button>
						</div>

						<div className="block is-flex is-align-items-center is-flex-direction-column">
							<div id="step2-product-content">{props.attributes.packet3Content}</div>
							<div id="step2-product-content-image" className='columns is-centered'><img className='column is-half' src={props.attributes.packet3ContentImage}></img></div>
							<div id="step2-product-price-container" className="block is-flex is-align-items-center is-flex-direction-column">
								<div>Abonnement</div>
								<div><span id="step2-product-finalprice">{props.attributes.packet3PriceWithDiscount}</span></div>
								<div><span id="step2-product-finalprice2">{props.attributes.packet3PriceWithDiscount2}</span></div>
								<div><span id="step2-product-discount">{props.attributes.packet3PriceDiscountAmount}</span></div>
								<div><span id="step2-product-discount2">{props.attributes.packet3PriceDiscountAmount2}</span></div>
								<div><span id="step2-product-price">{props.attributes.packet3Price}</span></div>						
								<div><span id="step2-product-price2">{props.attributes.packet3Price2}</span></div>						
								<div><span id="step2-product-commitment">{props.attributes.packet3Commitment}</span></div>
							</div>
							<div id="step2-settopbox"><label for="settopbox">{props.attributes.settopboxtext}</label><input id="settopbox" type="checkbox"></input></div>
							<div id="step2-birthdate"><label for="birthdate">Fødselsdato</label><input id="birthdate" type="date"></input></div>
							<div id="step2-startdate"><label for="startdate">Vælg venligst dato</label><input id="startdate" type="date"></input></div>
							<div id="step2-terms">Acceptér <a id="step2-terms-link">vilkår og betingelser</a><input type="checkbox"></input></div>
						</div>
						
						<div className="columns is-centered">
							<button className="column is-full-mobile is-half-tablet" id="allente-buy-button" data-pack1ident={props.attributes.packet1ProductIdent} data-pack2ident={props.attributes.packet2ProductIdent} data-pack3ident={props.attributes.packet3ProductIdent} disabled>Køb</button>
						</div>
						<div className="block is-flex is-justify-content-center allente-step2-outro-text">{props.attributes.step2InfoText}</div>
					</div>
					<h3>Step 3</h3>
					<div class="allente_frame_step_3">
						<div className="title is-4">Tak for din bestilling</div>
						<div id="allente-receipt-additional-text">{props.attributes.receiptInfoText}</div>
						<div className='title is-5'>Kvittering:</div>
						<div className="block is-flex is-align-items-center is-flex-direction-column">
							<div id="allente-receipt-package">Premium</div>
							<div id="allente-receipt-content">{props.attributes.packet3Content}</div>
							<div id="allente-receipt-content-image" className='columns is-centered'><img className='column is-half' src={props.attributes.packet3ContentImage}></img></div>
							<div id="allente-receipt-startdate">Start dato: <span id="allente-receipt-startdate-date">01/01/2022</span></div>
							<div id="allente-receipt-price-container" className="block is-flex is-align-items-center is-flex-direction-column">
								<div>Abonnement</div>
								<div><span id="allente-receipt-final-price">{props.attributes.packet3PriceWithDiscount}</span></div>
								<div><span id="allente-receipt-final-price2">{props.attributes.packet3PriceWithDiscount2}</span></div>
								<div><span id="allente-receipt-discount">{props.attributes.packet3PriceDiscountAmount}</span></div>
								<div><span id="allente-receipt-discount2">{props.attributes.packet3PriceDiscountAmount2}</span></div>
								<div><span id="allente-receipt-normal-price">{props.attributes.packet3Price}</span></div>						
								<div><span id="allente-receipt-normal-price2">{props.attributes.packet3Price2}</span></div>						
								<div><span id="allente-receipt-commitment">{props.attributes.packet3Commitment}</span></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div className="container" style={{'font-size': props.attributes.fontSize}}>
				<div className="allente_tv_container">
					<div className='allente-current-product-container is-hidden' data-packages={JSON.stringify(props.attributes)}>
						<div id="allente-current-product-info-container" className='is-flex is-flex-direction-column is-align-items-center'>
							<div>Du har følgende tv-pakke</div>
							<div id="allente-current-product-name"></div>
							<div id="allente-current-product-content"></div>
							<div className={"columns is-centered"} id="allente-current-product-content-image"><img className={"column is-half"}></img></div>
							<div id="allente-current-product-price">
								<div>Abonnement</div>
								<div id="allente-current-product-price-final"></div>
								<div id="allente-current-product-price-final2"></div>
								<div id="allente-current-product-price-discount"></div>
								<div id="allente-current-product-price-discount2"></div>
								<div id="allente-current-product-price-normal"></div>
								<div id="allente-current-product-price-normal2"></div>
							</div>
							<div><button id="allente-current-product-change-product">{props.attributes.changeproductbtntext}</button></div>
						</div>
						<div className='is-flex is-flex-direction-column is-align-items-center' id="allente-current-product-infotext">{props.attributes.currentProductInfoText}</div>
					</div>
					<div className="allente_frame_step_1 is-hidden">
						<div className='step1-intro-text'>
							{props.attributes.step1BeforeText}
						</div>
						<div className='step1-intro-text2'>
							{props.attributes.step1BeforeText2}
						</div>
						<div className="columns allente_product_list">
							<div className="column is-3-widescreen is-4-tablet is-offset-1-desktop">
								<div className="card">
									<div className="product-name is-flex is-justify-content-center">{props.attributes.packet1ProductName}</div>
									<div className="product-text is-flex is-align-items-center is-flex-direction-column">
										<div className="block product-content">{props.attributes.packet1Content}</div>
										<div className="block product-content2">{props.attributes.packet1Content2}</div>
										<div className="block product-content-image"><img src={props.attributes.packet1ContentImage}></img></div>
										<div className="block" ><button id="package-1-view-content" style={props.attributes.showShowContentButtons ? {}:{'display': 'none'}}>Se indhold</button></div>
										<div className="block product-final-price">{props.attributes.packet1PriceWithDiscount}</div>
										<div className="block product-final-price2">{props.attributes.packet1PriceWithDiscount2}</div>
										<div className="block product-discount">{props.attributes.packet1PriceDiscountAmount}</div>
										<div className="block product-discount2">{props.attributes.packet1PriceDiscountAmount2}</div>
										<div className="block product-price">{props.attributes.packet1Price}</div>
										<div className="block product-price2">{props.attributes.packet1Price2}</div>
										<button className="product-order-button" data-tvpacket={'1'}  data-name={props.attributes.packet1ProductName} data-content={props.attributes.packet1Content} data-content2={props.attributes.packet1Content2} data-contentimage={props.attributes.packet1ContentImage} data-tvpacketprice={props.attributes.packet1Price} data-tvpacketprice2={props.attributes.packet1Price2} data-tvpacketdiscount={props.attributes.packet1PriceDiscountAmount} data-tvpacketdiscount2={props.attributes.packet1PriceDiscountAmount2} data-tvpacketfinalprice={props.attributes.packet1PriceWithDiscount} data-tvpacketfinalprice2={props.attributes.packet1PriceWithDiscount2} data-tvpacketcommitment={props.attributes.packet1Commitment}>Bestil</button>
									</div>
								</div>
							</div>

							<div className="column is-3-widescreen is-4-tablet">
								<div className="card">
									<div className="product-name is-flex is-justify-content-center">{props.attributes.packet2ProductName}</div>
									<div className="product-text is-flex is-align-items-center is-flex-direction-column">
										<div className="block product-content">{props.attributes.packet2Content}</div>
										<div className="block product-content2">{props.attributes.packet2Content2}</div>
										<div className="block product-content-image"><img src={props.attributes.packet2ContentImage}></img></div>
										<div className="block" ><button id="package-2-view-content" style={props.attributes.showShowContentButtons ? {}:{'display': 'none'}}>Se indhold</button></div>
										<div className="block product-final-price">{props.attributes.packet2PriceWithDiscount}</div>
										<div className="block product-final-price2">{props.attributes.packet2PriceWithDiscount2}</div>
										<div className="block product-discount">{props.attributes.packet2PriceDiscountAmount}</div>
										<div className="block product-discount2">{props.attributes.packet2PriceDiscountAmount2}</div>
										<div className="block product-price">{props.attributes.packet2Price}</div>
										<div className="block product-price2">{props.attributes.packet2Price2}</div>
										<button className="product-order-button" data-tvpacket={'2'} data-name={props.attributes.packet2ProductName} data-content={props.attributes.packet2Content} data-content2={props.attributes.packet2Content2} data-contentimage={props.attributes.packet2ContentImage} data-tvpacketprice={props.attributes.packet2Price} data-tvpacketprice2={props.attributes.packet2Price2} data-tvpacketdiscount={props.attributes.packet2PriceDiscountAmount} data-tvpacketdiscount2={props.attributes.packet2PriceDiscountAmount2} data-tvpacketfinalprice={props.attributes.packet2PriceWithDiscount} data-tvpacketfinalprice2={props.attributes.packet2PriceWithDiscount2} data-tvpacketcommitment={props.attributes.packet2Commitment} >Bestil</button>
									</div>
								</div>
							</div>

							<div className="column is-3-widescreen is-4-tablet">
								<div className="card">
									<div className="product-name is-flex is-justify-content-center">{props.attributes.packet3ProductName}</div>
									<div className="product-text is-flex is-align-items-center is-flex-direction-column">
										<div className="block product-content">{props.attributes.packet3Content}</div>
										<div className="block product-content2">{props.attributes.packet3Content2}</div>
										<div className="block product-content-image"><img src={props.attributes.packet3ContentImage}></img></div>
										<div className="block" ><button id="package-3-view-content" style={props.attributes.showShowContentButtons ? {}:{'display': 'none'}}>Se indhold</button></div>
										<div className="block product-final-price">{props.attributes.packet3PriceWithDiscount}</div>
										<div className="block product-final-price2">{props.attributes.packet3PriceWithDiscount2}</div>
										<div className="block product-discount">{props.attributes.packet3PriceDiscountAmount}</div>
										<div className="block product-discount2">{props.attributes.packet3PriceDiscountAmount2}</div>
										<div className="block product-price">{props.attributes.packet3Price}</div>
										<div className="block product-price2">{props.attributes.packet3Price2}</div>
										<button className="product-order-button" data-tvpacket={'3'} data-name={props.attributes.packet3ProductName} data-content={props.attributes.packet3Content} data-content2={props.attributes.packet3Content2} data-contentimage={props.attributes.packet3ContentImage} data-tvpacketprice={props.attributes.packet3Price} data-tvpacketprice2={props.attributes.packet3Price2} data-tvpacketdiscount={props.attributes.packet3PriceDiscountAmount} data-tvpacketdiscount2={props.attributes.packet3PriceDiscountAmount2} data-tvpacketfinalprice={props.attributes.packet3PriceWithDiscount} data-tvpacketfinalprice2={props.attributes.packet3PriceWithDiscount2} data-tvpacketcommitment={props.attributes.packet3Commitment} >Bestil</button>
									</div>
								</div>
							</div>
						</div>
						<div style={props.attributes.showReadMoreAboutTvButton ? {}:{'display': 'none'}}><a href={props.attributes.readMoreAboutTvButtonLink}><button id="read-more-tv-button">Læs mere om tv</button></a></div>
						<div className="step1-outro-text">
						{props.attributes.step1AfterText}
						</div>
					</div>

					<div class="allente_frame_step_2 is-hidden is-flex is-justify-content-center is-flex-direction-column">
						<div className="title is-4">Bestillingsoverblik</div>
						<div className='columns is-centered'>
							<div className='column is-3-tablet is-half-mobile is-offset-one-quarter-mobile' id="allente-chosen-product"></div><button className="column is-3-tablet is-2-widescreen is-half-mobile is-offset-one-quarter-mobile" id="allente-change-product">Vælg anden pakke</button>
						</div>

						<div className="block is-flex is-align-items-center is-flex-direction-column">
							<div id="step2-product-content"></div>
							<div id="step2-product-content2"></div>
							<div id="step2-product-content-image" className='columns is-centered'><img className='column is-half'></img></div>
							<div id="step2-product-price-container" className="block is-flex is-align-items-center is-flex-direction-column">
								<div>Abonnement</div>
								<div><span id="step2-product-finalprice"></span></div>
								<div><span id="step2-product-finalprice2"></span></div>
								<div><span id="step2-product-discount"></span></div>
								<div><span id="step2-product-discount2"></span></div>
								<div><span id="step2-product-price"></span></div>						
								<div><span id="step2-product-price2"></span></div>						
								<div><span id="step2-product-commitment"></span></div>
							</div>
							<div id="step2-settopbox"><label for="settopbox">{props.attributes.settopboxtext}</label><input id="settopbox" type="checkbox"></input></div>
							<div id="step2-birthdate"><label for="birthdate">Fødselsdato</label><input id="birthdate" type="date"></input></div>
							<div id="step2-startdate"><label for="startdate">Vælg venligst dato</label><input id="startdate" type="date"></input></div>
							<div id="step2-terms">Acceptér <a id="step2-terms-link">vilkår og betingelser</a><input type="checkbox"></input></div>
						</div>
						
						<div className="columns is-centered">
							<button className="column is-full-mobile is-half-tablet" id="allente-buy-button" data-pack1ident={props.attributes.packet1ProductIdent} data-pack2ident={props.attributes.packet2ProductIdent} data-pack3ident={props.attributes.packet3ProductIdent} disabled>Køb</button>
						</div>
						<div className="columns is-centered">
							<div id="buy-button-alert" class="notification column is-half-tablet is-full-mobile is-hidden is-flex is-align-items-center is-flex-direction-column">
								<p>Tjek om du har valgt fødselsdato, samt valgt en start dato</p>
							</div>
						</div>
						<div className="block is-flex is-justify-content-center allente-step2-outro-text">{props.attributes.step2InfoText}</div>




					</div>
					<div class="allente_frame_step_3 is-hidden">
						<div className="title is-4">Tak for din bestilling</div>
						<div id="allente-receipt-additional-text">{props.attributes.receiptInfoText}</div>
						<div className='title is-5'>Kvittering:</div>
						<div className="block is-flex is-align-items-center is-flex-direction-column">
							<div id="allente-receipt-package"></div>
							<div id="allente-receipt-content"></div>
							<div id="allente-receipt-content-image" className='columns is-centered'><img className='column is-half'></img></div>
							<div id="allente-receipt-price-container" className="block is-flex is-align-items-center is-flex-direction-column">
								<div>Abonnement</div>
								<div><span id="allente-receipt-final-price"></span></div>
								<div><span id="allente-receipt-final-price2"></span></div>
								<div><span id="allente-receipt-discount"></span></div>
								<div><span id="allente-receipt-discount2"></span></div>
								<div><span id="allente-receipt-normal-price"></span></div>						
								<div><span id="allente-receipt-normal-price2"></span></div>						
								<div><span id="allente-receipt-commitment"></span></div>
							</div>
							<div id="allente-receipt-startdate">Start dato: <span id="allente-receipt-startdate-date"></span></div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});