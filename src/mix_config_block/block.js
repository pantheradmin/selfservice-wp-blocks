/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    InnerBlocks,
	useBlockProps,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
	CheckboxControl,
	FontSizePicker,
    SelectControl,
    Dashicon,
    Tooltip,
    TextControl
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-mix-config-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Mix-config-block' ), // Block title.
	icon: 'groups', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		ChannelLockDays: {type: 'int', default: 31},
		usageLabel: {type: 'string', default: '{antal} af {max} kanaler valgt'},
        valueLabel: {type: 'string', default: '{antal} point'},
		fontSize: {type: 'int', default: 16}
		
	},

	edit: (props) => {
		
		return (

			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						<PanelRow>
							<TextControl 
								label="Binding i dage for kanaler"
								value={props.attributes.ChannelLockDays}
								onChange={ function(value){
									props.setAttributes( { ChannelLockDays: value } );
								}}
							/>

						</PanelRow>
						<PanelRow>
							<TextControl 
								label="Aktive kanaler valgt / point brugt"
								help="{antal} og {max} elementer udskiftes automatisk med de relevante tal."
								value={props.attributes.usageLabel}
								onChange={ function(value){
									props.setAttributes( { usageLabel: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Kanalværdi"
								help="{antal} udskiftes automatisk med den relevante værdi."
								value={props.attributes.valueLabel}
								onChange={ function(value){
									props.setAttributes( { valueLabel: value } );
								}}
							/>
                        </PanelRow>

					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
					
				</BlockControls>


				<div class="container columns is-centered" style={{'font-size': props.attributes.fontSize}}>
						<div class="column mix-container">
							Mix-pakke konfiguration.
						</div>
				</div>
			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div className={"container columns is-centered is-mobile"} style={{'font-size': props.attributes.fontSize}}>
				<div className={"column mix-container"} data-attributes={JSON.stringify(props.attributes)}>
							
				</div>
			</div>
			
		);
	}
});