/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component, useState } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
    SelectControl,
	CheckboxControl,
	FontSizePicker,
    Dashicon,
    Tooltip,
    TextControl,
	ToolsPanel,
	ToolsPanelItem
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

var startString = '';
var stopString = '';



/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-signup-page-change', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Signup Page Change Block' ), // Block title.
	icon: 'calendar-alt', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-signup-page-change — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
        fromDiv: {type: 'string', default: ''},
        currentDiv: {type: 'string', default: ''},
        toDiv: {type: 'string', default: ''},
        backButtonLabel: {type: 'string', default: ''},
        nextButtonLabel: {type: 'string', default: ''},
		thisPageBreakSubmits: {type: 'boolean', default: false},
		submitType: {type: 'string', default: ''},
		whitelistAddress: {type: 'string', default: ''},
		acceptUrl: {type: 'string', default: ''},
		cancelUrl: {type: 'string', default: ''},
		validateFunction: {type: 'string', default: ''}
	},

	edit: (props) => {
		
		return (
			<div classname={ props.className }>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
                    {/********************************/}
                    
                    <PanelBody title="Side skift parametre">
                        <PanelRow>
							<TextControl 
								label="Forrige sides DIV"
								help="Id på forrige sides DIV hvis ikke dette er første side"
								value={props.attributes.fromDiv}
								onChange={ function(value){
									props.setAttributes( { fromDiv: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Denne sides DIV"
								help="Id på denne sides DIV"
								value={props.attributes.currentDiv}
								onChange={ function(value){
									props.setAttributes( { currentDiv: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Næste sides DIV"
								help="Id på næste sides DIV hvis ikke dette er sidste side"
								value={props.attributes.toDiv}
								onChange={ function(value){
									props.setAttributes( { toDiv: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Tekst på tilbage knap"
								help="Tekst på tilbage knap hvis ikke dette er første side"
								value={props.attributes.backButtonLabel}
								onChange={ function(value){
									props.setAttributes( { backButtonLabel: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Tekst på fortsæt knap"
								help="Tekst på næste knap hvis ikke dette er sidste side"
								value={props.attributes.nextButtonLabel}
								onChange={ function(value){
									props.setAttributes( { nextButtonLabel: value } );
								}}
							/>
                        </PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Afsend bestilling"
								help="Dette sideskift afsender bestillingen og skifter til bekræftelsen."
								checked={ props.attributes.thisPageBreakSubmits }
								onChange={ function(value){
									props.setAttributes( { thisPageBreakSubmits: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<SelectControl
									value={props.attributes.submitType}
									className="pa-calendar-unit"
									options={ [
										{ label: 'Ingen registrering', value: '' },
										{ label: 'Betalingskort', value: 'creditcard' },
										{ label: 'MobilePay', value: 'mobilepay-subscriptions' },
									] }
									onChange={ function(value){
										props.setAttributes( { submitType: value } );
									}}
								/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="API url hvis bestilling skal sendes andetsteds."
								help=""
								value={props.attributes.whitelistAddress}
								onChange={ function(value){
									props.setAttributes( { whitelistAddress: value } );
								}}
							/>
                        </PanelRow>
						<PanelRow>
							<TextControl 
								label="URL til ordrebekræftelse."
								help=""
								value={props.attributes.acceptUrl}
								onChange={ function(value){
									props.setAttributes( { acceptUrl: value } );
								}}
							/>
                        </PanelRow>
						<PanelRow>
							<TextControl 
								label="URL til afbrudt betaling."
								help=""
								value={props.attributes.cancelUrl}
								onChange={ function(value){
									props.setAttributes( { cancelUrl: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Valideringsfunktion"
								help="Javascript funktion som validerer og returnerer boolsk værdi."
								value={props.attributes.validateFunction}
								onChange={ function(value){
									props.setAttributes( { validateFunction: value } );
								}}
							/>
                        </PanelRow>
                    </PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
				
				</BlockControls>

				{/* here starts the edit block itself*/}
				<span class="content center-column">
					{props.attributes.backButtonLabel > "" ?
						<button>{props.attributes.backButtonLabel}</button> : ''}
					{props.attributes.nextButtonLabel > "" ?
						<button>{props.attributes.nextButtonLabel}</button> : ''}
			    </span>
            </div>
		);	
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
				<span id={"pa-signup-page-break-" + props.attributes.fromDiv + props.attributes.toDiv} data-attributes={JSON.stringify(props.attributes)} className="signup-page-break">

				{props.attributes.backButtonLabel && <button id={'changepage'+props.attributes.currentDiv+props.attributes.fromDiv}>{props.attributes.backButtonLabel}</button>}
				{props.attributes.nextButtonLabel && <button id={'changepage'+props.attributes.currentDiv+props.attributes.toDiv}>{props.attributes.nextButtonLabel}</button>}
				
				</span>
			
		);	
 }

});
