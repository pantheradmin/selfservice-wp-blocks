/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component, useState } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
    SelectControl,
	CheckboxControl,
	FontSizePicker,
    Dashicon,
    Tooltip,
    TextControl,
	ToolsPanel,
	ToolsPanelItem
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-internet-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Internet Info' ), // Block title.
	icon: 'businessperson', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-internet-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		provisioningModels: {type: 'string', default: ""},

		factorySettingsKey: {type: 'string', default: "wifidefaults"},
		defaultValue: {type: 'string', default: "default"},
		customValue: {type: 'string', default: "custom"},
		factoryDefaultMessage: {type: 'string', default: "Dit internet er i øjeblikket sat til fabriksindstillinger. Det betyder at du kan finde netværksnavn (SSID) og kodeord (Preeshared key) på bunden af dit modem."},

		showProductNameRow: {type: 'boolean', default: true},
		showProductNameLabel: {type: 'string', default: "Produkt"},

		showModemMac: {type: 'boolean', default: true},
		showModemMacLabel: {type: 'string', default: "Modem MAC-adresse"},

		showModemIp: {type: 'boolean', default: true},
		showModemIpLabel: {type: 'string', default: "Modem IP-adresse"},

		showModemType: {type: 'boolean', default: true},
		showModemTypeLabel: {type: 'string', default: "Modem Model"},
		
		showSSID: {type: 'boolean', default: false},
		SSIDKey: {type: 'string', default: "ssid"},
		SSIDLabel: {type: 'string', default: "WiFi brugernavn"},

		showSSID5: {type: 'boolean', default: false},
		SSID5Key: {type: 'string', default: "ssid5"},
		SSID5Label: {type: 'string', default: "WiFi brugernavn 5 Ghz."},

		showWpaPsk: {type: 'boolean', default: false},
		wpaPskKey: {type: 'string', default: "wpa-psk"},
		wpaPskLabel: {type: 'string', default: "WiFi adgangskode"},

		showModemSettings: {type: 'boolean', default: false},
        showAdditionalHTML: {type: 'string', default: ""},

		fontSize: {type: 'int', default: 16}
	},

	edit: (props) => {
		
		return (
			<div classname={ props.className }>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
					<PanelRow>
							<TextControl
								label={__(
									"Internet provisioneringsmodeller"
								)}
								value={props.attributes.provisioningModels}
								onChange={ function(value){
									props.setAttributes( { provisioningModels: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label="WiFi settings key"
								value={props.attributes.factorySettingsKey}
								onChange={ function(value){
											props.setAttributes( { factorySettingsKey: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label="WiFi settings factory defaults value"
								value={props.attributes.defaultValue}
								onChange={ function(value){
											props.setAttributes( { defaultValue: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label="WiFi settings custom value"
								value={props.attributes.customValue}
								onChange={ function(value){
											props.setAttributes( { customValue: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"WiFi settings default message"
								)}
								value={props.attributes.factoryDefaultMessage}
								onChange={ function(value){
									props.setAttributes( { factoryDefaultMessage: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Produktnavn"
								checked={props.attributes.showProductNameRow}
								help="Check the box if you want to show the field"
								onChange={ function(value){
											props.setAttributes( { showProductNameRow: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.showProductNameLabel}
								onChange={ function(value){
									props.setAttributes( { showProductNameLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Modem MAC adresse"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showModemMac }
								onChange={ function(value){
									props.setAttributes( { showModemMac: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.showModemMacLabel}
								onChange={ function(value){
									props.setAttributes( { showModemMacLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Modem IP adresse"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showModemIp }
								onChange={ function(value){
									props.setAttributes( { showModemIp: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.showModemIpLabel}
								onChange={ function(value){
									props.setAttributes( { showModemIpLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Modem type"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showModemType }
								onChange={ function(value){
									props.setAttributes( { showModemType: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.showModemTypeLabel}
								onChange={ function(value){
									props.setAttributes( { showModemTypeLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="SSID"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showSSID }
								onChange={ function(value){
									props.setAttributes( { showSSID: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Key for field value"
								)}
								value={props.attributes.SSIDKey}
								onChange={ function(value){
									props.setAttributes( { SSIDKey: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.SSIDLabel}
								onChange={ function(value){
									props.setAttributes( { SSIDLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="SSID 5 Ghz"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showSSID5 }
								onChange={ function(value){
									props.setAttributes( { showSSID5: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Key for field value"
								)}
								value={props.attributes.SSID5Key}
								onChange={ function(value){
									props.setAttributes( { SSID5Key: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.SSID5Label}
								onChange={ function(value){
									props.setAttributes( { SSID5Label: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="wpa-psk"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showWpaPsk }
								onChange={ function(value){
									props.setAttributes( { showWpaPsk: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Key for field value"
								)}
								value={props.attributes.wpaPskKey}
								onChange={ function(value){
									props.setAttributes( { wpaPskKey: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.wpaPskLabel}
								onChange={ function(value){
									props.setAttributes( { wpaPskLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Show modem settings"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showModemSettings }
								onChange={ function(value){
									props.setAttributes( { showModemSettings: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"HTML for field to the right of Modem settings button."
								)}
								value={props.attributes.showAdditionalHTML}
								onChange={ function(value){
									props.setAttributes( { showAdditionalHTML: value } );
								}}
							/>
						</PanelRow>

					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
				
				</BlockControls>

				{/* here starts the edit block itself*/}
				<div id="internet-info-container" class="content center-column" style={{'font-size': props.attributes.fontSize}}>
						<div class="container">
							<div class="columns is-multiline is-mobile">
								{/** Product name row*/}
								{ props.attributes.showProductNameRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="internet_productname">{props.attributes.showProductNameLabel}</div>
										</div> : ''
								}
								{ props.attributes.showProductNameRow ?
										<div class="column is-8">
											<div id="internet_productname" class="is-family-secondary right ">Internet 300/300 Mbit</div>
										</div> : ''
								}
								
								{/** Modem mac row*/ }
								{ props.attributes.showModemMac ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="internet_mac">{props.attributes.showModemMacLabel}</div>
										</div> : ''
								}
								{ props.attributes.showModemMac ?
										<div class="column is-8">
											<div id="internet_mac" class="is-family-secondary right ">47c7.d4c0.8dc8</div>
										</div> : ''
								}
								
								{/** Modem ip row*/}
								{ props.attributes.showModemIp ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_ip">{props.attributes.showModemIpLabel}</div>
										</div> : ''
								}
								{ props.attributes.showModemIp ?
										<div class="column is-8">
											<div id="internet_ip" class="is-family-secondary right ">10.10.10.10</div>
										</div> : ''
								}

								{/** Modem type row */}
								{ props.attributes.showModemType ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="internet_modem_type">{props.attributes.showModemTypeLabel}</div>
										</div> : ''
								}
								{ props.attributes.showModemType ?
										<div class="column is-8">
											<div id="internet_modem_type" class="is-family-secondary right ">cga4233</div>
										</div> : ''
								}		

								{/** SSID row */}
								{ props.attributes.showSSID ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_ssid">{props.attributes.SSIDLabel}</div>
										</div> : ''
								}
								{ props.attributes.showSSID ?
										<div class="column is-8">
											<div id="internet_ssid" class="is-family-secondary right ">SSID_Brugernavn</div>
										</div> : ''
								}

								{/** SSID5 row */}
								{ props.attributes.showSSID5 ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_ssid">{props.attributes.SSID5Label}</div>
										</div> : ''
								}
								{ props.attributes.showSSID5 ?
										<div class="column is-8">
											<div id="internet_ssid" class="is-family-secondary right ">SSID5_Brugernavn</div>
										</div> : ''
								}

								{/** wpa-psk row */}
								{ props.attributes.showWpaPsk ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_wpa_psk">{props.attributes.wpaPskLabel}</div>
										</div> : ''
								}
								{ props.attributes.showWpaPsk ?
										<div class="column is-8">
											<div id="internet_wpa_psk" class="is-family-secondary right ">hemm3lig.Kode</div>
										</div> : ''
								}
								{ props.attributes.showModemSettings ?
										<div class="column is-8">
											<button id="internet_modem_settings">Rediger indstillinger</button>
										</div> : ''
								}

								
							</div>
						</div>	
				</div>		
			</div>
			
		);	
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div id="internet-info-container" className={ props.className } data-attributes={JSON.stringify(props.attributes)} style={{'font-size': props.attributes.fontSize}}>
				<div id="iicontent" class="content center-column">
					
				</div>
			</div>
			
		);	
 }

});
