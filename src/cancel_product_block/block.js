/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    InnerBlocks,
	useBlockProps,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
	CheckboxControl,
	FontSizePicker,
    SelectControl,
    Dashicon,
    Tooltip,
    TextControl
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-cancel-product-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Cancel-product-block' ), // Block title.
	icon: 'groups', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		title: {type: "string", default: "Opsigelse"},
		productConfIds: {type: "string", default: ""},
		pretext: {type: "string", default: ""},
		receiptInfoText: {type: "string", default: ""},
		confirmCancelButtonText: {type: "string", default: "Bekræft opsigelse"},
		fontSize: {type: 'int', default: 16}

	},

	edit: (props) => {

		return (

			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						<PanelRow>
							<TextControl
								label="Title"
								value={props.attributes.title}
								help="Title on the block"
								onChange={ function(value){
											props.setAttributes( { title: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label="Product configuration ids"
								value={props.attributes.productConfIds}
								help="The product configuration ids that can be closed by this block"
								onChange={ function(value){
											props.setAttributes( { productConfIds: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
							label="Information text"
							value={props.attributes.pretext}
							help="Text shown before the cancel form,"
							onChange={function(value){
								props.setAttributes( { pretext: value} )
							}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
							label="Receipt information text"
							value={props.attributes.receiptInfoText}
							help="Text shown on the receipt page,"
							onChange={function(value){
								props.setAttributes( { receiptInfoText: value} )
							}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
							label="Confirm cancel button text"
							value={props.attributes.confirmCancelButtonText}
							onChange={function(value){
								props.setAttributes( { confirmCancelButtonText: value} )
							}}
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>

				</BlockControls>

				<div className={"container "+props.className} style={{'font-size': props.attributes.fontSize}}>
					<div className="cancel-product-container" data-ids={props.attributes.productConfIds}>
						<div class="card">
							<div className="is-flex is-align-items-center is-flex-direction-column">
								<div className="title is-4">{props.attributes.title}</div>
								<div className="columns is-centered">
									 <div class="column is-half" id="cancel-pretext">{props.attributes.pretext}</div>
								</div>
								<div className="block cancel-product-name">Produkt navn</div>
								<div>Slut dato:</div>
								<div style={{border:"2px dotted #000", 'min-height': "50px", "min-width": "50px"}}>
										<InnerBlocks/>
								</div>
								<button className='cancel-confirm-button' disabled >Bekræft opsigelse</button>
							</div>
						</div>
					</div>
				</div>

			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div className={"container "+props.className} style={{'font-size': props.attributes.fontSize}}>
					<div className="cancel-product-container" data-pcids={props.attributes.productConfIds}>
						<div class="card">
							<div className="is-flex is-align-items-center is-flex-direction-column">
								<div className="title is-4">{props.attributes.title}</div>
								<div className="columns is-centered">
									 <div class="column is-half" id="cancel-pretext">{props.attributes.pretext}</div>
								</div>
								<div>
									<div>Produkt:</div>
									<div className="block cancel-product-name"></div>
								</div>
								<div>Slut dato:</div>
								<div>
										<InnerBlocks.Content/>
								</div>
								<button className='cancel-confirm-button' disabled >Bekræft opsigelse</button>
							</div>
						</div>
					</div>
					<div className="cancel-product-receipt-container is-hidden">
						<div class="card">
							<div className="is-flex is-align-items-center is-flex-direction-column">
								<div className='title is-4'>Vi har nu modtaget din opsigelse.</div>
								<div className="columns is-centered">
									<div className="column is-half receipt-info-text">{props.attributes.receiptInfoText}</div>
								</div>
								<div>
									<div>Produkt:</div>
									<div className="receipt-product-name"></div>
								</div>
								<div>Slut dato: <span className="receipt-product-enddate"></span></div>
							</div>
						</div>
					</div>
				</div>



		);
	}
});