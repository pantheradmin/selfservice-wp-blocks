/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    InnerBlocks,
	useBlockProps,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
	CheckboxControl,
	FontSizePicker,
    SelectControl,
    Dashicon,
    Tooltip,
    TextControl
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-basket-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'basket-block' ), // Block title.
	icon: 'groups', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
    attributes: {
        fontSize: { type: 'int', default: 16 },
        productnamelabel: { type: 'string', default: "Produkt:" },
        productpricelabel: { type: 'string', default: "Pris:" },
        productstartdatelabel: { type: 'string', default: "På datoen:" },
        productquantitylabel: { type: 'string', default: "Antal:" },
        orderbutton: { type: 'string', default: "Bestil" },
        confirmationUrl: {type:"string", default:""}
	},

	edit: (props) => {
		
		return (

			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						<PanelRow>
							<TextControl
								label="Confirmation URL after submit basket"
								checked={props.attributes.confirmationUrl}
								help="The URL to redirect to after a successfull order submit"
								onChange={ function(value){
											props.setAttributes( { confirmationUrl: value } );
										}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl
								label="Confirmation URL after submit basket"
								checked={props.attributes.productnamelabel}
								help="The URL to redirect to after a successfull order submit"
								onChange={ function(value){
											props.setAttributes( { productnamelabel: value } );
										}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl
								label="Confirmation URL after submit basket"
								checked={props.attributes.productstartdatelabel}
								help="The URL to redirect to after a successfull order submit"
								onChange={ function(value){
											props.setAttributes( { productstartdatelabel: value } );
										}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl
								label="Confirmation URL after submit basket"
								checked={props.attributes.productpricelabel}
								help="The URL to redirect to after a successfull order submit"
								onChange={ function(value){
											props.setAttributes( { productpricelabel: value } );
										}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl
								label="Confirmation URL after submit basket"
								checked={props.attributes.productquantitylabel}
								help="The URL to redirect to after a successfull order submit"
								onChange={ function(value){
											props.setAttributes( { productquantitylabel: value } );
										}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl
								label="Confirmation URL after submit basket"
								checked={props.attributes.orderbutton}
								help="The URL to redirect to after a successfull order submit"
								onChange={ function(value){
											props.setAttributes( { orderbutton: value } );
										}}
							/>
                        </PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
					
				</BlockControls>

                <div class="container columns is-centered" style={{ 'font-size': props.attributes.fontSize }}>
                    <div className="column basket-container" >
                    <div class="basket-item-container">
                        <div class="basket-item-name">{props.attributes.productnamelabel}</div>
                        <div class="basket-item-normalprice">{props.attributes.productpricelabel}</div>
                        <div class="basket-item-startdate">{props.attributes.productstartdatelabel}</div>
                        <div class="basket-item-quantity">{props.attributes.productquantitylabel}</div>
                        <div class="basket-item-name"></div>
                        <div></div>
                    </div>
                    <div class="basket-item-container">
                        <div class="basket-item-name">Produkt 1</div>
                        <div class="basket-item-normalprice">250</div>
                        <div class="basket-item-startdate">01/01/2023</div>
                        <div class="basket-item-quantity">1</div>
                        <div class="basket-item-name"><i class="fa fa-trash"></i></div>
                        <div></div>
                        </div>
				    </div>
                    <div><button class="basket-order-button">{props.attributes.orderbutton}</button></div>
                </div>
                </div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
            <div className={"container columns is-centered is-mobile " + props.className} style={{ 'font-size': props.attributes.fontSize }}>
                <div className="column basket-container" >
                    <div class="basket-item-container">
                        <div class="basket-item-name">{props.attributes.productnamelabel}</div>
                        <div class="basket-item-normalprice">{props.attributes.productpricelabel}</div>
                        <div class="basket-item-startdate">{props.attributes.productstartdatelabel}</div>
                        <div class="basket-item-quantity">{props.attributes.productquantitylabel}</div>
                        <div></div>
                    </div>
                </div>
                <div class="order-button-container"><button style="display:none" onClick={ "submitBasket('"+props.attributes.confirmationUrl+"')"} class="basket-order-button">{props.attributes.orderbutton}</button></div>
			</div>
			
		);
	}
});