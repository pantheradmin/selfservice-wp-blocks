/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    PanelBody,
	PanelRow,
    CheckboxControl,
    TextControl,
	FontSizePicker
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-block4', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Customers & Products' ), // Block title.
	icon: 'groups', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		productRowLabel: {type: 'string', default: 'Produkt'},
		amountRowLabel: {type: 'string', default: 'Antal'},
		priceRowLabel: {type: 'string', default: 'Pris'},
		periodRowLabel: {type: 'string', default: 'Periode'},
		nextPaymentRowLabel: {type: 'string', default: 'Næste betaling'},
		hideUnitOnRow: {type: "boolean", default: false},
		productBlacklist: {type: "string"},
		hideDecimals: {type:  "boolean", default: false},
		loadMoreButtonLabel: {type: 'string', default: 'Se alle produkter'},
		loadLessButtonLabel: {type: 'string', default: 'Se færre produkter'},
		showLoadMoreButton: {type: 'boolean', default: true},
		isLoadMoreButton: {type: 'boolean', default: true},
		loadCount: {type: 'int', default: '5'},
		fontSize: {type: 'int', default: 16}
	},

	edit: ( props ) => {
		// Creates a <p class='wp-block-cgb-block-panther-selfservice-block'></p>.

		return (

			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody>
						<PanelRow title="General settings">
							<TextControl
								label={__(
									"Label for product row"
								)}
								value={props.attributes.productRowLabel}
								onChange={ function(value){
									props.setAttributes( { productRowLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for amount row"
								)}
								value={props.attributes.amountRowLabel}
								onChange={ function(value){
									props.setAttributes( { amountRowLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for price row"
								)}
								value={props.attributes.priceRowLabel}
								onChange={ function(value){
									props.setAttributes( { priceRowLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for period row"
								)}
								value={props.attributes.periodRowLabel}
								onChange={ function(value){
									props.setAttributes( { periodRowLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for next payment row"
								)}
								value={props.attributes.nextPaymentRowLabel}
								onChange={ function(value){
									props.setAttributes( { nextPaymentRowLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label='Hide period unit on each row. "pr. måned"'
								checked={props.attributes.hideUnitOnRow}
								onChange={ function(value){
											props.setAttributes( { hideUnitOnRow: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
						<TextControl
								label={__(
									"List of products to hide"
								)}
								help="Comma-separated list of product idents"
								value={props.attributes.productBlacklist}
								onChange={ function(value){
									props.setAttributes( { productBlacklist: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Hide decimals on all prices"
								checked={props.attributes.hideDecimals}
								onChange={ function(value){
									props.setAttributes({ hideDecimals: value})
								}}/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Show more/less button settings">
						<PanelRow>
							<TextControl
								label={__(
									"Label for show more button"
								)}
								value={props.attributes.loadMoreButtonLabel}
								onChange={ function(value){
									props.setAttributes( { loadMoreButtonLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for show less button"
								)}
								value={props.attributes.loadLessButtonLabel}
								onChange={ function(value){
									props.setAttributes( { loadLessButtonLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Count of items to be showed "
								)}
								value={props.attributes.loadCount}
								onChange={ function(value){
									props.setAttributes( { loadCount: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Activate show more button"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showLoadMoreButton }
								onChange={ function(value){
									props.setAttributes( { showLoadMoreButton: value } );
								} }
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>

				</BlockControls>



				<div id="product_container" className={ props.className } style={{'font-size': props.attributes.fontSize}}>
					<div class="container">
						<div id="pa-products">
							<div class="columns is-mobile is-hidden-mobile" id="pa-products-head">
								<div class="column is-4 has-text-weight-semibold" >{props.attributes.productRowLabel}</div>
								<div class="column has-text-weight-semibold" >{props.attributes.amountRowLabel}</div>
								<div class="column has-text-weight-semibold" >{props.attributes.priceRowLabel}</div>
								<div class="column has-text-weight-semibold" >{props.attributes.periodRowLabel}</div>
								<div class="column has-text-weight-semibold" >{props.attributes.nextPaymentRowLabel}</div>
							</div>
							<div class="columns is-mobile is-hidden-mobile">
								<div class="column is-4" >Produktnavn</div>
								<div class="column">1</div>
								<div class="column">100,-</div>
								<div class="column">01/01/2021 - 31/01/2021</div>
								<div class="column">01/02/2021</div>
							</div>
							<div class="columns is-mobile is-hidden-mobile">
								<div class="column is-4" >Produktnavn</div>
								<div class="column">2</div>
								<div class="column">200,-</div>
								<div class="column">01/01/2021 - 31/01/2021</div>
								<div class="column">01/02/2021</div>
							</div>

						</div>
						<div className="columns is-centered">
							<div className="column has-text-centered">
								{props.attributes.showLoadMoreButton &&
									<button id="loadmore_products">{props.attributes.loadMoreButtonLabel}</button>}
							</div>
						</div>
					</div>
				</div>

			</div>

		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {

		return (
			<div id="product_container" className={ props.className } style={{'font-size': props.attributes.fontSize}}>
				<div class="container">
					<div id="pa-products" data-head={JSON.stringify(props.attributes)} >
						<div class="columns is-mobile is-hidden-mobile" id="pa-products-head"></div>
					</div>
					<div className="columns is-centered">
						<div className="column has-text-centered">
							{props.attributes.showLoadMoreButton &&
								<button id="loadmore_products">{props.attributes.loadMoreButtonLabel}</button>}
						</div>
					</div>
				</div>

			</div>

		);
	}
});
