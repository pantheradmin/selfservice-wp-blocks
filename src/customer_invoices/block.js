/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
    SelectControl,
	FontSizePicker,
	CheckboxControl,
    Dashicon,
    Tooltip,
    TextControl
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-block3', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Customer Invoices' ), // Block title.
	icon: 'money-alt', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category → Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block → CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		invoiceNumRowLabel: {type: 'string', default: 'Faktura.Nr.'},
		paymentDateRowLabel: {type: 'string', default: 'Forfaldsdato'},
		typeRowLabel: {type: 'string', default: 'Type'},
		amountRowLabel: {type: 'string', default: 'Beløb'},
		isPaidRowLabel: {type: 'string', default: 'Betalt'},
		functionsRowLabel: {type: 'string', default: 'Funktioner'},
		pdfButtonLabel: {type: 'string', default: 'Hent PDF'},
		loadMoreButtonLabel: {type: 'string', default: 'Se alle regninger'},
		loadLessButtonLabel: {type: 'string', default: 'Se færre regninger'},
		showLoadMoreButton: {type: 'boolean', default: true},
		isLoadMoreButton: {type: 'boolean', default: true},
		loadCount: {type: 'int', default: '5'},
		fontSize: {type: 'int', default: 16},
		showA2AButton: {type: 'boolean', default: false},
		showMPButton: {type: 'boolean', default: false},
		showPayButtonOnInvoice: {type: 'boolean', default: true}
	},

	edit: ( props ) => {
		// Creates a <p class='wp-block-cgb-block-panther-selfservice-block'></p>.
		function toggleRows(count) {
			var table = document.getElementById("invoices_table");
			for (let j = 0; j < table.rows.length; j++) {
				if(j > count) {
					table.rows[j].style.display = table.rows[j].style.display === 'none' ? '' : 'none' ;
				}
			}
		}
		return (
			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						<PanelRow>
							<TextControl
								label={__(
									"Label for invoice-number row"
								)}
								value={props.attributes.invoiceNumRowLabel}
								onChange={ function(value){
									props.setAttributes( { invoiceNumRowLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for payment date row"
								)}
								value={props.attributes.paymentDateRowLabel}
								onChange={ function(value){
									props.setAttributes( { paymentDateRowLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for type row"
								)}
								value={props.attributes.typeRowLabel}
								onChange={ function(value){
									props.setAttributes( { typeRowLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for amount row"
								)}
								value={props.attributes.amountRowLabel}
								onChange={ function(value){
									props.setAttributes( { amountRowLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for 'is paid' row"
								)}
								value={props.attributes.isPaidRowLabel}
								onChange={ function(value){
									props.setAttributes( { isPaidRowLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for functions row"
								)}
								value={props.attributes.functionsRowLabel}
								onChange={ function(value){
									props.setAttributes( { functionsRowLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for pdf button"
								)}
								value={props.attributes.pdfButtonLabel}
								onChange={ function(value){
									props.setAttributes( { pdfButtonLabel: value } );
								}}
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Show more/less button settings">
						<PanelRow>
							<TextControl
								label={__(
									"Label for show more button"
								)}
								value={props.attributes.loadMoreButtonLabel}
								onChange={ function(value){
									props.setAttributes( { loadMoreButtonLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for show less button"
								)}
								value={props.attributes.loadLessButtonLabel}
								onChange={ function(value){
									props.setAttributes( { loadLessButtonLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Count of items to be showed "
								)}
								value={props.attributes.loadCount}
								onChange={ function(value){
									props.setAttributes( { loadCount: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Activate show more button"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showLoadMoreButton }
								onChange={ function(value){
									props.setAttributes( { showLoadMoreButton: value } );
								} }
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Payment settings">
					<PanelRow>
						<CheckboxControl
								label="Show a 'Pay' button on each invoice if unpaid"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showPayButtonOnInvoice }
								onChange={ function(value){
									props.setAttributes( { showPayButtonOnInvoice: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
						<CheckboxControl
								label="Show A2A / PISP payment option"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showA2AButton }
								onChange={ function(value){
									props.setAttributes( { showA2AButton: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
						<CheckboxControl
								label="Show Mobilepay payment option"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showMPButton }
								onChange={ function(value){
									props.setAttributes( { showMPButton: value } );
								} }
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}



				<div class="container" style={{'font-size': props.attributes.fontSize}}>
						<div class="columns">
							<div class="column table-container">
								<table id="invoices_table" class="table is-fullwidth">
									<thead>
										<tr>
											<th>{props.attributes.invoiceNumRowLabel}</th>
											<th>{props.attributes.typeRowLabel}</th>
											<th>{props.attributes.paymentDateRowLabel}</th>
											<th>{props.attributes.amountRowLabel}</th>
											<th>{props.attributes.isPaidRowLabel}</th>
											<th>{props.attributes.functionsRowLabel}</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>21589</td>
											<td>Kreditnota</td>
											<td>11-04-2021</td>
											<td>-1725,00</td>
											<td>12-04-2021</td>
											<td><button>{props.attributes.pdfButtonLabel}</button></td>
										</tr>
										<tr>
											<td>21589</td>
											<td>11-04-2021</td>
											<td>1725,00</td>
											{ props.attributes.showPayButtonOnInvoice ? <td><button>Betal</button></td> : <td>Ubetalt</td>}
											
											<td><button type="submit">{props.attributes.pdfButtonLabel}</button></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div className="columns is-centered">
							<div className="column has-text-centered">
								{props.attributes.showLoadMoreButton &&
									<button id="loadmore_invoice">{props.attributes.loadMoreButtonLabel}</button>}
							</div>
						</div>
				</div>


				<div class="modal" id="invoice-payment-modal">
					<div class="modal-background"></div>
					<div class="modal-content">
						<div class="box">
							<div id="invoice-payment-modal-title"></div>
							<div id="invoice-payment-modal-content"></div>
							<div id="invoice-payment-modal-footer"></div>
						</div>
						<button class="modal-close is-large" aria-label="close"></button>
					</div>
				</div>

			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div id="invoice_container" className={ props.className } data-attributes={JSON.stringify(props.attributes)} style={{'font-size': props.attributes.fontSize}}>
				<div class="container">
					<div class="columns">
						<div class="column table-container">
							<div id="pa_invoices" class="is-mobile">
								<div id="pa_invoices_head" class="columns is-mobile" >
								</div>
							</div>
						</div>
					</div>
					<div class="columns is-centered">
						<div class="column has-text-centered">
							{props.attributes.showLoadMoreButton && <button id="loadmore_invoice">{props.attributes.loadMoreButtonLabel}</button>}
						</div>
					</div>
				</div>

				<div class="modal" id="invoice-payment-modal">
					<div class="modal-background"></div>
					<div class="modal-content">
						<div class="box">
							<div id="invoice-payment-modal-title"></div>
							<div id="invoice-payment-modal-content"></div>
							<div id="invoice-payment-modal-footer"></div>
						</div>
					</div>
					<button class="modal-close is-large" aria-label="close"></button>
				</div>
			</div>


		);

} });
