/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component, useState } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
    SelectControl,
	CheckboxControl,
	FontSizePicker,
    Dashicon,
    Tooltip,
    TextControl,
	ToolsPanel,
	ToolsPanelItem
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-block2', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Customer Info' ), // Block title.
	icon: 'businessperson', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		enableEdit: {type:'boolean', default: false},
		nameRow: {type: 'boolean', default: true},
		companyNameCvrRow: {type: 'boolean', default: false},
		addressRow: {type: 'boolean', default: true},
		custnumRow: {type: 'boolean', default: true},
		phoneRow: {type: 'boolean', default: true},
		mobileRow: {type: 'boolean', default: true},
		emailRow: {type: 'boolean', default: true},
		paymentTypeRow: {type: 'boolean', default: true},
		nameLabel: {type: 'string', default: 'Navn'},
		companyNameLabel: {type: 'string', default: 'Firmanavn'},
		cvrLabel: {type: 'string', default: 'Cvr Nr.'},
		addressLabel: {type: 'string', default: 'Adresse'},
		custnumLabel: {type: 'string', default: 'Medlemsnummer'},
		phoneLabel: {type: 'string', default: 'Telefon'},
		mobileLabel: {type: 'string', default: 'Mobil'},
		emailLabel: {type: 'string', default: 'Email'},
		paymentTypeLabel: {type: 'string', default: 'Betalingsform'},
		invoiceByEmail: {type: 'boolean', default: false},
		invoiceByEmailLabel: {type: 'string', default: 'Send faktura via email'},
		invoiceByPaper: {type: 'boolean', default: false},
		invoiceByPaperLabel: {type: 'string', default: 'Send faktura på papir'},
		billingPeriodLabel: {type: 'string', default: 'Afregningsperiode'},
		billingPeriod1: {type: 'boolean', default: false},
		billingPeriodLabel1: {type: 'string', default: '1 måned'},
		billingPeriod3: {type: 'boolean', default: false},
		billingPeriodLabel3: {type: 'string', default: '3 måneder'},
		billingPeriod6: {type: 'boolean', default: false},
		billingPeriodLabel6: {type: 'string', default: '6 måneder'},
		billingPeriod12: {type: 'boolean', default: false},
		billingPeriodLabel12: {type: 'string', default: '12 måneder'},
		fontSize: {type: 'int', default: 16},
	},

	edit: (props) => {
		
		return (
			
			

			<div classname={ props.className }>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
					<PanelRow>
							<CheckboxControl
								label="Enable editing info"
								checked={props.attributes.enableEdit}
								help="Check the box if you want to enable editing info"
								onChange={ function(value){
											props.setAttributes( { enableEdit: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Navn"
								checked={props.attributes.nameRow}
								help="Check the box if you want to show the field"
								onChange={ function(value){
											props.setAttributes( { nameRow: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.nameLabel}
								onChange={ function(value){
									props.setAttributes( { nameLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Firmanavn og cvr"
								checked={props.attributes.companyNameCvrRow}
								help="Check the box if you want to show the field"
								onChange={ function(value){
											props.setAttributes( { companyNameCvrRow: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.companyNameLabel}
								placeholder="Firmanavn"
								onChange={ function(value){
									props.setAttributes( { companyNameLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.cvrLabel}
								placeholder="CVR Nr."
								onChange={ function(value){
									props.setAttributes( { cvrLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Adresse"
								help="Check the box if you want to show the field"
								checked={ props.attributes.addressRow }
								onChange={ function(value){
									props.setAttributes( { addressRow: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.addressLabel}
								onChange={ function(value){
									props.setAttributes( { addressLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Kundenr./Medlemsnr."
								help="Check the box if you want to show the field"
								checked={ props.attributes.custnumRow }
								onChange={ function(value){
									props.setAttributes( { custnumRow: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.custnumLabel}
								onChange={ function(value){
									props.setAttributes( { custnumLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Telefon"
								help="Check the box if you want to show the field"
								checked={ props.attributes.phoneRow }
								onChange={ function(value){
									props.setAttributes( { phoneRow: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.phoneLabel}
								onChange={ function(value){
									props.setAttributes( { phoneLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Mobil"
								help="Check the box if you want to show the field"
								checked={ props.attributes.mobileRow }
								onChange={ function(value){
									props.setAttributes( { mobileRow: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.mobileLabel}
								onChange={ function(value){
									props.setAttributes( { mobileLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Email"
								help="Check the box if you want to show the field"
								checked={ props.attributes.emailRow }
								onChange={ function(value){
									props.setAttributes( { emailRow: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.emailLabel}
								onChange={ function(value){
									props.setAttributes( { emailLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Betalingsform"
								help="Check the box if you want to show the field"
								checked={ props.attributes.paymentTypeRow }
								onChange={ function(value){
									props.setAttributes( { paymentTypeRow: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.paymentTypeLabel}
								onChange={ function(value){
									props.setAttributes( { paymentTypeLabel: value } );
								}}
							/>
						</PanelRow>


						<PanelRow>
							<CheckboxControl
								label="Send faktura via email"
								help="Check the box if you want to show the field (when applicable)"
								checked={ props.attributes.invoiceByEmail }
								onChange={ function(value){
									props.setAttributes( { invoiceByEmail: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.invoiceByEmailLabel}
								onChange={ function(value){
									props.setAttributes( { invoiceByEmailLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Send faktura på papir"
								help="Check the box if you want to show the field (when applicable)"
								checked={ props.attributes.invoiceByPaper }
								onChange={ function(value){
									props.setAttributes( { invoiceByPaper: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.invoiceByPaperLabel}
								onChange={ function(value){
									props.setAttributes( { invoiceByPaperLabel: value } );
								}}
							/>
						</PanelRow>


						</PanelBody>
						<PanelBody title="Valgbare afregningsperioder">
						<PanelRow>
							<CheckboxControl
								label="1 måned"
								checked={ props.attributes.billingPeriod1 }
								onChange={ function(value){
									props.setAttributes( { billingPeriod1: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="3 måneder"
								checked={ props.attributes.billingPeriod3 }
								onChange={ function(value){
									props.setAttributes( { billingPeriod3: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="6 måneder"
								checked={ props.attributes.billingPeriod6 }
								onChange={ function(value){
									props.setAttributes( { billingPeriod6: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="12 måneder"
								checked={ props.attributes.billingPeriod12 }
								onChange={ function(value){
									props.setAttributes( { billingPeriod12: value } );
								} }
							/>
						</PanelRow>

					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
				
				</BlockControls>

				{/* here starts the edit block itself*/}
				<div id="customer-info-container" class="content center-column" style={{'font-size': props.attributes.fontSize}}>
					
						<div class="container">
							<div class="columns is-multiline is-mobile">
								{/** Customer-num row*/}
								{ props.attributes.custnumRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="customer_membernumber">{props.attributes.custnumLabel}</div>
										</div> : ''
								}
								{ props.attributes.custnumRow ?
										<div class="column is-8">
											<div id="customer_membernumber" class="is-family-secondary right ">1000</div>
										</div> : ''
								}
								
								{/** Name row*/ }
								{ props.attributes.nameRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="customer_name">{props.attributes.nameLabel}</div>
										</div> : ''
								}
								{ props.attributes.nameRow ?
										<div class="column is-8">
											<div id="customer_name" class="is-family-secondary right ">E/F Boligforeningen</div>
										</div> : ''
								}

									{/** Companyname & cvr row*/ }
									{ props.attributes.companyNameCvrRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="customer_companyname">{props.attributes.companyNameLabel}</div>
										</div> : ''
								}
								{ props.attributes.companyNameCvrRow  && props.attributes.enableEdit ?
										<div class="column is-8">
											<div class="is-family-secondary right">
												<input type="text" id="customer_companyname" value="E/F Boligforeningen"/>
											</div>
										</div> : ''
								}
								{ props.attributes.companyNameCvrRow  && props.attributes.enableEdit == false?
										<div class="column is-8">
											<div id="customer_companyname" class="is-family-secondary right">E/F Boligforeningen</div>
										</div> : ''
								}
								{ props.attributes.companyNameCvrRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="customer_cvr">{props.attributes.cvrLabel}</div>
										</div> : ''
								}
								{ props.attributes.companyNameCvrRow && props.attributes.enableEdit ?
										<div class="column is-8">
											<div class="is-family-secondary right">
												<input type="text" id="customer_cvr" value="12345678"/>
											</div>
										</div> : ''
								}
								{ props.attributes.companyNameCvrRow && props.attributes.enableEdit == false?
										<div class="column is-8">
											<div id="customer_cvr" class="is-family-secondary right">12345678</div>
										</div> : ''
								}
								
								{/** Address row*/}
								{ props.attributes.addressRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="customer_address">{props.attributes.addressLabel}</div>
										</div> : ''
								}
								{ props.attributes.addressRow ?
										<div class="column is-8">
											<div id="customer_address" class="is-family-secondary right ">Klausdalsbrovej 1ø, 2750 Ballerup</div>
										</div> : ''
								}

								{/** Phone row */}
								{ props.attributes.phoneRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="customer_phone">{props.attributes.phoneLabel}</div>
										</div> : ''
								}
								{ props.attributes.phoneRow && props.attributes.enableEdit == false ?
										<div class="column is-8">
											<div id="customer_phone" class="is-family-secondary right ">12345678</div>
										</div> : ''
								}	
								{ props.attributes.phoneRow && props.attributes.enableEdit ?
										<div class="column is-8">
											<div class="is-family-secondary right">
												<input type="text" id="customer_phone" value="12345678"/>
											</div>
										</div> : ''
								}	

								{/** Mobile row */}
								{ props.attributes.mobileRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="customer_mobile">{props.attributes.mobileLabel}</div>
										</div> : ''
								}
								{ props.attributes.mobileRow && props.attributes.enableEdit == false ?
										<div class="column is-8">
											<div id="customer_mobile" class="is-family-secondary right ">12345678</div>
										</div> : ''
								}
								{ props.attributes.mobileRow && props.attributes.enableEdit ?
										<div class="column is-8">
											<div class="is-family-secondary right">
												<input type="text" id="customer_mobile" value="12345678"/>
											</div>
										</div> : ''
								}

								{/** Email row */}
								{ props.attributes.emailRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="customer_email">{props.attributes.emailLabel}</div>
										</div> : ''
								}
									{ props.attributes.emailRow && props.attributes.enableEdit == false ?
										<div class="column is-8">
											<div id="customer_email" class="is-family-secondary right ">cla@pantherapplications.com</div>
										</div> : ''
								}
								{ props.attributes.mobileRow && props.attributes.enableEdit ?
										<div class="column is-8">
											<div class="is-family-secondary right">
												<input type="text" id="customer_email" value="cla@pantherapplications.com"/>
											</div>
										</div> : ''
								}

								{/** Payment type row */}
								{ props.attributes.paymentTypeRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="customer_paymentform">{props.attributes.paymentTypeLabel}</div>
										</div> : ''
								}
								{ props.attributes.paymentTypeRow ?
										<div class="column is-8">
											<div id="customer_paymentform" class="is-family-secondary right ">Kort</div>
										</div> : ''
								}

								{ props.attributes.invoiceByEmail ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="customer_invoiceByEmail">{props.attributes.invoiceByEmailLabel}</div>
										</div> : ''
								}
								{ props.attributes.invoiceByEmail ?
										<div class="column is-8">
											<div class="is-family-secondary right ">
												<input type="checkbox" name="customer_invoiceByEmail" id="customer_invoiceByEmail"/>
											</div>
										</div> : ''
								}
								{ props.attributes.invoiceByPaper ?
										<div class="column is-4 invoiceByPaper">
											<div class="has-text-weight-semibold "for="customer_invoiceByPaper">{props.attributes.invoiceByPaperLabel}</div>
										</div> : ''
								}
								{ props.attributes.invoiceByPaper ?
										<div class="column is-8 invoiceByPaper">
											<div class="is-family-secondary right ">
												<input type="checkbox" name="customer_invoiceByPaper" id="customer_invoiceByPaper"/>
											</div>
										</div> : ''
								}

								{/** Billing period rows */}
								{ props.attributes.billingPeriod1 || props.attributes.billingPeriod3 || props.attributes.billingPeriod6 || props.attributes.billingPeriod12 ?
										<div class="column is-12">
											<div class="has-text-weight-semibold" id="billingPeriodLabel">{props.attributes.billingPeriodLabel}</div>
										</div> : ''
								}
								{ props.attributes.billingPeriod1 ?
											<div class="column is-9">											
												<div class="is-family-secondary right"><input type="radio" id="billingPeriod1" name="billingPeriod" value="1" /></div>
											</div> : ''
								}
								{ props.attributes.billingPeriod1 ?
											<div class="column is-3">											
												<div class="is-family-secondary" id="billingPeriodLabel1">{props.attributes.billingPeriodLabel1}</div>
											</div> : ''
								}
								{ props.attributes.billingPeriod3 ?
											<div class="column is-9">											
												<div class="is-family-secondary right"><input type="radio" id="billingPeriod3" name="billingPeriod" value="3" /></div>
											</div> : ''
								}
								{ props.attributes.billingPeriod3 ?
											<div class="column is-3">											
												<div class="is-family-secondary" id="billingPeriodLabel3">{props.attributes.billingPeriodLabel3}</div>
											</div> : ''
								}
								{ props.attributes.billingPeriod6 ?
											<div class="column is-9">											
												<div class="is-family-secondary right"><input type="radio" id="billingPeriod6" name="billingPeriod" value="6" /></div>
											</div> : ''
								}
								{ props.attributes.billingPeriod6 ?
											<div class="column is-3">											
												<div class="is-family-secondary" id="billingPeriodLabel6">{props.attributes.billingPeriodLabel6}</div>
											</div> : ''
								}
								{ props.attributes.billingPeriod12 ?
											<div class="column is-9">											
												<div class="is-family-secondary right"><input type="radio" id="billingPeriod12" name="billingPeriod" value="12" /></div>
											</div> : ''
								}
								{ props.attributes.billingPeriod12 ?
											<div class="column is-3">											
												<div class="is-family-secondary" id="billingPeriodLabel12">{props.attributes.billingPeriodLabel12}</div>
											</div> : ''
								}

							</div>
							<button id="logout_button">Log ud</button>
								<button id="change_pass_button">Skift kodeord</button>
								{props.attributes.enableEdit || props.attributes.invoiceByEmail || props.attributes.invoiceByPaper ? <button id="save-info-changes">Gem ændringer</button>:'' }
								{props.attributes.enableEdit || props.attributes.invoiceByEmail || props.attributes.invoiceByPaper ? <div id="info-save-notification" class="notification is-hidden">Ændringer gemt</div> : ''}
						</div>	
				</div>		
			</div>
			
		);	
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div id="customer-info-container" className={ props.className } style={{'font-size': props.attributes.fontSize}}>
				<div class="content center-column">
							<div class="container">
								<div class="columns is-multiline is-mobile">


									{/** Customer-num row*/}
								{ props.attributes.custnumRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="customer_membernumber">{props.attributes.custnumLabel}</div>
										</div> : ''
								}
								{ props.attributes.custnumRow ?
										<div class="column is-8">
											<div id="customer_membernumber" class="is-family-secondary right "></div>
										</div> : ''
								}
								
								{/** Name row*/ }
								{ props.attributes.nameRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="customer_name">{props.attributes.nameLabel}</div>
										</div> : ''
								}
								{ props.attributes.nameRow ?
										<div class="column is-8">
											<div id="customer_name" class="is-family-secondary right "></div>
										</div> : ''
								}

								{/** Companyname & cvr row*/ }
								{ props.attributes.companyNameCvrRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="customer_companyname">{props.attributes.companyNameLabel}</div>
										</div> : ''
								}
								{ props.attributes.companyNameCvrRow  && props.attributes.enableEdit ?
										<div class="column is-8">
											<div class="is-family-secondary right">
												<input type="text" id="customer_companyname" value=""/>
											</div>
										</div> : ''
								}
								{ props.attributes.companyNameCvrRow  && props.attributes.enableEdit == false?
										<div class="column is-8">
											<div id="customer_companyname" class="is-family-secondary right"></div>
										</div> : ''
								}
								{ props.attributes.companyNameCvrRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="customer_cvr">{props.attributes.cvrLabel}</div>
										</div> : ''
								}
								{ props.attributes.companyNameCvrRow && props.attributes.enableEdit ?
										<div class="column is-8">
											<div class="is-family-secondary right">
												<input type="text" id="customer_cvr" value=""/>
											</div>
										</div> : ''
								}
								{ props.attributes.companyNameCvrRow && props.attributes.enableEdit == false?
										<div class="column is-8">
											<div id="customer_cvr" class="is-family-secondary right"></div>
										</div> : ''
								}
								
								{/** Address row*/}
								{ props.attributes.addressRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="customer_address">{props.attributes.addressLabel}</div>
										</div> : ''
								}
								{ props.attributes.addressRow ?
										<div class="column is-8">
											<div id="customer_address" class="is-family-secondary right "></div>
										</div> : ''
								}

								{/** Phone row */}
								{ props.attributes.phoneRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="customer_phone">{props.attributes.phoneLabel}</div>
										</div> : ''
								}
								{ props.attributes.phoneRow && props.attributes.enableEdit == false ?
										<div class="column is-8">
											<div id="customer_phone" class="is-family-secondary right "></div>
										</div> : ''
								}	
								{ props.attributes.phoneRow && props.attributes.enableEdit ?
										<div class="column is-8">
											<div class="is-family-secondary right">
												<input type="text" id="customer_phone" value=""/>
											</div>
										</div> : ''
								}

								<div class="column is-4 is-hidden">
									<div class="has-text-weight-semibold "for="customer_contactperson">Kontaktperson</div>
								</div>
								<div class="column is-8 is-hidden">
									<div id="customer_contactperson" class="is-family-secondary right "></div>
								</div>

								{/** Mobile row */}
								{ props.attributes.mobileRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="customer_mobile">{props.attributes.mobileLabel}</div>
										</div> : ''
								}
								{ props.attributes.mobileRow && props.attributes.enableEdit == false ?
										<div class="column is-8">
											<div id="customer_mobile" class="is-family-secondary right "></div>
										</div> : ''
								}
								{ props.attributes.mobileRow && props.attributes.enableEdit ?
										<div class="column is-8">
											<div class="is-family-secondary right">
												<input type="text" id="customer_mobile" value=""/>
											</div>
										</div> : ''
								}

								{/** Email row */}
								{ props.attributes.emailRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="customer_email">{props.attributes.emailLabel}</div>
										</div> : ''
								}
								{ props.attributes.emailRow && props.attributes.enableEdit == false ?
										<div class="column is-8">
											<div id="customer_email" class="is-family-secondary right "></div>
										</div> : ''
								}
								{ props.attributes.mobileRow && props.attributes.enableEdit ?
										<div class="column is-8">
											<div class="is-family-secondary right">
												<input type="text" id="customer_email" value=""/>
											</div>
										</div> : ''
								}

								{/** Payment type rows */}
								{ props.attributes.paymentTypeRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="customer_paymentform">{props.attributes.paymentTypeLabel}</div>
										</div> : ''
								}
								{ props.attributes.paymentTypeRow ?
										<div class="column is-8">
											<div id="customer_paymentform" class="is-family-secondary right "></div>
										</div> : ''
								}

								{ props.attributes.invoiceByEmail ?
										<div class="column is-4 invoiceByEmail">
											<div class="has-text-weight-semibold "for="customer_invoiceByEmail">{props.attributes.invoiceByEmailLabel}</div>
										</div> : ''
								}
								{ props.attributes.invoiceByEmail ?
										<div class="column is-8 invoiceByEmail">
											<div class="is-family-secondary right ">
												<input type="checkbox" name="customer_invoiceByEmail" id="customer_invoiceByEmail"/>
											</div>
										</div> : ''
								}
								{ props.attributes.invoiceByPaper ?
										<div class="column is-4 invoiceByPaper">
											<div class="has-text-weight-semibold "for="customer_invoiceByPaper">{props.attributes.invoiceByPaperLabel}</div>
										</div> : ''
								}
								{ props.attributes.invoiceByPaper ?
										<div class="column is-8 invoiceByPaper">
											<div class="is-family-secondary right ">
												<input type="checkbox" name="customer_invoiceByPaper" id="customer_invoiceByPaper"/>
											</div>
										</div> : ''
								}

								{/** Billing period rows */}
								{ props.attributes.billingPeriod1 || props.attributes.billingPeriod3 || props.attributes.billingPeriod6 || props.attributes.billingPeriod12 ?
										<div class="column is-12">
											<div class="has-text-weight-semibold" id="billingPeriodLabel">{props.attributes.billingPeriodLabel}</div>
										</div> : ''
								}
                                { props.attributes.billingPeriod1 && props.attributes.enableEdit ?
                                            <div class="column is-12">
                                                <input type="radio" id="billingPeriod1" name="billingPeriod" value="1" />
												<div class="is-family-secondary" id="billingPeriodLabel1">{props.attributes.billingPeriodLabel1}</div>
                                            </div>: ''
								}
                                { props.attributes.billingPeriod3 && props.attributes.enableEdit ?
                                            <div class="column is-12">
                                                <input type="radio" id="billingPeriod3" name="billingPeriod" value="3" />
												<div class="is-family-secondary" id="billingPeriodLabel3">{props.attributes.billingPeriodLabel3}</div>
                                            </div>: ''
								}
                                { props.attributes.billingPeriod6 && props.attributes.enableEdit ?
                                            <div class="column is-12">
                                                <input type="radio" id="billingPeriod6" name="billingPeriod" value="6" />
												<div class="is-family-secondary" id="billingPeriodLabel6">{props.attributes.billingPeriodLabel6}</div>
                                            </div>: ''
								}
                                { props.attributes.billingPeriod12 && props.attributes.enableEdit ?
                                            <div class="column is-12">
                                                <input type="radio" id="billingPeriod12" name="billingPeriod" value="12" />
												<div class="is-family-secondary" id="billingPeriodLabel12">{props.attributes.billingPeriodLabel12}</div>
                                            </div>: ''
								}


								</div>
                                <div id="customer_info_action_buttons">
                                    <button id="logout_button">Log ud</button>
                                    <button id="change_pass_button">Skift kodeord</button>
                                    {props.attributes.enableEdit || props.attributes.invoiceByEmail || props.attributes.invoiceByPaper ? <button id="save-info-changes">Gem ændringer</button> : ''}
                            
                                </div>
                        
								{props.attributes.enableEdit || props.attributes.invoiceByEmail || props.attributes.invoiceByPaper ? <div id="info-save-notification" class="notification is-hidden">Ændringer gemt</div> : ''}
							</div>
					</div>

					<div class="modal" id="change-password-modal">
						<div class="modal-background"></div>
						<div class="modal-content">
							<div class="box">
								<div id="change-password-modal-title">Skift kodeord</div>
								<div id="change-password-modal-content">
									<div>
										<div>
											<input type="password" name="old_password" id="old_password" placeholder='Gammelt kodeord'></input>
										</div>
										<div>
											<input type="password" name="new_password" id="new_password" placeholder='Nyt kodeord'></input><span style="border: none !important" id="pass_tooltip1" class="has-tooltip-bottom has-tooltip-right-desktop has-tooltip-multiline" ><i class="fa fa-question-circle"></i></span>
										</div>
										<div>
											<input type="password" name="new2_password" id="new2_password" placeholder='Gentag nyt kodeord'></input><span style="border: none !important" id="pass_tooltip2" class="has-tooltip-bottom has-tooltip-right-desktop has-tooltip-multiline" ><i class="fa fa-question-circle"></i></span>
										</div>
										<input type="hidden" name="token" id="token"></input>
										<button id="change_password_save_button" >Gem</button>
                                    </div>
								</div>
								<div id="change-password-modal-footer"></div>
							</div>
							<button id="change-password-modal-close" class="modal-close is-large" aria-label="close"></button>
						</div>
					</div>

				</div>
			
		);	
 }

});
