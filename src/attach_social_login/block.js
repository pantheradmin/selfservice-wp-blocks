/**
 * BLOCK: my-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
	BlockControls,
	InspectorControls,
} from "@wordpress/block-editor";

import {
	PanelBody,
	PanelRow,
	CheckboxControl,
	TextControl,
	FontSizePicker
} from "@wordpress/components";

const {__} = wp.i18n; // Import __() from wp.i18n
const {registerBlockType} = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType('cgb/block-panther-selfservice-block-social-login', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __('Social Login'), // Block title.
	icon: 'google', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__('block-panther-selfservice-block-social-login — CGB Block'),
		__('CGB Example'),
		__('create-guten-block'),
	],
	attributes: {
		title: {type: 'string', default: 'Forbind med social konto'},
		showAttachFacebookButton: {type: 'boolean', default: true},
		showAttachGoogleButton: {type: 'boolean', default: true},
		showAttachAppleButton: {type: 'boolean', default: true},
		facebookButtonText: {type: 'string', default: 'Facebook'},
		googleButtonText: {type: 'string', default: 'Google'},
		appleButtonText: {type: 'string', default: 'Apple'},
		facebookRemoveButtonText: {type: 'string', default: 'Fjern tilknytning til Facebook'},
		googleRemoveButtonText: {type: 'string', default: 'Fjern tilknytning til Google'},
		appleRemoveButtonText: {type: 'string', default: 'Fjern tilknytning til Apple'},
		userAttachedFacebook: {type: 'boolean', default: false},
		userAttachedGoogle: {type: 'boolean', default: false},
		userAttachedApple: {type: 'boolean', default: false},
		fontSize: {type: 'int', default: 16}
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: (props) => {
		// Creates a <p class='wp-block-cgb-block-my-block'></p>.
		function updateTitle(newTitle) {
			props.setAttributes({title: newTitle});
		}

		function updateFacebookButtonText(newFacebookButtonText) {
			props.setAttributes({facebookButtonText: newFacebookButtonText});
		}

		function updateGoogleButtonText(newGoogleButtonText) {
			props.setAttributes({googleButtonText: newGoogleButtonText});
		}

		function updateAppleButtonText(newAppleButtonText) {
			props.setAttributes({appleButtonText: newAppleButtonText});
		}

		function updateFacebookRemoveButtonText(newFacebookRemoveButtonText) {
			props.setAttributes({facebookRemoveButtonText: newFacebookRemoveButtonText});
		}

		function updateGoogleRemoveButtonText(newGoogleRemoveButtonText) {
			props.setAttributes({googleRemoveButtonText: newGoogleRemoveButtonText});
		}

		function updateAppleRemoveButtonText(newAppleRemoveButtonText) {
			props.setAttributes({appleRemoveButtonText: newAppleRemoveButtonText});
		}

		return (
			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						<PanelRow>
							<TextControl
								label={__(
									"Title"
								)}
								value={props.attributes.title}
								onChange={updateTitle}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Activate Facebook button"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showAttachFacebookButton }
								onChange={ function(value){
									props.setAttributes( { showAttachFacebookButton: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Activate Google button"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showAttachGoogleButton }
								onChange={ function(value){
									props.setAttributes( { showAttachGoogleButton: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Activate Apple button"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showAttachAppleButton }
								onChange={ function(value){
									props.setAttributes( { showAttachAppleButton: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Facebook button text"
								)}
								value={props.attributes.facebookButtonText}
								onChange={updateFacebookButtonText}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Google button text"
								)}
								value={props.attributes.googleButtonText}
								onChange={updateGoogleButtonText}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Apple button text"
								)}
								value={props.attributes.appleButtonText}
								onChange={updateAppleButtonText}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Facebook disconnnect button text"
								)}
								value={props.attributes.facebookRemoveButtonText}
								onChange={updateFacebookRemoveButtonText}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Google disconnect button text"
								)}
								value={props.attributes.googleRemoveButtonText}
								onChange={updateGoogleRemoveButtonText}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Apple disconnect button text"
								)}
								value={props.attributes.appleRemoveButtonText}
								onChange={updateAppleRemoveButtonText}
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
								fontSizes={[
									{
										name: 'Small',
										slug: 'small',
										size: 12,
									},
									{
										name: 'Medium',
										slug: 'medium',
										size: 16,
									},
									{
										name: 'Big',
										slug: 'big',
										size: 26,
									},
								]}
								value={props.attributes.fontSize}
								fallbackFontSize={16}
								onChange={(newFontSize) => {
									props.setAttributes({fontSize: newFontSize});
								}}
								withSlider
							/>
						</PanelRow>
					</PanelBody>
				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>

				</BlockControls>
				<section>
					<div className={props.className}>
						<div style={{'font-size': props.attributes.fontSize}}>
							<h2 className="title has-text-centered"> {props.attributes.title} </h2>
							{props.attributes.showAttachFacebookButton &&
							<div className="columns is-mobile is-centered">
								<button id="facebookattachbutton" type="button"
										className="column is-three-quarters-touch is-half-desktop">
									<div className="level is-mobile">
										<div className="level-left">
											<span className="icon is-medium level-item"><i className="fab fa-facebook-f"/></span>
										</div>
										<span id="facebookAttachButtonText" className="level-item">{props.attributes.userAttachedFacebook ? props.attributes.facebookRemoveButtonText : props.attributes.facebookButtonText}</span>
									</div>
								</button>
							</div>}
							{props.attributes.showAttachGoogleButton &&
							<div class="columns is-mobile is-centered  mt-4">
								<button id="googleattachbutton" type="button"
										className="column is-three-quarters-touch is-half-desktop">
									<div class="level is-mobile">
										<div class="level-left">
										<span className="icon is-medium level-item"><i className="fab fa-google"/></span>
										</div>
										<span id="googleAttachButtonText" class="level-item">{props.attributes.userAttachedGoogle ? props.attributes.googleRemoveButtonText : props.attributes.googleButtonText}</span>
									</div>
								</button>
							</div>}
							{props.attributes.showAttachAppleButton &&
								<div className="columns is-mobile is-centered mt-4">
								<button id="appleattachbutton" type="button"
										className="column is-three-quarters-touch is-half-desktop">
									<div className="level is-mobile">
										<div className="level-left">
									<span className="icon is-medium level-item"><i className="fab fa-apple"/></span>
										</div>
										<span id="appleAttachButtonText" className="level-item">{props.attributes.userAttachedApple ? props.attributes.appleRemoveButtonText : props.attributes.appleButtonText}</span>
									</div>
								</button>
							</div>}
							<div id="feedback">
								<div className="notification">
									<div className="icon-text">
  							<span className="icon has-text-success">
    							<i className="fas fa-check-square"/>
  							</span>
										<span>Forbundet med Google konto</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {
		return (
			<div className={props.className} id="socialConnection-container" data-attributes={JSON.stringify(props.attributes)}>
				<div style={{'font-size': props.attributes.fontSize}}>
					<h2 className="title has-text-centered"> {props.attributes.title} </h2>
					{props.attributes.showAttachFacebookButton &&
					<div className="columns is-mobile is-centered">
						<button id="facebookattachbutton" type="button"
								className="column is-three-quarters-touch is-half-desktop" disabled>
							<div className="level is-mobile">
								<div className="level-left">
									<span className="icon is-medium level-item"><i className="fab fa-facebook-f"/></span>
								</div>
								<span id="facebookAttachButtonText" className="level-item">{props.attributes.userAttachedFacebook ? props.attributes.facebookRemoveButtonText : props.attributes.facebookButtonText}</span>
							</div>
						</button>
					</div>}
					{props.attributes.showAttachGoogleButton &&
					<div className="columns is-mobile is-centered  mt-4">
						<button id="googleattachbutton" type="button"
								className="column is-three-quarters-touch is-half-desktop" disabled>
							<div className="level is-mobile">
								<div className="level-left">
									<span className="icon is-medium level-item"><i className="fab fa-google"/></span>
								</div>
								<span id="googleAttachButtonText" className="level-item">{props.attributes.userAttachedGoogle ? props.attributes.googleRemoveButtonText : props.attributes.googleButtonText}</span>
							</div>
						</button>
					</div>}
					{props.attributes.showAttachAppleButton &&
					<div className="columns is-mobile is-centered mt-4">
						<button id="appleattachbutton" type="button"
								className="column is-three-quarters-touch is-half-desktop" disabled>
							<div className="level is-mobile">
								<div className="level-left">
									<span className="icon is-medium level-item"><i className="fab fa-apple"/></span>
								</div>
								<span id="appleAttachButtonText" className="level-item">{props.attributes.userAttachedApple ? props.attributes.appleRemoveButtonText : props.attributes.appleButtonText}</span>
							</div>
						</button>
					</div>}
					<div id="feedback">

					</div>
				</div>
			</div>
		);
	},
});
