/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component, useState } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
    SelectControl,
	CheckboxControl,
	FontSizePicker,
    Dashicon,
    Tooltip,
    TextControl,
	ToolsPanel,
	ToolsPanelItem
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-modem-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Modem Data' ), // Block title.
	icon: 'businessperson', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-modem-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		factorySettingsKey: {type: 'string', default: "wifidefaults"},
		defaultValue: {type: 'string', default: "default"},
		customValue: {type: 'string', default: "custom"},

		showProductNameRow: {type: 'boolean', default: true},
		showProductNameLabel: {type: 'string', default: "Produkt"},

		showWifi: {type: 'boolean', default: false},
		wifiKey: {type: 'string', default: "wifi"},
		wifiLabel: {type: 'string', default: "WiFi aktiv"},
		wifiHelpMessage: {type: 'string', default: "Forklaring"},

		showHeader: {type: 'boolean', default: true},
		showHeaderLabel: {type: 'string', default: "2,4 Ghz"},

		showHeader5: {type: 'boolean', default: true},
		showHeader5Label: {type: 'string', default: "5 Ghz"},

		showSSID: {type: 'boolean', default: false},
		SSIDKey: {type: 'string', default: "ssid"},
		SSIDLabel: {type: 'string', default: "WiFi brugernavn"},
		SSIDHelpMessage: {type: 'string', default: "Forklaring"},
		SSIDRegex: {type: 'string', default: "^[a-zA-Z0-9]+$"},
		SSIDErrorMessage: {type: 'string', default: "WiFi brugernavn m&aring; kun indeholde store og sm&aring; bogstaver og tal."},

		showSSID5: {type: 'boolean', default: false},
		SSID5Key: {type: 'string', default: "ssid"},
		SSID5Label: {type: 'string', default: "WiFi brugernavn"},
		SSID5HelpMessage: {type: 'string', default: "Forklaring"},
		SSID5Regex: {type: 'string', default: "^[a-zA-Z0-9]+$"},
		SSID5ErrorMessage: {type: 'string', default: "WiFi brugernavn m&aring; kun indeholde store og sm&aring; bogstaver og tal."},

		showWpaPsk: {type: 'boolean', default: false},
		wpaPskKey: {type: 'string', default: "wpa-psk"},
		wpaPskLabel: {type: 'string', default: "WiFi adgangskode"},
		wpaPskHelpMessage: {type: 'string', default: "Forklaring"},
		wpaPskRegex: {type: 'string', default: "^[a-zA-Z0-9]+$"},
		wpaPskErrorMessage: {type: 'string', default: "WiFi adgangskode m&aring; kun indeholde store og sm&aring; bogstaver og tal."},

		showWpaPsk5: {type: 'boolean', default: false},
		wpaPsk5Key: {type: 'string', default: "wpa-psk"},
		wpaPsk5Label: {type: 'string', default: "WiFi adgangskode"},
		wpaPsk5HelpMessage: {type: 'string', default: "Forklaring"},
		wpaPsk5Regex: {type: 'string', default: "^[a-zA-Z0-9]+$"},
		wpaPsk5ErrorMessage: {type: 'string', default: "WiFi adgangskode m&aring; kun indeholde store og sm&aring; bogstaver og tal."},

		showChannel: {type: 'boolean', default: false},
		channelKey: {type: 'string', default: "wifi-channel"},
		channelLabel: {type: 'string', default: "WiFi kanal"},
		channelHelpMessage: {type: 'string', default: "Forklaring"},

		showChannelWidth: {type: 'boolean', default: false},
		channelWidthKey: {type: 'string', default: "channelwidth"},
		channelWidthLabel: {type: 'string', default: "WiFi kanal-bredde"},
		channelWidthHelpMessage: {type: 'string', default: "Forklaring"},

		showChannel5: {type: 'boolean', default: false},
		channelKey5: {type: 'string', default: "wifi-channel"},
		channelLabel5: {type: 'string', default: "WiFi kanal (5 ghz)"},
		channelHelpMessage5: {type: 'string', default: "Forklaring"},

		showChannelWidth5: {type: 'boolean', default: false},
		channelWidthKey5: {type: 'string', default: "channelwidth"},
		channelWidthLabel5: {type: 'string', default: "WiFi kanal-bredde (5 ghz)"},
		channelWidthHelpMessage5: {type: 'string', default: "Forklaring"},

		fontSize: {type: 'int', default: 16}
	},

	edit: (props) => {
		
		return (
			<div classname={ props.className }>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						<PanelRow>
							<CheckboxControl
								label="Produktnavn"
								checked={props.attributes.showProductNameRow}
								help="Check the box if you want to show the field"
								onChange={ function(value){
											props.setAttributes( { showProductNameRow: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.showProductNameLabel}
								onChange={ function(value){
									props.setAttributes( { showProductNameLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label="WiFi settings key"
								value={props.attributes.factorySettingsKey}
								onChange={ function(value){
											props.setAttributes( { factorySettingsKey: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label="WiFi settings factory defaults value"
								value={props.attributes.defaultValue}
								onChange={ function(value){
											props.setAttributes( { defaultValue: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label="WiFi settings custom value"
								value={props.attributes.customValue}
								onChange={ function(value){
											props.setAttributes( { customValue: value } );
										}}
							/>
						</PanelRow> 
						<PanelRow>
							<CheckboxControl
								label="2,4 Ghz overskrift"
								checked={props.attributes.showHeader}
								help="Check the box if you want to show the field"
								onChange={ function(value){
											props.setAttributes( { showHeader: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.showHeaderLabel}
								onChange={ function(value){
									props.setAttributes( { showHeaderLabel: value } );
								}}
							/>
						</PanelRow>

						<PanelRow>
							<CheckboxControl
								label="5 Ghz overskrift"
								checked={props.attributes.showHeader5}
								help="Check the box if you want to show the field"
								onChange={ function(value){
											props.setAttributes( { showHeader5: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.showHeader5Label}
								onChange={ function(value){
									props.setAttributes( { showHeader5Label: value } );
								}}
							/>
						</PanelRow>


                        <PanelRow>
							<CheckboxControl
								label="WiFi aktiv"
								checked={props.attributes.showWifi}
								help="Check the box if you want to show the field"
								onChange={ function(value){
											props.setAttributes( { showWifi: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Key for field value"
								)}
								value={props.attributes.wifiKey}
								onChange={ function(value){
									props.setAttributes( { wifiKey: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.wifiLabel}
								onChange={ function(value){
									props.setAttributes( { wifiLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Help message for field"
								)}
								value={props.attributes.wifiHelpMessage}
								onChange={ function(value){
									props.setAttributes( { wifiHelpMessage: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="SSID"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showSSID }
								onChange={ function(value){
									props.setAttributes( { showSSID: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Key for field value (Enter keys for both 2.4 and 5 Ghz WiFi)"
								)}
								value={props.attributes.SSIDKey}
								onChange={ function(value){
									props.setAttributes( { SSIDKey: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.SSIDLabel}
								onChange={ function(value){
									props.setAttributes( { SSIDLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Help message for field"
								)}
								value={props.attributes.SSIDHelpMessage}
								onChange={ function(value){
									props.setAttributes( { SSIDHelpMessage: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Regular expression to validate field"
								)}
								value={props.attributes.SSIDRegex}
								onChange={ function(value){
									props.setAttributes( { SSIDRegex: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Error message for field"
								)}
								value={props.attributes.SSIDErrorMessage}
								onChange={ function(value){
									props.setAttributes( { SSIDErrorMessage: value } );
								}}
							/>
						</PanelRow>
                        
                        
                        
                        
                        <PanelRow>
							<CheckboxControl
								label="SSID 5Ghz"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showSSID5 }
								onChange={ function(value){
									props.setAttributes( { showSSID5: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Key for field value (Enter keys for both 2.4 and 5 Ghz WiFi)"
								)}
								value={props.attributes.SSID5Key}
								onChange={ function(value){
									props.setAttributes( { SSID5Key: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.SSID5Label}
								onChange={ function(value){
									props.setAttributes( { SSID5Label: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Help message for field"
								)}
								value={props.attributes.SSID5HelpMessage}
								onChange={ function(value){
									props.setAttributes( { SSID5HelpMessage: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Regular expression to validate field"
								)}
								value={props.attributes.SSID5Regex}
								onChange={ function(value){
									props.setAttributes( { SSID5Regex: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Error message for field"
								)}
								value={props.attributes.SSID5ErrorMessage}
								onChange={ function(value){
									props.setAttributes( { SSID5ErrorMessage: value } );
								}}
							/>
						</PanelRow>                        
						<PanelRow>
							<CheckboxControl
								label="wpa-psk"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showWpaPsk }
								onChange={ function(value){
									props.setAttributes( { showWpaPsk: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Key for field value (Enter keys for both 2.4 and 5 Ghz WiFi)"
								)}
								value={props.attributes.wpaPskKey}
								onChange={ function(value){
									props.setAttributes( { wpaPskKey: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.wpaPskLabel}
								onChange={ function(value){
									props.setAttributes( { wpaPskLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Help message for field"
								)}
								value={props.attributes.wpaPskHelpMessage}
								onChange={ function(value){
									props.setAttributes( { wpaPskHelpMessage: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Regular expression to validate field"
								)}
								value={props.attributes.wpaPskRegex}
								onChange={ function(value){
									props.setAttributes( { wpaPskRegex: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Error message for field"
								)}
								value={props.attributes.wpaPskErrorMessage}
								onChange={ function(value){
									props.setAttributes( { wpaPskErrorMessage: value } );
								}}
							/>
						</PanelRow>       



                        <PanelRow>
							<CheckboxControl
								label="wpa-psk5"
								help="Check the box if you want to show the field"
								checked={ props.attributes.showWpaPsk5 }
								onChange={ function(value){
									props.setAttributes( { showWpaPsk5: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Key for field value (Enter keys for both 2.4 and 5 Ghz WiFi)"
								)}
								value={props.attributes.wpaPsk5Key}
								onChange={ function(value){
									props.setAttributes( { wpaPsk5Key: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.wpaPsk5Label}
								onChange={ function(value){
									props.setAttributes( { wpaPsk5Label: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Help message for field"
								)}
								value={props.attributes.wpaPsk5HelpMessage}
								onChange={ function(value){
									props.setAttributes( { wpaPsk5HelpMessage: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Regular expression to validate field"
								)}
								value={props.attributes.wpaPsk5Regex}
								onChange={ function(value){
									props.setAttributes( { wpaPsk5Regex: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Error message for field"
								)}
								value={props.attributes.wpaPsk5ErrorMessage}
								onChange={ function(value){
									props.setAttributes( { wpaPsk5ErrorMessage: value } );
								}}
							/>
						</PanelRow>                        


                        
						<PanelRow>
							<CheckboxControl
								label="WiFi kanal"
								checked={props.attributes.showChannel}
								help="Check the box if you want to show the field"
								onChange={ function(value){
											props.setAttributes( { showChannel: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Key for field value"
								)}
								value={props.attributes.channelKey}
								onChange={ function(value){
									props.setAttributes( { channelKey: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.channelLabel}
								onChange={ function(value){
									props.setAttributes( { channelLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Help message for field"
								)}
								value={props.attributes.channelHelpMessage}
								onChange={ function(value){
									props.setAttributes( { channelHelpMessage: value } );
								}}
							/>
						</PanelRow>                        
						<PanelRow>
							<CheckboxControl
								label="WiFi bandwidth"
								checked={props.attributes.showChannelWidth}
								help="Check the box if you want to show the field"
								onChange={ function(value){
											props.setAttributes( { showChannelWidth: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Key for field value"
								)}
								value={props.attributes.channelWidthKey}
								onChange={ function(value){
									props.setAttributes( { channelWidthKey: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.channelWidthLabel}
								onChange={ function(value){
									props.setAttributes( { channelWidthLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Help message for field"
								)}
								value={props.attributes.channelWidthHelpMessage}
								onChange={ function(value){
									props.setAttributes( { channelWidthHelpMessage: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="WiFi kanal (5 ghz)"
								checked={props.attributes.showChannel5}
								help="Check the box if you want to show the field"
								onChange={ function(value){
											props.setAttributes( { showChannel5: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Key for field value"
								)}
								value={props.attributes.channelKey5}
								onChange={ function(value){
									props.setAttributes( { channelKey5: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.channelLabel5}
								onChange={ function(value){
									props.setAttributes( { channelLabel5: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Help message for field"
								)}
								value={props.attributes.channelHelpMessage5}
								onChange={ function(value){
									props.setAttributes( { channelHelpMessage5: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="WiFi bandwidth"
								checked={props.attributes.showChannelWidth5}
								help="Check the box if you want to show the field"
								onChange={ function(value){
											props.setAttributes( { showChannelWidth5: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Key for field value"
								)}
								value={props.attributes.channelWidthKey5}
								onChange={ function(value){
									props.setAttributes( { channelWidthKey5: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for field"
								)}
								value={props.attributes.channelWidthLabel5}
								onChange={ function(value){
									props.setAttributes( { channelWidthLabel5: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label={__(
									"Help message for field"
								)}
								value={props.attributes.channelWidthHelpMessage5}
								onChange={ function(value){
									props.setAttributes( { channelWidthHelpMessage5: value } );
								}}
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
				
				</BlockControls>

				{/* here starts the edit block itself*/}
				<div id="internet-info-container" class="content center-column" style={{'font-size': props.attributes.fontSize}}>
						<div class="container">
							<div class="columns is-multiline is-mobile">
								{/** Product name row*/}
								{ props.attributes.showProductNameRow ?
										<div class="column is-4">
											<div class="has-text-weight-semibold " for="internet_productname">{props.attributes.showProductNameLabel}</div>
										</div> : ''
								}
								{ props.attributes.showProductNameRow ?
										<div class="column is-8">
											<div id="internet_productname" class="is-family-secondary right ">Internet 300/300 Mbit</div>
										</div> : ''
								}								

                                {/** Wifi row */}
								{ props.attributes.showWifi ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_wifi">{props.attributes.wifiLabel} <span class="dashicons dashicons-editor-help"></span></div>
										</div> : ''
								}
								{ props.attributes.showWifi ?
										<div class="column is-8">
											<div class="is-family-secondary right "><select id="internet_wifi" name="internet_wifi">
                                                                                        <option value="1">Nej</option>
                                                                                        <option value="2">Ja</option>
                                                                                    </select></div>
										</div> : ''
								}
                                
								{/** SSID row */}
								{ props.attributes.showSSID ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_ssid">{props.attributes.SSIDLabel} <span class="dashicons dashicons-editor-help"></span></div>
										</div> : ''
								}
								{ props.attributes.showSSID ?
										<div class="column is-8">
											<div class="is-family-secondary right "><input type="text" id="internet_ssid" class="is-family-secondary right " value="wifibrugernavn"/></div>
										</div> : ''
								}

                                {/** SSID5 row */}
								{ props.attributes.showSSID5 ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_ssid">{props.attributes.SSID5Label} <span class="dashicons dashicons-editor-help"></span></div>
										</div> : ''
								}
								{ props.attributes.showSSID5 ?
										<div class="column is-8">
											<div class="is-family-secondary right "><input type="text" id="internet_ssid" class="is-family-secondary right " value="wifibrugernavn5"/></div>
										</div> : ''
								}
                                
								{/** wpa-psk row */}
								{ props.attributes.showWpaPsk ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_wpa_psk">{props.attributes.wpaPskLabel} <span class="dashicons dashicons-editor-help"></span></div>
										</div> : ''
								}
								{ props.attributes.showWpaPsk ?
										<div class="column is-8">
											<div class="is-family-secondary right "><input type="text" id="internet_wpa_psk" value="hemm3lig.Kode"/></div>
										</div> : ''
								}
								{/** wpa-psk5 row */}
								{ props.attributes.showWpaPsk5 ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_wpa_psk5">{props.attributes.wpaPsk5Label} <span class="dashicons dashicons-editor-help"></span></div>
										</div> : ''
								}
								{ props.attributes.showWpaPsk5 ?
										<div class="column is-8">
											<div class="is-family-secondary right "><input type="text" id="internet_wpa_psk5" value="hemm3lig.Kode"/></div>
										</div> : ''
								}
								{/** wifi-channel row */}
								{ props.attributes.showChannel ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_channel">{props.attributes.channelLabel} <span class="dashicons dashicons-editor-help"></span></div>
										</div> : ''
								}
								{ props.attributes.showChannel ?
										<div class="column is-8">
											<div class="is-family-secondary right ">
												<select id="internet_channel" name="internet_channel">
													<option value="auto">Auto</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
												</select>
											</div>
										</div> : ''
								}								
								{/** wifi-channelwidth row */}
								{ props.attributes.showChannelWidth ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_channelwidth">{props.attributes.channelWidthLabel} <span class="dashicons dashicons-editor-help"></span></div>
										</div> : ''
								}
								{ props.attributes.showChannelWidth ?
										<div class="column is-8">
											<div class="is-family-secondary right ">
												<select id="internet_channelwidth">
													<option value="20Mhz">20Mhz</option>
													<option value="40Mhz">40Mhz</option>
												</select>
											</div>
										</div> : ''
								}	
								{/** wifi-channel row */}
								{ props.attributes.showChannel5 ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_channel5">{props.attributes.channelLabel5} <span class="dashicons dashicons-editor-help"></span></div>
										</div> : ''
								}
								{ props.attributes.showChannel5 ?
										<div class="column is-8">
											<div class="is-family-secondary right ">
												<select id="internet_channel5">
													<option value="auto">Auto</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
												</select>
											</div>
										</div> : ''
								}								
								{/** wifi-channelwidth row */}
								{ props.attributes.showChannelWidth5 ?
										<div class="column is-4">
											<div class="has-text-weight-semibold "for="internet_channelwidth5">{props.attributes.channelWidthLabel5} <span class="dashicons dashicons-editor-help"></span></div>
										</div> : ''
								}
								{ props.attributes.showChannelWidth5 ?
										<div class="column is-8">
											<div class="is-family-secondary right ">
												<select id="internet_channelwidth5">
													<option value="20Mhz Only">20Mhz Only</option>
													<option value="Auto (20 or 40Mhz)">Auto (20 or 40Mhz)</option>
													<option value="Auto (20, 40 or 40Mhz)">Auto (20, 40 or 40Mhz)</option>
												</select>
											</div>
										</div> : ''
								}	
															
							</div>
						</div>	
				</div>		
			</div>
			
		);	
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div id="modem-data-container" className={ props.className } data-attributes={JSON.stringify(props.attributes)} style={{'font-size': props.attributes.fontSize}}>
				<div id="mdcontent" class="content center-column">

                        
                </div>
			</div>
			
		);
 }

});