/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    BlockControls,
	InnerBlocks,
	useBlockProps,
    InspectorControls,
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    PanelBody,
	PanelRow,
    CheckboxControl,
	FontSizePicker,
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-addons-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Addons-block' ), // Block title.
	icon: 'groups', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		showBullguard: {type: 'boolean', default: true},
		showBullguardIcon: {type: 'boolean', default: true},
		showFSecure: {type: 'boolean', default: true},
		fontSize: {type: 'int', default: 16}
		
	},

	edit: ( props ) => {
		
		return (

			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						<PanelRow>
							<CheckboxControl
								label="Bullguard"
								checked={props.attributes.showBullguard}
								help="Check the box if you want to show the Bullguard tile"
								onChange={ function(value){
											props.setAttributes( { showBullguard: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
							label="Vis Bullguard logo"
							checked={props.attributes.showBullguardIcon}
							help="Check the box if you want to show the Bullguard icon, or rather insert a block with image/text"
							onChange={ function(value){
										props.setAttributes( { showBullguardIcon: value } );
									}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="F-Secure"
								checked={props.attributes.showFSecure}
								help="Check the box if you want to show the F-secure tile"
								onChange={ function(value){
											props.setAttributes( { showFSecure: value } );
										}}
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
					
				</BlockControls>


				<div class="container columns is-centered" style={{'font-size': props.attributes.fontSize}}>
					{ props.attributes.showBullguard  && props.attributes.showBullguardIcon ? 
						<div class="column" id="bullguard_container">
							<div><img style={{height: "128px", width: "auto"}} src="/wp-content/plugins/panther-selfservice-block/images/bullguard-logo.png"></img></div>
							<div><button class="bullguard-order-button" >Bestil</button></div>
						</div> : ''
					}
					{ props.attributes.showBullguard  && !props.attributes.showBullguardIcon ? 
						<div class="column" id="bullguard_container">
							<InnerBlocks/>
							<div><button class="bullguard-order-button" >Bestil</button></div>
						</div> : ''
					}
					{ props.attributes.showFSecure ? 
						<div class="column" id="fsecure_container">
							<img style={{height: "128px", width: "auto"}} src="/wp-content/plugins/panther-selfservice-block/images/F-Secure_vertical-logo_RGB_blue.png"></img>
							<button class="fsecure-order-button" type="button">Bestil</button>			
						</div> : ''
					}
				</div>
			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div class="container columns is-centered" className={ props.className } style={{'font-size': props.attributes.fontSize}}>
				{ props.attributes.showBullguard && props.attributes.showBullguardIcon ? 
						<div class="column" id="bullguard_container">
								<div class="columns is-centered"><img class="column is-2 is-centered" style={{height: "128px", width: "auto"}} src="/wp-content/plugins/panther-selfservice-block/images/bullguard-logo.png"></img></div>
								<div class="columns is-centered"><button class="column is-2 is-hidden-touch bullguard-order-button"  type="button" style={{display: 'none'}}>Bestil</button></div>			
								<div class="columns is-centered"><button class="column is-6 is-mobile is-hidden-desktop bullguard-order-button"  type="button" style={{display: 'none'}}>Bestil</button></div>			
						</div> : ''
				}
				{ props.attributes.showBullguard && !props.attributes.showBullguardIcon ? 
						<div class="column" id="bullguard_container">
								<InnerBlocks.Content/>
								<div class="columns is-centered"><button class="column is-2 is-hidden-touch bullguard-order-button"  type="button" style={{display: 'none'}}>Bestil</button></div>			
								<div class="columns is-centered"><button class="column is-6 is-mobile is-hidden-desktop bullguard-order-button"  type="button" style={{display: 'none'}}>Bestil</button></div>			
						</div> : ''
				}

				{ props.attributes.showFSecure ? 
						<div class="column" id="fsecure_container">
							<div class="columns is-centered">
								<img class="column is-2 is-centered" style={{height: "128px", width: "auto"}} src="/wp-content/plugins/panther-selfservice-block/images/F-Secure_vertical-logo_RGB_blue.png"></img></div>
								<div class="columns is-centered"><button class="column is-2 is-hidden-touch fsecure-order-button" type="button" style={{display: 'none'}}>Bestil</button></div>			
								<div class="columns is-centered"><button class="column is-6 is-offset-2 is-mobile is-hidden-desktop fsecure-order-button-mobile" type="button" style={{display: 'none'}}>Bestil</button></div>			
						</div> : ''
					}
			</div>
			
		);
	}
});