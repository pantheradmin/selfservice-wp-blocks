/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    InnerBlocks,
	useBlockProps,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
	CheckboxControl,
	FontSizePicker,
    SelectControl,
    Dashicon,
    Tooltip,
    TextControl
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-overview-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'overview-block' ), // Block title.
	icon: 'dashboard', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
    attributes: {
        fontSize: { type: 'int', default: 16 },
        internetlinktext: { type: 'string', default: 'Gå til internet' },
        tvlinktext: { type: 'string', default: 'Gå til tv' },
        paymentslinktext: { type: 'string', default: 'Se alle betalinger' },
        internetpage: { type:'string', default: '/internet' },
        tvpage: { type:'string', default: '/tv' },
        paymentspage: { type:'string', default: '/regninger-og-betalingslosning' },
	},

	edit: (props) => {
		
		return (

			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
					
				</BlockControls>

                <div class={"overview-container columns"} style={{ 'font-size': props.attributes.fontSize }}>
                    <div class='overview-internet column is-4'>
                        <div class='overview-internet-container'>
                            <div class='overview-internet-header'>
                                <div class='overview-internet-icon'></div>
                                <h3>Din internet aftale</h3>
                            </div>
                            <hr/>
                            <div class='overview-internet-content'>
                                <div class='overview-internet-speed'>

                                </div>
                                <div class='overview-internet-status'>
                                </div>
                            </div>
                        </div>
                    <div class='overview-internet-link'><a href={props.attributes.internetpage}><button>{ props.attributes.internetlinktext }</button></a></div>
                </div>
                    <div class='overview-tv column is-4'>
                        <div class='overview-tv-container'>
                            <div class='overview-tv-header'>
                                <div class='overview-tv-icon'></div>
                                <h3>Din TV pakke</h3>
                            </div>
                            <hr/>
                            <div class='overview-tv-content'>
                                <div class='overview-tv-packet'>

                                </div>
                            </div>
                            
                        </div>
                        <div class='overview-tv-link'><a href={props.attributes.tvpage}><button>{ props.attributes.tvlinktext }</button></a></div>
                </div>
                    <div class='overview-payments column is-4'>
                        <div class='overview-payments-container'>
                            <div class='overview-payments-header'>
                                <div class='overview-payments-icon'></div>
                                <h3>Kommende betaling</h3>
                            </div>
                            <hr/>
                            <div class='overview-payments-content'>
                            </div>
                        </div>
                    <div class='overview-payments-link'><a href={props.attributes.paymentspage}><button>{ props.attributes.paymentslinktext }</button></a></div>
                </div>
            </div>
                </div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
            <div class={"overview-container columns"} style={{ 'font-size': props.attributes.fontSize }}>
                    <div class='overview-internet column is-4'>
                        <div class='overview-internet-container'>
                            <div class='overview-internet-header'>
                                <div class='overview-internet-icon'></div>
                                <h3>Din internet aftale</h3>
                            </div>
                            <hr/>
                            <div class='overview-internet-content'>
                                <div class='overview-internet-speed'>

                                </div>
                                <div class='overview-internet-status'>
                                </div>
                            </div>
                        </div>
                    <div class='overview-internet-link'><a href={props.attributes.internetpage}><button>{ props.attributes.internetlinktext }</button></a></div>
                </div>
                    <div class='overview-tv column is-4'>
                        <div class='overview-tv-container'>
                            <div class='overview-tv-header'>
                                <div class='overview-tv-icon'></div>
                                <h3>Din TV pakke</h3>
                            </div>
                            <hr/>
                            <div class='overview-tv-content'>
                                <div class='overview-tv-packet'>

                                </div>
                            </div>
                            
                        </div>
                        <div class='overview-tv-link'><a href={props.attributes.tvpage}><button>{ props.attributes.tvlinktext }</button></a></div>
                </div>
                    <div class='overview-payments column is-4'>
                        <div class='overview-payments-container'>
                            <div class='overview-payments-header'>
                                <div class='overview-payments-icon'></div>
                                <h3>Kommende betaling</h3>
                            </div>
                            <hr/>
                            <div class='overview-payments-content'>
                                <div class="overview-payments-amount"></div>
                                <div class="overview-payments-duedate"></div>
                                <div class="overview-payments-paymentform"></div>
                            </div>
                        </div>
                    <div class='overview-payments-link'><a href={props.attributes.paymentspage}><button>{ props.attributes.paymentslinktext }</button></a></div>
                </div>
            </div>
			
		);
	}
});