/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component, useState } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
    SelectControl,
	CheckboxControl,
	FontSizePicker,
    Dashicon,
    Tooltip,
    TextControl,
	ToolsPanel,
	ToolsPanelItem
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-dawa-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'DAWA Block' ), // Block title.
	icon: 'calendar-alt', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-dawa-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
        zipLimit: {type: 'string', default: ''},
		whitelistAddress: {type: 'string', default: ''}
	},

	edit: (props) => {
		
		return (
			<div classname={ props.className }>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
                    {/********************************/}
                    
                    <PanelBody title="Kommasepareret liste af postnumre">
                        <PanelRow>
							<TextControl 
                                className="dawa-zip-numbers"
								value={props.attributes.zipLimit}
								onChange={ function(value){
									props.setAttributes( { zipLimit: value } );
								}}
							/>
                        </PanelRow>
                    </PanelBody>

                    <PanelBody title="API url hvis adressedata skal hentes andet sted fra">
                        <PanelRow>
							<TextControl 
                                className="whitelist-address"
								value={props.attributes.whitelistAddress}
								onChange={ function(value){
									props.setAttributes( { whitelistAddress: value } );
								}}
							/>
                        </PanelRow>
                    </PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
				
				</BlockControls>

				{/* here starts the edit block itself*/}
				<div id="pa-dawa-container" class="content center-column" data-attributes={JSON.stringify(props.attributes)}>
                    <div class="container">
                        <input type="text" id="adresse"/>
						<input type="hidden" id="dawaid"/>
						<input type="hidden" id="nodelivery" value="0"/>
				    </div>		
			    </div>
            </div>
		);	
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
            <div id="pa-dawa-container" className={ props.className } data-attributes={JSON.stringify(props.attributes)}>
                <div class="container">
                    <input type="text" id="adresse" name="adresse"/>
					<input type="hidden" id="dawaid" name="dawaid"/>
					<input type="hidden" id="nodelivery" value="0"/>
					</div>	
            </div>
			
		);	
 }

});
