/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */
import './login_block/block.js';
import './customer_info/block.js';
import './customer_invoices/block.js';
import './customer_products/block.js';
import './addons_block/block.js';
import './tickets_block/block.js';
import './customer_cardpayment/block.js';
import './internet_info/block.js';
import './wifi_data/block.js';
import './dawa_block/block.js';
import './modem_data/block.js';
import './addon_product_block/block.js';
import './order_state_block/block.js';
import './technician_block/block.js';
import './tv_allente_block/block.js';
import './my_overview_block/block.js';
import './attach_social_login/block.js';
import './subscription_block/block.js';
import './pa_calendar_block/block.js';
import './signup_input/block.js';
import './signup_page_change/block.js';
import './conax_block/block.js';
import './basket_block/block.js';
import './cancel_product_block/block.js';
import './sbbu_block/block.js';
import './webtv_block/block.js';
import './mix_config_block/block.js';
import './overview_block/block.js';
