/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component, useState } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
    SelectControl,
	CheckboxControl,
	FontSizePicker,
    Dashicon,
    Tooltip,
    TextControl,
	ToolsPanel,
	ToolsPanelItem
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-calendar-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Calendar Block' ), // Block title.
	icon: 'calendar-alt', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-calendar-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
        calendarType: {type: 'string', default: 'Opgradering'},

		includeWeekends: {type: 'boolean', default: false},
        includeHolidays: {type: 'boolean', default: false},
        
		inSoManyEnabled: {type: 'boolean', default: false},
        inSoMany: {type: 'int', default: 1},
        inSoManyUnit: {type: 'string', default: 'D'},

		theNthEnabled: {type: 'boolean', default: false},
		theNth: {type: 'int', default: 1},
        theNthUnits: {type: 'string', default: 'D'},
        inXNextComing: {type: 'int', default: 1},
        inXNextComingUnit: {type: 'string', default: 'M'},
        
        untilDistanceToNextComing: {type: 'int', default: 1},
        untilDistanceToNextComingUnit: {type: 'string', default: 'D'},

		fixedDistanceEnabled: {type: 'boolean', default: false},
        fixedDistanceFromStartDate: {type: 'int', default: 1},
        fixedDistanceFromStartDateUnit: {type: 'string', default: 'M'},
       
        showDatesUntil: {type: 'int', default: 1},
        showDatesUntilUnit: {type: 'string', default: 'Y'},

        fixedYearlyDates: {type: 'string', default: ''},
        closedDates: {type: 'string', default: ''},
	},

	edit: (props) => {
		
		return (
			<div classname={ props.className }>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
                <PanelBody title="Kalender type" initialOpen={ true }>
                        <PanelRow>
							<SelectControl
                                value={props.attributes.calendarType}
                                options={ [
                                    { label: 'Opstart', value: 'Opstart' },
                                    { label: 'Opsigelse', value: 'Opsigelse' },
                                    { label: 'Opgradering', value: 'Opgradering' },
                                    { label: 'Nedgradering', value: 'Nedgradering' }
                                ] }
                                onChange={ function(value){
									props.setAttributes( { calendarType: value } );
								}}
                            />
						</PanelRow>
                        
                    </PanelBody>
					<PanelBody title="Weekender og helligdage" initialOpen={ false }>
						<PanelRow>
							<CheckboxControl
								label="inkluder Weekender"
								checked={props.attributes.includeWeekends}
								help="Datoer i weekender er mulige at vælge."
								onChange={ function(value){
											props.setAttributes( { includeWeekends: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="inkluder helligdage"
								checked={props.attributes.includeHolidays}
								help="Datoer på helligdage er mulige at vælge."
								onChange={ function(value){
											props.setAttributes( { includeHolidays: value } );
										}}
							/>
						</PanelRow>
                    </PanelBody>
                    <PanelBody title="Vis datoer efter" initialOpen={ false }>
						<PanelRow>
							<CheckboxControl
								label="Aktiver"
								checked={props.attributes.inSoManyEnabled}
								onChange={ function(value){
											props.setAttributes( { inSoManyEnabled: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
                        <table><tr><td>
							<TextControl 
								value={props.attributes.inSoMany}
                                className="pa-calendar-number"
								onChange={ function(value){
									props.setAttributes( { inSoMany: value } );
								}}
							/>
                            </td><td>
                            <SelectControl
                                value={props.attributes.inSoManyUnit}
                                className="pa-calendar-unit"
                                options={ [
                                    { label: 'Dage', value: 'D' },
                                    { label: 'Uger', value: 'W' },
                                    { label: 'Måneder', value: 'M' },
                                    { label: 'År', value: 'Y' }
                                ] }
                                onChange={ function(value){
									props.setAttributes( { inSoManyUnit: value } );
								}}
                            /></td></tr>
                            </table>
						</PanelRow>
                    </PanelBody>
                    
                    {/********************************/}
                    
                    <PanelBody title="Vis datoer i faste intervaller" initialOpen={ false }>
						<PanelRow>
							<CheckboxControl
								label="Aktiver"
								checked={props.attributes.theNthEnabled}
								onChange={ function(value){
											props.setAttributes( { theNthEnabled: value } );
										}}
							/>
						</PanelRow>
						<PanelRow>
                            <table>
                            <tr><td>Den...</td></tr>
                            <tr><td><table><tr><td>
							<TextControl 
                                className="pa-calendar-number"
								value={props.attributes.theNth}
								onChange={ function(value){
									props.setAttributes( { theNth: value } );
								}}
							/>
                            </td><td>
                            <SelectControl
                                className="pa-calendar-unit"
                                value={props.attributes.theNthUnits}
                                options={ [
                                    { label: 'Dag', value: 'D' },
                                    { label: 'Måned', value: 'M' },
                               ] }
                                onChange={ function(value){
									props.setAttributes( { theNthUnits: value } );
								}}
                            /></td></tr>
                            </table>
                            </td></tr></table>
                        </PanelRow>
                        <PanelRow>
                            <table>
                            <tr><td>I hver...</td></tr>
                            <tr><td><table><tr><td>
							<TextControl 
                                className="pa-calendar-number"
								value={props.attributes.inXNextComing}
								onChange={ function(value){
									props.setAttributes( { inXNextComing: value } );
								}}
							/>
                            </td><td>
							<SelectControl
                                className="pa-calendar-unit"
                                value={props.attributes.inXNextComingUnit}
                                options={ [
                                    { label: 'Måned', value: 'M' },
                                    { label: 'Kvartal', value: 'Q' },
                                    { label: 'År', value: 'Y' }
                               ] }
                                onChange={ function(value){
									props.setAttributes( { inXNextComingUnit: value } );
								}}
                            />
                            </td></tr>
                            </table>
                            </td></tr></table>
                        </PanelRow>
                        <PanelRow>
                            <table>
                            <tr><td>Indtil afstand til næste er...</td></tr>
                            <tr><td><table><tr><td>
                            <TextControl 
                                className="pa-calendar-number"
								value={props.attributes.untilDistanceToNextComing}
								onChange={ function(value){
									props.setAttributes( { untilDistanceToNextComing: value } );
								}}
							/>
                            </td><td>
							<SelectControl
                                className="pa-calendar-unit"
                                value={props.attributes.untilDistanceToNextComingUnit}
                                options={ [
                                    { label: 'Dage', value: 'D' },
                                    { label: 'Uge', value: 'W' },
                                    { label: 'Måneder', value: 'M' },
                                    { label: 'År', value: 'Y' }
                               ] }
                                onChange={ function(value){
									props.setAttributes( { untilDistanceToNextComingUnit: value } );
								}}
                            /></td></tr>
                            </table>
                            </td></tr></table>
						</PanelRow>
                        
                        {/*********************/}
                    </PanelBody>
                    <PanelBody title="Fast afstand fra startdato" initialOpen={ false }>
                        <PanelRow>
                            <CheckboxControl
                                label="Aktiver"
                                checked={props.attributes.fixedDistanceEnabled}
                                onChange={ function(value){
                                            props.setAttributes( { fixedDistanceEnabled: value } );
                                        }}
                            />
                        </PanelRow>
                       <PanelRow>
                        <table><tr><td>
							<TextControl 
                                className="pa-calendar-number"
								value={props.attributes.fixedDistanceFromStartDate}
								onChange={ function(value){
									props.setAttributes( { fixedDistanceFromStartDate: value } );
								}}
							/>
                            </td><td>
							<SelectControl
                                className="pa-calendar-unit"
                                value={props.attributes.fixedDistanceFromStartDateUnit}
                                options={ [
                                    { label: 'Dage', value: 'D' },
                                    { label: 'Uger', value: 'W' },
                                    { label: 'Måneder', value: 'M' },
                                    { label: 'År', value: 'Y' }
                               ] }
                                onChange={ function(value){
									props.setAttributes( { fixedDistanceFromStartDateUnit: value } );
								}}
                            /></td></tr>
                            </table>
						</PanelRow>
                    </PanelBody>
                    <PanelBody title="Vis datoer så langt frem" initialOpen={ false }>
                        <PanelRow>
                            <table><tr><td>
							<TextControl
                                className="pa-calendar-number"
								value={props.attributes.showDatesUntil}
								onChange={ function(value){
									props.setAttributes( { showDatesUntil: value } );
								}}
							/>
                            </td><td>
							<SelectControl
                                className="pa-calendar-unit"
                                value={props.attributes.showDatesUntilUnit}
                                options={ [
                                    { label: 'Dage', value: 'D' },
                                    { label: 'Uger', value: 'W' },
                                    { label: 'Måneder', value: 'M' },
                                    { label: 'År', value: 'Y' }
                               ] }
                                onChange={ function(value){
									props.setAttributes( { showDatesUntilUnit: value } );
								}}
                            /></td></tr>
                            </table>
						</PanelRow>
                    </PanelBody>
                    <PanelBody title="Faste datoer" initialOpen={ false }>
                        <PanelRow>
							<TextControl 
								label="Faste årlige datoer"
								value={props.attributes.fixedYearlyDates}
                                help="kommasepareret liste datoformat: dd/mm"
								onChange={ function(value){
									props.setAttributes( { fixedYearlyDates: value } );
								}}
							/>
						</PanelRow>
                        <PanelRow>
							<TextControl 
								label="Reserverede datoer"
								value={props.attributes.closedDates}
                                help="kommasepareret liste datoformat: dd/mm"
								onChange={ function(value){
									props.setAttributes( { closedDates: value } );
								}}
							/>
						</PanelRow>
                    </PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
				
				</BlockControls>

				{/* here starts the edit block itself*/}
				<div id="pa-calendar-container" class="content center-column">
						<div class="container">
                        Ingen visning.
						</div>	
				</div>		
			</div>
			
		);	
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div id={"pa-calendar-container-" + props.attributes.calendarType} className={ props.className } data-attributes={JSON.stringify(props.attributes)} data-calendar-type={ props.attributes.calendarType }>
            </div>
			
		);	
 }

});
