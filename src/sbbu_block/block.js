/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    InnerBlocks,
	useBlockProps,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
	CheckboxControl,
	FontSizePicker,
    SelectControl,
    Dashicon,
    Tooltip,
    TextControl
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-sbbu-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'sbbu-block' ), // Block title.
	icon: 'media-document', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
    attributes: {
        fontSize: { type: 'int', default: 16 },
        providername: { type: "string", default: "" },
	},

	edit: (props) => {
		
		return (

			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
                    <PanelBody title="General settings">
                        <PanelRow>
                            <TextControl
                                label="Provider name"
                                value={props.attributes.providername}
                                onChange={(providername) => props.setAttributes({ providername })}
                            />
                        </PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
					
				</BlockControls>

                <div class="container columns is-centered" style={{ 'font-size': props.attributes.fontSize }}>
                    <form id="sbbuform" action="#/wp-json/pantheradmin/v1/sendsbbu" method="POST" style={{ "display": "flex", "flex-direction":"column" }}>
                        <label for="name">Navn</label> <input id="sbbu_name" type="Text" name="name"></input>
                        { /* if business START*/ }
                        <label for="entitled">Tegningsberettiget</label> <input id="sbbu_entitled"type="Text" name="entitled"></input>
                        <label for="cvr">Cvr</label> <input id="sbbu_cvr" type="Text" name="cvr"></input>
                        { /* if business END*/}
                        <label for="address">Adresse</label> <input id="sbbu_address"type="Text" name="address"></input>
                        
                    <select required id="sbbu_current_provider" name="current_provider">
                        <option></option>
                        <option value="AURA Fiber A/S">AURA Fiber A/S</option>
                        <option value="BOLIG-NET A/S">BOLIG-NET A/S</option>
                        <option value="Dansk Kabel TV A/S">Dansk Kabel TV A/S</option>
                        <option value="DanskNET ">DanskNET </option>
                        <option value="Energi Fyn (Waoo)">Energi Fyn (Waoo)</option>
                        <option value="EWII Bredbånd">EWII Bredbånd</option>
                        <option value="Fastspeed">Fastspeed</option>
                        <option value="Fibia (Kazoom)">Fibia (Kazoom)</option>
                        <option value="Fibia (Waoo)">Fibia (Waoo)</option>
                        <option value="Global Connect (SP)">Global Connect (SP)</option>
                        <option value="HAS - Hjørring Antenneselskab">HAS - Hjørring Antenneselskab</option>
                        <option value="Hiper">Hiper</option>
                        <option value="Jetnet">Jetnet</option>
                        <option value="Kviknet">Kviknet</option>
                        <option value="Mit Tele">Mit Tele</option>
                        <option value="Norlys Erhverv">Norlys Erhverv</option>
                        <option value="Norlys Privat">Norlys Privat</option>
                        <option value="OdderNet">OdderNet</option>
                        <option value="TDC Erhverv">TDC Erhverv</option>
                        <option value="Telenor">Telenor</option>
                        <option value="Telia Consumer">Telia Consumer</option>
                        <option value="Telia Enterprise">Telia Enterprise</option>
                        <option value="YouSee">YouSee</option>
                    </select>
                        <label for="current_provider_no">Kundenummer hos nuværende udbyder</label> <input id="sbbu_current_provider_no" type="Text" name="current_provider_no"></input>

                        <input required name="confirm" id="sbbu_confirm" type="checkbox" style={{"margin-top":"10px"}}></input>
                        <label for="confirm">Jeg giver tilladelse til, at {props.attributes.providername} på mine vegne opsiger mit internetabonnement hos min nuværende udbyder og flytter internetforbindelsen til et nyt abonnement hos {props.attributes.providername}. </label>

                        <input name="no_marketing" id="sbbu_no_marketing" type="checkbox" style={{"margin-top":"10px"}}></input>
                        <label for="no_marketing">Min nuværende internetudbyder må ikke kontakte mig med markedsføring om internet.</label>

                        <button id="sbbu_send_button">Send fuldmagt</button>
                    </form>
                </div>
                </div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
            <div className={"sbbu-container container columns is-centered is-mobile " + props.className} style={{ 'font-size': props.attributes.fontSize }}>
                <form id="sbbuform" action="#"> 
                    <input type="hidden" name="customer-token" ></input>
                    <label for="name">Navn</label> <input required id="sbbu_name" type="Text" name="name"></input>
                    { /* if business START*/ }
                    <label for="entitled">Tegningsberettiget ( Kun relevant hvis erhverv )</label> <input id="sbbu_entitled"type="Text" name="entitled"></input>
                    <label for="cvr">Cvr ( Kun relevant hvis erhverv )</label> <input id="sbbu_cvr" type="Text" name="cvr"></input>
                    { /* if business END*/}
                    <label for="address">Adresse</label> <input disabled id="sbbu_address"type="Text" name="address"></input>
                    
                    <label for="current_provider">Nuværende udbyder</label>
                    <select required id="sbbu_current_provider" name="current_provider">
                        <option></option>
                        <option value="AURA Fiber A/S">AURA Fiber A/S</option>
                        <option value="BOLIG-NET A/S">BOLIG-NET A/S</option>
                        <option value="Dansk Kabel TV A/S">Dansk Kabel TV A/S</option>
                        <option value="DanskNET ">DanskNET </option>
                        <option value="Energi Fyn (Waoo)">Energi Fyn (Waoo)</option>
                        <option value="EWII Bredbånd">EWII Bredbånd</option>
                        <option value="Fastspeed">Fastspeed</option>
                        <option value="Fibia (Kazoom)">Fibia (Kazoom)</option>
                        <option value="Fibia (Waoo)">Fibia (Waoo)</option>
                        <option value="Global Connect (SP)">Global Connect (SP)</option>
                        <option value="HAS - Hjørring Antenneselskab">HAS - Hjørring Antenneselskab</option>
                        <option value="Hiper">Hiper</option>
                        <option value="Jetnet">Jetnet</option>
                        <option value="Kviknet">Kviknet</option>
                        <option value="Mit Tele">Mit Tele</option>
                        <option value="Norlys Erhverv">Norlys Erhverv</option>
                        <option value="Norlys Privat">Norlys Privat</option>
                        <option value="OdderNet">OdderNet</option>
                        <option value="TDC Erhverv">TDC Erhverv</option>
                        <option value="Telenor">Telenor</option>
                        <option value="Telia Consumer">Telia Consumer</option>
                        <option value="Telia Enterprise">Telia Enterprise</option>
                        <option value="YouSee">YouSee</option>
                    </select>
                    
                    <label for="current_provider_no">Kundenummer hos nuværende udbyder</label> <input required id="sbbu_current_provider_no" type="Text" name="current_provider_no"></input>

                    <input required name="confirm" value="1" id="sbbu_confirm" type="checkbox"></input>
                    
                    <label for="confirm"> Jeg giver tilladelse til, at {props.attributes.providername} på mine vegne opsiger mit internetabonnement hos min nuværende udbyder og flytter internetforbindelsen til et nyt abonnement hos {props.attributes.providername}.</label>
                    <br />
                    
                    <input name="no_marketing" value="1" id="sbbu_no_marketing" type="checkbox" style={{"margin-top":"10px"}}></input>
                    <label for="no_marketing"> Min nuværende internetudbyder må ikke kontakte mig med markedsføring om internet.</label>
                    <br />
                    
                    <button type="button" id="sbbu_send_button">Send fuldmagt</button>
                </form>
			</div>
			
		);
	}
});