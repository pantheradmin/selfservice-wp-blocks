/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
	InnerBlocks,
    URLInput
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
	NumberControl,
    TextareaControl,
    CheckboxControl,
    Dashicon,
    Tooltip,
    TextControl,
	FontSizePicker
} from "@wordpress/components";
import axios from 'axios';

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-subscription-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Subscription-block' ), // Block title.
	icon: 'products', // Block icon from Dashicons ? https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category ? Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block ? CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		fontSize: {type: 'int', default: 16},
		amountOfProducts: {type: 'int', default: 0},
		products: {type: 'string', default: '{}'},
		productType: {type: "string", default: "TV pakke"},
		changeproductbtntext: {type: "string", default: "Skift tv pakke"},
		showCancelSubscriptionButton: {type: "bolean", default: true},
		cancelSubscriptionButtonUrl: {type: "string"},
		cancelSubscriptionButtonText: {type: "string", default: "Opsig tv pakke"},
        confirmTitle: { type: "string", default: "Bestilling" },
        pricePrMonth: { type: "string", default: "Pris pr måned" },
        priceUnit: { type: "string", default: "kr." },
        commitment: { type: "string", default: "Bindingsperiode" },
        commitmentUnit: { type: "string", default: "mdr." },
        minimumPrice: { type: "string", default: "Minimums pris" },
        pricetext: { type: "string", default: "Pris" },
        receiptTitle: { type: "string", default: "Lagt i kurv" },
        buyButtonText: { type: "string", default: "Køb" },
        subscriptionText: { type: "string", default: "Abonnement" },
        youHaveFollowingProductText: { type: "string", default: "Du har følgende " }

,

        
		
	},

	edit: ( props ) => {
		// Creates a <p class='wp-block-cgb-block-panther-selfservice-block'></p>.

		function createProductsettings(){
			let panels = []
			for (let index = 0; index < props.attributes.amountOfProducts; index++) {
				if(!(index in JSON.parse(props.attributes.products))){
					let tmp = JSON.parse(props.attributes.products)
					tmp[index] = {productId:"", imageUrl: "", description: ""}
					props.setAttributes({products: JSON.stringify(tmp)})
				}
					panels[index] = <PanelBody title={"Product #"+(index+1)+" settings"}>
									<PanelRow>
									<TextControl
										label="Id"
										value={index in JSON.parse(props.attributes.products) ? JSON.parse(props.attributes.products)[index].productId : ''}
										help="The id of the product configuration."
										onChange={ function(value){
											var tmpArray = JSON.parse(props.attributes.products);
											tmpArray[index].productId = value
											props.setAttributes({products: JSON.stringify(tmpArray)})
										}}
									/>
									</PanelRow>
									<PanelRow>
									<TextControl
										label="Description"
										value={index in JSON.parse(props.attributes.products) ? JSON.parse(props.attributes.products)[index].description : ''}
										help="The description of the product."
										onChange={ function(value){
											var tmpArray = JSON.parse(props.attributes.products);
											tmpArray[index].description = value
											props.setAttributes({products: JSON.stringify(tmpArray)})
										}}
									/>
									</PanelRow>
									<PanelRow>
										<TextControl
										label="Image Url"
										value={index in JSON.parse(props.attributes.products) ? JSON.parse(props.attributes.products)[index].imageUrl : ''}
										help="Image shown with product"
										onChange={ function(value){
											var tmpArray = JSON.parse(props.attributes.products);
											tmpArray[index].imageUrl = value
											props.setAttributes({products: JSON.stringify(tmpArray)})
										}}
									/>
									</PanelRow>
									</PanelBody>
		   }
		   return panels
		}

		function createCards(){
			var products = JSON.parse(props.attributes.products);
            var cards = [];
            if (products.length > 0) {
                 for (let index = 0; index < props.attributes.amountOfProducts; index++) {
                    cards[index] = <li class={"item-"+index+" glide__slide"}>
                                    <div className="columns is-centered">
                                        <div className="column is-8">
                                            <div className="card subscription-product-panel" style="{height:props.attributes.panelHeight}" >
                                                <div className={"productid-"+products[index].productId+" is-flex is-align-items-center is-flex-direction-column"}>
                                                    <div class="subscription-card-product-name"></div>
                                                    <div class="subscription-card-product-image"><img src={products[index].imageUrl}></img></div>
                                                    <div class="subscription-card-product-desc">description</div>
                                                    <div class="subscription-card-product-price"></div>
                                                    <div class="subscription-card-product-buy-btn"><button>Køb</button></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </li>
                }
            }
            
			return cards
		}

		return (
			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General">
						<PanelRow>
							<TextControl
										label="Amount of products"
										value={props.attributes.amountOfProducts}
										help="Amount of products in carousel"
										onChange={ function(newValue){
													props.setAttributes( { amountOfProducts: newValue } );
												}}
									/>
						</PanelRow>
						<PanelRow>
							<TextControl
										label="Product type"
										value={props.attributes.productType}
										help="The product which is sold at this site"
										onChange={ function(newValue){
													props.setAttributes( { productType: newValue } );
												}}
									/>
						</PanelRow>
						
						<PanelRow>
							<CheckboxControl
										label="Show cancel subscription button"
										checked={props.attributes.showCancelSubscriptionButton}
										help="Show or hide the button with link to cancel page"
										onChange={ function(newValue){
													props.setAttributes( { showCancelSubscriptionButton: newValue } );
												}}
									/>
						</PanelRow>
						
						<PanelRow>
							<TextControl
										label="Subscription cancel page url"
										value={props.attributes.cancelSubscriptionButtonUrl}
										help="The url to the page with info abount cancelling a subscription"
										onChange={ function(newValue){
													props.setAttributes( { cancelSubscriptionButtonUrl: newValue } );
												}}
									/>
						</PanelRow>
						
						{createProductsettings()}
					</PanelBody>

					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
                                {
                                    name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
                                    name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
                                    name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
                                props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
                    </PanelBody>
                    
                    <PanelBody title="Content texts">
                        <PanelRow>
                            <TextControl
                                            label="Tilte on the confirm screen"
                                            value={props.attributes.confirmTitle}
                                            onChange={ function(newValue){
                                                        props.setAttributes( { confirmTitle: newValue } );
                                                    }}
                                        />
                        </PanelRow>    
                        <PanelRow>
                            <TextControl
                                            label="Price pr month text"
                                            value={props.attributes.pricePrMonth}
                                            onChange={ function(newValue){
                                                        props.setAttributes( { pricePrMonth: newValue } );
                                                    }}
                                        />
                        </PanelRow>    
                        <PanelRow>
                            <TextControl
                                            label="The unit the price is presented in"
                                            value={props.attributes.priceUnit}
                                            onChange={ function(newValue){
                                                        props.setAttributes( { priceUnit: newValue } );
                                                    }}
                                        />
                        </PanelRow>    
                        <PanelRow>
                            <TextControl
                                            label="Commitment text"
                                            value={props.attributes.commitment}
                                            onChange={ function(newValue){
                                                        props.setAttributes( { commitment: newValue } );
                                                    }}
                                        />
                        </PanelRow>    
                        <PanelRow>
                            <TextControl
                                            label="The unit of the commitment period"
                                            value={props.attributes.commitmentUnit}
                                            onChange={ function(newValue){
                                                        props.setAttributes( { commitmentUnit: newValue } );
                                                    }}
                                        />
                        </PanelRow>    
                        <PanelRow>
                            <TextControl
                                            label="Minimum price text"
                                            value={props.attributes.minimumPrice}
                                            onChange={ function(newValue){
                                                        props.setAttributes( { minimumPrice: newValue } );
                                                    }}
                                        />
                        </PanelRow>    
                        <PanelRow>
                            <TextControl
                                            label="Minimum price text"
                                            value={props.attributes.pricetext}
                                            onChange={ function(newValue){
                                                        props.setAttributes( { pricetext: newValue } );
                                                    }}
                                        />
                        </PanelRow>    
                        <PanelRow>
                            <TextControl
                                            label="Title on the receipt screen"
                                            value={props.attributes.receiptTitle}
                                            onChange={ function(newValue){
                                                        props.setAttributes( { receiptTitle: newValue } );
                                                    }}
                                        />
                        </PanelRow>    
                        <PanelRow>
                            <TextControl
                                            label="Text on the buy button"
                                            value={props.attributes.buyButtonText}
                                            onChange={ function(newValue){
                                                        props.setAttributes( { buyButtonText: newValue } );
                                                    }}
                                        />
                        </PanelRow> 
                        <PanelRow>
							<TextControl
										label="Change product button text"
										value={props.attributes.changeproductbtntext}
										help="Text on the change product button"
										onChange={ function(newValue){
													props.setAttributes( { changeproductbtntext: newValue } );
												}}
									/>
                        </PanelRow>
                        <PanelRow>
							<TextControl
										label="Cancel product button text"
										value={props.attributes.cancelSubscriptionButtonText}
										help="Text on the cancel product button"
										onChange={ function(newValue){
													props.setAttributes( { cancelSubscriptionButtonText: newValue } );
												}}
									/>
						</PanelRow>
                        <PanelRow>
							<TextControl
										label="You have the folling product text"
										value={props.attributes.youHaveFollowingProductText}
										onChange={ function(newValue){
													props.setAttributes( { youHaveFollowingProductText: newValue } );
												}}
									/>
                        </PanelRow>
                        <PanelRow>
							<TextControl
										label="Subscription"
										value={props.attributes.subscriptionText}
										onChange={ function(newValue){
													props.setAttributes( { subscriptionText: newValue } );
												}}
									/>
                        </PanelRow>
                        


                    </PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>

				</BlockControls>

				<div class="subscription-container" data-products={props.attributes.products} style={{'font-size': props.attributes.fontSize}}>
					<h3>Current product</h3>
				<div class="subscription-current-product-container">
						<div id="subscription-current-product-info-container" className='is-flex is-flex-direction-column is-align-items-center'>
							<div>Du har følgende {props.attributes.productType}</div>
							<div id="subscription-current-product-name">Produkt 1</div>
							<div id="subscription-current-product-content">Et produkt</div>
							<div className={"columns is-centered"} id="subscription-current-product-content-image"><img src={"https://dummyimage.com/750x750/2f2f30/ffffff&text=Test_billede"} className={"column is-half"}></img></div>
							<div id="subscription-current-product-price">
								<div>Abonnement</div>
									<div id="subscription-current-product-price">199,-</div>
							</div>
							<div><button id="subscription-current-product-change-product">{props.attributes.changeproductbtntext}</button></div>
							{props.attributes.showCancelSubscriptionButton ? <div><a href={props.attributes.cancelSubscriptionButtonUrl}><button id="subsscription-current-product-cancel-product">{props.attributes.cancelSubscriptionButtonText}</button></a></div> : ""}
						</div>
					</div>
						<h3>Product carousel</h3>
					<div class="carousel-container">
						<div id="subscription-carousel" class="carousel columns is-multiline">
							{createCards()}
						</div>
					</div>
						<h3>Confirm page</h3>
					<div class="order-confirm-container">
						<div class="card">
							<div className="columns is-mobile">
								<div className="column is-flex is-align-items-center is-flex-direction-column is-hidden-mobile">
									<div className="order-product-name title is-4">Produkt navn</div>
									<div className="order-product-image"><img style={{height:"256px"}} src="https://dummyimage.com/750x750/2f2f30/ffffff&text=Tv+pakke+indhold+2" alt="dummy image" /></div>
									<div className="order-product-desc">Produkt beskrivelse</div>
								</div>
								<div className="column is-flex is-align-items-center is-flex-direction-column">
									<div className="title is-4">Bestilling</div>
									<div className="block order-product-name">Produkt navn</div>
									<div className="order-price-summary">
									<table className="table is-narrow order-product-summary">
										<tbody>
                                                <tr><td>{props.attributes.pricePrMonth} 150 {props.attributes.priceUnit}</td></tr>
                                                <tr><td>{props.attributes.commitment} 6 {props.attributes.commitmentUnit}</td></tr>
                                                <tr><td>{props.attributes.minimumPrice} 900 {props.attributes.priceUnit}</td></tr>
										</tbody>
									</table>
									</div>
									<div>
										<div>Start dato:</div><div style={{border:"2px dotted #000", 'min-height': "50px"}}><InnerBlocks/></div>
									</div>
									<div>
										<span>Acceptér <a id="order-terms-link">vilkår</a></span><input type="checkbox" name="order-accept-terms" id="order-accept-terms" />
									</div>
									<button className='order-confirm-button' disabled >Bekræft bestilling</button>
								</div>
							</div>
							
						</div>
					</div>
							<h3>Receipt page</h3>
					<div class="order-receipt-container">
                        <div className="title is-4">{props.attributes.receiptTitle}</div>
						<div className="block is-flex is-align-items-center is-flex-direction-column">
							<div id="order-receipt-product">Produkt 1</div>
							<div id="order-receipt-desc">Et produkt</div>
							<div id="order-receipt-image" className='columns is-centered'><img src={"https://dummyimage.com/750x750/2f2f30/ffffff&text=Test_billede"} className='column is-half'></img></div>
							<div id="order-receipt-startdate">Start dato:<span id="order-receipt-startdate-date">02/02/2022</span></div>
							<div id="order-receipt-price-container" className="block is-flex is-align-items-center is-flex-direction-column">
								<div>Abonnement</div>
                                <div><span id="order-receipt-product-price">{props.attributes.minimumPrice} 150,- {props.attributes.priceUnit}</span></div>					
                                <div><span id="order-receipt-commitment">{props.attributes.commitment} 6 {props.attributes.commitmentUnit}</span></div>
							</div>
						</div>
					</div>

				</div>

			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {
		function createCards(){
			var products = JSON.parse(props.attributes.products);
            var cards = [];
            for (let index = 0; index < props.attributes.amountOfProducts; index++) {
                if (!products[index]) { 
                    break;
                }
                    cards[index] = <div class={"item-"+index+" swiper-slide"}>
                                        <div className="columns is-centered">
                                            <div className="column is-8">
                                                <div className="card subscription-product-panel" style={{height:props.attributes.panelHeight}} >
                                                    <div className={"productid-"+products[index].productId+" is-flex is-align-items-center is-flex-direction-column"}>
                                                        <div className="subscription-card-product-name"></div>
                                                        <div className="subscription-card-product-image"><img src={products[index].imageUrl}></img></div>
                                                        <div className="subscription-card-product-desc">{products[index].description}</div>
                                                        <div className="subscription-card-product-price" data-pricetext={props.attributes.pricetext} data-priceunit={props.attributes.priceUnit}></div>												
                                                        <div className="subscription-card-product-buy-btn"><button data-productdesc={products[index].description} data-productid={products[index].productId} data-productimage={products[index].imageUrl} >{props.attributes.buyButtonText}</button></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
            }
			return cards
		}

		return (
            <div class="subscription-container" data-amount-of-products={props.attributes.amountOfProducts} data-products={props.attributes.products} style={{ 'font-size': props.attributes.fontSize }}>
                <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
					<div class="subscription-current-product-container is-hidden">
						<div id="subscription-current-product-info-container" className='is-flex is-flex-direction-column is-align-items-center'>
                        <div>{props.attributes.youHaveFollowingProductText} {props.attributes.productType}:</div>
							<div id="subscription-current-product-name"></div>
							<div id="subscription-current-product-desc"></div>
							<div className={"columns is-centered"} id="subscription-current-product-image"><img className={"column is-half"}></img></div>
							<div id="subscription-current-product-price-container">
                                <div id="subscription-current-product-price-subtext">{props.attributes.subscriptionText}</div>
                                <div id="subscription-current-product-price" data-pricePrMonth={props.attributes.pricePrMonth} data-priceUnit={props.attributes.priceUnit}></div>
							</div>
							<div><button id="subscription-current-product-change-product">{props.attributes.changeproductbtntext}</button></div>
							{props.attributes.showCancelSubscriptionButton ? <div><a href={props.attributes.cancelSubscriptionButtonUrl}><button id="subsscription-current-product-cancel-product">{props.attributes.cancelSubscriptionButtonText}</button></a></div> : ""}
						</div>
					</div>
					
                <div class="swiper carousel-container is-hidden">
                    <div class="swiper-wrapper">
                        {createCards()}
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>

                </div>

					<div class="order-confirm-container is-hidden">
						<div class="card">
							<div className="columns is-mobile">
								<div className="column is-flex is-align-items-center is-flex-direction-column is-hidden-mobile">
									<div className="order-product-name title is-4"></div>
									<div className="order-product-image"><img style={{height:"256px"}}/></div>
									<div className="order-product-desc"></div>
								</div>
								<div className="column is-flex is-align-items-center is-flex-direction-column">
                                <div className="title is-4">{props.attributes.confirmTitle}</div>
									<div className="block order-product-name"></div>
									<div className="order-price-summary">
									<table className="table is-narrow order-product-summary">
										<tbody>
                                            <tr><td>{props.attributes.pricePrMonth}<span id="order-product-price-pr-month"></span> {props.attributes.priceUnit}</td></tr>
                                            <tr><td>{props.attributes.commitment}<span id="order-commitment"></span> {props.attributes.commitmentUnit}</td></tr>
                                        <tr><td>{props.attributes.minimumPrice} <span id="order-minimum-price"></span> {props.attributes.priceUnit}</td></tr>
										</tbody>
									</table>
									</div>
									<div>
										<div>Start dato:</div><InnerBlocks.Content/>
									</div>
									<div>
										<span>Acceptér <a id="order-terms-link">vilkår</a></span><input type="checkbox" name="order-accept-terms" id="order-accept-terms" />
									</div>
									<button className='order-confirm-button' disabled >Bekræft bestilling</button>
								</div>
							</div>
							
						</div>
					</div>

					<div class="order-receipt-container is-hidden">
                    <div className="title is-4">{props.attributes.receiptTitle}</div>
						<div className='title is-5'></div>
						<div className="block is-flex is-align-items-center is-flex-direction-column">
							<div id="order-receipt-product"></div>
							<div id="order-receipt-desc"></div>
							<div id="order-receipt-image" className='columns is-centered'><img className='column is-half'></img></div>
							<div id="order-receipt-startdate">Start dato:<span id="order-receipt-startdate-date"></span></div>
							<div id="order-receipt-price-container" className="block is-flex is-align-items-center is-flex-direction-column">
								<div>Abonnement</div>
								<div><span id="order-receipt-product-price"></span></div>					
								<div><span id="order-receipt-commitment"></span></div>
							</div>
						</div>
					</div>
				</div>
		);
	}
});
