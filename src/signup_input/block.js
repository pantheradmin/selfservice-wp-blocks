/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component, useState } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
    SelectControl,
	CheckboxControl,
	FontSizePicker,
    Dashicon,
    Tooltip,
    TextControl,
	ToolsPanel,
	ToolsPanelItem
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-signup-input', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Signup Input Block' ), // Block title.
	icon: 'calendar-alt', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-signup-input — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
        selectorName: {type: 'string', default: ''},
		selectType: {enum: [ 'select', 'radio', 'checkbox', 'text' ], default: 'radio'},
		selfserviceProds: {type: 'boolean', default: false},
		presentation: {type: 'string', default: '{k} / {n} / {s}'},
        provisioningModel: {type: 'string', default: ''},
        group: {type: 'string', default: ''},
        productIdents: {type: 'string', default: ''},
        productIds: {type: 'string', default: ''},
        preSelectId: {type: 'string', default: ''},
		sortBy: {enum: [ 's', 'f', 'n'], default: 's'},
		sortByDir: {enum: [ 'asc', 'dsc'], default: 'asc'},
		noneOption: {type: 'boolean', default: false},
		noneOptionAbove: {type: 'boolean', default: false},
		noneOptionLabel: {type: 'string', default: ''},
		isCustom: {type: 'boolean', default: false},
        customLabels: {type: 'string', default: ''},
		customRegex: {type: 'string', default: "^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"},
		customErrorMessage: {type: 'string', default: "Indtast venligst en gyldig email"},
	},

	edit: (props) => {
		
		return (
			<div classname={ props.className }>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
                    {/********************************/}
                    
                    <PanelBody title="ABC">
                        <PanelRow>
							<TextControl 
								label="Navn på variablen"
								help="Navnet variablen findes under i process scope"
								value={props.attributes.selectorName}
								onChange={ function(value){
									props.setAttributes( { selectorName: value } );
								}}
							/>
                        </PanelRow>
						<PanelRow>
							<SelectControl
								label="Type"
								value={props.attributes.selectType}
								options={ [
									{ label: 'select', value: 'select' },
									{ label: 'radio', value: 'radio' },
									{ label: 'checkbox', value: 'checkbox' },
									{ label: 'text', value: 'text' },
								] }
								onChange={function(value){
									props.setAttributes( { selectType: value } );
								}}
								__nextHasNoMarginBottom
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Kun selvbetjeningsprodukter"
								help="Kun produkter tilgængelige for selvbtjeningen"
								checked={ props.attributes.selfserviceProds }
								onChange={ function(value){
									props.setAttributes( { selfserviceProds: value } );
								} }
							/>
						</PanelRow>
                        <PanelRow>
							<TextControl 
								label="Præsentation"
								help="Hvordan vises hvert element. Brug gerne HTML {k}: kontrol, {n}: navn, {f}: engangspris, {s}: abonnement"
								value={props.attributes.presentation}
								onChange={ function(value){
									props.setAttributes( { presentation: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Provisioneringsmodel(ler) - kommasepareret"
								help="Provisioneringsmodel(ler) for valgbare produkter"
								value={props.attributes.provisioningModel}
								onChange={ function(value){
									props.setAttributes( { provisioningModel: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Kundegruppe(r) - kommasepareret"
								help="Kundegruppe(r) for valgbare produkter"
								value={props.attributes.group}
								onChange={ function(value){
									props.setAttributes( { group: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Produkt ident(s) - kommasepareret"
								help="Idents for specifikke valgbare produkter"
								value={props.attributes.productIdents}
								onChange={ function(value){
									props.setAttributes( { productIdents: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Produkt id's - kommasepareret"
								help="Id's for specifikke valgbare produkter"
								value={props.attributes.productIds}
								onChange={ function(value){
									props.setAttributes( { productIds: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Forudvalgt produkt ID"
								help="Id for initielt autovalg"
								value={props.attributes.preSelectId}
								onChange={ function(value){
									props.setAttributes( { preSelectId: value } );
								}}
							/>
                        </PanelRow>
						<PanelRow>
							<SelectControl
								label="Sorter efter"
								value={props.attributes.sortBy}
								options={ [
									{ label: 'abonnement', value: 's' },
									{ label: 'engangsbeløb', value: 'f' },
									{ label: 'navn', value: 'n' }
								] }
								onChange={function(value){
									props.setAttributes( { sortBy: value } );
								}}
								__nextHasNoMarginBottom
							/>
						</PanelRow>
						<PanelRow>
							<SelectControl
								label="Sorteringsretning"
								value={props.attributes.sortByDir}
								options={ [
									{ label: 'stigende', value: 'asc' },
									{ label: 'faldende', value: 'dsc' }
								] }
								onChange={function(value){
									props.setAttributes( { sortByDir: value } );
								}}
								__nextHasNoMarginBottom
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Mulighed for fravalg"
								help="Ekstra felt til fravalg af denne type produkt"
								checked={ props.attributes.noneOption }
								onChange={ function(value){
									props.setAttributes( { noneOption: value } );
								} }
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Fravalg ligger øverst"
								help="Fravalg muligheden ligger over tilgængelige produkter"
								checked={ props.attributes.noneOptionAbove }
								onChange={ function(value){
									props.setAttributes( { noneOptionAbove: value } );
								} }
							/>
						</PanelRow>
                        <PanelRow>
							<TextControl 
								label="Label for fravalg felt"
								help=""
								value={props.attributes.noneOptionLabel}
								onChange={ function(value){
									props.setAttributes( { noneOptionLabel: value } );
								}}
							/>
                        </PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Brugerdefineret felt"
								help="Marker hvis dette er et produktuafhængigt felt"
								checked={ props.attributes.isCustom }
								onChange={ function(value){
									props.setAttributes( { isCustom: value } );
								} }
							/>
						</PanelRow>
                        <PanelRow>
							<TextControl 
								label="Brugerdefinerede label(s)"
								help="Label(s) for indhold i brugerdefineret felt"
								value={props.attributes.customLabels}
								onChange={ function(value){
									props.setAttributes( { customLabels: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Brugerdefineret regex"
								help="Regex til at validere tekstfelt (Eksempel for email)"
								value={props.attributes.customRegex}
								onChange={ function(value){
									props.setAttributes( { customRegex: value } );
								}}
							/>
                        </PanelRow>
                        <PanelRow>
							<TextControl 
								label="Fejlmeddelelse"
								help="Meddelelsen vises hvis regex fejler"
								value={props.attributes.customErrorMessage}
								onChange={ function(value){
									props.setAttributes( { customErrorMessage: value } );
								}}
							/>
                        </PanelRow>
                    </PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>
				
				</BlockControls>

				{/* here starts the edit block itself*/}
				<div id={"pa-signup-select-container-" + props.attributes.selectorName} class="content center-column signupinput">
					{props.attributes.selectType == "radio" ?
						'<input type="radio" />one<br/><input type="radio" /> two<br/><input type="radio" /> three' : ''}
					{props.attributes.selectType == "select" ?
						'<select><option>one</option><option>two</option><option>three</option></select>' : ''}
					{props.attributes.selectType == "checkbox" ?
						'<input type="checkbox" />one<br/><input type="checkbox" /> two<br/><input type="checkbox" /> three' : ''}
					{props.attributes.selectType == "text" ?
						'<input type="text" value="two" />' : ''}
			    </div>
            </div>
		);	
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
            <div id={"pa-signup-select-container-" + props.attributes.selectorName} data-attributes={JSON.stringify(props.attributes)} className="signupinput">
            </div>
			
		);	
 }

});
