/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
    CheckboxControl,
    Dashicon,
    Tooltip,
    TextControl,
	FontSizePicker
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-tickets-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Tickets-block' ), // Block title.
	icon: 'groups', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		fontSize: {type: 'int', default: 16},
		tickettypes: {type: 'string', default: 'Selfservice,Selfservice_htmlemail'},
		ticketCardView: {type: 'boolean', default: true},
		loadMoreButtonLabel: {type: 'string', default: 'Se alle beskeder'},
		loadLessButtonLabel: {type: 'string', default: 'Se færre beskeder'},
		showLoadMoreButton: {type: 'boolean', default: true},
		isLoadMoreButton: {type: 'boolean', default: true},
		loadCount: {type: 'int', default: '5'}
	},

	edit: ( props ) => {
		// Creates a <p class='wp-block-cgb-block-panther-selfservice-block'></p>.

		return (



			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General">
					<PanelRow>
							<TextControl
							label="Tickettypes to show"
							value={props.attributes.tickettypes}
							help="Enter a comma separeted list of the different tickettypes to show"
							onChange={ function(value){
										props.setAttributes( { tickettypes: value } );
									}}>

							</TextControl>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Card view"
								help="Check to use card view, uncheck for list view."
								checked={ props.attributes.ticketCardView }
								onChange={ function(value){
									props.setAttributes( { ticketCardView: value } );
								} }
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Show more/less button settings">
						<PanelRow>
							<TextControl
								label={__(
									"Label for show more button"
								)}
								value={props.attributes.loadMoreButtonLabel}
								onChange={ function(value){
									props.setAttributes( { loadMoreButtonLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Label for show less button"
								)}
								value={props.attributes.loadLessButtonLabel}
								onChange={ function(value){
									props.setAttributes( { loadLessButtonLabel: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Count of items to be showed "
								)}
								value={props.attributes.loadCount}
								onChange={ function(value){
									props.setAttributes( { loadCount: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Activate show more button"
								help="Check the box if you want to show the button"
								checked={ props.attributes.showLoadMoreButton }
								onChange={ function(value){
									props.setAttributes( { showLoadMoreButton: value } );
								} }
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>

				</BlockControls>


				{ props.attributes.ticketCardView ?
				<div id="tickets-container" data-tickettypes={props.attributes.tickettypes} data-cardview={props.attributes.ticketCardView} style={{'font-size': props.attributes.fontSize}}>
					<div class="ticket box">
						<div class="ticket-title title is-4">
							#12345 - 06/10/2021 20:34
						</div>
						<div class="ticket-body columns">
							<div class="column">
							<p>
								You guys, take him in back and I'll be right there. Well c'mon, this ain't no peep show. <br/>
								Well, Marty, I'm almost eighteen-years-old, it's not like I've never parked before.<br/>
								So you're my Uncle Joey. Better get used to these bars, kid.
							</p>
							</div>
						</div>
					</div>

					<div class="ticket box">
						<div class="ticket-title title is-4">
							#12344 - 04/10/2021 10:34
						</div>
						<div class="ticket-body columns">
							<div class="column">
								Doc, Doc, it's me, Marty.<br/>
								That's George McFly.<br/>
								Right about here.<br/>
								I'm telling the truth, Doc, you gotta believe me.<br/>
								Right.<br/>
								Wait a minute, wait a minute, Doc, are you telling me that you built a time machine out of a delorean.
							</div>
						</div>
					</div>
					<div className="columns is-centered">
						<div className="column has-text-centered">
							{props.attributes.showLoadMoreButton &&
								<button id="loadmore_tickets">{props.attributes.loadMoreButtonLabel}</button>}
						</div>
					</div>
				</div> :
				<div class="columns">
					<div class="ticket-type column">SMS</div>
					<div class="ticket-date column">01/04/2022 10:30</div>
					<div class="ticket-functions column"><button class="ticket-view-content-button" ><span class="dashicons dashicons-external"></span></button></div>
                </div>
			}
			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: (props) => {

		return (
			<div style={{'font-size': props.attributes.fontSize}}>
				<div id="tickets-container" data-tickettypes={props.attributes.tickettypes} data-attributes={JSON.stringify(props.attributes)} data-cardview={props.attributes.ticketCardView} className={ props.className }>
				</div>
				<div class="columns is-centered">
					<div class="column has-text-centered">
						{props.attributes.showLoadMoreButton &&
							<button id="loadmore_tickets" class="mt-5">{props.attributes.loadMoreButtonLabel}</button>}
					</div>
				</div>
			</div>
		);
	}
});
