/**
 * BLOCK: panther-selfservice-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/block-editor";
import { useState } from '@wordpress/element';
import { __ } from "@wordpress/i18n";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
	PanelRow,
    TextareaControl,
	CheckboxControl,
    SelectControl,
    Dashicon,
    Tooltip,
    TextControl,
	FontSizePicker
} from "@wordpress/components";

const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-panther-selfservice-webtv-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'WebTV' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'panther_selfservice-category', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'panther-selfservice-block — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		separatedLogin: {type: "boolean", default: false},
		usernameField: {type: 'string', default: 'Brugernavn'},
		passwordField: {type: 'string', default: 'Adgangskode'},
		activateDescrText: {type: 'string', default: 'Her kan du aktivere WebTV'},
		activateButtonText: {type: 'string', default: 'Aktiver'},
		savePWDescrText1: {type: 'string', default: 'WebTV er aktivt'},
		savePWDescrText2: {type: 'string', default: 'Her kan du justere brugernavn og kodeord for WebTV'},
		savePWButtonText: {type: 'string', default: 'Gem'},
		devicesDescr: {type: 'string', default: 'Disse enheder er registreret på dit WebTV'},
		devicesTableHead1: {type: 'string', default: 'Enhedsnavn'},
		devicesTableHead2: {type: 'string', default: 'Type'},
		devicesTableHead3: {type: 'string', default: 'Oprettet'},
		devicesTableHead4: {type: 'string', default: 'Sidst brugt'},
		devicesTableHead5: {type: 'string', default: 'Aktiv'},


		fontSize: {type: 'int', default: 16}
	},

	edit: ( props ) => {
		return (
			<div>
				{/* InspectorControls define the settings bar on the right in the editor */}
				<InspectorControls>
					<PanelBody title="General settings">
						<PanelRow>
							<CheckboxControl
										label="Username and password editable"
										checked={props.attributes.separatedLogin}
										help="Show or hide username and password fields."
										onChange={ function(newValue){
													props.setAttributes( { separatedLogin: newValue } );
												}}
									/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Username/Email"
								)}
								value={props.attributes.usernameField}
								onChange={ function(value){
									props.setAttributes( { usernameField: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Password"
								)}
								value={props.attributes.passwordField}
								onChange={ function(value){
									props.setAttributes( { passwordField: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Activate button text"
								)}
								value={props.attributes.activateButtonText}
								onChange={ function(value){
									props.setAttributes( { activateButtonText: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Save password button text"
								)}
								value={props.attributes.savePWButtonText}
								onChange={ function(value){
									props.setAttributes( { savePWButtonText: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Activate description"
								)}
								value={props.attributes.activateDescrText}
								onChange={ function(value){
									props.setAttributes( { activateDescrText: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Active WebTV description"
								)}
								value={props.attributes.savePWDescrText1}
								onChange={ function(value){
									props.setAttributes( { savePWDescrText1: value } );
								}}
							/>
						</PanelRow>
					<PanelRow>
							<TextControl
								label={__(
									"Save password description"
								)}
								value={props.attributes.savePWDescrText2}
								onChange={ function(value){
									props.setAttributes( { savePWDescrText2: value } );
								}}
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label={__(
									"Devices description"
								)}
								value={props.attributes.devicesDescr}
								onChange={ function(value){
									props.setAttributes( { devicesDescr: value } );
								}}
							/>
						</PanelRow>

						<PanelRow>
							<TextControl
								label={__(
									"Devices header 1"
								)}
								value={props.attributes.devicesTableHead1}
								onChange={ function(value){
									props.setAttributes( { devicesTableHead1: value } );
								}}
							/>
						</PanelRow>

						<PanelRow>
							<TextControl
								label={__(
									"Devices header 2"
								)}
								value={props.attributes.devicesTableHead2}
								onChange={ function(value){
									props.setAttributes( { devicesTableHead2: value } );
								}}
							/>
						</PanelRow>

						<PanelRow>
							<TextControl
								label={__(
									"Devices header 3"
								)}
								value={props.attributes.devicesTableHead3}
								onChange={ function(value){
									props.setAttributes( { devicesTableHead3: value } );
								}}
							/>
						</PanelRow>

						<PanelRow>
							<TextControl
								label={__(
									"Devices header 4"
								)}
								value={props.attributes.devicesTableHead4}
								onChange={ function(value){
									props.setAttributes( { devicesTableHead4: value } );
								}}
							/>
						</PanelRow>

						<PanelRow>
							<TextControl
								label={__(
									"Devices header 5"
								)}
								value={props.attributes.devicesTableHead5}
								onChange={ function(value){
									props.setAttributes( { devicesTableHead5: value } );
								}}
							/>
						</PanelRow>

					</PanelBody>
					<PanelBody title="Font size">
						<PanelRow>
							<FontSizePicker
							fontSizes={[
								{
									name: 'Small',
									slug: 'small',
									size: 12,
								},
								{
									name: 'Medium',
									slug: 'medium',
									size: 16,
								},
								{
									name: 'Big',
									slug: 'big',
									size: 26,
								},
							]}
							value={props.attributes.fontSize}
							fallbackFontSize={ 16 }
							onChange={ ( newFontSize ) => {
								props.setAttributes( {fontSize: newFontSize} );
							}}
							withSlider
							/>
						</PanelRow>
					</PanelBody>

				</InspectorControls>

				{/* BlockControls define the options available on the toolbar of this block */}
				<BlockControls>

				</BlockControls>


				<section>
				<div class="content center-column" style={{'font-size': props.attributes.fontSize}}>
					<div class="columns is-multiline">
						<div id="webtv-chng-pwd-container">
						<div id="webtv_chng-pwd_descr1">{props.attributes.savePWDescrText1}</div>
						<div id="webtv_chng-pwd_descr2" class="upfield">{props.attributes.savePWDescrText2}</div>
								<div class="column upfield">
								<label class="label">{props.attributes.usernameField}</label>
								<input class="input" id="webtv_user" type="text" placeholder={props.attributes.usernameField} name="webtv_user"/>
							</div>

							<div class="column upfield">
								<label class="label">{props.attributes.passwordField}</label>
								<input class="input" id="webtv_pass" type="password" placeholder={props.attributes.passwordField} name="webtv_pass"/>
							</div>
							<div className="column columns is-mobile is-centered">
								<button className="column is-half" class="upfield"	type="submit" id="webtv_update_but">{props.attributes.savePWButtonText}</button>
							</div>
						</div>
						<div id="webtv-devices-container" class="container">
							<span id="devices">{props.attributes.devicesDescr}</span>
							<div class="table-container">    
								<table id="devices_table" class="table">
									<thead>
										<tr>
											<td>{props.attributes.devicesTableHead1}</td>
											<td>{props.attributes.devicesTableHead2}</td>
											<td>{props.attributes.devicesTableHead3}</td>
											<td>{props.attributes.devicesTableHead4}</td>
											<td>{props.attributes.devicesTableHead5}</td>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>                        

					</div>
				</div>
				</section>
			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {

		return (

			<section>
				<div class="content center-column webtv-container" id="webtv-container" data-showupw={props.attributes.separatedLogin} style={{'font-size': props.attributes.fontSize}}>
					<div class="columns is-multiline">
						<div id="webtv-activate-container" style="display: none;">
						<div id="webtv_activate_descr">{props.attributes.activateDescrText}</div>
						<div class="column upfield">
								<label class="label">{props.attributes.usernameField}</label>
								<input class="input" id="webtv_new_user" type="text" placeholder={props.attributes.usernameField} name="webtv_new_user"/>
							</div>

							<div class="column upfield">
								<label class="label">{props.attributes.passwordField}</label>
								<input class="input" id="webtv_new_pass" type="password" placeholder={props.attributes.passwordField} name="webtv_new_pass"/>
							</div>
							<div className="column columns is-mobile is-centered">
								<button className="column is-half"	type="submit" id="webtv_activate_but">{props.attributes.activateButtonText}</button>
							</div>
						</div>

						<div id="webtv-chng-pwd-container" style="display: none;">
						<div id="webtv_chng-pwd_descr1">{props.attributes.savePWDescrText1}</div>
						<div id="webtv_chng-pwd_descr2" class="upfield">{props.attributes.savePWDescrText2}</div>
							<div class="column upfield">
								<label class="label">{props.attributes.usernameField}</label>
								<input class="input" id="webtv_user" type="text" placeholder={props.attributes.usernameField} name="webtv_user"/>
							</div>

							<div class="column upfield">
								<label class="label">{props.attributes.passwordField}</label>
								<input class="input" id="webtv_pass" type="password" placeholder={props.attributes.passwordField} name="webtv_pass"/>
							</div>
							<div className="column columns is-mobile is-centered">
								<button className="column is-half upfield" type="submit" id="webtv_update_but">{props.attributes.savePWButtonText}</button>
							</div>
						</div>

						<div id="webtv-devices-container" class="container" style="display: none;">
							<span id="devices">{props.attributes.devicesDescr}</span>
							<div class="table-container">    
								<table id="devices_table" class="table">
									<thead>
										<tr>
											<th>{props.attributes.devicesTableHead1}</th>
											<th>{props.attributes.devicesTableHead2}</th>
											<th>{props.attributes.devicesTableHead3}</th>
											<th>{props.attributes.devicesTableHead4}</th>
											<th>{props.attributes.devicesTableHead5}</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>                        

					</div>
				</div>
				</section>
		);
	},
} );
