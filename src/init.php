<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * Assets enqueued:
 * 1. blocks.style.build.css - Frontend + Backend.
 * 2. blocks.build.js - Backend.
 * 3. blocks.editor.build.css - Backend.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-element} for WP Element abstraction — structure of blocks.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */

function panther_selfservice_categories( $categories, $post ){
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'panther_selfservice-category',
                'title'=> __('panther_selfservice Category', 'panther_selfservice-blocks'),
                'icon' => 'cloud'
            )
        )
            );
}
add_filter('block_categories_all','panther_selfservice_categories',10,2);

function panther_selfservice_block_cgb_block_assets() { // phpcs:ignore
	// Register block styles for both frontend + backend.
    $options = get_option('psb_plugin_options');
    if (!empty($options) && array_key_exists('psb_disable_css', $options)) {
        // error_log("Not including Styles.CSS because psb_disable_css is set to true.");
    } else {
        wp_register_style(
            'panther_selfservice_block-cgb-style-css', // Handle.
            plugins_url( 'dist/blocks.style.build.css', dirname( __FILE__ ) ), // Block style CSS.
            is_admin() ? array( 'wp-editor' ) : null, // Dependency to include the CSS after it.
            null // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
        );
        wp_enqueue_style( 'panther_selfservice_block-cgb-style-css',plugins_url( 'dist/blocks.style.build.css', dirname( __FILE__ ) ));
    }

	//wp_enqueue_script('jquery');
    //wp_enqueue_script( 'ui-core', get_site_url() . '/wp-includes/js/jquery/ui/core.min.js' );
    //wp_enqueue_script( 'datepicker', get_site_url() . '/wp-includes/js/jquery/ui/datepicker.min.js' );
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style( 'jquery-ui',plugins_url( 'css/jquery-ui.min.css', dirname( __FILE__ ) ) );
    wp_enqueue_style( 'jquery-ui-structure',plugins_url( 'css/jquery-ui.structure.min.css', dirname( __FILE__ ) ) );
    wp_enqueue_style( 'jquery-ui-theme',plugins_url( 'css/jquery-ui.theme.min.css', dirname( __FILE__ ) ) );
	wp_enqueue_script('facebook','https://connect.facebook.net/da_DK/sdk.js');
	wp_enqueue_script('apple','https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js');
	wp_enqueue_script('alertify','https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.13.1/alertify.min.js');

	//Should only be enqued when dawa selector is present
	wp_enqueue_script('cf-core','https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.min.js');
	wp_enqueue_script('cf-fetch','https://cdnjs.cloudflare.com/ajax/libs/fetch/2.0.3/fetch.min.js');
	wp_enqueue_script('dawa',plugins_url('dist/js/dawa-autocomplete2.min.js', dirname( __FILE__ )), false);

	wp_enqueue_style( 'dashicons' );
	wp_enqueue_style( 'font-awesome-free', '//use.fontawesome.com/releases/v5.6.3/css/all.css' );
	wp_enqueue_style( 'alertify', '//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css' );
	wp_enqueue_style( 'alertify-default', '//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css' );


	wp_enqueue_script('frontend-js',plugins_url( 'dist/frontend.js', dirname( __FILE__ ) ), false);

	wp_add_inline_script( 'frontend-js', 'const JQUERY_VARS = ' . json_encode( array(
		'siteUrl' => get_site_url(),
        'nonce'=> wp_create_nonce("wp_rest"),
        'restUrl'=> get_rest_url()."pantheradmin/v1",
		'loginPage' => get_option('psb_plugin_options') ? get_option('psb_plugin_options')['login_page'] : null,
		'customerInfoPage'=> get_option('psb_plugin_options') ? get_option('psb_plugin_options')['customerinfo_page'] : null,
		'customerInvoicesPage'=> get_option('psb_plugin_options') ? get_option('psb_plugin_options')['customerinvoices_page'] : null,
		'customerProductsPage'=> get_option('psb_plugin_options') ? get_option('psb_plugin_options')['customerproducts_page'] : null,
		'customerCommunicationPage'=> get_option('psb_plugin_options') ? get_option('psb_plugin_options')['customercommunication_page'] ?? [] : null,
		'paymentTOSUrl' => get_option('psb_plugin_options') ? get_option('psb_plugin_options')['payment_payment_tos'] : null,
		'additionalMenuItems' => get_option('psb_plugin_options') ? get_option('psb_plugin_options')['additional_hidden_menus'] : null,
		'facebookClientId' => get_option('psb_plugin_options') ? get_option('psb_plugin_options')['facebook_client_id'] : null,
		'googleClientId' => get_option('psb_plugin_options') ? get_option('psb_plugin_options')['google_client_id'] : null,
		'appleClientId' => get_option('psb_plugin_options') ? get_option('psb_plugin_options')['apple_client_id'] : null,
		'appleRedirectUrl' => get_option('psb_plugin_options') ? get_option('psb_plugin_options')['apple_redirect_url'] : null,
        'pass_length'=>get_option('psb_plugin_options') ? get_option('psb_plugin_options')['password_rules_length'] : null,
        'pass_chars'=>get_option('psb_plugin_options') ? get_option('psb_plugin_options')['password_rules_chars'] : null,
		) ), 'before' );


	// Register block editor script for backend.
	wp_register_script(
		'panther_selfservice_block-cgb-block-js', // Handle.
		plugins_url( '/dist/blocks.build.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ), // Dependencies, defined above.
		null, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
		true // Enqueue the script in the footer.
	);

	// Register block editor styles for backend.
	wp_register_style(
		'panther_selfservice_block-cgb-block-editor-css', // Handle.
		plugins_url( 'dist/blocks.editor.build.css', dirname( __FILE__ ) ), // Block editor CSS.
		array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
		null // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
	);

	// WP Localized globals. Use dynamic PHP stuff in JavaScript via `cgbGlobal` object.
	wp_localize_script(
		'panther_selfservice_block-cgb-block-js',
		'cgbGlobal', // Array containing dynamic data for a JS Global.
		[
			'pluginDirPath' => plugin_dir_path( __DIR__ ),
			'pluginDirUrl'  => plugin_dir_url( __DIR__ ),
			'siteUrl' => get_site_url(),
            'nonce'=> wp_create_nonce("wp_rest"),
            'restUrl'=> get_rest_url()."pantheradmin/v1",
			// Add more data here that you want to access from `cgbGlobal` object.
		]
	);


	/**
	 * Register Gutenberg block on server-side.
	 *
	 * Register the block on server-side to ensure that the block
	 * scripts and styles for both frontend and backend are
	 * enqueued when the editor loads.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
	 * @since 1.16.0
	 */
	register_block_type(
		'cgb/block-panther-selfservice-block', array(
			// Enqueue blocks.style.build.css on both frontend & backend.
			'style'         => 'panther_selfservice_block-cgb-style-css',
			// Enqueue blocks.build.js in the editor only.
			'editor_script' => 'panther_selfservice_block-cgb-block-js',
			// Enqueue blocks.editor.build.css in the editor only.
			'editor_style'  => 'panther_selfservice_block-cgb-block-editor-css',
		)
	);
}

// Hook: Block assets.
add_action( 'init', 'panther_selfservice_block_cgb_block_assets' );


