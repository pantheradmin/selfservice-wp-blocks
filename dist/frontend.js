jQuery(document).ready(function(){

    
    if (sessionStorage.getItem('customertoken') == null){
        jQuery("#menu-item-"+JQUERY_VARS.customerInfoPage.menu_item_id).css('display','none');
        jQuery("#menu-item-"+JQUERY_VARS.customerInvoicesPage.menu_item_id).css('display','none');
        jQuery("#menu-item-"+JQUERY_VARS.customerProductsPage.menu_item_id).css('display','none');
        jQuery("#menu-item-"+JQUERY_VARS.customerCommunicationPage.menu_item_id).css('display','none');
        jQuery(".menu-item-"+JQUERY_VARS.customerInfoPage.menu_item_id).css('display','none');
        jQuery(".menu-item-"+JQUERY_VARS.customerInvoicesPage.menu_item_id).css('display','none');
        jQuery(".menu-item-"+JQUERY_VARS.customerProductsPage.menu_item_id).css('display','none');
        jQuery(".menu-item-"+JQUERY_VARS.customerCommunicationPage.menu_item_id).css('display','none');
        jQuery(".page-item-"+JQUERY_VARS.customerInfoPage.menu_item_id).css('display','none');
        jQuery(".page-item-"+JQUERY_VARS.customerInvoicesPage.menu_item_id).css('display','none');
        jQuery(".page-item-"+JQUERY_VARS.customerProductsPage.menu_item_id).css('display','none');
        jQuery(".page-item-"+JQUERY_VARS.customerCommunicationPage.menu_item_id).css('display','none');
        if(JQUERY_VARS.additionalMenuItems != null){
            var additionalMenuItems = JQUERY_VARS.additionalMenuItems.split(',');
            additionalMenuItems.forEach(item => {
                jQuery("#menu-item-"+item.trim()).css('display','none');
                jQuery(".menu-item-" + item.trim()).css('display', 'none');
                jQuery(".page-item-" + item.trim()).css('display', 'none');
            });
        }
    } else {
        jQuery("#menu-item-"+JQUERY_VARS.loginPage.menu_item_id).css('display','none');
        jQuery(".menu-item-"+JQUERY_VARS.loginPage.menu_item_id).css('display','none');
        jQuery(".page-item-"+JQUERY_VARS.loginPage.menu_item_id).css('display','none');
    }


    function goToLogin(){
        window.location.replace(JQUERY_VARS.siteUrl+'/'+JQUERY_VARS.loginPage.page+'/');
    }

    function isLoggedIn(){
        if(sessionStorage.getItem('customertoken') == null) {
            sessionStorage.clear();
            goToLogin();
        }
        if(sessionStorage.getItem('customertoken') != null && sessionStorage.getItem('tokenExpire') <= Math.floor(Date.now() / 1000)) {
            sessionStorage.clear();
            goToLogin();
        }
    }

    window.notifySuccess = function notifySuccess(text){
        alertify.set('notifier','position', 'top-center');
        alertify.notify(text, 'success', 10, function(){});
    }

    window.notifyError = function notifyError(text){
        alertify.set('notifier','position', 'top-center');
        alertify.notify(text, 'error', 10, function(){});
    }

    if(jQuery("#login_form").length){
        import('./js/login.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery("#customer-info-container").length) {
        isLoggedIn();
        import('./js/customerinfo.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery("#cardpay_container").length) {
        isLoggedIn();
        import('./js/cardpay.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery("#invoice_container").length) {
        isLoggedIn();
        import('./js/invoices.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery("#product_container").length) {
        isLoggedIn();
        import('./js/products.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery("#bullguard_container").length) {
        isLoggedIn();
        import('./js/bullguard.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery("#fsecure_container").length) {
        isLoggedIn();
        import('./js/f_secure.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery(".addon_product_container").length) {
        isLoggedIn();
        import('./js/addon_products.js')
        .then((module) => {
            module.init();
        });

    }

    if(jQuery("#tickets-container").length) {
        isLoggedIn();
        import('./js/tickets.js')
        .then((module) => {
            module.init();
        });
    }


    if(jQuery(".order_progress_container").length) {
        isLoggedIn();
        import('./js/ordreprogress.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery("#internet-info-container").length) {
        isLoggedIn();
        import('./js/internet.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery("#modem-data-container").length) {
        isLoggedIn();
        import('./js/modemdata.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery("#technician-container").length){
        isLoggedIn();
        import('./js/technician.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery("#wifi-data-container").length) {
        isLoggedIn();
        import('./js/live_wifi.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery(".pa-my-overview").length){
        isLoggedIn();
        import('./js/myoverview.js')
        .then((module) => {
            module.init();
        });
    }

    if(jQuery(".allente_tv_container").length){
        isLoggedIn();
        import('./js/allente.js')
        .then((module) => {
            module.init();
        });
    }
    
	if(jQuery("#socialConnection-container").length) {
		isLoggedIn();
		import('./js/attach_social_login.js')
			.then((module) => {
				module.init();
			});
	}

	if(jQuery(".subscription-container").length) {
		isLoggedIn();
		import('./js/subscription.js')
			.then((module) => {
				module.init();
			});
	}
    
    if(jQuery("#pa-calendar-container-Opstart").length) {
		import('./js/pa-calendar.js')
			.then((module) => {
				module.init('Opstart');
			});
	}

    if(jQuery("#pa-calendar-container-Opsigelse").length) {
		isLoggedIn();
		import('./js/pa-calendar.js')
			.then((module) => {
				module.init('Opsigelse');
			});
	}

    if(jQuery("#pa-calendar-container-Opgradering").length) {
		isLoggedIn();
		import('./js/pa-calendar.js')
			.then((module) => {
				module.init('Opgradering');
			});
	}

    if(jQuery("#pa-calendar-container-Nedgradering").length) {
		isLoggedIn();
		import('./js/pa-calendar.js')
			.then((module) => {
				module.init('Nedgradering');
			});
	}

    if(jQuery(".cancel-product-container").length) {
		isLoggedIn();
		import('./js/cancel.js')
			.then((module) => {
				module.init();
			});
	}



    if(jQuery("#pa-dawa-container").length) {
		import('./js/dawa_block.js')
			.then((module) => {
				module.init();
			});
	}

    if(jQuery(".signupinput").length) {
		import('./js/signup_input.js')
			.then((module) => {
				module.init();
			});
	}

    if(jQuery(".signup-page-break").length) {
		import('./js/signup_page_change.js')
			.then((module) => {
				module.init();
			});
	}

    if(jQuery(".conax-container-multipackets").length || jQuery(".conax-container-singlepackets").length || jQuery(".conax-container-eventpackets").length) {
		isLoggedIn();
        import('./js/conax_block.js')
			.then((module) => {
				module.init();
			});
	}

    if(jQuery(".mix-container").length) {
		isLoggedIn();
		import('./js/mix_config_block.js')
			.then((module) => {
				module.init();
			});
	}
    
    if (jQuery(".basket-container").length) {
		isLoggedIn();
		import('./js/basket.js')
			.then((module) => {
				module.init();
			});
    }
    
    if (jQuery(".sbbu-container").length) {
		isLoggedIn();
		import('./js/sbbu.js')
			.then((module) => {
				module.init();
			});
	}
    
    if (jQuery(".overview-container").length) {
		isLoggedIn();
		import('./js/overview_block.js')
			.then((module) => {
				module.init();
			});
	}

    //webtv-container
    if (jQuery(".webtv-container").length) {
		isLoggedIn();
		import('./js/webtv.js')
			.then((module) => {
				module.init();
			});
	}

    // Check for error messages in the url
    const queryString = window.location.search;
    urlParams = new URLSearchParams(queryString);
    if(urlParams.get("e")){
        window.parent.notifyError(urlParams.get("e"));
    }
    if(urlParams.get("success")){
        window.parent.notifySuccess(urlParams.get("success"));
    }

}
);

function validText(theText, errorMessage, regex){
    if(!regex.length)
        return true;

    var validRegex = new RegExp(regex);
        
    if(!validRegex.test(theText)){
        window.parent.notifyError(errorMessage);
        return false;
    }else
        return true;
}

function getProductName(product){
    return product.selfserviceNames.da;
}

function addNonce(url) { 
    const appendChar = url.includes('?') ? '&' : '?';
    return url + appendChar + '_wpnonce=' + JQUERY_VARS.nonce;
}

//Helper for the following functions
function getBasket(){
    var b = false;

    try{b = JSON.parse(sessionStorage.getItem("pa_basket"));}
    catch(err){}

    return b;
}

/**
 * action: { 'add' | 'remove' | 'update' | 'config' }
 */
function addToBasket(productConfID, provModel, action, quantity = 1, actionDate = 0){
    var basket = getBasket();
    var ap = JSON.parse(sessionStorage.getItem('pa_availableProducts'));
    var parameterStr = "";
    //This could be the first item added
    if(!basket){basket = [];}

    //Increase quantity if item already exists
    for(let i in basket){
        if(basket[i].id == productConfID){
            basket[i].quantity += quantity;
            sessionStorage.setItem("pa_basket", JSON.stringify(basket));
            return true;
        }
    }

    //Create and add item if it does not exist
    var pconf = ap[productConfID];
    let aDate = !actionDate  ? Math.round(Date.now()/1000) : Math.round(actionDate/1000); 
    let item = {id: productConfID, ident: pconf.ident, name: getProductName(pconf), quantity: quantity, provModel: provModel, parameters: parameterStr, action: action, actionDate: aDate};
    basket.push(item);
    sessionStorage.setItem("pa_basket", JSON.stringify(basket));
    window.dispatchEvent(new Event('pa_basket_update'));
}

/**
 * Hard push to basket
 * item is a correctly formatted basket item
 */
function pushToBasket(item){
    let basket = getBasket();
    if(!basket){basket = [];}
    basket.push(item);
    sessionStorage.setItem("pa_basket", JSON.stringify(basket));
}

function removeFromBasket(productConfID, provModel = "", quantity = 1, action=""){
    var basket = getBasket();
    if(!basket){return false;}

    for(let i in basket){
        if(((provModel != "" && action == basket[i].action) || (action == "" && basket[i].provModel == provModel)) || (basket[i].id == productConfID)){
            console.log("decrease basket pos: " + i);
            //Decrease quantity
            basket[i].quantity -= quantity;

            //Delete item if quantity has reached 0
            if(basket[i].quantity <= 0){
                basket.splice(i,1);
            }
        }
    }

    sessionStorage.setItem("pa_basket", JSON.stringify(basket));
    window.dispatchEvent(new Event('pa_basket_update'));
}

function clearBasket(){
    sessionStorage.removeItem("pa_basket");
}

function submitBasket(redirect){
    //In case a mix packet has been ordered or configured
    //The selection is finalized here
    saveSelectedMixChannels();
    //return;
    jQuery.ajax({
        type:"post",
        url:addNonce(JQUERY_VARS.restUrl+'/customer/order?token='+sessionStorage.getItem('customertoken')),
        data: sessionStorage.getItem("pa_basket"),
        success: function(data){
            sessionStorage.removeItem("pa_basket");
            sessionStorage.removeItem("pa_selectedSlugs");
            window.location.replace(redirect);
        }, error: function(data){
            window.parent.notifyError('Bestilling fejlet.');
        }
    });
}

function findInBasket(pcid, provModel = ""){
	var basket = getBasket();
	for(let pos in basket){
		if(provModel.length > 0 && basket[pos].provModel == provModel){
			return basket[pos];
		} else if(basket[pos].id == pcid){
			return basket[pos];
		}
	}
	
	return null;
}

function getPCIDFromString(str){
	var pcid = "";
	
	for(let chr of str){
	  if(Number.isInteger(Number.parseInt(chr))){
		  pcid = pcid.concat(chr);
	  }
	}

	return pcid;
}

function allowAdd(pcid, customerProducts){
    let requires = JSON.parse(sessionStorage.getItem('pa_requires'));

    if(!Object.hasOwn(requires,pcid)){
        return true;
    }

    let requiredPresent = false;

    //Run through access giving combinations
    for(const akey in requires[pcid]){
        requiredPresent = true;
        for(const bkey in requires[pcid][akey]){
            if(!Object.hasOwn(customerProducts,requires[pcid][akey][bkey].id)){
                requiredPresent = false;
                break;
            } 
        }

        if(requiredPresent){
            return true;
        }
    }

    return requiredPresent;
}

function allowRemove(pcid, customerProducts){
    let isRequired = JSON.parse(sessionStorage.getItem('pa_isrequired'));

    //Do we have a product that requires this product
    if(Object.hasOwn(isRequired, pcid)){
        for(const akey in isRequired[pcid]){
            if(Object.hasOwn(customerProducts,isRequired[pcid][akey].id)){
                return false;
            }
        }
    }

    //Do we have a product that require this products possible channels
    for(const i in customerProducts){
        if(customerProducts[i] == pcid && Object.hasOwn(isRequired, i) ){
            //Do we have the products listed in isRequired
            for(let n in isRequired[i]){
                if(Object.hasOwn(customerProducts, isRequired[i][n].id)){
                    return false;
                }
            }
        }
    }

    return true;
}

function getDefaultSelectedSlugs(mixParameters){
    let slugs = new Object();
    let mixElements = mixParameters.split(';');
    let mixInfo = JSON.parse(sessionStorage.getItem('pa_mixCounter'));

    for(let a in mixElements){
        let keyVal = mixElements[a].split(':');

        if((keyVal[0] == 'packetmixmandatory') || (keyVal[0] == 'defaultchannels')){
            let slugList = keyVal[1].split(',');
            let typeValue = (keyVal[0] == 'packetmixmandatory') ? 'M' : 'D';
            for(let b in slugList){
                slugs[slugList[b]] = typeValue;
            }
        }
    }

    return slugs;
}

function setSelection(elementId, provModel, unSelectedClass, selectedClass, markedForRemoveClass){
	var customerProductIds = JSON.parse(sessionStorage.getItem('pa_customerProductIds'));
	var customerChannelIds = JSON.parse(sessionStorage.getItem('pa_customerChannelIds'));
	var pcid = getPCIDFromString(elementId);
	let itemInBasket = findInBasket(pcid);
	
	if(itemInBasket == null && !Object.hasOwn(customerProductIds,pcid)){
        //A product not owned and event (add) not in the basket

        //Does this product require other products
        if(!allowAdd(pcid, {...customerProductIds,...customerChannelIds})){
            alert('Produktet kan ikke bestilles. Det kræver andre aktive produkter.');
            return false;
        }

		addToBasket(pcid, provModel, 'add');
		jQuery("#" + elementId).removeClass(unSelectedClass).addClass(selectedClass);
	}else if(itemInBasket == null && Object.hasOwn(customerProductIds,pcid)){
        //A product owned and event (terminate) not in the basket

        //Is this product required by other products
        if(!allowRemove(pcid, {...customerProductIds,...customerChannelIds})){
            alert('Produktet kan ikke afbestilles. Det kræves af andre aktive produkter.');
            return false;
        }

		addToBasket(pcid, provModel, 'remove');
		jQuery("#" + elementId).removeClass(unSelectedClass).addClass(markedForRemoveClass);
	}else{
        //An add or remove event is in the basket
		removeFromBasket(pcid);
        if(Object.hasOwn(customerProductIds,pcid)){
            //Products is owned and the terminate event has been deleted
		    jQuery("#" + elementId).removeClass(markedForRemoveClass).addClass(unSelectedClass);
        }else{
            //Product is not owned and the add event has been deleted 
            jQuery("#" + elementId).removeClass(selectedClass).addClass(unSelectedClass);
        }
	}
}

//NEEDS IMPLEMENTATION ***************************
function setSelectionUnique(elementId, provModel, unSelectedClass, selectedClass){
    return false;
}

function findMixInBasket(mixProductConfs){
    for(let p in mixProductConfs){
        let bItem = findInBasket(p);
        if(bItem != null){
            return bItem;
        }
    }

    return null;
}

function getMixProductConfs(){
    var prds = JSON.parse(sessionStorage.getItem('pa_availableProducts'));
    var mps = new Object();
    for(let p in prds){
        if(prds[p].parameters.search('packetmix') >= 0){
            mps[p] = prds[p];
        }
    }

    return mps;
}


//To be used just before submitting mix config
function saveSelectedMixChannels(){
    //If no mix in basket - return
    var mixProduct = findMixInBasket(getMixProductConfs());
    if(mixProduct == null){
        return false;
    }

    let selectedSlugs = JSON.parse(sessionStorage.getItem('pa_selectedSlugs'));
    var mixInfo = JSON.parse(sessionStorage.getItem('pa_mixCounter'));
    var customerProducts = JSON.parse(sessionStorage.getItem('pa_customerProducts'));
    let pointValues = JSON.parse(sessionStorage.getItem('pa_pointValues'));


    var configStr = "chosentvchannels:";
    var channelCount = 0;
    for(let i in selectedSlugs){
        if(selectedSlugs[i] != 'M'){
            channelCount += pointValues[i];
            //If a channel is deselected and selected again it changes to 'edited' - But that does not mean it requires a new expiry date
            if(selectedSlugs[i] == 'edited'){
                    selectedSlugs[i] = getActiveChannelExpiry(i, customerProducts, mixInfo);
            }

            let theDateTime = ((selectedSlugs[i] == 'edited') || (selectedSlugs[i] == 'D')) ? new Date(parseInt(mixInfo.ChannelLockDays)*24*60*60000 + Date.now()) : new Date(selectedSlugs[i]);
            configStr += i + "\$" + Math.round(theDateTime.getTime()/1000) + ",";
        }
    }

    //If correct number of channels are selected - generate new config string
    if(channelCount <= mixInfo.size){
        configStr = configStr.substring(0, configStr.length-1);

        //We need to see if unrelated stuff is present in the products config
        //Don't want to remove that
        let customerProducts = JSON.parse(sessionStorage.getItem('pa_customerProducts'));
        let configSet = false;
        for(let c in customerProducts){
            if(customerProducts[c].productConf.id == mixInfo.pcid){
                mergeConfig(customerProducts[c].options , configStr, mixInfo.pcid);
                configSet = true;
                break;
            } 
        }

        if(!configSet){
            mergeConfig("" , configStr, mixInfo.pcid);
        }
    }else{
        //Fejlmeddelelse
        window.parent.notifyError('Der er valgt for mange kanaler.');
    }
}

function getActiveChannelExpiry(slug, customerProducts, mixInfo){
    for(let i in customerProducts){
        if(customerProducts[i].productConf.id == mixInfo.pcid){
            if((customerProducts[i].options.search(':' + slug + '\\$') > 0) || (customerProducts[i].options.search(',' + slug + '\\$') > 0)){
                return new Date(Date.now() - 86400000);
            }
        }
    }

    return 'edited';
}

function mergeConfig(config, mixConfig, pcid){

    let configItems = config.split(";");
    let newConfig = [];
    newConfig.push(mixConfig);

    for(let n in configItems){
        if(configItems[n].search('chosentvchannels') != 0){
            newConfig.push(configItems[n]);
        }
    }

    var basket = getBasket();
    //Update basket
    if(basket != null){
        for(let i in basket){
            if(basket[i].id == pcid){
                basket[i].parameters = newConfig.join(';');
                sessionStorage.setItem("pa_basket", JSON.stringify(basket));
            }
        }
    }
}