export function init(){
    updateInfo();
}

var showupw = jQuery("#webtv-container").data('showupw');

function updateInfo(){
    //Get webtv info
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+'/customer/webtv?token='+sessionStorage.getItem('customertoken')+"&is-master="+sessionStorage.getItem('isMaster')),
        credentials: 'same-origin',
        success: function(data) {
            if(!data){
                showActivate();
            }else{
                showActive(JSON.parse(data));
                showDevices();
            }

        },
        error: function(data){
            //Kunne ikke hente...
            //Fejlmeddelelse
            showActivate();
        }
    });

}

function showActive(data){
    if(!showupw){
        jQuery(".upfield").hide();
    }

    jQuery("#webtv-activate-container").hide();
    jQuery("#webtv-chng-pwd-container").show();
    jQuery("#webtv_user").val(data.login);
}

function showActivate(){
    if(!showupw){
        jQuery(".upfield").hide();
    }
    jQuery("#webtv-chng-pwd-container").hide();
    jQuery("#webtv-devices-container").hide();
    jQuery("#webtv-activate-container").show();
    jQuery("#webtv_new_user").attr("readonly", true);

    //Need customer email
    jQuery.ajax({
        type:"get",
        url: addNonce(JQUERY_VARS.restUrl+"/customer?token="+sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function(data){
            var customer = JSON.parse(data);
            if(customer['isMaster'] === 1){
                jQuery("#webtv_new_user").val(customer['contact']['email']);
            } else {
                jQuery("#webtv_new_user").val(customer['email']);
            }

        }, error: function(data){

        }
    });
}

function showDevices(){
    //jQuery("#webtv-devices-container").show();

    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+'/customer/webtv/devices?token='+sessionStorage.getItem('customertoken')+"&is-master="+sessionStorage.getItem('isMaster')),
        credentials: 'same-origin',
        success: function(data) {
            if(!data){
                jQuery("#webtv-devices-container").hide();
            }else{
                jQuery("#webtv-devices-container").show();
                jQuery("#devices_table").empty();

                data = JSON.parse(data);
                //Populate container then show it
                for(const d in data){
                    let dhtml = "";
                    dhtml += "<tr>";
                    dhtml += "<td>" + data[d]["name"] + "</td>";
                    dhtml += "<td>" + data[d]["dtype"] + "</td>";
                    dhtml += "<td>" + data[d]["registered"] + "</td>";
                    dhtml += "<td>" + data[d]["lastused"] + "</td>";
                    dhtml += "<td>" + (data[d]["active"] ? "Ja" : "Nej")+ "</td>"; 

                    if(data[d]["dtype"] != "STB"){
                        dhtml += "<td><button className=\"column is-half\" class=\"device_dereg_button\" type=\"submit\" value=\"ddb_" + data[d]["uid"] + "\">Afregistrer</button></td>"; 
                    }

                    dhtml += "</tr>";
                    jQuery("#devices_table").append(dhtml);
                }
            }

        },
        error: function(data){
            //Kunne ikke hente...
            //Fejlmeddelelse
            jQuery("#webtv-devices-container").hide();
            window.parent.notifyError("Registrerede enheder kunne ikke hentes");

        }
    });

}

jQuery("#webtv_activate_but").on('click', function(){
    let user = jQuery("#webtv_new_user").val();
    let pass = jQuery("#webtv_new_pass").val();
    activate(user, pass);
});

jQuery("#webtv_update_but").on('click', function(){
    let user = jQuery("#webtv_user").val();
    let pass = jQuery("#webtv_pass").val();
    update(user, pass);
});

jQuery("body").on('click', 'button', function(){
    deleteDevice(jQuery(this).val());
});


function update(user, pass){
    //Do things
    var sdata = {'login': user.trim(), 'pass': pass.trim(), 'token': sessionStorage.getItem('customertoken')};

    if(!sdata.login.length || !sdata.pass.length){
        window.parent.notifyError("Indtast b&aring;de brugernavn og password");
        return;
    }

    jQuery.ajax({
        type:"put",
        url: addNonce(JQUERY_VARS.restUrl+'/customer/webtv'),
        credentials: 'same-origin',
        data: sdata,
        success: function(data){
            updateInfo();
            window.parent.notifySuccess("Brugernavn og/eller password er &aelig;ndret");
        }, error: function(data){
            window.parent.notifyError("Brugernavn og/eller password blev ikke &aelig;ndret");
        }
    });
}

function activate(user, pass){
    //Do things
    var sdata = {'login': user.trim(), 'pass': pass.trim(), 'token': sessionStorage.getItem('customertoken')};

    if(showupw && (!sdata.login.length || !sdata.pass.length)){
        window.parent.notifyError("Indtast b&aring;de brugernavn og password");
        return;
    }

    jQuery.ajax({
        type:"post",
        url: addNonce(JQUERY_VARS.restUrl+'/customer/webtv'),
        credentials: 'same-origin',
        data: sdata,
        success: function(data){
            updateInfo();
            window.parent.notifySuccess("Konto er oprettet");
        }, error: function(data){
            window.parent.notifyError("Konto blev ikke oprettet");
        }
    });
}

function deleteDevice(deviceId){
    if(deviceId.indexOf("ddb_") < 0)
        return;

    deviceId = deviceId.substring(4);
    jQuery.ajax({
        type:"DELETE",
        url: addNonce(JQUERY_VARS.restUrl+'/customer/webtv/devices/' + deviceId + '?token='+sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function(data){
            updateInfo();
            window.parent.notifySuccess("Enhed er blevet slettet");

        }, error: function(data){
            window.parent.notifyError("Enhed blev ikke slettet");
        }
    });
}