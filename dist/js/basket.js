export function init() { 
    var basket = JSON.parse(sessionStorage.getItem("pa_basket"));
    var productids = {};
    basket.forEach(element => {
        // add id to productids
        if (element.id != null) {
            productids[element.id] = element.id;
        }
    });
    var url = addNonce(JQUERY_VARS.restUrl+"/customer/available-products");
    jQuery.get(url, { "token": sessionStorage.getItem("customertoken"), "productids": JSON.stringify(productids) }, function (result) {
        sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
        var result = JSON.parse(result);
        basket.forEach(element => { 
            result.forEach(product => {
                if (element.id == product.id) {
                    let normalmonthlyprice = 0;
                    let discountedPrice = null;
                    product.subscriptions.forEach(sub => { 
                        normalmonthlyprice =+ sub.price;
                        if (sub.discount != null) { 
                            switch (sub.discount.rule) { 
                                case "setprice":
                                    discountedPrice = sub.discount.value
                                    break;
                                case "percent":
                                    discountedPrice = normalmonthlyprice - (normalmonthlyprice * sub.discount.value / 100)
                                    break;
                                case "amount":
                                case "line":
                                    discountedPrice = sub.price-sub.discount.value  * sub.vat;
                                    break;
                            }
                            element.discountedPrice = discountedPrice.toFixed(2);
                        }
                    })
                    element.normalprice = normalmonthlyprice;
                    element.startdate = new Date(element.actionDate * 1000);
                }
            })
            if (element.action == "remove") { 
                element.name = element.name + " (Lukning af produkt)";
                element.normalprice = "";
                element.discountedPrice = "";
            }
            if (element.action == "config") {
                if (element.provModel == "conaxpacket") {
                    element.name = element.name + " (Ændring i kanaler)";
                } else { 
                    element.name = element.name + " (Ændringer i produkt)";
                }
                element.normalprice = "";
                element.discountedPrice = "";
            }
            let html = '<div class="basket-item-container">\
                            <div class="basket-item-name" >'+element.name+'</div>';
                            if (element.discountedPrice != null) {
                                html = html + '<div class="basket-item-discountedprice" >' + element.discountedPrice + '</div>';
                            } else { 
                                html = html + '<div class="basket-item-normalprice" >'+ element.normalprice+'</div>';
                            }
                            html = html+ '<div class="basket-item-startdate" >'+element.startdate.getDate()+"/"+(element.startdate.getMonth()+1)+"/"+element.startdate.getFullYear()+'</div>\
                            <div class="basket-item-quantity">'+ element.quantity +'</div>\
                            <div class="basket-item-remove"><button title="Fjern produkt" onClick="removeFromBasket('+element.id+'); window.location.reload();" ><i class="fa fa-trash"></i></button></div>\
                    </div><br>';
            jQuery(".basket-order-button").show();
            jQuery(".basket-container").append(html);
        })
    })
}