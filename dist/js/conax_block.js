function fetchChannels() { 
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+'/prchannels'),
        credentials: 'same-origin',
        success: function (data) {
            sessionStorage.setItem('pa_channels', getChannelsOrganized(data));
        },
        error: function(data){
            //Kunne ikke hente...
        }
    });
}

export function init() {
    if(sessionStorage.getItem('pa_channels') == null){
        fetchChannels()
    }
    //Get available products if they are not already present
    if (sessionStorage.getItem('pa_availableProducts') == null) {
        if(sessionStorage.getItem('pa_channels') == null){
            fetchChannels()
        }   
        jQuery.ajax({
            type: 'GET',
            url: addNonce(JQUERY_VARS.restUrl+'/customer/product-configs?token=' + sessionStorage.getItem('customertoken') + "&is-master=" + sessionStorage.getItem('isMaster')),
            credentials: 'same-origin',
            success: function (data) {
                let availProd = {};
                for (const element of JSON.parse(data)  ) {
                    availProd[element.id] = element;
                }
                sessionStorage.setItem('pa_availableProducts', JSON.stringify(availProd));
                fetchCustomerProducts();
                buildDependencies();
            },
            error: function(data){
                //Kunne ikke hente...
            }
        });
    }else{
        fetchCustomerProducts();
        buildDependencies();
    }
    
}

function buildDependencies(){
	if((sessionStorage.getItem('pa_requires') == null) || (sessionStorage.getItem('pa_isrequired') == null)){
		var products = JSON.parse(sessionStorage.getItem('pa_availableProducts'));
		var productsO = new Object();
        //requires contains arrays of products required by product identified by the key (pcid)
		var requires = new Object();
        //isrequired contains arrays of products that require the product identified by the key (pcid)
		var isrequired = new Object();

		for(const i in products){
			productsO[products[i].id] = products[i];
		}

        for(const id in productsO){
			if(productsO[id].accessGrantingProducts.length > 0){
				requires[id] = [];
				for(const aid in productsO[id].accessGrantingProducts){
					let tempReq = [];

                    for(const bid in productsO[id].accessGrantingProducts[aid]){
                        let requiresID = productsO[id].accessGrantingProducts[aid][bid];
                        tempReq.push({id: requiresID, name: getProductName(productsO[requiresID])});

                        if(isrequired[requiresID] == null){
                            isrequired[requiresID] = [];
                        }
                        isrequired[requiresID].push({id: id, name: getProductName(productsO[id])});
                    }
                    
                    requires[id].push(tempReq);

				}
			}
		}

        sessionStorage.setItem('pa_requires', JSON.stringify(requires));
        sessionStorage.setItem('pa_isrequired', JSON.stringify(isrequired));
	}
}

function fetchCustomerProducts(){
    //Get customer products always
        jQuery.ajax({
            type: 'GET',
            url: addNonce(JQUERY_VARS.restUrl+'/customer/products?token='+sessionStorage.getItem('customertoken')+"&is-master="+sessionStorage.getItem('isMaster')),
            credentials: 'same-origin',
            success: function (data) {
                let custProd = {};
                for (const element of JSON.parse(data)  ) {
                    custProd[element.id] = element;
                }
                sessionStorage.setItem('pa_customerProducts', JSON.stringify(custProd));
                sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);

                presentConax();

            },
            error: function(data){
                //Kunne ikke hente...
            }
        });
}


function presentConax(){
    var products = JSON.parse(sessionStorage.getItem('pa_availableProducts'));
    var channels = JSON.parse(sessionStorage.getItem('pa_channels'));
    var customerProductsIds = getCustomerProductIds();
	sessionStorage.setItem('pa_customerProductIds', JSON.stringify(customerProductsIds));
    var elementId = "";
    var conaxSingle = [];
    var conaxMulti = [];
    var conaxMix = [];
    var conaxEvent = [];

    //Find
    for(const i in products){
        if(products[i].isAvailableInSelfService){
            if(products[i].provisioningModel == "conaxpacket"){

                if(channels[products[i].id].length == 1 && jQuery(".conax-container-singlepackets")[0]){
                    addConaxPacketSingle(products[i], channels[products[i].id], conaxSingle);
                }
                if(channels[products[i].id].length > 1 && jQuery(".conax-container-multipackets")[0]){
                    //Mix packets
                    if(products[i].parameters.indexOf('packetmix') >= 0 && jQuery(".conax-container-mixpackets")[0]){
                        addConaxPacketMix(products[i], channels[products[i].id], conaxMix);
                    }else{
                        addConaxPacketMulti(products[i], channels[products[i].id], conaxMulti);
                    }
                }
            }

            if(products[i].provisioningModel == "conaxeventpacket"){
                if(jQuery(".conax-container-eventpackets")[0]){
                    addConaxPacketEvent(products[i], channels[products[i].id], conaxEvent);
                }
            }
        }
    }

    //Sort
    var csAttrs = jQuery(".conax-container-singlepackets").data('attributes');
    var cmAttrs = jQuery(".conax-container-multipackets").data('attributes');
    var ceAttrs = jQuery(".conax-container-eventpackets").data('attributes');
    var cxAttrs = jQuery(".conax-container-mixpackets").data('attributes');
    sortItems(csAttrs, conaxSingle);
    sortItems(cmAttrs, conaxMulti);
    sortItems(ceAttrs, conaxEvent);
    sortItems(cxAttrs, conaxMix);

    //Present
    for(var a in conaxSingle){
        jQuery(".conax-container-singlepackets").append(conaxSingle[a].html);
        //Set the conax product selected if it is active
        if (conaxSingle[a].elementId != "" && (customerProductsIds[conaxSingle[a].id] || isSelected(conaxSingle[a].id))) {
            if (customerProductsIds[conaxSingle[a].id]) { 
                jQuery("#" + conaxSingle[a].elementId).addClass("channel-owned");
                if(isSelected(conaxSingle[a].id, "remove")){
                    jQuery("#" + conaxSingle[a].elementId).addClass("channel-marked-for-removal");
                }
            } else { 
                jQuery("#" + conaxSingle[a].elementId).addClass("channel-selected");
            }
        }    
        jQuery('#' + conaxSingle[a].elementId).on('click', function() {
            setSelection(this.id, 'conaxpacket', '', 'channel-selected', 'channel-marked-for-removal');
        });

    }
    for(var b in conaxMulti){
        jQuery(".conax-container-multipackets").append(conaxMulti[b].html);
        //Set the conax product selected if it is active
        if (conaxMulti[b].elementId != "" && (customerProductsIds[conaxMulti[b].id] || isSelected(conaxMulti[b].id))) {
            if (customerProductsIds[conaxMulti[b].id]) {
                jQuery("#" + conaxMulti[b].elementId).addClass("channel-owned");
                if(isSelected(conaxMulti[b].id, "remove")){
                    jQuery("#" + conaxMulti[b].elementId).addClass("channel-marked-for-removal");
                }
            } else {
                jQuery("#" + conaxMulti[b].elementId).addClass("channel-selected");
            }
        }
        jQuery('#' + conaxMulti[b].elementId).on('click', function() {
            setSelection(this.id, 'conaxpacket', '', 'channel-selected', 'channel-marked-for-removal');
        });
    }
    for(var c in conaxEvent){
        jQuery(".conax-container-eventpackets").append(conaxEvent[c].html);
        //Set the conax product selected if it is active
        if(conaxEvent[c].elementId != "" && (customerProductsIds[conaxEvent[c].id] || isSelected(conaxEvent[c].id))){
            if (customerProductsIds[conaxEvent[c].id]) { 
                jQuery("#" + conaxEvent[c].elementId).addClass("channel-owned");
                if(isSelected(conaxEvent[c].id, "remove")){
                    jQuery("#" + conaxEvent[c].elementId).addClass("channel-marked-for-removal");
                }
            } else {
                jQuery("#" + conaxEvent[c].elementId).addClass("channel-selected");
            }
        }
        jQuery('#' + conaxEvent[c].elementId).on('click', function() {
            setSelection(this.id, 'conaxeventpacket', '', 'channel-selected', 'channel-marked-for-removal');
        });
    }
    for(var c in conaxMix){
        jQuery(".conax-container-mixpackets").append(conaxMix[c].html);
        //Set the conax product selected if it is active
        if (conaxMix[c].elementId != "" && (customerProductsIds[conaxMix[c].id] || isSelected(conaxMix[c].id))) {
            if (customerProductsIds[conaxMix[c].id]) {
                jQuery("#" + conaxMix[c].elementId).addClass("channel-owned");
                if(isSelected(conaxMix[c].id, "remove")){
                    jQuery("#" + conaxMix[c].elementId).addClass("channel-marked-for-removal");
                }
            } else {
                console.log(conaxMix[c].name + ' Is selected');
                jQuery("#" + conaxMix[c].elementId).addClass("channel-selected");
            }
        }
        jQuery('#' + conaxMix[c].elementId).on('click', function() {
            setSelection(this.id, 'conaxpacket', 'mixpackets-channel', 'channel-selected', 'channel-marked-for-removal');
        });

    }
}

function isSelected(pcid, action = "add"){
    let itemInBasket = findInBasket(pcid);
    return (itemInBasket != null && itemInBasket.action == action);
}

function sortItems(attrs, items){
    if(attrs == null){
        return;
    }
    if(attrs.multiSort == 'name'){
        if(attrs.sortDir == 'asc'){            
            items.sort(orderByNameAsc);
        }else{
            items.sort(orderByNameDesc);
        }
    }else if(attrs.multiSort == 'price'){
        if(attrs.sortDir == 'asc'){            
            items.sort(orderBySubAsc);
        }else{
            items.sort(orderBySubDesc);
        }
    }
}

function orderByNameAsc(a, b){
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();
    if (nameA > nameB) {
      return 1;
    }
    if (nameA < nameB) {
      return -1;
    }
  
    // names must be equal
    return 0;
}
function orderByNameDesc(a, b){
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();
    if (nameA > nameB) {
      return -1;
    }
    if (nameA < nameB) {
      return 1;
    }
  
    // names must be equal
    return 0;
}
function orderBySubAsc(a, b){
    return (a.sub < b.sub) ? -1 : (a.sub > b.sub) ? 1 : 0;
}
function orderBySubDesc(a, b){
    return (a.sub < b.sub) ? 1 : (a.sub > b.sub) ? -1 : 0;
}

function accumulateSubs(subs){
    var aSub = 0;
    for(var n in subs){
        let from = subs[n].from == -1 ? new Date(0) : new Date(subs[n].from*1000);
        let to = subs[n].to == -1 ? new Date(((Date.now()/1000)+3600*24*365)*1000): new Date(subs[n].to*1000);

        if(isActive(from.toString(), to.toString())){
            aSub += subs[n].price - subs[n].discount;
        }
    }

    return aSub;
}



function addConaxPacketSingle(product, channels, conaxSingle){
    var elementId = 'sc_' + product.id;
    var csElement = new Object();
    csElement.id = product.id;
    csElement.elementId = elementId;
    csElement.name = getProductName(product);
    csElement.sub = accumulateSubs(product.subscriptions);

    //Show single channel packet
    csElement.html = '<div id="' + elementId + '" class="single-channel">'
                                                    + '<div class="single-channel-product-name">' + product.name + '</div>'
                                                    + '<div class="single-channel-logo"><img src="/wp-content/plugins/panther-selfservice-block/images/channel-logos/' + channels[0].logo + '"></img></div>'
                                                    + '<div class="single-channel-price">' + csElement.sub + '</div>'
                                                    + '</div>';
    conaxSingle.push(csElement);

}

function addConaxPacketMulti(product, channels, conaxMulti){
    var elementId = 'mc_' + product.id;
    var cmElement = new Object();
    cmElement.id = product.id;
    cmElement.elementId = elementId;
    cmElement.name = getProductName(product);
    cmElement.sub = accumulateSubs(product.subscriptions);

    //Show multi channel packet
    var cpmHtml = '<div id="' + elementId + '" class="multipackets-channel">'
        + '<div class="multipackets-channel-product-name">' + product.name + '</div>'
        + '<div class="multipackets-channel-logo-container">';

    var channelHtml = null;
    var channelsToSort = [];
    for(const j in channels){
        channelHtml = new Object();
        channelHtml["html"] = '<div class="multipackets-channel-logo"><img src="/wp-content/plugins/panther-selfservice-block/images/channel-logos/' + channels[j].logo + '"></img></div>';
        channelHtml["sortorder"] = channels[j].sortorder;
        channelsToSort.push(channelHtml);
    }

    channelsToSort.sort(orderBySortOrder);
    for(let i in channelsToSort){
        cpmHtml += channelsToSort[i].html;
    }
    cpmHtml += '</div><div class="multipackets-channel-price">' + cmElement.sub + '</div>'
            + '</div>';

    cmElement.html = cpmHtml;
    conaxMulti.push(cmElement);
}

function addConaxPacketMix(product, channels, conaxMix){
    var elementId = 'mx_' + product.id;
    var cmElement = new Object();
    cmElement.id = product.id;
    cmElement.elementId = elementId;
    cmElement.name = getProductName(product);
    cmElement.sub = accumulateSubs(product.subscriptions);

    //Show mix channel packet
    var cpmHtml = '<div id="' + elementId + '" class="mixpackets-channel">'
        + '<div class="mixpackets-channel-product-name">' + product.name + '</div>'
        + '<div class="mixpackets-channel-logo-container">';

    let defaultSlugs = getDefaultSelectedSlugs(product.parameters);

    var channelHtml = null;
    var channelsToSort = [];
    for(const j in channels){
        channelHtml = new Object();
        channelHtml["html"] = getConaxPacketMixChannel(channels[j], defaultSlugs);
        try{channelHtml["sortorder"] = channels[j].sortorderAdvanced[product.id];}catch( e){}
        channelsToSort.push(channelHtml);
    }

    channelsToSort.sort(orderBySortOrder);
    for(let i in channelsToSort){
        cpmHtml += channelsToSort[i].html;
    }

    cpmHtml += '</div><div class="mixpackets-channel-price">' + cmElement.sub + '</div>'
            + '</div>';

    cmElement.html = cpmHtml;
    conaxMix.push(cmElement);
}

function orderBySortOrder(a, b){
    return (a.sortorder < b.sortorder) ? -1 : (a.sortorder > b.sortorder) ? 1 : 0;
}

function getConaxPacketMixChannel(channel, defaultSelected){
    let channelClass = (Object.hasOwn(defaultSelected,channel.slug)) ? 'mix-channel-selected' : 'mix-channel';
    //Show required and default selected channels
    return '<div class="'+channelClass+'"><img src="/wp-content/plugins/panther-selfservice-block/images/channel-logos/' + channel.logo + '"></img></div>';
}

function addConaxPacketEvent(product, channels, conaxEvent){
    var elementId = 'ec_' + product.id;
    var ceElement = new Object();
    ceElement.id = product.id;
    ceElement.elementId = elementId;
    ceElement.name = getProductName(product);
    ceElement.sub = accumulateSubs(product.subscriptions);

    //Show event packet
    ceElement.html = '<div id="' + elementId + '" class="event-channel">'
                                                   + '<div class="event-channel-product-name">' + product.name + '</div>'
                                                   + '<div class="event-channel-logo"><img src="' + channels[0].logo + '"></img></div>'
                                                   + '<div class="event-channel-price">' + product.subscriptions[0].price + '</div>'
                                                   + '</div>';
    
    conaxEvent.push(ceElement);
                        
}

function getCustomerProductIds(){
    var cpids = new Object();

    sessionStorage.setItem('pa_customerChannelIds', JSON.stringify([]));

    var customerProducts = JSON.parse(sessionStorage.getItem('pa_customerProducts'));

    for(const i in customerProducts){
        //Only active products
        if(isActive(customerProducts[i].startDate, customerProducts[i].endDate)){
            cpids[customerProducts[i].productConf.id] = true;
            //If the product is TV packet or conax then add channels too
            //The value will be the parent pcid (TV packet or conax multi packet) and not just true
            getCustomerChannels(customerProducts[i]);
        }
    }

    return cpids;
}

function isActive(startDateStr, expireDateStr){
    if(startDateStr == null){return false;}
    let now = new Date();

    let startDate = new Date(startDateStr);

    if(startDate <= now){
        if(expireDateStr == null){return true;}
        let expireDate = new Date(expireDateStr);
        if(expireDate < now){return false;}
        return true;
    }

    return false;
}

function getCustomerChannels(product){
    var target = Object.assign({}, JSON.parse(sessionStorage.getItem('pa_customerChannelIds')));
    var channels = JSON.parse(sessionStorage.getItem('pa_channels'));


    //Extract channels from TV packet
    if(product.productConf.provisioningModel.indexOf("tvpacket") >= 0){
        extractChannels(product, channels["M" + product.productConf.mediationGroup], channels, target);
        sessionStorage.setItem('pa_customerChannelIds', JSON.stringify(target));
    }
    
    ///And we need to find the single channel conax product representing each channel in the conax packet
    if(product.productConf.provisioningModel.indexOf("conax") == 0){
        if(typeof channels[product.productConf.id] != 'undefined' && channels[product.productConf.id].length > 1){
            extractChannels(product, channels[product.productConf.id], channels, target);
            sessionStorage.setItem('pa_customerChannelIds', JSON.stringify(target));
        }
    }

}

function extractChannels(product, channelGroup, channels, target){
    for(let i in channelGroup){
        //We need to find the single channel conax product representing each channel in the TV packet or conax multi packet
        if(typeof channelGroup[i].prClearPacketID != "undefined"){
            var conaxProducts = channelGroup[i].prClearPacketID.split(',');

            for(var j=0; j<conaxProducts.length; j++){
                if(channels[conaxProducts[j]].length == 1){
                    target[conaxProducts[j]] = product.productConf.id;
                    break;
                }
            }
        }
    }
}

function getChannelsOrganized(chnls){
    var providerChannels = JSON.parse(chnls);
    var channels = providerChannels[0].channels;
    var sortedChannels = new Object();
    var channelId2Product = new Object();
    
    for(const ci in channels){
        //Which conax products contain the channel
        let mIds = channels[ci].prClearPacketID;
        let listIds = [];
        if(typeof mIds !== "undefined"){
            listIds = mIds.split(',');
        }
    
        for(var i=0; i<listIds.length; i++){
            if(sortedChannels[listIds[i]] == null){
                sortedChannels[listIds[i]] = [];
            }
            //The channel is inserted into a package for that specific conax product having pcid as key
            sortedChannels[listIds[i]].push(channels[ci]);
        }

        //Which TV packet products contain the channel
        let nIds = channels[ci].clearPacket;
        let nListIds = [];
        if(typeof nIds !== "undefined"){
            nListIds = nIds.split(',');
        }
    
        for(var i=0; i<nListIds.length; i++){
            if(!Number.isInteger(parseInt(nListIds[i]))){
                continue;
            }

            if(sortedChannels["M" + nListIds[i]] == null){
                sortedChannels["M" + nListIds[i]] = [];
            }
            //The channel is inserted into a package for that specific conax product having pcid as key
            sortedChannels["M" + nListIds[i]].push(channels[ci]);
        }

    }

    for(const cj in sortedChannels){        
        if(sortedChannels[cj].length == 1){
            let slugInfo = new Object();
            slugInfo['pcid'] = cj;
            slugInfo['slug'] = sortedChannels[cj][0].slug;
            channelId2Product[sortedChannels[cj][0].id] = slugInfo;
        }
    }

    sessionStorage.setItem('pa_channelId2Product', JSON.stringify(channelId2Product));
    return JSON.stringify(sortedChannels);
}