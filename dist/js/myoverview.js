// JS code for My overview block

export function init(){
    jQuery.ajax({
        type: "GET",
        url: addNonce(JQUERY_VARS.restUrl+"/customer?token="+sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function(data){
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var customer = JSON.parse(data);
            jQuery(".pa-my-overview-address>.value").text(customer.address);
            jQuery(".pa-my-overview-customernumber>.value").text(customer.memberNumber);
            jQuery(".pa-my-overview-name>.value").text(customer.name);
        },error: function(data){
            console.log('error fetching customer info');
        }
    });
    var hideDecimals = jQuery(".pa-my-overview").data("decimals");
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+"/customer/products?token="+sessionStorage.getItem('customertoken')+"&is-master="+sessionStorage.getItem('isMaster')),
        credentials: 'same-origin',
        success: function(data) {
            var monthly = 0, quarterly;
            var provmodels = jQuery(".pa-my-overview").data('provmodels').split(',');
            var productIdents = jQuery(".pa-my-overview").data('productidents').split(',');
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var products = JSON.parse(data);
            console.log(products);
            products.forEach(product => {
                if(provmodels.includes(product.productConf.provisioningModel)){
                    jQuery(".pa-my-overview-product>.value").text(product.productConf.selfserviceNames.da);
                }

                var result = productIdents.filter(function(item){
                    return product.productConf.ident.indexOf(item) > -1;            
                });
                if(result.length){
                    jQuery(".pa-my-overview-product>.value").text(product.productConf.selfserviceNames.da);
                }
                if(new Date(product.endDate) >= new Date() || product.endDate == null){
                    if(hideDecimals){
                        monthly += parseInt(product.vatSum);
                    } else {
                        monthly += parseFloat(product.vatSum);
                    }
                }
            });
            quarterly = monthly*3;
            jQuery(".pa-my-overview-monthly>.value").text(monthly+' kr');
            jQuery(".pa-my-overview-quarterly>.value").text(quarterly+' kr');
            return products;
        }
    });


}