import "https://unpkg.com/swiper@11.0.7/swiper-bundle.js";
export function init() {

    let amountOfProducts = jQuery(".subscription-container").data("amount-of-products");
    let products = jQuery(".subscription-container").data("products");
    let productIds = [];
    for (let index = 0; index < amountOfProducts; index++) {
        const element = products[index];
        productIds.push(element.productId);
    }

    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+'/customer/products?token=' + sessionStorage.getItem('customertoken')) + "&productids=" + JSON.stringify(productIds),
        credentials: 'same-origin',
        success: function (data) {
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var result = JSON.parse(data)
            if (result.length > 0) {
                var allClosed = true;
                result.forEach(element => {
                    var date = new Date(element.endDate)
                    if (date > new Date() || element.endDate == null ) { 
                        allClosed = false;
                    }
                });
                if (allClosed) {
                    initCarousel();
                } else {
                    let imageUrl;
                    let description;
                    for (let index = 0; index < amountOfProducts; index++) {
                        const element = products[index];
                        if (element.productId == result[0].productConf.id) {
                            imageUrl = element.imageUrl;
                            description = element.description;
                        }
                    }
                    jQuery("#subscription-current-product-name").text(result[0].productConf.selfserviceNames.da);
                    jQuery("#subscription-current-product-desc").text(description);
                    jQuery("#subscription-current-product-image>img").prop("src", imageUrl);
                    jQuery("#subscription-current-product-price").text(jQuery("#subscription-current-product-price").data("priceprmonth")+" : " + result[0].vatSum.toFixed(2) + jQuery("#subscription-current-product-price").data("priceunit"));
                    jQuery(".carousel-container").addClass("is-hidden");
                    jQuery(".subscription-current-product-container").removeClass("is-hidden");
    
                    jQuery("#subsscription-current-product-cancel-product").on("click", function () {
                        sessionStorage.setItem("currentProduct", JSON.stringify({ "pcid": result[0].productConf.id, "cpid": result[0].id }));
                    })
    
                    jQuery("#subscription-current-product-change-product").on("click", function () {
                        sessionStorage.setItem("ordersaleType", "change");
                        jQuery(".subscription-card-product-buy-btn>button[data-productid=" + result[0].productConf.id + "]").text("Nuværende");
                        jQuery(".subscription-card-product-buy-btn>button[data-productid=" + result[0].productConf.id + "]").prop("disabled", true);
                        sessionStorage.setItem("currentProductPrice", result[0].vatSum.toFixed(2));
                        jQuery(".subscription-current-product-container").addClass("is-hidden");
                        initCarousel();
                        sessionStorage.removeItem("pa_subs_current_product");
                        sessionStorage.setItem("pa_subs_current_product", result[0].productConf.id)
                    })
                }
            } else {
                initCarousel();
            }
        }
    })


    function initCarousel() {
        async function getProducts() {
            jQuery.ajax({
                type: 'GET',
                url: addNonce(JQUERY_VARS.restUrl+'/customer/available-products?token=' + sessionStorage.getItem('customertoken') + "&productids=" + JSON.stringify(productIds)),
                credentials: 'same-origin',
                success: function (data) {
                    sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000) + 900);
                    var result = JSON.parse(data);
                    sessionStorage.setItem("availableProducts", JSON.stringify(result));
                    result.forEach(element => { 
                        let card;
                        if (jQuery(".productid-" + element.id)) {
                            jQuery(".productid-" + element.id + ">.subscription-card-product-name").text(element.selfserviceNames.da);
                            let totalPrice = 0;
                            let currentUnixTimestamp = Math.floor(Date.now() / 1000);
                            element.subscriptions.forEach(element => {
                                if (element.from < currentUnixTimestamp && element.to > currentUnixTimestamp || element.to == -1) { 
                                    totalPrice += element.price;
                                } 
                            })
                            totalPrice = totalPrice.toFixed(2);
                            jQuery(".productid-" + element.id + ">.subscription-card-product-price").text(jQuery(".subscription-card-product-price").data("pricetext")+" : " + totalPrice + jQuery(".subscription-card-product-price").data("priceunit"));
                        }
                    })
                },
                error: function (data) {
                }
            })
        }

        getProducts().then(function () {
            jQuery(".carousel-container").removeClass("is-hidden");
            const swiper = new Swiper('.swiper', {
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                rewind: true,
                mousewheel: {
                    invert: true
                },
                keyboard: {
                    enabled: true,
                  },
                breakpoints: {
                    320: {
                        slidesPerView: 1,
                    },
                    768: {
                        slidesPerView: 2,
                        centeredSlides: true
                    }
                }
            });
            jQuery("#order-terms-link").attr("href", JQUERY_VARS.paymentTOSUrl);
        })
    }

    var saleType = sessionStorage.getItem("ordersaleType");
    var dateInputId;
    switch (saleType) {
        case "create":
            dateInputId = "#pa-date-select-Opstart";
            break
        case "close":
            dateInputId = "#pa-date-select-Opsigelse";
            break
        case "change":
            dateInputId = "#pa-date-select-Opgradering";
            break
        default:
            dateInputId = "#pa-date-select-Opstart";
            break
    }

    jQuery(".subscription-card-product-buy-btn>button").each(function () {
        jQuery(this).on("click", function () {
            var productId = jQuery(this).data("productid");
            var provModel = '';
            var productDesc = jQuery(this).data("productdesc");
            var productImg = jQuery(this).data("productimage");
            var products = JSON.parse(sessionStorage.getItem("availableProducts"));
            products.forEach(element => {
                if (productId == element.id) {
                    provModel = element.provisioningModel;
                    jQuery(".order-product-name").each(function () {
                        jQuery(this).text(element.name);
                    })
                    jQuery(".order-product-desc").text(productDesc);
                    jQuery(".order-product-image>img").prop("src", productImg);
                    var totalSubscriptionPrice = 0;
                    var totalOneTimePrice = 0;
                    element.subscriptions.forEach(subscription => {
                        totalSubscriptionPrice += subscription.price;
                    })
                    element.buys.forEach(buy => {
                        totalOneTimePrice += buy.price;
                    })
                    jQuery("#order-product-price-pr-month").text(totalSubscriptionPrice)
                    jQuery("#order-commitment").text(element.commitment)
                    if (element.commitment == 0) {
                        jQuery("#order-minimum-price").text(totalSubscriptionPrice + totalOneTimePrice);
                    } else {
                        jQuery("#order-minimum-price").text(totalSubscriptionPrice * element.commitment + totalOneTimePrice);
                    }
                    if (sessionStorage.getItem("currentProductPrice") && saleType == "change") {
                        if (totalSubscriptionPrice < sessionStorage.getItem("currentProductPrice")) {
                            dateInputId = dateInputId = "#pa-date-select-Nedgradering";
                            saleType = "downgrade";
                        } else {
                            saleType = "upgrade";
                        }
                    }
                    switch (saleType) {
                        case "create":
                            dateInputId = "#pa-date-select-Opstart";
                            jQuery("#pa-date-select-Opsigelse").addClass("is-hidden");
                            jQuery("#pa-date-select-Opgradering").addClass("is-hidden");
                            jQuery("#pa-date-select-Nedgradering").addClass("is-hidden");
                            break
                        case "close":
                            dateInputId = "#pa-date-select-Opsigelse";
                            jQuery("#pa-date-select-Opstart").addClass("is-hidden");
                            jQuery("#pa-date-select-Opgradering").addClass("is-hidden");
                            jQuery("#pa-date-select-Nedgradering").addClass("is-hidden");
                            break
                        case "upgrade":
                            dateInputId = "#pa-date-select-Opgradering";
                            jQuery("#pa-date-select-Opstart").addClass("is-hidden");
                            jQuery("#pa-date-select-Opsigelse").addClass("is-hidden");
                            jQuery("#pa-date-select-Nedgradering").addClass("is-hidden");
                            break
                        case "downgrade":
                            dateInputId = "#pa-date-select-Nedgradering";
                            jQuery("#pa-date-select-Opstart").addClass("is-hidden");
                            jQuery("#pa-date-select-Opsigelse").addClass("is-hidden");
                            jQuery("#pa-date-select-Opgradering").addClass("is-hidden");
                            break
                        default:
                            dateInputId = "#pa-date-select-Opstart";
                            jQuery("#pa-date-select-Opsigelse").addClass("is-hidden");
                            jQuery("#pa-date-select-Opgradering").addClass("is-hidden");
                            jQuery("#pa-date-select-Nedgradering").addClass("is-hidden");
                            break
                    }
                    // receipt data
                    jQuery("#order-receipt-product").text(element.name);
                    jQuery("#order-receipt-desc").text(productDesc);
                    jQuery("#order-receipt-image>img").prop("src", productImg);
                    jQuery("#order-receipt-product-price").text(totalSubscriptionPrice + " pr. mdr");
                    jQuery("#order-receipt-commitment").text("Bindingsperiode: " + element.commitment + " mdr.");
                }
            })

            jQuery(".order-confirm-button").on("click", function () {
                var pickedDate
                if (jQuery(dateInputId + ".hasDatepicker").length > 0) {
                    var dateTypeVar = jQuery(dateInputId).datepicker('getDate');
                    pickedDate = jQuery.datepicker.formatDate('@', dateTypeVar);
                } else {
                    pickedDate = jQuery(dateInputId).val();
                }
                jQuery("#order-receipt-startdate-date").text(jQuery.datepicker.formatDate('dd/mm/yy', dateTypeVar));

                addToBasket(productId, provModel, "add", 1, pickedDate);
                if (sessionStorage.getItem("pa_subs_current_product") > 0) { 
                    addToBasket(sessionStorage.getItem("pa_subs_current_product"), provModel, "remove", 1, pickedDate);
                }

                jQuery(".order-confirm-container").addClass("is-hidden");
                jQuery(".order-receipt-container").removeClass("is-hidden");
            })

            jQuery(".order-confirm-container").removeClass("is-hidden");
            jQuery(".carousel-container").addClass("is-hidden");
        })
    })

    jQuery("input#order-accept-terms").on("click", function () {
        if (jQuery(this).prop("checked") == true) {
            jQuery(".order-confirm-button").prop("disabled", false);
        } else {
            jQuery(".order-confirm-button").prop("disabled", true);
        }
    })




}