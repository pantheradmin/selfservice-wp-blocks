export function init(){
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+"/customer/products?token="+sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function(data) {
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var productsList = JSON.parse(data);
            if(productsList.length)
                listInternetProducts(productsList);
        },
        error: function(data){
        }
    });    
}

function listInternetProducts(productsList){
    var iAttributes = jQuery("#internet-info-container").data('attributes');
    var provisioningModels = iAttributes.provisioningModels.split(/[,; |]/);


    productsList.forEach(function(element){
        var provisioningModel = element.productConf.provisioningModel;
        if(provisioningModels.includes(provisioningModel))
            jQuery.ajax({
                type: 'GET',
                url: addNonce(JQUERY_VARS.restUrl+'/customer/products/'+element.id+'/internet-information?token='+sessionStorage.getItem('customertoken')),
                credentials: 'same-origin',
                success: function(data) {
                    var inetData = JSON.parse(data);
                    console.log(inetData);
                    var internetSection = '<div class="container">'
                                        + '<div id="iinfobox" class="columns">';


                    //Data may be bundled in various ways
                    if(typeof inetData.options === 'undefined')
                        inetData.options = inetData.optionsext;

                    var defaultValues = false; 

                    if(typeof inetData.options[iAttributes.factorySettingsKey] !== 'undefined')
                        defaultValues = (inetData.options[iAttributes.factorySettingsKey]['value'] == iAttributes.defaultValue);

                    if(defaultValues)
                        jQuery("#wifi-data-container").hide();


                    if(iAttributes.showProductNameRow){
                        internetSection += '<div class="column is-2"><div class="has-text-weight-semibold ">' + iAttributes.showProductNameLabel + '</div></div>';
                        internetSection += '<div class="column is-4"><div id="customer_name" class="is-family-secondary">' + inetData.productname + '</div></div>';
                    }
                    if(iAttributes.showModemMac){
                        internetSection += '<div class="column is-2"><div class="has-text-weight-semibold ">' + iAttributes.showModemMacLabel + '</div></div>';
                        internetSection += '<div class="column is-4"><div id="customer_name" class="is-family-secondary">' + inetData.modemmac + '</div></div>';
                    }
                    if(iAttributes.showModemIp){
                        internetSection += '<div class="column is-2"><div class="has-text-weight-semibold ">' + iAttributes.showModemIpLabel + '</div></div>';
                        internetSection += '<div class="column is-4"><div id="customer_name" class="is-family-secondary">' + inetData.modemip + '</div></div>';
                    }
                    if(iAttributes.showModemType){
                        internetSection += '<div class="column is-2"><div class="has-text-weight-semibold ">' + iAttributes.showModemTypeLabel + '</div></div>';
                        internetSection += '<div class="column is-4"><div id="customer_name" class="is-family-secondary">' + inetData.modemtype + '</div></div>';
                    }
                    if(defaultValues)
                        internetSection += '<div class="column is-12"><div id="defaultSettingsMessage" >' + iAttributes.factoryDefaultMessage + '</div></div>';

                    

                    if(!defaultValues){
                        if(iAttributes.showSSID){
                            internetSection += '<div class="column is-2"><div class="has-text-weight-semibold ">' + iAttributes.SSIDLabel + '</div></div>';
                            internetSection += '<div class="column is-4"><div id="customer_name" class="is-family-secondary">' + (typeof inetData.options[iAttributes.SSIDKey]['value'] !== 'undefined' ? inetData.options[iAttributes.SSIDKey]['value'] : inetData.options[iAttributes.SSIDKey]) + '</div></div>';
                        }
                        if(iAttributes.showSSID5){
                            internetSection += '<div class="column is-2"><div class="has-text-weight-semibold ">' + iAttributes.SSID5Label + '</div></div>';
                            internetSection += '<div class="column is-4"><div id="customer_name" class="is-family-secondary">' + (typeof inetData.options[iAttributes.SSID5Key]['value'] !== 'undefined' ? inetData.options[iAttributes.SSID5Key]['value'] : inetData.options[iAttributes.SSID5Key]) + '</div></div>';
                        }
                        if(iAttributes.showWpaPsk){
                            internetSection += '<div class="column is-2"><div class="has-text-weight-semibold ">' + iAttributes.wpaPskLabel + '</div></div>';
                            internetSection += '<div class="column is-4"><div id="customer_name" class="is-family-secondary">' + (typeof inetData.options[iAttributes.wpaPskKey]['value'] !== 'undefined' ? inetData.options[iAttributes.wpaPskKey]['value'] : inetData.options[iAttributes.wpaPskKey]) + '</div></div>';
                        }
                        internetSection += '</div>';
                    }
                    internetSection += '<div class="columns is-multiline is-mobile">';
                    if(iAttributes.showModemSettings){
                        internetSection += '<div class="column is-4"><div class="has-text-weight-semibold ">';
                        internetSection += '<button type="button" class="internet-settings-button" onClick="window.location.assign(\'/modem-data/?mac='+inetData.modemmac.replace(/\./g,'')+' \')">Rediger indstillinger</button></div></div>';
                    }
                    if(iAttributes.showAdditionalHTML.length){
                        internetSection += '<div class="column is-8"><div class="has-text-weight-semibold ">';
                        internetSection += iAttributes.showAdditionalHTML + '</div></div>';
                    }

                    internetSection += '</div></div>';
                    jQuery("#iicontent").append(internetSection);
                },
                error: function(data){
                    console.log('Internet data not found');
                }
            });
    });
}