export function init(){
    //Close all pages but the first
    var closeIt = false;
    jQuery('.signup-page').each(function() {
        if(closeIt)
            jQuery(this).hide();

        closeIt = true;
    });


    //Add listeners to buttons
    jQuery('.signup-page-break').each(function() {
        var attrs = jQuery(this).data('attributes');
        var previousPage = attrs.fromDiv || '';
        var fromPage = attrs.currentDiv;
        var toPage = attrs.toDiv || '';
        var thisPageBreakSubmits = attrs.thisPageBreakSubmits;
        var submitType = attrs.submitType || '';
        var acceptUrl = attrs.acceptUrl || '';
        var cancelUrl = attrs.cancelUrl || '';
        var apiTarget = attrs.whitelistAddress || '';
        var validateFunction = attrs.validateFunction || '';

        jQuery('#changepage' + fromPage + previousPage).on('click', function() {
            changePage(fromPage, previousPage, true);
        });

        jQuery('#changepage' + fromPage + toPage).on('click', function() {
            var isGood = true; 

            //Execute function defined in validateFunction            
            if (validateFunction && typeof window[validateFunction] === 'function') {
                isGood = window[validateFunction]();
            }

            if(thisPageBreakSubmits){
                if (apiTarget.trim().length) {
                    isGood = submitSignupAlternate(fromPage, toPage, isGood, submitType, acceptUrl, cancelUrl, apiTarget);
                }else{
                    isGood = submitSignup(fromPage, toPage, isGood, submitType, acceptUrl, cancelUrl);
                }
            }

            changePage(fromPage, toPage, isGood);
        });
    });
}




function changePage(from, to, isGood){
    if(isGood){
        jQuery("#" + from).hide();
        jQuery("#" + to).show();
    }
}

function submitSignup(from, to, isGood, submitType, acceptUrl, cancelUrl){
    if(isGood){
        var sdata = new Object();
        for(const key in sessionStorage)
            if(key.indexOf('pa_') === 0 && key != 'pa_products' && key != 'pa_adresse')
                sdata[key.replace('pa_','')]  =  sessionStorage.getItem(key);

        if(submitType != ''){
            //Data for payment gateway
            sdata["amount"] = 0;
            sdata["subscription"] = true;
            sdata["acceptUrl"] = acceptUrl;
            sdata["cancelUrl"] = cancelUrl;
            sdata["paymentMethods"] = submitType;
            jQuery.post(addNonce(JQUERY_VARS.restUrl+'/signup/paymentgatewaylink'), sdata, function(data){
                var result = JSON.parse(data);
                window.location.replace(result.url);
            }, 'json');

            return false;
        }

        jQuery.ajax({
            type:"post",
            url:addNonce(JQUERY_VARS.restUrl+'/submit-signup'),
            credentials: 'same-origin',
            data: JSON.stringify(sdata),
            success: function(data){
                changePage(from, to, true);
            }, error: function(data){
                window.parent.notifyError('Bestilling fejlet.');
            }
        });
    }
    
    return false;
}

//Submission not sent directly to Pantheradmin
function submitSignupAlternate(from, to, isGood, submitType, acceptUrl, cancelUrl, apiTarget){
    if(isGood){
        var attrs = jQuery("#pa-dawa-container").data('attributes');

        var sdata = new Object();
        for(const key in sessionStorage)
            if(key.indexOf('pa_') === 0 && key != 'pa_products' && key != 'pa_adresse')
                sdata[key.replace('pa_','')]  =  sessionStorage.getItem(key);


        sdata["apiTarget"] = apiTarget;

        if(submitType != ''){
            //Data for payment gateway
            sdata["amount"] = 0;
            sdata["subscription"] = true;
            sdata["acceptUrl"] = acceptUrl;
            sdata["cancelUrl"] = cancelUrl;
            sdata["paymentMethods"] = submitType;
            jQuery.post('/wp-json/pantheradmin/v1/signup/paymentgatewaylink?token=', sdata, function(data){
                var result = JSON.parse(data);
                window.location.replace(result.url);
            }, 'json');

            return false;
        }

        jQuery.ajax({
            type:"post",
            url:addNonce(JQUERY_VARS.restUrl+'/submit-signup'),
            credentials: 'same-origin',
            data: JSON.stringify(sdata),
            success: function(data){
                changePage(from, to, true);
            }, error: function(data){
                console.log(JSON.stringify(data));
                window.parent.notifyError('Bestilling fejlet.');
            }
        });
    }
    
    return false;
}
