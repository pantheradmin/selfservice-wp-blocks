export function init(){
    var products_container_attributes = jQuery("#pa-products").data('head');
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+"/customer/products?token="+sessionStorage.getItem('customertoken')+"&is-master="+sessionStorage.getItem('isMaster')),
        credentials: 'same-origin',
        success: function(data) {
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var table_body = jQuery("#pa-products");
            var table_head = jQuery("#pa-products-head");
            var products_table_headings = jQuery("#pa-products").data('head');
            var productBlacklist = products_table_headings['productBlacklist']?products_table_headings['productBlacklist'].split(','):[];
            if(products_table_headings['hideUnitOnRow']){
                var period_text = ' kr';
            } else {
                var period_text = ' pr. måned';
            }
            
            
            var parsed = JSON.parse(data);
            if('customername' in parsed[0]){
                table_head.append(
                    '<div class="column is-4 has-text-weight-semibold" >Navn</div>\
                    <div class="column is-3 has-text-weight-semibold" >Kundenr.</div>\
                    <div class="column is-4 has-text-weight-semibold" >Adresse</div>\
                    <div class="column is-1"></div>\
                    ');
                    var customers = JSON.parse(data);
                    if(window.location.hash.includes('#name') || window.location.hash == '') {
                        customers.sort(function(a,b){
                            var aName = a.customername.toUpperCase();
                            var bName = b.customername.toUpperCase();
                            if( aName < bName){ return -1; }
                            if( aName > bName){ return 1; }
                            return 0;
                        });
                    }
                    if(window.location.hash.includes('#membernum')){
                        customers.sort(function(a,b){
                            var isANumeric = a.customernumber.match(/^\d{1,}$/);
                            var isBNumeric = b.customernumber.match(/^\d{1,}$/);
                            if(isANumeric && isBNumeric){
                                return a.customernumber - b.customernumber;
                            }
                            var aNumber = a.customernumber.toUpperCase();
                            var bNumber = b.customernumber.toUpperCase();
                            if( a.customernumber < b.customernumber){ return -1; }
                            if( a.customernumber > b.customernumber){ return 1; }
                            return 0;
                        });
                    }
                    if(window.location.hash.includes('reverse')){
                        customers.reverse();
                    }
                    
                    var count = 1;
                    customers.forEach(function(customer){
                        var total = 0;
                        table_body.append(
                            // først indsættes kundens generelle information
                            // header class bruges til at forbinde show/hide toggle
                            '<div class="header columns is-mobile">\
                            <div class="column is-4" >'+customer.customername+'</div>\
                            <div class="column is-3" >'+customer.customernumber+'</div>\
                            <div class="column is-4" >'+customer.customeraddress+'</div>\
                            <div data-customer-number="'+customer.customernumber+'" class="foldIcon column is-1 dashicons dashicons-arrow-down-alt2"></div>\
                            </div>\
                            <div class="customer-'+customer.customernumber+' product columns is-hidden is-hidden-mobile">\
                            <div class="column has-text-weight-semibold">Produkt</div>\
                            <div class="column has-text-weight-semibold">Antal</div>\
                            <div class="column has-text-weight-semibold">Næste periode</div>\
                            <div class="column has-text-weight-semibold">Betalingsdato</div>\
                            <div class="column has-text-weight-semibold">Pris u. moms</div>\
                            <div class="column has-text-weight-semibold">Pris m. moms</div>\
                            </div>'
                            // dernæst loopes over den enkelte kundens produkter
                            )
                            customer.products.forEach(function(product){
                                
                                if(productBlacklist.includes(product.productConf.ident)){
                                    return;
                                }
                                
                                if(product.vatsum <= 0) {
                                    
                                } else {
                                    if(products_container_attributes['hideDecimals']){
                                        product.novatsum = parseInt(product.vatsum * 0.8);
                                    } else{
                                        product.novatsum = product.vatsum * 0.8;
                                        product.novatsum = product.novatsum.toFixed(2);
                                        product.novatsum = product.novatsum.toString().match(/\.\d/) ? product.novatsum : product.novatsum + ',00';
                                        product.novatsum = product.novatsum.toString().replace(/\./,',');
                                    }
                                    
                                    total += product.vatsum;
                                    if(products_container_attributes['hideDecimals']){
                                        product.vatsum = parseInt(product.vatSum);
                                    } else {
                                        product.vatsum = product.vatsum.toString().match(/\.\d/) ? product.vatsum : product.vatsum + ',00';
                                        product.vatsum = product.vatsum.toString().replace(/\./,',');
                                    }

                                    product.next_invoice_period = product.next_invoice_period != '' ? product.next_invoice_period : '-';
                                    
                                    if (product.next_invoice_period.includes("sel_blactive")){
                                        product.next_invoice_period = product.next_invoice_period.replace("[sel_blactive]","aktiv");
                                    }
                                    if (product.next_invoice_period.includes("sel_blinactive")){
                                        product.next_invoice_period = product.next_invoice_period.replace("[sel_blinactive]","lukket");
                                    }
                                    if (product.next_invoice_period.includes("sel_blarrears")) {
                                        product.next_invoice_period = product.next_invoice_period.replace("[sel_blarrears]","restance lukket");
                                    }
                                    if (product.next_invoice_period.includes("sel_blinactivestart")) {
                                        product.next_invoice_period = product.next_invoice_period.replace("[sel_blinactivestart]","venter på opstart");
                                    }
                                    if (product.next_invoice_period.includes("sel_blsubclosed")) {
                                        product.next_invoice_period = product.next_invoice_period.replace("[sel_blsubclosed]","abb. lukket");
                                    }
                                    if (product.next_invoice_period.includes("sel_blfree")) {
                                        product.next_invoice_period = product.next_invoice_period.replace("[sel_blfree]","Gratis");
                                    }
                                    
                                    // product class bruges til at assigne display: none
                                    
                                    table_body.append(
                                        '<div id="productrow-'+ customer.customernumber +'" class="customer-'+customer.customernumber+' product columns is-hidden is-hidden-mobile">\
                                        <div  class="column">'+product.name+'</div>\
                                        <div  class="column">'+product.quantity+'</div>\
                                        <div  class="column">'+product.next_invoice_period+'</div>\
                                        <div  class="column">'+product.next_bill_date+'</div>\
                                        <div  class="column">'+product.novatsum+period_text+'</div>\
                                        <div  class="column">'+product.vatsum+period_text+'</div>\
                                        </div>\
                                        <div class="customer-'+customer.customernumber+' box is-hidden-tablet is-hidden">\
                                        <div class="columns is-mobile">\
                                        <div class="column has-text-weight-semibold">Produkt: </div><div class="column">'+product.name+'</div>\
                                        </div>\
                                        <div class="columns is-mobile">\
                                        <div class="column has-text-weight-semibold">Antal.: </div><div class="column">'+product.quantity+'</div>\
                                        </div>\
                                        <div class="columns is-mobile">\
                                        <div class="column has-text-weight-semibold">U.moms: </div><div class="column">'+product.novatsum+period_text+'</div>\
                                        </div>\
                                        <div class="columns is-mobile">\
                                        <div class="column has-text-weight-semibold">M.moms: </div><div class="column">'+product.vatsum+period_text+'</div>\
                                        </div>\
                                        <div class="columns is-mobile">\
                                        <div class="column has-text-weight-semibold">Periode: </div><div class="column">'+product.nextInvoicePeriod+'</div>\
                                        </div>\
                                        <div class="columns is-mobile">\
                                        <div class="column has-text-weight-semibold">Næste betaling: </div><div class="column">'+product.nextBillDate+'</div>\
                                        </div>\
                                        </div>'
                                        )
                                    }})
                                })
                            } else {
                                table_head.append(
                                    '<div class="column is-4 has-text-weight-semibold" >'+products_table_headings['productRowLabel']+'</div>\
                                    <div class="column is-1 has-text-weight-semibold" >'+products_table_headings['amountRowLabel']+'</div>\
                                    <div class="column  is-2 has-text-weight-semibold" >'+products_table_headings['priceRowLabel']+'</div>\
                                    <div class="column is-3 has-text-weight-semibold" >'+products_table_headings['periodRowLabel']+'</div>\
                                    <div class="column is-2 has-text-weight-semibold" >'+products_table_headings['nextPaymentRowLabel']+'</div>\
                                    ');
                                    parsed.forEach(function(product){
                                        if(productBlacklist.includes(product.productConf.ident)){
                                            return;
                                        }
                                        if(product.vatSum <= 0) {
                                            
                                        } else {
                                            if(products_container_attributes['hideDecimals']){
                                                product.noVatSum = parseInt(product.vatSum * 0.8);
                                                product.vatSum = parseInt(product.vatSum);
                                            } else {
                                                product.noVatSum = product.vatSum * 0.8;
                                                product.noVatSum = product.noVatSum.toFixed(2);
                                                product.noVatSum = product.noVatSum.toString().match(/\.\d/) ? product.noVatSum : product.noVatSum + ',00';
                                                product.noVatSum = product.noVatSum.toString().replace(/\./,',');
                                                product.vatSum = product.vatSum.toString().match(/\.\d/) ? product.vatSum : product.vatSum + ',00';
                                                product.vatSum = product.vatSum.toString().replace(/\./,',');
                                            }
                                            
                                            product.next_invoice_period = product.next_invoice_period != '' ? product.next_invoice_period : '-';
                                        }
                                        var extraClass = "";
                                        if (product.nextInvoicePeriod.includes("sel_blactive")){
                                            product.nextInvoicePeriod = product.nextInvoicePeriod.replace("[sel_blactive]", "aktiv");
                                            extraClass = "product-active";
                                        }
                                        if (product.nextInvoicePeriod.includes("sel_blinactive")){
                                            product.nextInvoicePeriod = product.nextInvoicePeriod.replace("[sel_blinactive]", "lukket");
                                            extraClass = "product-inactive";
                                        }
                                        if (product.nextInvoicePeriod.includes("sel_blarrears")) {
                                            product.nextInvoicePeriod = product.nextInvoicePeriod.replace("[sel_blarrears]", "restance lukket");
                                            extraClass = "product-arrears";
                                        }
                                        if (product.nextInvoicePeriod.includes("sel_blinactivestart")) {
                                            product.nextInvoicePeriod = product.nextInvoicePeriod.replace("[sel_blinactivestart]", "venter på opstart");
                                            extraClass = "product-inactive-start";
                                        }
                                        if (product.nextInvoicePeriod.includes("sel_blsubclosed")) {
                                            product.nextInvoicePeriod = product.nextInvoicePeriod.replace("[sel_blsubclosed]", "abb. lukket");
                                            extraClass = "product-subclosed";
                                        }
                                        if (product.nextInvoicePeriod.includes("sel_blfree")) {
                                            product.nextInvoicePeriod = product.nextInvoicePeriod.replace("[sel_blfree]", "Gratis");
                                            extraClass = "product-free";
                                        }

                                        
                                        if (product.nextInvoicePeriod.includes("open")){
                                            product.nextInvoicePeriod = product.nextInvoicePeriod.replace("open", "aktiv");
                                            extraClass = "product-active";
                                        }
                                        if (product.nextInvoicePeriod.includes("closed")){
                                            product.nextInvoicePeriod = product.nextInvoicePeriod.replace("closed", "lukket");
                                            extraClass = "product-inactive";
                                        }
                                        if (product.nextInvoicePeriod.includes("arrears")) {
                                            product.nextInvoicePeriod = product.nextInvoicePeriod.replace("arrears", "restance lukket");
                                            extraClass = "product-arrears";
                                        }
                                        if (product.nextInvoicePeriod.includes("not started yet")) {
                                            product.nextInvoicePeriod = product.nextInvoicePeriod.replace("not started yet", "venter på opstart");
                                            extraClass = "product-inactive-start";
                                        }
                                        if (product.nextInvoicePeriod.includes("Sub. closed")) {
                                            product.nextInvoicePeriod = product.nextInvoicePeriod.replace("Sub. closed", "abb. lukket");
                                            extraClass = "product-subclosed";
                                        }
                                        if (product.nextInvoicePeriod.includes("Free")) {
                                            product.nextInvoicePeriod = product.nextInvoicePeriod.replace("Free", "Gratis");
                                            extraClass = "product-free";
                                        
                                        }
                                        
                                        table_body.append(
                                            '<div id="productrow-'+ product.productConf.id +'" class="columns is-hidden-mobile">\
                                            <div  class="column is-4">'+product.productConf.name+'</div>\
                                            <div  class="column is-1" >'+product.quantity+'</div>\
                                            <div  class="column is-2" >'+product.vatSum+period_text+'</div>\
                                            <div  class="column is-3 '+extraClass+'" >'+product.nextInvoicePeriod+'</div>\
                                            <div  class="column is-2" >'+product.nextBillDate+'</div>\
                                            </div>\
                                            <div class="box is-hidden-tablet">\
                                            <div class="columns is-mobile">\
                                            <div class="column has-text-weight-semibold">'+products_table_headings['productRowLabel']+' </div><div class="column">'+product.productConf.name+'</div>\
                                            </div>\
                                            <div class="columns is-mobile">\
                                            <div class="column has-text-weight-semibold">'+products_table_headings['amountRowLabel']+' </div><div class="column">'+product.quantity+'</div>\
                                            </div>\
                                            <div class="columns is-mobile">\
                                            <div class="column has-text-weight-semibold">'+products_table_headings['priceRowLabel']+' </div><div class="column">'+product.vatSum+period_text+'</div>\
                                            </div>\
                                            <div class="columns is-mobile">\
                                            <div class="column has-text-weight-semibold">'+products_table_headings['periodRowLabel']+' </div><div class="column">'+product.nextInvoicePeriod+'</div>\
                                            </div>\
                                            <div class="columns is-mobile">\
                                            <div class="column has-text-weight-semibold">'+products_table_headings['nextPaymentRowLabel']+' </div><div class="column">'+product.nextBillDate+'</div>\
                                            </div>\
                                            </div>\
                                            '
                                            );
                                        })
                                        
                                    }
                                    
                                    jQuery(".foldIcon").on('click', function(){
                                        var customerNumber = jQuery(this).data('customer-number');
                                        jQuery(this).parent().siblings('.customer-'+customerNumber).toggleClass('is-hidden');
                                        jQuery(this).toggleClass('dashicons-arrow-down-alt2');
                                        jQuery(this).toggleClass('dashicons-arrow-up-alt2');
                                    });
                                    
                                    
                                    let loadCount = 0;
                                    jQuery('[id^="productrow-"]').each(function () {
                                        if (loadCount >= products_container_attributes['loadCount']) {
                                            jQuery(this).toggle();
                                        }
                                        loadCount++;
                                    });
                                    
                                }
                            });
                            
                            if (jQuery("#loadmore_products").length) {
                                jQuery('#loadmore_products').on('click', function () {
                                    products_container_attributes['isLoadMoreButton'] = !products_container_attributes['isLoadMoreButton'];
                                    if (products_container_attributes['isLoadMoreButton']) {
                                        jQuery(this).text(products_container_attributes['loadMoreButtonLabel']);
                                    } else {
                                        jQuery(this).text(products_container_attributes['loadLessButtonLabel']);
                                    }
                                    
                                    let loadCount = 0;
                                    jQuery('[id^="productrow-"]').each(function () {
                                        if (loadCount >= products_container_attributes['loadCount']) {
                                            jQuery(this).slideToggle("slow");
                                        }
                                        loadCount++;
                                    });
                                });
                            }
                            
                        }