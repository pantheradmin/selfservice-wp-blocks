export function init(){
    var attrs = jQuery("#pa-dawa-container").data('attributes');

    var parms = {};
    if(attrs.zipLimit.length > 0){
        parms = {postnr: attrs.zipLimit.replaceAll(',','|')};
    }

    dawaAutocomplete.dawaAutocomplete( document.getElementById("adresse"), {
    select: function(selected) {
        clearData();
        jQuery("#dawaid").val(selected.data.id);
        jQuery("#dawaid").trigger("change");
        getAddressData(jQuery("#dawaid").val(), attrs.whitelistAddress);        
    },
    params: parms
    });
}


function getAddressData(dawaid, wurl){
    let sdata = [];
    jQuery.ajax({
        type: 'POST',
        url: addNonce(JQUERY_VARS.restUrl+"/signup/address/" + dawaid + "/product-configs"),
        credentials: 'same-origin',
        data: {'wurl': wurl},
        success: function (data) {
            sessionStorage.setItem("pa_adresse", encodeURI(jQuery("#adresse").val()));
            sessionStorage.setItem("pa_dawaid", dawaid);
            let pa_products = {};
            for (const element of JSON.parse(data)  ) {
                pa_products[element.id] = element;
            }
            jQuery("#nodelivery").val(0);
            if(pa_products.length == 0){
                jQuery("#nodelivery").val(1);
                jQuery("#nodelivery").trigger("change");
                return false;
            }        

            sessionStorage.setItem("pa_products", JSON.stringify(pa_products));
            jQuery("#dawaid").trigger("change");
            jQuery("#nodelivery").trigger("change");
        },
        error: function(data){
            jQuery("#nodelivery").val(1);
            jQuery("#nodelivery").trigger("change");
            return false;
        }
    });    

}

function clearData(){
    for(const key in sessionStorage){
        if(key.indexOf('pa_') === 0){
            sessionStorage.removeItem(key);
        }
    }
}