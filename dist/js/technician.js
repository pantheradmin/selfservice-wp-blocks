export function init(){
    jQuery("#preferred-date-form>input[name=token]").val(sessionStorage.getItem('customertoken'));
    if(sessionStorage.getItem('techTicketId')){
        jQuery("#technician-current-time-text").html('<div>Der er en ombooking undervejs.<i id="tech-loading-spinner" class="fas fa-spinner fa-spin"></i></div>');
        jQuery("#technician-open-modal-button").hide();
        checkTechRescheduleStatus("#technician-current-time-text")
    } else {
        jQuery.get(addNonce(JQUERY_VARS.restUrl+"/products/technician-required"), "token="+sessionStorage.getItem('customertoken'), function(data){
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var parsed = JSON.parse(data);
            if(parsed.techRequired == true){
                var currentTechDate = new Date(parsed.techDate);
                var currentTechDateMonth = currentTechDate.getMonth()+1;
                jQuery("#technician-current-time-text").text('Du har en tekniker tid d. '+currentTechDate.getDate()+'/'+currentTechDateMonth+'/'+currentTechDate.getFullYear());
                sessionStorage.setItem("techProductId",parsed.productId);
                customerProductId = sessionStorage.getItem("techProductId");
                jQuery("#technician-open-modal-button").show();
                var now = new Date(),
                // minimum date the user can choose, in this case now and in the future
                minDate = now.toISOString().substring(0,10);
                jQuery("#technician-calendar").attr('min', minDate);
                
                jQuery("#technician-open-modal-button").on('click', function(){
                    jQuery("#technician-modal").toggleClass("is-active");
                });
                
                jQuery(".modal-background").on('click', function(){
                    jQuery(this).parent().toggleClass('is-active');
                });
                jQuery(".modal-close").on('click', function(){
                    jQuery(this).parent().parent().toggleClass('is-active')
                });
                
                jQuery("#preferred-date-form").submit(function(e){
                    e.preventDefault();
                    jQuery.ajax({
                        url: addNonce(JQUERY_VARS.restUrl+'/products/'+customerProductId+'/technician/available-dates'),
                        type: 'post',
                        data:jQuery("#preferred-date-form").serialize(),
                        success:function(data){
                            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
                            Date.prototype.getFullMinutes = function () {
                                if (this.getMinutes() < 10) {
                                    return '0' + this.getMinutes();
                                }
                                return this.getMinutes();
                            };
                            const weekday = ["Man.", "Tirs.", "Ons.", "Tors.", "Fre.","Lør.","Søn."];
                            
                            var parsed = JSON.parse(data);
                            if(parsed.technicianNeeded){
                                parsed.appointments.forEach(element => {
                                    var from = new Date(element.from);
                                    var to = new Date(element.to);
                                    var now = new Date();
                                    if(from.getFullYear() == to.getFullYear()){
                                        if(from.getMonth() == to.getMonth()){
                                            if(from.getDate() == to.getDate()){
                                                var date = weekday[from.getDay()]+' '+from.getDate()+'/'+from.getMonth()+'/'+from.getFullYear();
                                                var fromToTime = from.getHours()+':'+from.getFullMinutes()+'-'+to.getHours()+':'+to.getFullMinutes();
                                                jQuery("#technician_datetime").append(new Option(date+' '+fromToTime,'{"from":"'+element.from+'", "to":"'+element.to+'"}'));
                                            }
                                        }
                                    }
                                });
                                jQuery("#technician_datetime").attr("disabled", false);
                                jQuery("#technician-time-form").show();
                            }
                        }
                        
                    });
                });
                
                jQuery("#technician-time-form").submit(function(e){
                    e.preventDefault();
                    jQuery.ajax({
                        url: addNonce(JQUERY_VARS.restUrl+'/products/'+customerProductId+'/technician/reschedule?token='+sessionStorage.getItem('customertoken')),
                        type: 'post',
                        data: JSON.parse(jQuery("#technician_datetime").val()),
                        success:function(data){
                            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
                            var parsedResponse = JSON.parse(data);
                            sessionStorage.setItem('techTicketId', parsedResponse.ticketid);
                            jQuery("#technician_datetime-btn").toggleClass("button");
                            jQuery("#technician_datetime-btn").toggleClass("is-loading");
                            sessionStorage.setItem('techCheckRetries', 0);
                            checkTechRescheduleStatus("#technician_datetime-btn");
                            
                        }
                        
                    });
                });
            }
        })
    }
}

function checkTechRescheduleStatus(element){
    var productId = sessionStorage.getItem("techProductId");
    if(sessionStorage.getItem('techCheckRetries') == 6){
        jQuery("#technician_datetime-btn").toggleClass("button");
        jQuery("#technician_datetime-btn").toggleClass("is-loading");
        jQuery(element).after('<div id="technician_error_msg">Noget gik galt under ombookingen, kontakt kundeservice.</div>');
        return false;
    }
    setTimeout(function(){jQuery.get(addNonce(JQUERY_VARS.restUrl+'/products/'+productId+'/technician/reschedule/'+sessionStorage.getItem('techTicketId')+'?token='+sessionStorage.getItem('customertoken')),function(data){
        sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
        var parsed = JSON.parse(data);
        if(parsed.status == 'DONE'){
            jQuery("#technician_datetime-btn").toggleClass("button");
            jQuery("#technician_datetime-btn").toggleClass("is-loading");
            jQuery("#technician_datetime-btn").hide();
            jQuery(element).after('<div id="technician_msg">Teknikeren er nu ombooket til ønsket tid.</div>');
            sessionStorage.removeItem('techCheckRetries');
            sessionStorage.removeItem('techTicketId');

            setTimeout(function(){window.location.reload()}, 10000);
        } else if(parsed.status == 'FAILED'){
            jQuery("#technician_datetime-btn").toggleClass("button");
            jQuery("#technician_datetime-btn").toggleClass("is-loading");
            jQuery("#technician_datetime-btn").hide();
            jQuery(element).after('<div id="technician_error_msg">Noget gik galt under ombookingen, kontakt kundeservice.</div>');
            sessionStorage.removeItem('techCheckRetries');
            sessionStorage.removeItem('techTicketId');
        } else {
            sessionStorage.setItem('techCheckRetries', (Number(sessionStorage.getItem('techCheckRetries'))+1))
            checkTechRescheduleStatus();
        }
    })}, 5000);
}