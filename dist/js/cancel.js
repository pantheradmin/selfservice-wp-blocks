export function init(){
    var dateInputId = "#pa-date-select-Opsigelse";
    var pcids = jQuery(".cancel-product-container").data("pcids").split(",");
    var current = JSON.parse(sessionStorage.getItem("currentProduct"));
    var provModel;
    if (!pcids.includes(current.pcid)){
        jQuery(".cancel-product-container").addClass("is-hidden");
    }

    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+'/customer/products?token='+sessionStorage.getItem('customertoken')+"&productids="+JSON.stringify([current.pcid])),
        credentials: 'same-origin',
        success: function(data) {
            var result = JSON.parse(data)
            if(result[0].id=current.cpid){
                jQuery(".cancel-product-name").text(result[0].productConf.selfserviceNames.da);
                jQuery(".receipt-product-name").text(result[0].productConf.selfserviceNames.da);
                provModel = result[0].productConf.provisioningModel;
            }
        }
    })

    jQuery(dateInputId).on("change", function(){
        if(jQuery(this).val() != ''){
            jQuery(".cancel-confirm-button").prop("disabled", false)
        }
    })


    jQuery(".cancel-confirm-button").on("click", function(){
        var pickedDate
        if(jQuery(dateInputId+".hasDatepicker").length > 0){
            var dateTypeVar = jQuery(dateInputId).datepicker('getDate')
            pickedDate = jQuery.datepicker.formatDate('dd/mm/yy', dateTypeVar)
        } else {
            pickedDate = jQuery(dateInputId).val()
        }

        addToBasket(current.pcid, provModel, "remove", 1);
        submitBasket("#");

        jQuery(".receipt-product-enddate").text(pickedDate)
        jQuery(".cancel-product-container").addClass("is-hidden")
        jQuery(".cancel-product-receipt-container").removeClass("is-hidden");
    })
    

}