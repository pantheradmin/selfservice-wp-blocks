export function init(){
    jQuery("#dawaid").on("change", prepareElements);    
}

function prepareElements(){
    jQuery('.signupinput').each(function() {
        var attrs = jQuery(this).data('attributes'); 
        
        if(attrs.isCustom){
            setupCustomElement(attrs);
        } else{
            setupElement(attrs);
        }
    });
}

function setupElement(attrs){
    var allProducts = JSON.parse(sessionStorage.getItem("pa_products"));
    var products = filterByAttrs(allProducts, attrs);
    var preparedData = prepareProductsList(products, attrs);

    //Adding first item as selected
    if(preparedData.length && attrs.selectType != "checkbox"){
        sessionStorage.setItem("pa_" + attrs.selectorName, preparedData[0].id);
    }

    switch(attrs.selectType){
        case "select":
            makeSelect(attrs, preparedData);
            break;
        case "radio":
            makeRadio(attrs, preparedData);
            break;
        case "checkbox":
            makeCheckbox(attrs, preparedData);
            break;
    }
}

function setupCustomElement(attrs){
    var preparedData = extractIdsAndNames(attrs.customLabels);

    //Adding first item as selected
    if(preparedData.length && attrs.selectType != "text" && attrs.selectType != "checkbox"){
        sessionStorage.setItem("pa_" + attrs.selectorName, preparedData[0].id);
    }


    switch(attrs.selectType){
        case "select":
            makeSelect(attrs, preparedData);
            break;
        case "radio":
            makeRadio(attrs, preparedData);
            break;
        case "checkbox":
            makeCheckbox(attrs, preparedData);
            break;
        case "text":
            makeText(attrs, preparedData, attrs.customRegex, attrs.customErrorMessage);
            break;
        }
}

function makeRadio(attrs, items){
    var html = '';
    var elementId = attrs.selectorName;
    var checked = '';
    var storageValue = '';
    var control = '';
    var name = '';
    var price = '';
    
    for(var i=0; i<items.length; i++){
        checked = (attrs.preSelectId == items[i].id) ? ' checked' : '';
        storageValue = items[i].id;//encodeURI(JSON.stringify(items[i]));
        control = '<input type="radio" name="' + elementId + '" id="' + elementId + '_' + items[i].id + '" value="' + items[i].id + 
                  '" onChange="sessionStorage.setItem(\'pa_' + elementId + '\', \'' + storageValue + '\');"' + checked + '>';
        name = items[i].name;

        var thtml = attrs.presentation;
        thtml = thtml.replace('{k}', control);
        thtml = thtml.replace('{n}', name);
        thtml = thtml.replace('{f}', calculatePrice(items[i], "f"));
        thtml = thtml.replace('{s}', calculatePrice(items[i], "s"));

        html += thtml;
    }

    if(html == ''){
        html = '<p><i>Ingen produkter fundet</i></p>';
    }

    jQuery("#pa-signup-select-container-" + elementId).html(html);
}

function makeCheckbox(attrs, items){
    var html = '';
    var elementId = attrs.selectorName;
    var storageValue = '';
    var control = '';
    var name = '';
    var price = '';

    for(var i=0; i<items.length; i++){
        sessionStorage.setItem("pa_" + elementId + '_' + items[i].id, 'false');
        storageValue = items[i].id;//encodeURI(JSON.stringify(items[i]));
        control = '<input type="checkbox" name="' + elementId + '_' + items[i].id + '" id="' + elementId + '_' + items[i].id + '" value="' + items[i].id + 
                  '" onChange="if(this.checked) sessionStorage.setItem(\'pa_' + elementId + '_' + items[i].id + '\',\'' + storageValue + '\'); else sessionStorage.setItem(\'pa_' + elementId + '_' + items[i].id + '\', \'false\');">';
        name = items[i].name;

        var thtml = attrs.presentation;        
        thtml = thtml.replace('{k}', control);
        thtml = thtml.replace('{n}', name);
        thtml = thtml.replace('{f}', calculatePrice(items[i], "f"));
        thtml = thtml.replace('{s}', calculatePrice(items[i], "s"));

        html += thtml;
    }

    if(html == ''){
        html = '<p><i>Ingen produkter fundet</i></p>';
    }

    jQuery("#pa-signup-select-container-" + elementId).html(html);
}

function makeSelect(attrs, items){
    var elementId = attrs.selectorName;
    var selected = '';
    var storageValue = '';
    var html = '<select name="' + elementId + '" id="' + elementId + '" onChange="sessionStorage.setItem(\'pa_' + elementId + '\', decodeURI(this.value));">';
    var control = '';
    var name = '';
    var price = '';
   
    for(var i=0; i<items.length; i++){
        storageValue = items[i].id;//encodeURI(JSON.stringify(items[i]));
        selected = (attrs.preSelectId == items[i].id) ? ' selected' : '';
        var thtml = '<option value="' + storageValue + '"' + selected + '>' + attrs.presentation + '</option>';
        name = items[i].name;
        thtml = thtml.replace('{k}', control);
        thtml = thtml.replace('{n}', name);
        thtml = thtml.replace('{f}', calculatePrice(items[i], "f"));
        thtml = thtml.replace('{s}', calculatePrice(items[i], "s"));

        html += thtml;
    }

    html += '</select>';

    jQuery("#pa-signup-select-container-" + elementId).html(html);
}

function makeText(attrs, items, regex, errorMessage){
    var elementId = attrs.selectorName;
    var html = '';
    var control = '';
    var name = '';
    var price = '';   

    for(var i=0; i<items.length; i++){
        sessionStorage.setItem("pa_" + elementId + '_' + items[i].id, '');
        control = '<input type="text" name="' + elementId + '_' + items[i].id + '" value="" onBlur="if(validText(this.value, \'' + errorMessage + '\', \'' + regex.replace(/\\/g,'\\\\') + '\'))sessionStorage.setItem(\'pa_' + elementId + '_' + items[i].id + '\', this.value);">';
        name = items[i].name;
 
        var thtml = attrs.presentation;        
        thtml = thtml.replace('{k}', control);
        thtml = thtml.replace('{n}', name);

        html += thtml;
    }

    jQuery("#pa-signup-select-container-" + elementId).html(html);
}

function filterByAttrs(products, attrs){
    var provModels = attrs.provisioningModel.split(/[,; |]/);
    var groupTree = attrs.group.split(/[,;|]/);
    var idents = attrs.productIdents.split(/[,; |]/);
    var ids = attrs.productIds.split(/[,; |]/);
    var onlySS = attrs.selfserviceProds;
    var prds = [];
    let matches = [];


    for(const i in products){
        //Does provisioning model match
        if(provModels.length && provModels[0].length){
            for(var j=0; j<provModels.length; j++){
                if(products[i].provisioningModel == provModels[j]){
                    if(!onlySS || products[i].isAvailableInSelfService){
                        matches.push(1);
                        break;
                    }
                }
            }
        }else{matches.push(1);}
 
        //Does group match
        if(groupTree.length && groupTree[0].length){
            for(var j=0; j<groupTree.length; j++){
                let prodGroupTree = products[i].groupFullPath.split('\/');
                if(prodGroupTree.indexOf(groupTree[j]) > -1){
                    if(!onlySS || products[i].isAvailableInSelfService){
                        matches.push(1);
                        break;
                    }
                }
            }
        }else{matches.push(1);}
        
        //Does ident match
        if(idents.length && idents[0].length){
            for(var j=0; j<idents.length; j++){
                if(products[i].ident == idents[j]){
                    if(!onlySS || products[i].isAvailableInSelfService){
                        matches.push(1);
                        break;
                    }
                }
            }
        }else{matches.push(1);}
    
        //Does id match
        if(ids.length && ids[0].length){
            for(var j=0; j<ids.length; j++){
                if(products[i].id == ids[j]){
                    if(!onlySS || products[i].isAvailableInSelfService){
                        matches.push(1);
                        break;
                    }
                }   
            }
        }else{matches.push(1);}

        if(matches.length == 4){
            prds.push(products[i]);
        }
        matches = [];
    }

    sortProducts(prds, attrs);

    return prds;
}

function prepareProductsList(products, attrs){
    var idn = [];

    //Add empty field pre-list
    if(attrs.noneOption && attrs.noneOptionAbove){
        pushProduct(idn, -1, attrs.noneOptionLabel);
    }

    for(var i=0; i<products.length; i++){
        idn.push(products[i]);
    }

    //Add empty field post-list
    if(attrs.noneOption && !attrs.noneOptionAbove){
        pushProduct(idn, -1, attrs.noneOptionLabel);
    }

    return idn;
}

function pushProduct(list, value, label){
    var tmpobj = new Object();
    tmpobj.id = value;
    tmpobj.name = label;
    list.push(tmpobj);
}

function extractIdsAndNames(labels){
    var lbls = labels.split(',');
    var idn = [];
    var tmpobj = null;
    for(var i=0; i<lbls.length; i++){
        tmpobj = new Object();
        tmpobj.id = i;
        tmpobj.name = lbls[i];
        idn.push(tmpobj);
    }

    return idn;
}

function calculatePrice(product, priceType){
    var accPrice = 0;

    if(priceType == "f"){
        for(const key in product.buys){
            accPrice += product.buys[key].price;
        }
    } else if(priceType == "s"){
        for(const key in product.subscriptions){
            accPrice += product.subscriptions[key].price;
        }
    }
    
    return accPrice.toLocaleString();
}

function sortProducts(prds, attrs){
    if(attrs.sortBy == "n"){
        if(attrs.sortByDir == "asc"){
            prds.sort(nameSortAsc);
        } else if(attrs.sortByDir == "dsc"){
            prds.sort(nameSortDsc);
        }

        return true;
    }

    //Summing fees and subs
    for(var i=0; i<prds.length; i++){
        prds[i].accFees = calculatePrice(prds[i], "f");
        prds[i].accSubs = calculatePrice(prds[i], "s");
    }

    if(attrs.sortBy == "s"){
        if(attrs.sortByDir == "asc"){
            prds.sort(subSortAsc);
        } else if(attrs.sortByDir == "dsc"){
            prds.sort(subSortDsc);
        }

    }else if(attrs.sortBy == "f"){
        if(attrs.sortByDir == "asc"){
            prds.sort(feeSortAsc);
        } else if(attrs.sortByDir == "dsc"){
            prds.sort(feeSortDsc);
        }
    }
    return true;
}



function nameSortAsc( a, b ) {
    if (a.selfserviceNames["da"] > b.selfserviceNames["da"]){
      return 1;
    }
    if (a.selfserviceNames["da"] < b.selfserviceNames["da"]){
      return -1;
    }
    return 0;
}

function nameSortDsc( a, b ) {
    if (a.selfserviceNames["da"] > b.selfserviceNames["da"]){
      return -1;
    }
    if (a.selfserviceNames["da"] < b.selfserviceNames["da"]){
      return 1;
    }
    return 0;
}

function subSortAsc( a, b ) {
    if (a.accSubs > b.accSubs){
      return 1;
    }
    if (a.accSubs < b.accSubs){
      return -1;
    }
    return 0;
}

function subSortDsc( a, b ) {
    if (a.accSubs > b.accSubs){
      return -1;
    }
    if (a.accSubs < b.accSubs){
      return 1;
    }
    return 0;
}

function feeSortAsc( a, b ) {
    if (a.accFees > b.accFees){
      return 1;
    }
    if (a.accFees < b.accFees){
      return -1;
    }
    return 0;
}

function feeSortDsc( a, b ) {
    if (a.accFees > b.accFees){
      return -1;
    }
    if (a.accFees < b.accFees){
      return 1;
    }
    return 0;
}