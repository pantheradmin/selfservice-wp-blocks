export function init(){
    autoRefreshValues();
}

function autoRefreshValues(){
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+"/wifi-devices?token="+sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function(data) {
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            console.log(data);
            var wifiData = JSON.parse(data);
            updateScreen(wifiData);
        },
        error: function(data){
        }
    });

    /*var testData = [];
    testData['devices'] = [];
    testData['clients'] = [];
    testData['dhcpleases'] = [];
    updateScreen(testData);*/
}
var maintablelocked = false;
var maintable2locked = false;

jQuery("#ap_wifi_toggle_advanced").on("click", function(){
        return toggleVisibility();
    });

function toggleVisibility(){
    jQuery( '.hideme' ).toggle();

        if(jQuery('.hideme').is(":visible")){
            maintablelocked = true;
            maintable2locked = true;
        }else{
            maintablelocked = false;
            maintable2locked = false;
        }

}

function updateScreen(wifiData){
    var wifiAttributes = jQuery("#wifi-data-container").data('attributes');
    var now = new Date();
    var result = wifiData;
    try{
      var tabledata = result;
      //console.log(result);
    }catch(e){
        console.log(e); //error in the above string(in this case,yes)!
        console.log('code: '+result); //error in the above string(in this case,yes)!
        setTimeout('autoRefreshValues();', 2000);
        return;
    }
    var tdarr = [];
    var ssid24 = '';
    var ssid50 = '';
    var ssid24c=0;
    var ssid50c=0;
    if(!maintablelocked){
    //console.log("Updating maintable");
    jQuery("#accesspoints_table > tbody").html("");
      for(var i=0;i<tabledata['devices'].length;i++)
        jQuery('#accesspoints_table > tbody:last-child').append('<tr>' + wifiDeviceGenerateTR(tabledata['devices'][i], wifiAttributes) + '</tr>');
    }

    var tdarr = [];
    if(!maintable2locked){
    //console.log("Updating maintable2");
      jQuery("#clients_table > tbody").html("");
      for(var i=0;i<tabledata['clients'].length;i++)
        jQuery('#clients_table > tbody:last-child').append('<tr>' + wifiClientGenerateTR(tabledata['clients'][i], tabledata['dhcpleases'], wifiAttributes) + '</tr>');
    }

    setTimeout(autoRefreshValues, 9000);
}

function jsuptime(sec) {
	var osec = sec;
	if(sec==0) return '-';
	if(sec==null) return '-';
	var hr = Math.floor(sec / 3600);
	var min = Math.floor((sec - (hr * 3600))/60);
	sec -= ((hr * 3600) + (min * 60));
	sec += ''; min += '';
	while (min.length < 2) {min = '0' + min;}
	while (sec.length < 2) {sec = '0' + sec;}
	hr = (hr)?''+hr+'t ':'';
	return hr + min+'m ' + sec+'s';//+' ('+osec+')';
}

function jsdate(inp_unix){
	var date = new Date(inp_unix*1000);
	var hours = date.getHours();
	var minutes = "0" + date.getMinutes();
	var seconds = "0" + date.getSeconds();
	var mday = '0'+date.getDate();
	var month = '0'+(date.getMonth()+1);
	return formattedTime = mday.substr(-2)+'/'+month.substr(-2)+' '+hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

}

function formatBytes(bytes,decimals) {
   if(bytes == 0) return '-';
   var k = 1000; // or 1024 for binary
   var dm = decimals + 1 || 1;
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
   var i = Math.floor(Math.log(bytes) / Math.log(k));
   return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function addVisibilityCSS(isAdvanced){
    var vcss = '';

    if(isAdvanced){
        vcss += ' class="hideme" style="display: none;"';
    }

    return vcss;
}

function wifiDeviceGenerateTR(r, wifiAttributes){
	var trhtml = '';
    if(wifiAttributes.showAName)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.aNameAdvanced) + '>' + r['name'] + '</td>';
    if(wifiAttributes.showAIP)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.aIPAdvanced) + '>' + r['ip'] + '</td>';
    if(wifiAttributes.showAModel)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.aModelAdvanced) + '>' + r['model'] + '</td>';
    if(wifiAttributes.showAVersion)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.aVersionAdvanced) + '>' + r['version'] + '</td>';
    if(wifiAttributes.showASSID2)
        trhtml += ((typeof r['ssid24']=='undefined') || (r['c24'] == 0))?'<td' + addVisibilityCSS(wifiAttributes.aSSID2Advanced) + '>' + '-' + '</td>':'<td' + addVisibilityCSS(wifiAttributes.aSSID2Advanced) + '>' + r['ssid24'] + '</td>';
    if(wifiAttributes.showAChannel2)
        trhtml += (r['c24'] == 0) ? '<td' + addVisibilityCSS(wifiAttributes.aChannel2Advanced) + '>' + 'Afbrudt' + '</td>' : '<td' + addVisibilityCSS(wifiAttributes.aChannel2Advanced) + '>' + r['c24'] + '</td>';
    if(wifiAttributes.showASSID5)
        trhtml += ((typeof r['ssid50']=='undefined') || (r['c50'] == 0))?'<td' + addVisibilityCSS(wifiAttributes.aSSID5Advanced) + '>' + '-' + '</td>':'<td' + addVisibilityCSS(wifiAttributes.aSSID5Advanced) + '>' + r['ssid50'] + '</td>';
    if(wifiAttributes.showAChannel5)
        trhtml += (r['c50'] == 0) ? '<td' + addVisibilityCSS(wifiAttributes.aChannel5Advanced) + '>' + 'Afbrudt' + '</td>' : '<td' + addVisibilityCSS(wifiAttributes.aChannel5Advanced) + '>' + r['c50'] + '</td>';
    if(wifiAttributes.showAUpTime)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.aUpTimeAdvanced) + '>' + jsuptime(r['uptime']) + '</td>';
    if(wifiAttributes.showAStatus)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.aStatusAdvanced) + '>' + statusgif(r['status']) + '</td>';
	return trhtml;
}

function wifiClientGenerateTR(r, dhcpleases, wifiAttributes){

	var trhtml = '';
    if(wifiAttributes.showCMAC)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.cMACAdvanced) + '>' + r['mac'] + '</td>';
    if(wifiAttributes.showCHardware)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.cHardwareAdvanced) + '>' + r['macoui'] + '</td>';
    if(wifiAttributes.showCName){
        if(dhcpleases[r['mac']]!=null)
            trhtml += '<td' + addVisibilityCSS(wifiAttributes.cNameAdvanced) + '>' + dhcpleases[r['mac']]['clientid'] + '</td>';
        else
            trhtml += '<td' + addVisibilityCSS(wifiAttributes.cNameAdvanced) + '>' + 'Ukendt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '</td>';
    }
    if(wifiAttributes.showCIP)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.cIPAdvanced) + '>' + r['ip'] + '</td>';
    if(wifiAttributes.showCAccessPoint)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.cAccessPointAdvanced) + '>' + r['ap'] + '</td>';
    if(wifiAttributes.showCRadio)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.cRadioAdvanced) + '>' + r['radio'] + '</td>';
    if(wifiAttributes.showCRXRate)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.cRXRateAdvanced) + '>' + r['rx_rate']+'M' + '</td>';
    if(wifiAttributes.showCTXRate)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.cTXRateAdvanced) + '>' + r['tx_rate']+'M' + '</td>';
    if(wifiAttributes.showCSignal)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.cSignalAdvanced) + '>' + '<span style="color:'+signalColorclass(r['signal'])+';">'+r['signal']+'</span>' + '</td>';
    if(wifiAttributes.showCDown)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.cDownAdvanced) + '>' + formatBytes(r['rx_bytes']) + '</td>';
    if(wifiAttributes.showCUp)
        trhtml += '<td' + addVisibilityCSS(wifiAttributes.cUpAdvanced) + '>' + formatBytes(r['tx_bytes']) + '</td>';
	//trhtml.push(jsuptime(r['uptime']));
	return trhtml;
}

function signalColorclass(val){
	if(val<=-76) return '#FF0000';
	if(val<=-70) return '#FFA500';
	return '#00AA00';
}
function statusgif(status){
	var gif = "grn1.gif";
	if(status=='pending') gif = 'ora1.gif';
	if(status=='booting') gif = 'ora_b11.gif';
	if(status=='unknown') gif = 'grn_b11.gif';
	if(status=='offline') gif = 'red1.gif';
	return '<center><img src=/wp-content/plugins/panther-selfservice-block/images/'+gif+' border=0></center>';
}

function jprintcell(attrstr,innerhtml){
	return '<td '+attrstr+'>'+innerhtml;
}
