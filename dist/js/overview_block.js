export function init() {
    var tvprod;
    var internetprod;

    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+'/customer?token=' + sessionStorage.getItem('customertoken') + "&is-master=" + sessionStorage.getItem('isMaster')),
        credentials: 'same-origin',
        success: function (data) { 
            let customer = JSON.parse(data);
            jQuery("#welcome-customer-name").append("<h2 class='wp-block-heading'>Velkommen " + customer.name) + "</h2>";
            
            switch(customer.invoiceBy){
                case "bs":
                var paymenttype = 'Betalingsservice';
                break;
                case "ean":
                var paymenttype = 'EAN-Nummer';
                break;
                case "email":
                var paymenttype = 'Faktura på email';
                break;
                case "card":
                var paymenttype = 'Kortbetaling';
                break;
                case "mobilepay":
                var paymenttype = 'Mobilepay';
                break;
                case "paper":
                var paymenttype = 'faktura';
                break;
            }

            jQuery(".overview-payments-paymentform").append("<div><b>Betalingsform:</b> "+paymenttype+"</div>");
        }
    });
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+'/customer/products?token=' + sessionStorage.getItem('customertoken') + "&is-master=" + sessionStorage.getItem('isMaster')),
        credentials: 'same-origin',
        success: function (data) {
            let custProd = {};
            for (const element of JSON.parse(data)) {
                custProd[element.id] = element;
            }
            sessionStorage.setItem('pa_customerProducts', JSON.stringify(custProd));
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000) + 900);

            for (const key in custProd) {
                if (custProd[key].hasOwnProperty("productConf")) {
                    switch (custProd[key].productConf.provisioningModel) {
                        case "conaxpacket":
                            if (custProd[key].productConf.parameters.includes("packetmix")) {
                                tvprod = custProd[key];
                                tvOverview(tvprod);
                            }
                            break;
                        case "cablemodem2":
                            internetprod = custProd[key];
                            internetOverview(internetprod);
                            internetStatus();
                            break;
                    }
                }
            }

            if (!tvprod) { 
                jQuery(".overview-tv-packet").append("<div>Du har ikke nogen aktiv TV pakke</div>");
            }
            if (!internetprod) { 
                jQuery(".overview-internet-speed").append("<div>Du har ikke noget aktivt internet produkt</div>");
            }
        },
        error: function (data) {
            console.log("Unable to fetch products");
        }
    });

    paymentOverview();

}

function internetOverview(internetProd) {
    let name = internetProd.productConf.selfserviceNames.da;
    let downstream;
    let upstream;
    let options = stringToObject(internetProd.options);
    if (options.hasOwnProperty("downstream-speed") && options.hasOwnProperty("upstream-speed")) {
        downstream = options["downstream-speed"] / 1000000;
        upstream = options["upstream-speed"] / 1000000;
    }

    let html = "<div>"
        + name
        + "</div>";
    
    if (downstream && upstream) { 
        html += "<div>" + downstream + "/" + upstream + "Mbps</div>";
    }

    jQuery(".overview-internet-speed").html(html);
}

function internetStatus() { 
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+"/wifi-devices?token="+sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function(data) {
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var wifiData = JSON.parse(data);
            let status = "offline";
            for (const key in wifiData.devices) {
                if (wifiData.devices[key].hasOwnProperty("status")) { 
                    if (status == "ok") {
                        // If one is ok, dont change
                    } else { 
                        status = wifiData.devices[key].status;
                    }
                }
            }
            jQuery(".overview-internet-status").html("<div>Status: "+statusgif(status)+"</div>");

        },
        error: function(data){
        }
    });
}

function tvOverview(tvprod) {
    let name = tvprod.productConf.selfserviceNames.da;
    let html = "<div>"
        + name
        + "</div>";

    jQuery(".overview-tv-packet").html(html);
    jQuery(".overview-tv-content").append("<div class='overview-tv-config'><a class='mix-config-link' href='/tv-config'><button class='tv-packet-config-button'>Konfigurér kanaler</button></a></div>");

}

function paymentOverview() {
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+"/customer/invoices?token=" + sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function (data) {
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000) + 900);
            var invoices = JSON.parse(data);
            let due, amount, paymenttype;
            let html = "";
            for (const key in invoices) {
                if (invoices[key].paid == 0) { 
                    if (!due) {
                        due = invoices[key].transaction.dueStamp;
                        amount = invoices[key].transaction.debitAmount;
                    } else { 
                        if (invoices[key].transaction.dueStamp < due) { 
                            due = invoices[key].transaction.dueStamp;
                            amount = invoices[key].transaction.debitAmount;
                        }
                    }
                }
            }

            if (due == null) {
                html += "<div>Du har ikke nogen afventede regninger pt.</div>";
            } else { 
                jQuery(".overview-payments-amount").append("<div>" + amount + " Kr. </div>");

                jQuery(".overview-payments-duedate").append("<div><b>Betales:</b> " + formatDueDate(due) + "</div>");
            }

        }
    });
}

function stringToObject(str) {
    const object = {};
    const entries = str.split(';');
    for (const entry of entries) {
        const [key, value] = entry.split(':');
        if (key && value) {
            const trimmedKey = key.trim();
            const trimmedValue = value.trim();

            // Skip empty key-value pairs and handle potential errors
            if (trimmedKey && trimmedValue) {
                try {
                    // Handle numerical values appropriately
                    const numericValue = parseInt(trimmedValue, 10);
                    // Only convert to number if it's a valid integer
                    if (!isNaN(numericValue)) {
                        object[trimmedKey] = numericValue;
                    } else {
                        object[trimmedKey] = trimmedValue;
                    }
                } catch (error) {
                    console.error(`Error parsing value for key '${trimmedKey}': ${error}`);
                }
            }
        }
    }

    return object;
}

function statusgif(status){
	var gif = "grn1.gif";
	if(status=='pending') gif = 'ora1.gif';
	if(status=='booting') gif = 'ora_b11.gif';
	if(status=='unknown') gif = 'grn_b11.gif';
	if(status=='offline') gif = 'red1.gif';
	return '<img class="internet-status-icon" src=/wp-content/plugins/panther-selfservice-block/images/'+gif+' border=0>';
}

function formatDueDate(dateString) { 
    let year = dateString.substr(6, 4);
    let month = dateString.substr(3, 2);
    let day = dateString.substr(0,2);

    return day+"/"+month+"/"+year;
}