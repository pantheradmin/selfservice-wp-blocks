export function init(){
    jQuery.ajax({
        type: "GET",
        url: addNonce(JQUERY_VARS.restUrl+"/customer?token="+sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function(data){
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var result = JSON.parse(data);      
            var progress_percent = result.customAttributes.ordreprogress;
            var progress_text = result.customAttributes.ordreprogresstext;
            if(progress_percent > 0) {
                jQuery(".order_progress_container").show();
                jQuery(".pa-progress-value").css("width",progress_percent+"%");
                jQuery(".pa-progress-value-pct").text(progress_percent+"%");
                jQuery(".pa-progress-message").append(progress_text);
            }
        }});
    }