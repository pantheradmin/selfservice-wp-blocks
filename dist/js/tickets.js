export function init(){
    var types = jQuery("#tickets-container").data('tickettypes');
    var cardView = jQuery("#tickets-container").data('cardview');
    var tickets_block_attributes = jQuery("#tickets-container").data('attributes');
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+'/customer/tickets?token=' + sessionStorage.getItem('customertoken') + "&is-master=" + sessionStorage.getItem('isMaster') + "&tickettypes=" + types),
        credentials: 'same-origin',
        success: function (data) {
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var result = JSON.parse(data);
            if (cardView == false) {
                jQuery("#tickets-container").append(
                    '<div class="modal" id="ticket-view-modal">\
                        <div class="modal-background"></div>\
                        <div class="modal-content">\
                            <div class="box">\
                                <div id="ticket-view-modal-title"></div>\
                                <div id="ticket-view-modal-content"></div>\
                                <div id="ticket-view-modal-footer"></div>\
                            </div>\
                            <button class="modal-close is-large" aria-label="close"></button>\
                        </div>\
                        </div>\
                    \
                    <div class="columns is-mobile">\
                        <div class="ticket-subject-title column is-3">Emne</div>\
                        <div class="ticket-type-title column is-3">Type</div>\
                        <div class="ticket-date-title column is-3">Dato</div>\
                        <div class="ticket-functions-title column is-3">Se meddelelse</div>\
                    </div>\
                    ');
            }

            result.forEach(element => {
                if (element.pdf == 1) {
                    element.content = '<div id="ticket-pdf-button-container"><a target="_blank" href="'+addNonce(JQUERY_VARS.restUrl+'/customer/tickets/' + element.ticketid + '/pdf?token=' + sessionStorage.getItem('customertoken')) + '"><button><span>PDF</span></button></a></div>'
                    element.type = 'Email';
                } else if (element.content.toString().includes('smssend info')) {
                    element.type = 'Sms';
                    var toNumber = element.content.slice(element.content.indexOf('Til:') + 4, element.content.indexOf('Body:'));
                    ;
                    var body = element.content.slice(element.content.indexOf('Body:') + 5);
                    element.content = '<div>SMS sendt til:' + toNumber + '</div><div>' + body + '</div>'
                } else {
                    element.type = 'Besked';
                }

                if (cardView == false) {
                    if (element.type == 'Sms' || element.type == 'Besked') {
                        // var subject = element.type == 'Sms' ? 'Sms afsendt' : '';
                        jQuery("#tickets-container").append(
                            '<div class="columns is-mobile" id="ticketrow-' + element.ticketid + '">\
                            <div class="ticket-subject column is-3"><p></p></div>\
                            <div class="ticket-type column is-3"><p>' + element.type + '</p></div>\
                            <div class="ticket-date column is-3">' + element.time + '</div>\
                            <div class="ticket-functions column is-3"><button class="ticket-view-content-button" data-tickettype="' + element.type + '" data-ticketcontent="' + element.content + '"><span>Se Besked</span></button></div>\
                        </div>\
                        \
                        ');
                    } else if (element.type == 'Email') {
                        jQuery("#tickets-container").append(
                            '<div class="columns is-mobile" id="ticketrow-' + element.ticketid + '">\
                                <div class="ticket-subject column is-3">' + element.subject + '</div>\
                                <div class="ticket-type column is-3">' + element.type + '</div>\
                                <div class="ticket-date column is-3">' + element.time + '</div>\
                                <div class="ticket-functions column is-3">' + element.content + '</div>\
                            </div>\
                            \
                            ');
                    }
                } else {
                    // If a box with ticket id already exists, append to that instead of creating a new box
                    if (jQuery("#ticketrow-" + element.ticketid).length) {
                        jQuery("#ticketrow-" + element.ticketid).append(
                            "<div class=\"ticket-title is-size-4\">\
                            " + element.time + "\
                            </div>\
                            <div class=\"ticket-body\">\
                                " + element.content + "\
                            </div>"
                        );
                    } else {
                        jQuery("#tickets-container").append(
                            "<div id=\"ticketrow-" + element.ticketid + "\"class=\"ticket box\">\
                                <div class=\"ticket-title is-size-4\">\
                                    " + element.time + "\
                                </div>\
                                <div class=\"ticket-body\">\
                                    " + element.content + "\
                                </div>\
                            </div>"
                        );
                    }
                }
            });
            if (cardView == false) {
                jQuery(".ticket-view-content-button").on('click', function () {
                    var content = jQuery(this).data('ticketcontent');
                    var type = jQuery(this).data('tickettype');
                    jQuery("#ticket-view-modal").toggleClass('is-active');
                    if (type == 'Sms') {
                        jQuery("#ticket-view-modal-content").html(content);
                    } else {
                        jQuery("#ticket-view-modal-content").text(content);
                    }
                });

                jQuery(".modal-background").on('click', function () {
                    jQuery(this).parent().toggleClass('is-active');
                });
                jQuery(".modal-close").on('click', function () {
                    jQuery(this).parent().parent().toggleClass('is-active')
                });
            }

            let loadCount = 0;
            jQuery('[id^="ticketrow-"]').each(function () {
                if (loadCount >= tickets_block_attributes['loadCount']) {
                    jQuery(this).toggle();
                }
                loadCount++;
            });

        },
        error: function (data) {
        }
    });

    if (jQuery("#loadmore_tickets").length) {
        jQuery('#loadmore_tickets').on('click', function () {
            tickets_block_attributes['isLoadMoreButton'] = !tickets_block_attributes['isLoadMoreButton'];
            if (tickets_block_attributes['isLoadMoreButton']) {
                jQuery(this).text(tickets_block_attributes['loadMoreButtonLabel']);
            } else {
                jQuery(this).text(tickets_block_attributes['loadLessButtonLabel']);
            }

            let loadCount = 0;
            jQuery('[id^="ticketrow-"]').each(function () {
                if (loadCount >= tickets_block_attributes['loadCount']) {
                    jQuery(this).slideToggle("slow");
                }
                loadCount++;
            });
        });
    }
}
