export function init(){
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+'/customer/fsecure?token='+sessionStorage.getItem('customertoken')+"&is-master="+sessionStorage.getItem('isMaster')),
        credentials: 'same-origin',
        success: function(data) {
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var result = JSON.parse(data);
            jQuery(".fsecure-order-button").hide();
            // jQuery("#fsecure_container").append("<div class=\"columns is-centered\"><div class=\"column is-2 is-hidden-touch\"><details class=\"fsecure-login-button\"><summary>Login oplysninger</summary><p>Brugernavn: "+result['username']+"<br>Adgangskode: "+result['password']+"</p></details></div></div>");
            jQuery("#fsecure_container").append("<div class=\"columns is-centered\"><button class=\"column is-2 is-hidden-touch fsecure-cancel-button\">Opsig</button></div>");
            // jQuery("#fsecure_container").append("<div class=\"columns is-centered is-mobile\"><div class=\"column is-6 is-offset-2 is-hidden-desktop\"><details class=\"fsecure-login-button\"><summary>Login oplysninger</summary><p>Brugernavn: "+result['username']+"<br>Adgangskode: "+result['password']+"</p></details></div></div>");
            jQuery("#fsecure_container").append("<div class=\"columns is-centered is-mobile\"><button class=\"column is-6 is-offset-2 is-hidden-desktop fsecure-cancel-button\">Opsig</button></div>");


            jQuery(".fsecure-cancel-button").on('click', function(data){
                jQuery.ajax({
                    type: 'DELETE',
                    url: addNonce(JQUERY_VARS.restUrl+'/customer/fsecure/cancel?token='+sessionStorage.getItem('customertoken')),
                    credentials: 'same-origin',
                    success: function(data) {
                        var result = JSON.parse(data);
                        console.log(result);
                        window.parent.notifySuccess("Produkt opsagt");
                        setTimeout(function(){location.reload()}, 3000);
                    },
                    error: function(data){
                        window.parent.notifyError("Der skete en fejl, pr�v igen senere");
                    }
                });
            });



        },
        error: function(data){
            jQuery(".fsecure-order-button").show();
            jQuery(".fsecure-order-button").on('click', function(data){
                jQuery.post(addNonce(JQUERY_VARS.restUrl+'/customer/fsecure/order?token='+sessionStorage.getItem('customertoken')), null, function(data){
                    window.parent.notifySuccess("Produkt tilf�jet");
                    setTimeout(function(){location.reload()}, 3000);
                });
            });
        }
    });

}