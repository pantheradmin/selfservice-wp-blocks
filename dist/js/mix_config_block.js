export function init() {
    import('./conax_block.js')
        .then((module) => {
            module.init();
    });
    
    if(sessionStorage.getItem('pa_availableProducts') == null){
        jQuery.ajax({
            type: 'GET',
            url: addNonce(JQUERY_VARS.restUrl+'/customer/product-configs?token='+sessionStorage.getItem('customertoken')+"&is-master="+sessionStorage.getItem('isMaster')),
            credentials: 'same-origin',
            success: function (data) {
                let availProd = {};
                for (const element of JSON.parse(data)  ) {
                    availProd[element.id] = element;
                }
                sessionStorage.setItem('pa_availableProducts', JSON.stringify(availProd));
                fetchCustomerProducts();
            },
            error: function(data){
                //Kunne ikke hente...
            }
        });
    }else{
        fetchCustomerProducts();
    }
}


function generateMixPage(){
    var mixPCs = getMixProductConfs();
    var mixInBasket = findMixInBasket(mixPCs);
    if((mixInBasket != null) && ((mixInBasket.action == 'add') || (mixInBasket.action == 'update') || (mixInBasket.action == 'config'))){
        //Config a new mix product (add/update event with mix in basket)

        buildMixScreen(mixInBasket, mixPCs[mixInBasket.id]);
    }else{
        //Edit active mix product (no mix in basket)
        let activeMix = findActiveMix(mixPCs);
        buildMixScreen(activeMix, mixPCs[activeMix.id]);
    }

}

function fetchCustomerProducts(){
    //Get customer products always
        jQuery.ajax({
            type: 'GET',
            url: addNonce(JQUERY_VARS.restUrl+'/customer/products?token='+sessionStorage.getItem('customertoken')+"&is-master="+sessionStorage.getItem('isMaster')),
            credentials: 'same-origin',
            success: function(data) {
                let custProd = {};
                for (const element of JSON.parse(data)  ) {
                    custProd[element.id] = element;
                }
                sessionStorage.setItem('pa_customerProducts', JSON.stringify(custProd));
                sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
                generateMixPage()

            },
            error: function(data){
                //Kunne ikke hente...
            }
        });
}

function findActiveMix(mixProductConfs){
    var cPrds = JSON.parse(sessionStorage.getItem('pa_customerProducts'));
    for (let p in mixProductConfs) {
        for(const i in cPrds){
            if(p == cPrds[i].productConf.id){
                let cItem = new Object();
                cItem['parameters'] = cPrds[i].options;
                cItem['id'] = cPrds[i].productConf.id;
                //Add config event to basket
                let aDate = Math.round(Date.now()/1000);
                pushToBasket({id: cPrds[i].productConf.id, ident: cPrds[i].productConf.ident, name: getProductName(cPrds[i].productConf), quantity: 1, provModel: cPrds[i].productConf.provisioningModel, parameters: cPrds[i].options, action: 'config', actionDate: aDate});

                return cItem;
            }
        }
    }
    return null;
}

function getExtraMixPoints(){
    var prds = JSON.parse(sessionStorage.getItem('pa_customerProducts'));
    var mPoints = 0;
    for(const p in prds){
        let sd = new Date(prds[p]['startDate']);
        let ed = (prds[p]['endDate'] != null && prds[p]['endDate'] > 0) ? new Date(prds[p]['endDate']) : false;

        if(sd <= Date.now() && (!ed || ed > Date.now())){
            if(prds[p].productConf.parameters.search('pointValue') >= 0){
                let parms = prds[p].productConf.parameters.split(';');
                for(const q in parms){
                    if(parms[q].search('pointValue') == 0){
                        let pvParm = parms[q].split(':');
                        mPoints += parseInt(pvParm[1].trim());
                    }
                }
            }
        }
    }

    return mPoints;
}

function setUpMixCounter(mixPC, selectedSlugs){
    let mixCounter = new Object();
    let params = mixPC.parameters;
    let paramList = params.split(';');

    for(let p in paramList){
        let paramPair = paramList[p].split(':');

        if(paramPair[0] == 'packetmix'){
            mixCounter['pcid'] = mixPC.id;
            mixCounter['size'] = parseInt(paramPair[1]) + parseInt(getExtraMixPoints());
            let mixAttrs = jQuery(".mix-container").data('attributes');
            mixCounter['ChannelLockDays'] = parseInt(mixAttrs.ChannelLockDays);

            break;
        }
    }

    sessionStorage.setItem('pa_mixCounter', JSON.stringify(mixCounter));
    setMixChannelCounter(selectedSlugs);
}

function buildMixScreen(currentMix, mixPC){
    var channels = JSON.parse(sessionStorage.getItem('pa_channels'));
    channels = channels[mixPC.id];
    var mixChannels = [];
    //Create pointValue list
    var pVal = new Object();


    //var mixHtml = '<div class="mix-config" data-content="' + encodeURI(currentMix.parameters) + '">'
    var mixHtml = '<div class="mix-config">'
                + '<div class="mix-config-product-name">' + getProductName(mixPC) + '</div>'
        + '<div class="mix-config-channel-count">&nbsp;</div>'
        + '<div class="mix-channel-container">';
    
    //Find
    var selectedSlugs = null;
    if(sessionStorage.getItem('pa_selectedSlugs') == null){
        selectedSlugs = getSelectedSlugs(currentMix, mixPC.parameters);
        sessionStorage.setItem('pa_selectedSlugs', JSON.stringify(selectedSlugs));
    }else{
        selectedSlugs = JSON.parse(sessionStorage.getItem('pa_selectedSlugs'));
    }

    for(const j in channels){
        let channelPackage = getEditMixChannel(channels[j], selectedSlugs);
        channelPackage.sortorder = parseInt(channelPackage.sortorder[mixPC.id]);
        mixChannels.push(channelPackage); 
        pVal[channelPackage.slug] = channelPackage.pointValue;
    }
    sessionStorage.setItem('pa_pointValues', JSON.stringify(pVal));

    //Sort
    mixChannels.sort(orderBySortOrder);

    //Show
    for(let i=0; i<mixChannels.length; i++){
        mixHtml += mixChannels[i].html;
    }

    mixHtml += '</div></div>';

    jQuery(".mix-container").append(mixHtml);

    //Add listeners to the channels
    for(let i=0; i<mixChannels.length; i++){   
        if(mixChannels[i].channelEvent != -1){
            jQuery('#slug_' + mixChannels[i].channelEvent).on('click', function() {
                setMixSelection(mixChannels[i].channelEvent);
            });
        }
    }

    setUpMixCounter(mixPC, selectedSlugs);

}

function orderBySortOrder(a, b){
    return (a.sortorder < b.sortorder) ? -1 : (a.sortorder > b.sortorder) ? 1 : 0;
}

function getEditMixChannel(channel, selectedSlugs){
    let channelPackage = new Object();
    let channelClass = 'mix-channel';
    let requiredMark = '&nbsp;';
    if(Object.hasOwn(selectedSlugs,channel.slug)){
        channelClass = 'mix-channel-selected';

        if(selectedSlugs[channel.slug] == 'M'){
            requiredMark = 'Obligatorisk';
        }else{
            //For how long is the channel locked
            let today = new Date();
            let expiry = (selectedSlugs[channel.slug] instanceof Date) ? selectedSlugs[channel.slug] : new Date(selectedSlugs[channel.slug]);
            let days = (expiry.getTime() - today.getTime())/(1000*60*60*24);
            let daysStr = Math.ceil(days) > 1 ? 'dage' : 'dag';
            requiredMark = expiry > today ? 'Låst ' + Math.ceil(days) + ' ' + daysStr : '&nbsp;';
        }
    }

    channelPackage.channelEvent = (requiredMark == '&nbsp;') ?  channel.id : -1;
    channelPackage.sortorder = channel.sortorderAdvanced;
    channelPackage.pointValue = channel.pointValue;
    channelPackage.slug = channel.slug;

    let mixAttrs = jQuery(".mix-container").data('attributes');
    let valueLabel = mixAttrs.valueLabel;
    valueLabel = valueLabel.replace('{antal}', channel.pointValue);


    channelPackage.html = '<div class="'+channelClass+'" id="slug_' + channel.id + '"><div style="font-size:9pt;">' + requiredMark + '</div><img src="/wp-content/plugins/panther-selfservice-block/images/channel-logos/' + channel.logo + '"></img><div>' + valueLabel + '</div></div>';
    return channelPackage;
}

function getSelectedSlugs(currentMix, productParameters){
    let defaultSelectedSlugs = getDefaultSelectedSlugs(productParameters);

    //If user has not yet selected anything
    if(currentMix.parameters == ""){
        return defaultSelectedSlugs;
    }

    let selectedSlugs = new Object();

    //Adding mandatory channels
    for(let s in defaultSelectedSlugs){
        if(defaultSelectedSlugs[s] == 'M'){
            selectedSlugs[s] = 'M';
        }
    }

    //Extract user selected channels
    //chosentvchannels:canal-9$1691841945,CNN$1691841945,discovery-dk$1691841945,discovery-science$1691841945,disney-channel$1691841945,dk-4$1691841945,Ekstrakanalen PPV$1691841972,esportstv$1691841972,eurosport-1-dk$1691841972,eurosport-2-dk$1691841972;
    let parameters = currentMix.parameters.split(';');
    let chosenChannels = null;
    for(let j=0; j<parameters.length; j++){
        chosenChannels = parameters[j].split(':');

        if(chosenChannels[0] == 'chosentvchannels'){
            break; 
        }
    }

    if(chosenChannels != null){
        let channelUnits = chosenChannels[1].split(',');

        for(let k=0; k<channelUnits.length; k++){
            let channelParts = channelUnits[k].split('\$');
            selectedSlugs[channelParts[0]] = new Date(parseInt(channelParts[1])*1000);
            //FOR TESTING ********************************
            //if(k%4==0)
            //    selectedSlugs[channelParts[0]] = new Date(Date.now() - 86400000);
        }
    }

    return selectedSlugs;
}

function setMixSelection(slugId){
    let selectedSlugs = JSON.parse(sessionStorage.getItem('pa_selectedSlugs'));
    let channelId2Product = JSON.parse(sessionStorage.getItem('pa_channelId2Product'));
	var customerProductIds = JSON.parse(sessionStorage.getItem('pa_customerProductIds'));
    var customerChannelIds = JSON.parse(sessionStorage.getItem('pa_customerChannelIds'));
    if(Object.hasOwn(selectedSlugs,channelId2Product[slugId].slug)){
        //Deselect
        //Is this channel required by other products
        if(!allowRemove(channelId2Product[slugId.pcid], {...customerProductIds,...customerChannelIds})){
            alert('Kanalen kan ikke afbestilles. Den kræves af andre aktive produkter.');
            return false;
        }

        delete selectedSlugs[channelId2Product[slugId].slug];
        jQuery("#slug_" + slugId).removeClass('mix-channel-selected').addClass('mix-channel');

    }else{
        //Select

        //Does this product require other products
        if(!allowAdd(channelId2Product[slugId.pcid], {...customerProductIds,...customerChannelIds})){
            alert('Kanalen kan ikke bestilles. Den kræver andre aktive produkter.');
            return false;
        }
        

        //let mixAttrs = jQuery(".mix-container").data('attributes');
        //parseInt(mixAttrs.ChannelLockDays)*24*60*60000 + Date.now();
        selectedSlugs[channelId2Product[slugId].slug] = "edited";
        jQuery("#slug_" + slugId).removeClass('mix-channel').addClass('mix-channel-selected');
    }

    sessionStorage.setItem('pa_selectedSlugs', JSON.stringify(selectedSlugs));
    setMixChannelCounter(selectedSlugs);
    window.dispatchEvent(new Event('pa_basket_update'));

}

function setMixChannelCounter(selectedSlugs){
    let mixCounter = JSON.parse(sessionStorage.getItem('pa_mixCounter'));
    let pointValues = JSON.parse(sessionStorage.getItem('pa_pointValues'));
    let mCount = 0;

    for(let s in selectedSlugs){
        if(selectedSlugs[s] != 'M'){mCount += pointValues[s];}
    }

    mixCounter['current'] = mCount;
    sessionStorage.setItem('pa_mixCounter', JSON.stringify(mixCounter));

    let mixAttrs = jQuery(".mix-container").data('attributes');
    let usageLabel = mixAttrs.usageLabel;
    usageLabel = usageLabel.replace('{antal}', mixCounter['current']);
    usageLabel = usageLabel.replace('{max}', mixCounter['size']);


    jQuery('.mix-config-channel-count').html(usageLabel);
}
