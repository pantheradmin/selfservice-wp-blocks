export function init(type){
    var cAttributes = jQuery("#pa-calendar-container-" + type).data('attributes');
    
    if(cAttributes.inSoManyEnabled)
        buildCalendar(cAttributes);
    else
        buildSelect(cAttributes);
}

function buildCalendar(attrs){
    var d = new Date();
   jQuery('#pa-calendar-container-' + attrs.calendarType).html('<input type="text" id="pa-date-select-' + attrs.calendarType + '" name="pa-date-select-' + attrs.calendarType + '" value="">' + 
                                                                '<input type="hidden" id="pa-calendar-startdate-' + attrs.calendarType + '" value="' + d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate() + '">' +
                                                                '<input type="hidden" id="pa-calendar-lock-in-date-' + attrs.calendarType + '" value="' + d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate() + '">');
        
    var holidays = getHolidays(d.getFullYear());
    holidays.concat(getHolidays(d.getFullYear() + 1));
    
    jQuery('#pa-date-select-' + attrs.calendarType).datepicker({
        dateFormat: "dd/mm/yy",
        dayNamesMin: [ "S", "M", "T", "O", "T", "F", "L" ],
        monthNames: [ "Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December" ],
        beforeShowDay: function(date){
           var wah = weekendsAndHolidays(date, holidays, attrs);
           var afa = afterFirstAvailable(date, new Date(), attrs);
           var iwr = isWithinRange(date, new Date(), attrs);
           var ccd = checkClosedDate(date, attrs);
           var iwl = !isWithinLockIn(date, attrs);
           return [ wah && afa && iwr && ccd && iwl];
        }
    });    
}

function buildSelect(attrs){
    var d = new Date();
    var holidays = getHolidays(d.getFullYear());
    holidays.concat(getHolidays(d.getFullYear() + 1));
    jQuery('#pa-calendar-container-' + attrs.calendarType).html('<input type="hidden" id="pa-calendar-startdate-' + attrs.calendarType + '" value="' + d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate() + '">' +
                                                                '<input type="hidden" id="pa-calendar-lock-in-date-' + attrs.calendarType + '" value="' + d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate() + '">');
    var availableDates = attrs.fixedDistanceEnabled ? getFixedRange(holidays, attrs) : getIntervalRange(holidays, attrs);
    
    var selectHtml = '<select name="pa-date-select-' + attrs.calendarType + '" id="pa-date-select-' + attrs.calendarType + '">\n';    
    for(var i=0; i< availableDates.length; i++)
        selectHtml += '<option value="' + availableDates[i] + '">' + availableDates[i] + '</option>\n';
    selectHtml += '</select>\n';
    
    jQuery('#pa-calendar-container-' + attrs.calendarType).html(jQuery('#pa-calendar-container-' + attrs.calendarType).html() + selectHtml);   
}

//Helpers
function isWithinLockIn(date, attrs){
    var lockInDate = new Date(jQuery('#pa-calendar-lock-in-date-' + attrs.calendarType).val());
    return lockInDate > date;
}

function getFixedRange(holidays, attrs){
    var dates = [];
    //YYYY-MM-DD
    var initialDate = jQuery('#pa-calendar-startdate-' + attrs.calendarType).val();
    var nextMatch = new Date(initialDate);
    setMidnight(nextMatch);
    var untilDate = new Date();
    setMidnight(untilDate);
    addPeriodToDate(untilDate, attrs.showDatesUntilUnit, attrs.showDatesUntil);
    while(nextMatch < untilDate){
        var daysAdded = 0;
        while(!weekendsAndHolidays(nextMatch, holidays, attrs) || !checkClosedDate(nextMatch, attrs)){
            addPeriodToDate(nextMatch, 'D', 1);
            daysAdded ++;
        }
        if(nextMatch > Date.now() && !isWithinLockIn(nextMatch, attrs))
            dates.push(nextMatch.getDate() + '/' + (nextMatch.getMonth()+1) + '/' + nextMatch.getFullYear());
        getDateBefore(nextMatch, 'D', daysAdded);
        addPeriodToDate(nextMatch, attrs.fixedDistanceFromStartDateUnit, attrs.fixedDistanceFromStartDate);
    }
    
    return dates;
}

function getIntervalRange(holidays, attrs){
    var dates = [];
    var nextMatch = new Date();
    setMidnight(nextMatch);
    
    if(attrs.theNthEnabled){
        if(attrs.inXNextComingUnit == 'M'){
            nextMatch.setMonth(nextMatch.getMonth() + getInt(attrs.inXNextComing));
            nextMatch.setDate(getInt(attrs.theNth));        
            loopDates(nextMatch, holidays, 'M', attrs, dates);
        } else if(attrs.inXNextComingUnit == 'Q'){
            var thisQuarter = Math.floor(nextMatch.getMonth() / 3);
            nextMatch.setMonth(thisQuarter  * 3 + getInt(attrs.inXNextComing) * 3);
            if(attrs.theNthUnits == 'D')
                nextMatch.setDate(getInt(attrs.theNth));
            else if(attrs.theNthUnits == 'M')
                nextMatch.setMonth(nextMatch.getMonth() + getInt(attrs.theNth));
            loopDates(nextMatch, holidays, 'Q', attrs, dates);
        } else if(attrs.inXNextComingUnit == 'Y'){
            nextMatch.setFullYear(nextMatch.getFullYear() + getInt(attrs.inXNextComing));
            nextMatch.setMonth(0);
            if(attrs.theNthUnits == 'D')
                nextMatch.setDate(getInt(attrs.theNth));
            else if(attrs.theNthUnits == 'M')
                nextMatch.setMonth(getInt(attrs.theNth));
            loopDates(nextMatch, holidays, 'Y', attrs, dates);        
        } 
    }else if(attrs.fixedYearlyDates.trim() != '')
            loopDatesList(attrs, dates);
   
    return dates;
}

function loopDates(nextMatch, holidays, scale, attrs, dates){
        var notAfter = new Date(nextMatch.getTime());
        getDateBefore(notAfter, attrs.untilDistanceToNextComingUnit, attrs.untilDistanceToNextComing);
        var d = new Date();
        if(notAfter < d)
            addPeriodToDate(nextMatch, scale, attrs.inXNextComing);
        
        var untilDate = d;
        setMidnight(untilDate);

        addPeriodToDate(untilDate, attrs.showDatesUntilUnit, attrs.showDatesUntil);

        while(nextMatch < untilDate){
            var daysAdded = 0;
            while(!weekendsAndHolidays(nextMatch, holidays, attrs) || !checkClosedDate(nextMatch, attrs)){
                addPeriodToDate(nextMatch, 'D', 1);
                daysAdded ++;
            }
            if(!isWithinLockIn(nextMatch, attrs))
                dates.push(nextMatch.getDate() + '/' + (nextMatch.getMonth()+1) + '/' + nextMatch.getFullYear());
            getDateBefore(nextMatch, 'D', daysAdded);
            addPeriodToDate(nextMatch, scale, attrs.inXNextComing);
        }

}

function loopDatesList(attrs, dates){
    var now = new Date();
    var d = new Date();
    var year = d.getFullYear();
    var datesList = attrs.fixedYearlyDates.trim().split(',');
    var untilDate = new Date();
    setMidnight(untilDate);

    addPeriodToDate(untilDate, attrs.showDatesUntilUnit, attrs.showDatesUntil);

    for(var i=0; i<datesList.length; i++){
        var dateItems = datesList[i].split('/');
        d.setFullYear(year);
        d.setDate(getInt(dateItems[0]));
        d.setMonth(getInt(dateItems[1]) - 1);
        if(d.getTime() >= now && d.getTime() < untilDate.getTime())
            dates.push(d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear());
        
        if(i == datesList.length-1 && d.getTime() < untilDate.getTime()){
            year++;
            i=-1;
        }
    }
}

function checkClosedDate(date, attrs){
    var datesList = attrs.closedDates.trim().split(',');
    var d = new Date();
    setMidnight(d);

    for(var i=0; i<datesList.length; i++){
        var dateItems = datesList[i].split('/');
        d.setFullYear(date.getFullYear());
        d.setDate(getInt(dateItems[0]));
        d.setMonth(getInt(dateItems[1]) - 1);
        if(d.getTime() == date.getTime())
            return false;
    }
    
    return true;    
}

//date will be altered
function getDateBefore(date, distUnit, dist){
    switch(distUnit){
        case 'D':
            date.setDate(date.getDate() - getInt(dist));
            break;
        case 'W':
            date.setDate(date.getDate() - getInt(dist)*7);
            break;
        case 'M':
            date.setMonth(date.getMonth() - getInt(dist));
            break;
        case 'Q':
            date.setMonth(date.getMonth() - getInt(dist)*3);
            break;
        case 'Y':
            date.setFullYear(date.getFullYear() - getInt(dist));
            break;
    }    
}

//date will be altered
function addPeriodToDate(date, unit, amount){
    switch(unit){
        case 'D':
            date.setDate(date.getDate() + getInt(amount));
            break;
        case 'W':
            date.setDate(date.getDate() + getInt(amount)*7);
            break;
        case 'M':
            date.setMonth(date.getMonth() + getInt(amount));
            break;
        case 'Q':
            date.setMonth(date.getMonth() + getInt(amount)*3);
            break;
        case 'Y':
            date.setFullYear(date.getFullYear() + getInt(amount));
            break;
    }    
}

function isWithinRange(date, today, attrs){
    switch(attrs.showDatesUntilUnit){
        case 'D':
            today.setDate(today.getDate() + getInt(attrs.showDatesUntil));
            break;
        case 'W':
            today.setDate(today.getDate() + getInt(attrs.showDatesUntil)*7);
            break;
        case 'M':
            today.setMonth(today.getMonth() + getInt(attrs.showDatesUntil));
            break;
        case 'Y':
            today.setFullYear(today.getFullYear() + getInt(attrs.showDatesUntil));
            break;
    }
    
    return date.getTime() < today.getTime();    
}

function afterFirstAvailable(date, today, attrs){
    
    switch(attrs.inSoManyUnit){
        case 'D':
            today.setDate(today.getDate() + getInt(attrs.inSoMany) - 1);
            break;
        case 'W':
            today.setDate(today.getDate() + getInt(attrs.inSoMany)*7 - 1);
            break;
        case 'M':
            today.setMonth(today.getMonth() + getInt(attrs.inSoMany));
            break;
        case 'Y':
            today.setFullYear(today.getFullYear() + getInt(attrs.inSoMany));
            break;
    }

    return date.getTime() >= today.getTime();
}

function weekendsAndHolidays(date, holidays, attrs){
    var theTimestamp = date.getTime(); 
    var isNotHoliday = (holidays.indexOf(theTimestamp) == -1) || attrs.includeHolidays;
    var isNotWeekend = (date.getDay() > 0 && date.getDay() < 6) || attrs.includeWeekends;
    
    return isNotHoliday && isNotWeekend;
}

function getHolidays(year){
	var oneDay = 86400000;
	var year = year;
	var easter = Easter(year);
	var dda = new Date(year, 0, 1, 0, 0, 0, 0);
	var ddb = new Date(year, 11, 24, 0, 0, 0, 0);
	var ddc = new Date(year, 11, 25, 0, 0, 0, 0);
	var ddd = new Date(year, 11, 26, 0, 0, 0, 0);
	var dde = new Date(year, 11, 31, 0, 0, 0, 0);
	var ddf = new Date(year, 5, 5, 0, 0, 0, 0);

	var holidays = [
		easter.getTime() - 3*oneDay,
		easter.getTime() - 2*oneDay,
		easter.getTime(),
		easter.getTime() + oneDay,
		easter.getTime() + 26*oneDay,
		easter.getTime() + 39*oneDay,
		easter.getTime() + 49*oneDay,
		easter.getTime() + 50*oneDay,
		dda.getTime(),
		ddb.getTime(),
		ddc.getTime(),
		ddd.getTime(),
		dde.getTime(),
		ddf.getTime()
	];

	return holidays;
}

function Easter(Y) {
    var C = Math.floor(Y/100);
    var N = Y - 19*Math.floor(Y/19);
    var K = Math.floor((C - 17)/25);
    var I = C - Math.floor(C/4) - Math.floor((C - K)/3) + 19*N + 15;
    I = I - 30*Math.floor((I/30));
    I = I - Math.floor(I/28)*(1 - Math.floor(I/28)*Math.floor(29/(I + 1))*Math.floor((21 - N)/11));
    var J = Y + Math.floor(Y/4) + I + 2 - C + Math.floor(C/4);
    J = J - 7*Math.floor(J/7);
    var L = I - J;
    var M = 3 + Math.floor((L + 40)/44);
    var D = L + 28 - 31*Math.floor(M/4);

    return new Date(Y, M-1, D, 0, 0, 0, 0);
}

function getInt(eInt){
    var vInt = parseInt(eInt, 10);
    return (isNaN(vInt)) ? 0 : vInt;
}

function setMidnight(aDate){
    aDate.setHours(0);
    aDate.setMinutes(0);
    aDate.setSeconds(0);
    aDate.setMilliseconds(0);    
}
