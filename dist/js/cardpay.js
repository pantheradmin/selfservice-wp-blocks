export function init(){
    var debtDueFormatted;
    var paymentgateway = false;

    //We need the paymentgateway
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+"/paymentgateway?token="+sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function(data){
            paymentgateway = JSON.parse(data);
            showContent();
        },
        error:function(data){
            window.parent.notifyError("Der skete en fejl. Der mangler info på siden.");
        }
    });


    function showContent(){
        jQuery.ajax({
            type: 'GET',
            url: addNonce(JQUERY_VARS.restUrl+"/customer?token="+sessionStorage.getItem('customertoken')),
            credentials: 'same-origin',
            success: function(data){
                sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
                var title = jQuery("#pa_invoices_debt_due_title");
                var body = jQuery("#pa_invoices_debt_due_body");
                var customer = JSON.parse(data);
                var urlParams = new URLSearchParams(window.location.search);
                var paidAmount = 0;
                var payment_block_attributes = jQuery("#cardpay_container").data('attributes');
                if(urlParams.has('amount')){
                    paidAmount = urlParams.get('amount');
                }
                switch(customer.invoiceBy){
                    case "bs":
                    var type = 'betalingsservice';
                    break;
                    case "ean":
                    var type = 'EAN-Nummer';
                    break;
                    case "email":
                    var type = 'faktura på email';
                    break;
                    case "card":
                    var type = 'automatisk kortbetaling';
                    break;
                    case "mobilepay":
                    var type = 'Mobilepay';
                    break;
                    case "paper":
                    var type = 'faktura';
                    break;
                }
                if(type != ''){
                    body.append('<div>'+payment_block_attributes['currentPaymentMethod']+' '+type+'.</div><br/>');
                }
                if(customer.debtDue-paidAmount > 0) {
                    debtDueFormatted = customer.debtDue.toString().match(/\.\d{1}\b/) ? customer.debtDue+"0" : customer.debtDue;
                    debtDueFormatted = debtDueFormatted.toString().match(/\.\d/) ? debtDueFormatted : debtDueFormatted + ',00';
                    debtDueFormatted = debtDueFormatted.toString().replace(/\./,',');
                    body.append(
                        '<div>'+payment_block_attributes['debtDueMessage']+'</div>\
                        <div>Beløb: <span style="color:red;">'+debtDueFormatted+'</span></div>\
                        <div><button type="button" id="debtdue-pay-button" data-debtdue-amount="'+customer.debtDue+'">'+payment_block_attributes['payDebtButtonText']+'</button></div>\
                        ');
                        
                        jQuery("#debtdue-pay-button").on('click', function(){
                            var debtDueAmount = jQuery(this).data('debtdue-amount');
                            jQuery("#subscription-payment-modal").toggleClass('is-active');
                            jQuery("#subscription-payment-modal-title").html('<h2>Betaling af udestående beløb</h2>');
                            var a2adebtbtnstr =  payment_block_attributes['showA2AButton'] ?'<button id="debtdue-modal-pay-button-a2a" class="block" type="button" data-debtdue-amount="'+customer.debtDue+'" style="margin-left: 10px" disabled>Gå til betaling med konto betaling</button>' : '';
                            var mpdebtbtnstr =  payment_block_attributes['showMPButton'] ? '<button id="debtdue-modal-pay-mobilepay-button" class="block" type="button" data-debtdue-amount="'+customer.debtDue+'" disabled>Gå til betaling med Mobilepay</button>' : '';
                            
                            var cardsubblock = '';
                            if(paymentgateway.gateway.toLowerCase() == 'epay' && customer.invoiceBy != 'card'){
                                cardsubblock = '<div class="block"><label for="subscription"><input type="checkbox" name="cardsubcp" id="cardsubcp" value="1"> Tilmeld automatisk kortbetaling</label></div>';
                            }    
                            
                            jQuery("#subscription-payment-modal-content").html('\
                            <div>\
                            <div class="block">Beløb: '+debtDueAmount+'</div>'
                                                  + cardsubblock +
                            '<div class="block"><label for="tos"><input type="checkbox" onchange="jQuery(\'#debtdue-modal-pay-button, #debtdue-modal-pay-button-a2a, #debtdue-modal-pay-mobilepay-button\').attr(\'disabled\', !this.checked)" name="tos" type="checkbox"/> Acceptér <a href="'+JQUERY_VARS.paymentTOSUrl+'" target="_blank" >betalings-vilkår</a></label></div>'+
                            '<button id="debtdue-modal-pay-button" class="block" type="button" data-debtdue-amount="'+customer.debtDue+'" disabled>Gå til betaling med kreditkort</button>'+
                            a2adebtbtnstr+
                            mpdebtbtnstr+
                            '</div>\
                            ');
                            jQuery("#subscription-payment-modal-footer").html('<div class="columns"><br/></div>');
                            
                            jQuery("#debtdue-modal-pay-button").on('click', function(){
                                var amount = jQuery(this).data('debtdue-amount');
                                jQuery(this).toggleClass(['button','is-loading']);
                                var subscription = jQuery('#cardsubcp').is(":checked") ? 1 : 0;

                                jQuery.get(addNonce(JQUERY_VARS.restUrl+'/paymentgateway/link?token='+sessionStorage.getItem('customertoken')), {'amount':amount*100, 'paymentMethods':'creditcard', 'subscription': subscription}, function(data){
                                    var result = JSON.parse(data);
                                    window.location.replace(result.url);
                                }, 'json');
                            });
                            
                            jQuery("#debtdue-modal-pay-mobilepay-button").on('click', function(){
                                var amount = jQuery(this).data('debtdue-amount');
                                jQuery(this).toggleClass(['button','is-loading']);
                                jQuery.get(addNonce(JQUERY_VARS.restUrl+'/paymentgateway/link?token='+sessionStorage.getItem('customertoken')), {'amount':amount*100, 'paymentMethods':'mobilepay'}, function(data){
                                    var result = JSON.parse(data);
                                    window.location.replace(result.url);
                                }, 'json');
                            });
                            
                            jQuery("#debtdue-modal-pay-button-a2a").on('click', function(){
                                var amount = jQuery(this).data('debtdue-amount');
                                jQuery(this).toggleClass(['button','is-loading']);
                                jQuery.get(addNonce(JQUERY_VARS.restUrl+'/paymentgateway/bs/pisp/register?token='+sessionStorage.getItem('customertoken')), {'amount':amount*100}, function(data){
                                    var result = JSON.parse(data);
                                    window.location.replace(result.data.pisp_checkout_url);
                                }, 'json');
                            });
                        });
                    } else {
                        var ongoingBSMandate = false;
                        if(payment_block_attributes['showBSMandateButton']){
                            if(customer.invoiceBy != 'betalingsservice'){
                                jQuery.get(addNonce(JQUERY_VARS.restUrl+'/paymentgateway/bsmandate/status?token='+sessionStorage.getItem('customertoken')), function(data){
                                    var result = JSON.parse(data);
                                    if(result.active >= 1){
                                        body.append('Du har allerede en igangværende tilmelding til betalingsservice');
                                        ongoingBSMandate = true;
                                    } else {
                                        body.append('<div>'+payment_block_attributes['subscribeBsMessage']+'</div>');
                                        if(customer.invoiceBy == "card"){
                                            body.append(' <div><button type="button" id="subscribtion-subscribe-button">'+payment_block_attributes['subscribeChangeCardButtonText']+'</button></div>');
                                        } else {
                                            body.append(' <div><button type="button" id="subscribtion-subscribe-button">'+payment_block_attributes['subscribeButtonText']+'</button></div>');
                                        }
                                        addCardListener(payment_block_attributes['cardpayInfo']);
                                        if(payment_block_attributes['showInvoiceByEmailButton']){
                                            if(customer.invoiceBy == "email"){
                                                body.append('<div><button disabled type="button" id="subscribtion-subscribe-email-button">'+payment_block_attributes['subscribeEmailButtonText']+'</button></div>');
                                            } else {
                                                body.append('<div><button type="button" id="subscribtion-subscribe-email-button">'+payment_block_attributes['subscribeEmailButtonText']+'</button></div>');
                                            }
                                            addEmailInvoiceListener(payment_block_attributes["emailpayInfo"]);
                                        }

                                        if(payment_block_attributes['showMPSubButton']){
                                            if(customer.invoiceBy != 'mobilepay' && ongoingBSMandate == false){
                                                body.append('<div><button type="button" id="subscribtion-subscribe-mp-button">'+payment_block_attributes['subscribeMPButtonText']+'</button></div>');
                                                addMPListener(payment_block_attributes['mobilepayInfo']);
                                            }
                                        }
                                        body.append('<div><button type="button" id="subscribtion-subscribe-bs-mandate-button" style="margin-top:10px">'+payment_block_attributes['subscribeBSMandateButtonText']+'</button></div>');
                                        addBSMandateListener(payment_block_attributes['bsMandateInfo']);
                                    }
                                }, 'json');
                            }
                        } else {
                            body.append('<div>'+payment_block_attributes['subscribeBsMessage']+'</div>');
                            
                            if(customer.invoiceBy == "card"){
                                body.append(' <div><button type="button" id="subscribtion-subscribe-button">'+payment_block_attributes['subscribeChangeCardButtonText']+'</button></div>');
                            } else {
                                body.append(' <div><button type="button" id="subscribtion-subscribe-button">'+payment_block_attributes['subscribeButtonText']+'</button></div>');
                            }
                            addCardListener(payment_block_attributes['cardpayInfo']);
                            if(payment_block_attributes['showInvoiceByEmailButton']){
                                if(customer.invoiceBy == "email"){
                                    body.append('<div><button disabled type="button" id="subscribtion-subscribe-email-button">'+payment_block_attributes['subscribeEmailButtonText']+'</button></div>');
                                } else {
                                    body.append('<div><button type="button" id="subscribtion-subscribe-email-button">'+payment_block_attributes['subscribeEmailButtonText']+'</button></div>');
                                }
                                addEmailInvoiceListener(payment_block_attributes["emailpayInfo"]);
                            }
                            
                            if(payment_block_attributes['showMPSubButton']){
                                if(customer.invoiceBy != 'mobilepay' && ongoingBSMandate == false){
                                    body.append('<div><button type="button" id="subscribtion-subscribe-mp-button">'+payment_block_attributes['subscribeMPButtonText']+'</button></div>');
                                    addMPListener(payment_block_attributes['mobilepayInfo']);
                                }
                            }
                        }

                        jQuery(".modal-close, .modal-background").click(function(){
                            jQuery("#subscription-payment-modal").toggleClass('is-active');
                        });

                        function addCardListener(infoText){
                            jQuery("#subscribtion-subscribe-button").on('click', function(){
                                jQuery("#subscription-payment-modal").toggleClass('is-active');
                                jQuery("#subscription-payment-modal-title").html('<h2>Tilmelding af automatisk kortbetaling.</h2>');
                                jQuery("#subscription-payment-modal-content").html('\
                                <div>\
                                <div>'+infoText+'</div>\
                                <div class="block"><label for="tos"><input type="checkbox" onchange="jQuery(\'#subscription-modal-subscribe-button\').attr(\'disabled\', !this.checked)" name="tos" type="checkbox"/> Acceptér <a href="'+JQUERY_VARS.paymentTOSUrl+'" target="_blank" >betalings-vilkår</a></label></div>\
                                <button id="subscription-modal-subscribe-button" class="block" type="button" disabled>Gå til tilmelding</button>\
                                </div>\
                                ');
                                jQuery("#subscription-payment-modal-footer").html('<div class="columns"><br/></div>');
                                jQuery("#subscription-modal-subscribe-button").on('click', function(){
                                    jQuery(this).toggleClass(['button','is-loading'])
                                    jQuery.get(addNonce(JQUERY_VARS.restUrl+'/paymentgateway/link?token='+sessionStorage.getItem('customertoken')), {"amount":0, "subscription":true}, function(data){
                                        var result = JSON.parse(data);
                                        window.location.replace(result.url);
                                    }, 'json');
                                });
                            });
                        }

                        function addEmailInvoiceListener(infoText){
                            jQuery("#subscribtion-subscribe-email-button").on('click', function(){
                                jQuery("#subscription-payment-modal").toggleClass('is-active');
                                jQuery("#subscription-payment-modal-title").html('<h2>Tilmelding af faktura via email.</h2>');
                                jQuery("#subscription-payment-modal-content").html('\
                                <div>\
                                <div>'+infoText+'</div>\
                                <div class="block"><label for="tos"><input type="checkbox" onchange="jQuery(\'#subscription-modal-subscribe-button\').attr(\'disabled\', !this.checked)" name="tos" type="checkbox"/> Acceptér <a href="'+JQUERY_VARS.paymentTOSUrl+'" target="_blank" >betalings-vilkår</a></label></div>\
                                <button id="subscription-modal-subscribe-button" class="block" type="button" disabled>Tilmeld fakura via email</button>\
                                </div>\
                                ');
                                jQuery("#subscription-payment-modal-footer").html('<div class="columns"><br/></div>');
                                jQuery("#subscription-modal-subscribe-button").on('click', function(){
                                    jQuery(this).toggleClass(['button','is-loading'])
                                    jQuery.ajax({
                                        type:"PUT",
                                        url:addNonce(JQUERY_VARS.restUrl+"/customer"),
                                        data: {"invoiceBy":"email", "token": sessionStorage.getItem('customertoken')},
                                        success: function(data){
                                            window.parent.notifySuccess("Skiftet til faktura via email.");
                                            setTimeout(function(){window.location.reload()}, 5000);
                                        },
                                        error:function(data){
                                            window.parent.notifyError("Der skete en fejl, prøv igen. Kontakt kundeservice hvis fejlen forsætter.");
                                        }
                                    });
                                });
                            });


                        }

                        function addMPListener(infoText){
                            jQuery("#subscribtion-subscribe-mp-button").on('click', function(){
                                jQuery("#subscription-payment-modal").toggleClass('is-active');
                                jQuery("#subscription-payment-modal-title").html('<h2>Tilmelding af Mobilepay abbonnement.</h2>');
                                jQuery("#subscription-payment-modal-content").html('\
                                <div>\
                                <div>'+infoText+'</div>\
                                <div class="block"><label for="tos"><input type="checkbox" onchange="jQuery(\'#subscription-modal-subscribe-button\').attr(\'disabled\', !this.checked)" name="tos" type="checkbox"/> Acceptér <a href="'+JQUERY_VARS.paymentTOSUrl+'" target="_blank" >betalings-vilkår</a></label></div>\
                                <button id="subscription-modal-subscribe-button" class="block" type="button" disabled>Gå til tilmelding</button>\
                                </div>\
                                ');
                                jQuery("#subscription-payment-modal-footer").html('<div class="columns"><br/></div>');
                                jQuery("#subscription-modal-subscribe-button").on('click', function(){
                                    jQuery(this).toggleClass(['button','is-loading'])
                                    jQuery.get(addNonce(JQUERY_VARS.restUrl+'/paymentgateway/link?token='+sessionStorage.getItem('customertoken')), {"amount":0, "subscription":true, "paymentMethods":"mobilepay-subscriptions"}, function(data){
                                        var result = JSON.parse(data);
                                        window.location.replace(result.url);
                                    }, 'json');
                                });
                            });
                        }
                        
                        function addBSMandateListener(infoText){
                            jQuery("#subscribtion-subscribe-bs-mandate-button").on('click', function(){
                                jQuery("#subscription-payment-modal").toggleClass('is-active');
                                jQuery("#subscription-payment-modal-title").html('<h2>Tilmelding af betalingsservice via App.</h2>');
                                jQuery("#subscription-payment-modal-content").html('\
                                <div>\
                                <div>'+infoText+'</div>\
                                <div class="block"><label for="tos"><input type="checkbox" onchange="jQuery(\'#subscription-modal-subscribe-bs-mandate-button\').attr(\'disabled\', !this.checked)" name="tos" type="checkbox"/> Acceptér <a href="'+JQUERY_VARS.paymentTOSUrl+'" target="_blank" >betalings-vilkår</a></label></div>\
                                <div><label for="subscription-modal-subscribe-cpr">Cpr. Nr.</label><input name="subscription-modal-subscribe-cpr" id="subscription-modal-subscribe-cpr" type="password" placeholder="CPR"></input></div>\
                                <div><label for="subscription-modal-subscribe-regnr">Reg. Nr.</label><input name="subscription-modal-subscribe-regnr" id="subscription-modal-subscribe-regnr" type="text" placeholder="Reg. Nr."></input></div>\
                                <div><label for="subscription-modal-subscribe-accnr">Konto. Nr.</label><input name="subscription-modal-subscribe-accnr" id="subscription-modal-subscribe-accnr" type="text" placeholder="Konto. Nr."></input></div>\
                                <button id="subscription-modal-subscribe-bs-mandate-button" class="block" type="button" disabled>Tilmeld</button>\
                                </div>\
                                ');
                                jQuery("#subscription-payment-modal-footer").html('<div class="columns"><br/></div>');
                                
                                jQuery("#subscription-modal-subscribe-bs-mandate-button").on('click', function(){
                                    jQuery(this).toggleClass(['button','is-loading'])
                                    if(jQuery("#subscription-modal-subscribe-cpr").val() == 0 || !jQuery("#subscription-modal-subscribe-cpr").val().match(/(\d{10})|(\d{6}\-\d{4})/)){
                                        jQuery("#subscription-modal-subscribe-cpr").css('border', '3px solid red');
                                        jQuery(this).toggleClass(['button','is-loading'])
                                        return;
                                    }
                                    
                                    jQuery.post(addNonce(JQUERY_VARS.restUrl+'/paymentgateway/bsmandate/init?token='+sessionStorage.getItem('customertoken')), {"cpr":jQuery("#subscription-modal-subscribe-cpr").val(),"regno":jQuery("#subscription-modal-subscribe-regnr").val(),"accountno":jQuery("#subscription-modal-subscribe-accnr").val(), "subscription":true}, function(data){
                                        var result = JSON.parse(data);
                                        window.location.reload();
                                    }, 'json')
                                    .fail(function(response){
                                        console.log(response.responseJSON);
                                        alert(response.responseJSON.message);
                                        jQuery(this).toggleClass(['button','is-loading']);
                                        jQuery("#subscription-payment-modal").toggleClass('is-active');
                                    });
                                
                                });
                            });
                        }
                        
                    }
                }
            });
        }
    }

    