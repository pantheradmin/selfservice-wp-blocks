export function init(){
    var invoice_block_attributes = jQuery("#invoice_container").data('attributes');
    function SortByInvoiceId(a, b){
        var aInvoiceId = a.invoiceid;
        var bInvoiceId = b.invoiceid;
        return ((aInvoiceId < bInvoiceId) ? -1 : ((aInvoiceId > bInvoiceId) ? 1 : 0));
    }

    var customer = false;
    var paymentgateway = false;

    //We need the customer
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+"/customer?token="+sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function(data){
            customer = JSON.parse(data);
            getPaymentGateway();
        },
        error:function(data){
            window.parent.notifyError("Der skete en fejl. Der mangler info på siden.");
        }
    });
    
    function getPaymentGateway(){
        //We need the paymentgateway
        jQuery.ajax({
            type: 'GET',
            url: addNonce(JQUERY_VARS.restUrl+"/paymentgateway?token="+sessionStorage.getItem('customertoken')),
            credentials: 'same-origin',
            success: function(data){
                paymentgateway = JSON.parse(data);
                showContent();
            },
            error:function(data){
                window.parent.notifyError("Der skete en fejl. Der mangler info på siden.");
            }
        });
    }


function showContent(){
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+"/customer/invoices?token="+sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function(data){
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var invoices = JSON.parse(data);
            var table_body = jQuery("#pa_invoices");
            
            
            
            if(sessionStorage.getItem('isMaster') == 'true') {
                jQuery("#pa_invoices_head").append(
                    '<div class="column has-text-weight-semibold">'+invoice_block_attributes['invoiceNumRowLabel']+'</div>\
                    <div class="column has-text-weight-semibold is-hidden-touch">'+invoice_block_attributes['paymentDateRowLabel']+'</div>\
                    <div class="column has-text-weight-semibold is-hidden-touch">Beløb u.moms</div>\
                    <div class="column has-text-weight-semibold">Beløb m.moms</div>\
                    <div class="column has-text-weight-semibold">'+invoice_block_attributes['isPaidRowLabel']+'</div>\
                    <div class="column has-text-weight-semibold is-hidden-touch">'+invoice_block_attributes['functionsRowLabel']+'</div>\
                    <div class="column has-text-weight-semibold is-hidden-desktop"></div>');
                    
                } else {
                    jQuery("#pa_invoices_head").append(
                        '<div class="column has-text-weight-semibold">'+invoice_block_attributes['invoiceNumRowLabel']+'</div>\
                        <div class="column has-text-weight-semibold">'+invoice_block_attributes['typeRowLabel']+'</div>\
                        <div class="column has-text-weight-semibold is-hidden-touch">'+invoice_block_attributes['paymentDateRowLabel']+'</div>\
                        <div class="column has-text-weight-semibold">'+invoice_block_attributes['amountRowLabel']+'</div>\
                        <div class="column has-text-weight-semibold">'+invoice_block_attributes['isPaidRowLabel']+'</div>\
                        <div class="column has-text-weight-semibold is-hidden-touch">'+invoice_block_attributes['functionsRowLabel']+'</div>\
                        <div class="column has-text-weight-semibold is-hidden-desktop"></div>');
                    }
                    
                    invoices.sort(SortByInvoiceId);
                    invoices.reverse();
                    var urlParams = new URLSearchParams(window.location.search);
                    var paidInvoice;
                    if(urlParams.has('invoice-id')){
                        paidInvoice = urlParams.get('invoice-id');
                    }
                    invoices.forEach(function(element){
                        var invoice_id = element.invoiceId;
                        var invoice_duedate = element.paystamp.match(/\d{2}-\d{2}-\d{4}/)[0];
                        
                        //noVAT
                        element.decFinalPricenovat = element.decFinalPrice*0.8;
                        element.decFinalPricenovatFormatted = element.decFinalPricenovat.toFixed(2);
                        element.decFinalPricenovatFormatted = element.decFinalPricenovatFormatted.toString().match(/\.\d{1}\b/) ? element.decFinalPricenovatFormatted+"0" : element.decFinalPricenovatFormatted;
                        element.decFinalPricenovatFormatted = element.decFinalPricenovatFormatted.toString().match(/\.\d/) ? element.decFinalPricenovatFormatted : element.decFinalPricenovatFormatted + ',00';
                        element.decFinalPricenovatFormatted = element.decFinalPricenovatFormatted.toString().replace(/\./,',');
                        
                        // withVAT
                        element.decFinalPriceFormatted = element.decFinalPrice.toFixed(2);
                        element.decFinalPriceFormatted = element.decFinalPriceFormatted.toString().match(/\.\d{1}\b/) ? element.decFinalPriceFormatted+"0" : element.decFinalPriceFormatted;
                        element.decFinalPriceFormatted = element.decFinalPriceFormatted.toString().match(/\.\d/) ? element.decFinalPriceFormatted : element.decFinalPriceFormatted + ',00';
                        element.decFinalPriceFormatted = element.decFinalPriceFormatted.toString().replace(/\./,',');
                        
                        var invoice_amountnovat = element.decFinalPricenovatFormatted;
                        var invoice_amount = element.decFinalPriceFormatted;
                        if(paidInvoice == invoice_id){
                            var invoice_ispaid = new Date().toLocaleDateString('en-GB').replace(/\//gm,'-');
                        } else if(parseFloat(invoice_amount) < 0) {
                            var invoice_ispaid = 'Tilbagebetalt';
                        } else {
                            //if(($invoice["paystate"] == "bsok") && ($invoice["paystamp"] > time()-864000) && ($invoice["paid"]!=1) && ($bookkeeping["betalingsservice"]["bookkeepcancel"] != "1")){ cant pay
                            var paidByBS = (element.paystate == 'bsok') && (Math.floor(new Date(element.paystamp).getTime() / 1000) > Date.now()-864000) && (element.paid != 1)  && element.bsbookkeepcancel != 1;
                            if(invoice_block_attributes['showPayButtonOnInvoice']) {
                                var invoice_ispaid = paidByBS || element.paid == 1 ? new Date(element.paidDate*1000).toLocaleDateString('en-GB').replace(/\//gm,'-') : '<button type="button "data-invoice-id="'+element.invoiceId+'" data-invoice-amount="'+element.decFinalPrice+'" class="invoice-pay-button">Betal</button>';
                            } else {
                                var invoice_ispaid = paidByBS || element.paid == 1 ? new Date(element.paidDate*1000).toLocaleDateString('en-GB').replace(/\//gm,'-') : 'Ubetalt';
                            }
                        }
                        if(parseFloat(invoice_amount) < 0) {
                            var invoice_type = 'Kreditnota';
                        } else {
                            var invoice_type ='Regning';
                        }
                        if(sessionStorage.getItem('isMaster') == 'true') {
                            var html_str = '<div id="invoicerow-'+invoice_id+'" class="columns is-mobile">\
                            <div class="column ">'+invoice_id+'</div>\
                            <div class="column  is-hidden-touch">'+invoice_duedate+'</div>\
                            <div class="column  is-hidden-touch">'+invoice_amountnovat+'</div>\
                            <div class="column ">'+invoice_amount+'</div>\
                            <div class="column ">'+invoice_ispaid+'</div>\
                            <div class="column  is-hidden-touch"><a target="_blank" href="'+addNonce(JQUERY_VARS.restUrl+'/customer/invoices/'+element.id+'/pdf?token='+sessionStorage.getItem('customertoken'))+'"><button>'+invoice_block_attributes['pdfButtonLabel']+'</button></a></div>\
                            <div class="column is-hidden-desktop"><span data-invoice-id="'+invoice_id+'" class="expand-invoice dashicons dashicons-arrow-down-alt2"></span></div>\
                            </div>\
                            <div id="invoice-info-'+invoice_id+'" class="is-hidden is-hidden-desktop invoice-info-box">\
                            <div class="columns is-mobile"><div class="column has-text-weight-semibold">Forfalds dato:</div><div class="column">'+invoice_duedate+'</div></div>\
                            <div class="columns is-mobile"><div class="column has-text-weight-semibold">U.moms:</div><div class="column">'+invoice_amountnovat+'</div></div>\
                            <div class="columns is-mobile"><div class="column has-text-weight-semibold">M.moms:</div><div class="column">'+invoice_amount+'</div></div>\
                            <div class="columns is-mobile"><div class="column"><a target="_blank" href="'+addNonce(JQUERY_VARS.restUrl+'/customer/invoices/'+element.id+'/pdf?token='+sessionStorage.getItem('customertoken'))+'"><button>'+invoice_block_attributes['pdfButtonLabel']+'</button></a></div></div>\
                            </div>';
                        } else {
                            var html_str = '<div id="invoicerow-'+invoice_id+'" class="columns is-mobile">\
                            <div class="column ">'+invoice_id+'</div>\
                            <div class="column">'+invoice_type+'</div>\
                            <div class="column  is-hidden-touch">'+invoice_duedate+'</div>\
                            <div class="column ">'+invoice_amount+'</div>\
                            <div class="column ">'+invoice_ispaid+'</div>\
                            <div class="column  is-hidden-touch"><a target="_blank" href="'+addNonce(JQUERY_VARS.restUrl+'/customer/invoices/'+element.id+'/pdf?token='+sessionStorage.getItem('customertoken'))+'"><button>'+invoice_block_attributes['pdfButtonLabel']+'</button></a></div>\
                            <div class="column is-hidden-desktop" style="text-align: center;"><span data-invoice-id="'+invoice_id+'" class="expand-invoice dashicons dashicons-arrow-down-alt2"></span></div>\
                            </div>\
                            <div id="invoice-info-'+invoice_id+'" class="is-hidden is-hidden-desktop invoice-info-box">\
                            <div class="columns is-mobile"><div class="column has-text-weight-semibold">Forfalds dato:</div><div class="column">'+invoice_duedate+'</div></div>\
                            <div class="columns is-mobile invoice_mobile_pdf_link_container"><div class="column invoice_mobile_pdf_link"><a target="_blank" href="'+addNonce(JQUERY_VARS.restUrl+'/customer/invoices/'+element.id+'/pdf?token='+sessionStorage.getItem('customertoken'))+'"><button>'+invoice_block_attributes['pdfButtonLabel']+'</button></a></div></div>\
                            </div>';
                        }
                        
                        table_body.append(html_str);
                    });
                    
                    
                    
                    let loadCount = 0;
                    jQuery('[id^="invoicerow-"]').each(function (){
                        if(loadCount >= invoice_block_attributes['loadCount'] ){
                            jQuery(this).toggle();
                        }
                        loadCount++;
                    });
                    
                    
                    jQuery(".expand-invoice").on('click', function(){
                        var invoiceId = jQuery(this).data('invoice-id');
                        jQuery("#invoice-info-"+invoiceId).toggleClass("is-hidden");
                        jQuery(this).toggleClass('dashicons-arrow-down-alt2');
                        jQuery(this).toggleClass('dashicons-arrow-up-alt2');
                    });
                    
                    jQuery(".invoice-pay-button").on('click', function(){
                        var invoiceId = jQuery(this).data('invoice-id');
                        var invoiceAmount = jQuery(this).data('invoice-amount');
                        var a2ainvoicebtnstr =  invoice_block_attributes['showA2AButton'] ?'<button id="invoice-modal-pay-button-a2a" type="button" data-invoice-id="'+invoiceId+'" data-invoice-amount="'+invoiceAmount+'" style="margin-left: 10px" disabled>Gå til betaling med konto betaling</button>' : '';
                        var mpinvoicebtnstr =  invoice_block_attributes['showMPButton'] ? '<button id="invoice-modal-pay-mobilepay-button" class="block" type="button" data-invoice-id="'+invoiceId+'" data-invoice-amount="'+invoiceAmount+'" disabled>Gå til betaling med Mobilepay</button>' : '';
                        var cardsubblock = '';
                        if(paymentgateway.gateway.toLowerCase() == 'epay' && customer.invoiceBy != 'card'){
                            cardsubblock = '<div class="block"><label for="subscription"><input type="checkbox" name="cardsub' + invoiceId + '" id="cardsub' + invoiceId + '" value="1"> Tilmeld automatisk kortbetaling</label></div>';
                        }
                        jQuery("#invoice-payment-modal").toggleClass('is-active');
                        jQuery("#invoice-payment-modal-title").html('<h2>Betaling af faktura</h2>');
                        jQuery("#invoice-payment-modal-content").html('\
                        <div class="">\
                        <div class="block">Faktura nr: '+invoiceId+'</div>\
                        <div class="block">Beløb: '+invoiceAmount+'</div>'
                        + cardsubblock + 
                        '<div class="block"><label for="tos"><input type="checkbox" onchange="jQuery(\'#invoice-modal-pay-button, #invoice-modal-pay-mobilepay-button, #invoice-modal-pay-button-a2a\').attr(\'disabled\', !this.checked)" name="tos" type="checkbox"/> Acceptér <a href="'+JQUERY_VARS.paymentTOSUrl+'" target="_blank" >betalings-vilkår</a></label></div>\
                        <button id="invoice-modal-pay-button" type="button" data-invoice-id="'+invoiceId+'" data-invoice-amount="'+invoiceAmount+'" disabled>Gå til betaling</button>'+
                        a2ainvoicebtnstr+
                        mpinvoicebtnstr+
                        '</div>\
                        ');
                        jQuery("#invoice-payment-modal-footer").html('<div class="columns"></div>');
                        
                        jQuery("#invoice-modal-pay-button").on('click', function(){
                            var amount = jQuery(this).data('invoice-amount');
                            var invoiceId = jQuery(this).data('invoice-id');
                            var subscription = jQuery('#cardsub' + invoiceId + '').is(":checked") ? 1 : 0;

                            jQuery(this).toggleClass(['button','is-loading']);
                            jQuery.get(addNonce(JQUERY_VARS.restUrl+'/paymentgateway/link?token='+sessionStorage.getItem('customertoken')), {'amount':parseFloat(amount)*100, 'invoiceId':invoiceId, 'paymentMethods':'creditcard', 'subscription': subscription}, function(data){
                                var result = JSON.parse(data);
                                window.location.replace(result.url);
                            }, 'json');
                        });
                        
                        jQuery("#invoice-modal-pay-mobilepay-button").on('click', function(){
                            var amount = jQuery(this).data('invoice-amount');
                            var invoiceId = jQuery(this).data('invoice-id');
                            jQuery(this).toggleClass(['button','is-loading']);
                            jQuery.get(addNonce(JQUERY_VARS.restUrl+'/paymentgateway/link?token='+sessionStorage.getItem('customertoken')), {'amount':parseFloat(amount)*100, 'invoiceId':invoiceId, 'paymentMethods':'mobilepay'}, function(data){
                                var result = JSON.parse(data);
                                window.location.replace(result.url);
                            }, 'json');
                        });
                        
                        jQuery("#invoice-modal-pay-button-a2a").on('click', function(){
                            var amount = jQuery(this).data('invoice-amount');
                            var invoiceId = jQuery(this).data('invoice-id');
                            jQuery(this).toggleClass(['button','is-loading']);
                            jQuery.get(addNonce(JQUERY_VARS.restUrl+'/paymentgateway/bs/pisp/register?token='+sessionStorage.getItem('customertoken')), {'amount':parseFloat(amount)*100, 'invoice':invoiceId}, function(data){
                                var result = JSON.parse(data);
                                window.location.replace(result.data.pisp_checkout_url);
                            }, 'json');
                        });
                    });
                    
                    jQuery(".modal-background").on('click', function(){
                        jQuery(this).parent().toggleClass('is-active');
                    });
                    jQuery(".modal-close").on('click', function(){
                        jQuery(this).parent().parent().toggleClass('is-active')
                    });
                    
                }});
            }
                
                if(jQuery("#loadmore_invoice").length) {
                    jQuery('#loadmore_invoice').on('click', function () {
                        invoice_block_attributes['isLoadMoreButton'] = !invoice_block_attributes['isLoadMoreButton'];
                        if (invoice_block_attributes['isLoadMoreButton']) {
                            jQuery(this).text(invoice_block_attributes['loadMoreButtonLabel']);
                        } else {
                            jQuery(this).text(invoice_block_attributes['loadLessButtonLabel']);
                        }
                        
                        let loadCount = 0;
                        jQuery('[id^="invoicerow-"]').each(function () {
                            if (loadCount >= invoice_block_attributes['loadCount']) {
                                jQuery(this).slideToggle("slow");
                            }
                            loadCount++;
                        });
                    });
                }



                //Email test
                /*jQuery.ajax({
                    type: 'GET',
                    url: addNonce(JQUERY_VARS.restUrl+'/customer/email?token='+sessionStorage.getItem('customertoken')),
                    credentials: 'same-origin',
                    success: function(data) {
                        var result = JSON.parse(data);
                        console.log(result);
                    },
                    error: function(data){
                        console.log(addNonce(JQUERY_VARS.restUrl+'/customer/email?token='+sessionStorage.getItem('customertoken')));
                        window.parent.notifyError("Der skete en fejl, prøv igen senere");
                    }
                });*/                
}