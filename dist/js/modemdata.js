export function init(){
    var thisUrl = window.location.href;
    var urlParts = thisUrl.split('mac=');
    var urlParts2 = urlParts[1].split('&');
    sessionStorage.setItem('currentMAC', urlParts2[0]);
    
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+"/customer/cpe/" + sessionStorage.getItem('currentMAC') + "/modemsettings?token="+sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function(data) {
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var modemData = JSON.parse(data);
            if(Object.keys(modemData).length > 1)
            presentModemData(modemData);
        },
        error: function(data){
        }
    });
}


function getHelpSection(id, mAttributes){
    var helpHtml = '';
    if(mAttributes[id].length > 0){
        helpHtml = '<span class="dashicons dashicons-editor-help help-icon" data-message-id="' + id + '"></span>';
        helpHtml += '<span id="help-message-' + id + '" class="popuptext">' + mAttributes[id] + '</span>';
    }
    return helpHtml;
}

function presentModemData(modemData){
    jQuery(document).ready(function(){
        jQuery(".help-icon").mouseover(function(){
            //alert('ABC');
            var messageId = jQuery(this).data('message-id');
            jQuery("#help-message-"+messageId).show();
        });
        jQuery(".help-icon").mouseout(function(){
            var messageId = jQuery(this).data('message-id');
            jQuery("#help-message-"+messageId).hide();
        });
    });
    
    var mAttributes = jQuery("#modem-data-container").data('attributes');
    var mdSection = '<form id="modem_data_form" method="POST" action="'+addNonce(JQUERY_VARS.restUrl+'/customer/cpe/' + sessionStorage.getItem('currentMAC') + '/modemsettings?token='+sessionStorage.getItem('customertoken')) + '">'
    + '<div class="container">'
    + '<div class="columns is-multiline is-mobile">';
    
    //Data may be bundled in various ways
    if(typeof modemData.options === 'undefined')
    modemData.options = modemData.optionsext;

    //Are default values active on the modem
    var defaultValues = false; 
                    
    if(typeof modemData[mAttributes.factorySettingsKey] !== 'undefined'){
        defaultValues = (modemData[mAttributes.factorySettingsKey]['value'] == mAttributes.defaultValue);
        mdSection += '<input type="hidden" id="' + mAttributes.factorySettingsKey + '" name="' + mAttributes.factorySettingsKey + '" value="' + mAttributes.customValue + '">';
    }
    mdSection += '<input type="hidden" id="' + mAttributes.factorySettingsKey + '" name="' + mAttributes.factorySettingsKey + '" value="' + mAttributes.customValue + '">';


    if(mAttributes.showWifi){
        mdSection += '<div class="column is-4"><div class="has-text-weight-semibold ">' + mAttributes.wifiLabel + ' ' + getHelpSection("wifiHelpMessage", mAttributes) + '</div></div>';
        mdSection += '<div class="column is-8"><div class="is-family-secondary right ">' + getModemDataRadioRange(mAttributes.wifiKey, modemData[mAttributes.wifiKey]['parms'], modemData[mAttributes.wifiKey]['value']) + '</div></div>';
    }

    if(mAttributes.showHeader){
        mdSection += '<div class="column is-12 wifiheader" id="header2Row">' + mAttributes.showHeaderLabel + '</div>';
    }
    
    if(mAttributes.showSSID){
        var ssids = mAttributes.SSIDKey.split(/[,; |]/);
        var ssidValue0 = defaultValues ? "" : modemData[ssids[0]]['value'];        

        mdSection += '<div class="column is-4"><div class="has-text-weight-semibold ">' + mAttributes.SSIDLabel + ' ' + getHelpSection("SSIDHelpMessage", mAttributes) + '</div></div>';
        mdSection += '<div class="column is-8"><div class="is-family-secondary right "><input type="text" name="' + ssids[0] + '" id="' + ssids[0] + '" value="' + ssidValue0 + '" ';
        if(ssids.length == 2)
        mdSection += ' onBlur="jQuery(\'#' + ssids[1] + '\').val(this.value);"';
        mdSection += '></div></div>';
        if(ssids.length == 2){
            var ssidValue1 = defaultValues ? "" : modemData[ssids[1]]['value'];
            mdSection += '<input type="hidden"  name="' + ssids[1] + '" id="' + ssids[1] + '" value="' + ssidValue1 + '">';
        }
        mdSection += '<div id="ssid_error"></div>';
    }
  
    if(mAttributes.showWpaPsk){
        var wpapsks = mAttributes.wpaPskKey.split(/[,; |]/);
        var wpaPskValue0 = defaultValues ? "" : modemData[wpapsks[0]]['value'];

        mdSection += '<div class="column is-4"><div class="has-text-weight-semibold ">' + mAttributes.wpaPskLabel + ' ' + getHelpSection("wpaPskHelpMessage", mAttributes) + '</div></div>';
        mdSection += '<div class="column is-8"><div class="is-family-secondary right "><input type="text" name="' + wpapsks[0] + '" id="' + wpapsks[0] + '" value="' +wpaPskValue0 + '"';
        if(wpapsks.length == 2)
        mdSection += ' onBlur="jQuery(\'#' + wpapsks[1] + '\').val(this.value);"';
        mdSection += '></div></div>';
        if(wpapsks.length == 2){
            var wpaPskValue1 = defaultValues ? "" : modemData[wpapsks[1]]['value'];
            mdSection += '<input type="hidden"  name="' + wpapsks[1] + '" id="' + wpapsks[1] + '" value="' + wpaPskValue1 + '">';
        }
        mdSection += '<div id="wpapsk_error"></div>';
    }
    
    if(mAttributes.showChannel){
        mdSection += '<div class="column is-4"><div class="has-text-weight-semibold ">' + mAttributes.channelLabel + ' ' + getHelpSection("channelHelpMessage", mAttributes) + '</div></div>';
        mdSection += '<div class="column is-8"><div class="is-family-secondary right "><select name="' + mAttributes.channelKey + '" id="' + mAttributes.channelKey + '">' + getModemDataSelectRange(modemData[mAttributes.channelKey]['parms'], modemData[mAttributes.channelKey]['value']) + '</select></div></div>';
    }
        
    if(mAttributes.showChannelWidth){
        mdSection += '<div class="column is-4"><div class="has-text-weight-semibold " for="customer_name">' + mAttributes.channelWidthLabel + ' ' + getHelpSection("channelWidthHelpMessage", mAttributes) + '</div></div>';
        mdSection += '<div class="column is-8"><div class="is-family-secondary right "><select name="' + mAttributes.channelWidthKey + '" id="' + mAttributes.channelWidthKey + '">' + getModemDataSelectRange(modemData[mAttributes.channelWidthKey]['parms'], modemData[mAttributes.channelWidthKey]['value']) + '</select></div></div>';
    }
 
    if(mAttributes.showHeader5){
        mdSection += '<div class="column is-12 wifiheader" id="header5Row">' + mAttributes.showHeader5Label + '</div>';
    }

    if(mAttributes.showSSID5){
        var ssids = mAttributes.SSID5Key.split(/[,; |]/);
        var ssid5Value0 = defaultValues ? "" : modemData[ssids[0]]['value'];

        mdSection += '<div class="column is-4"><div class="has-text-weight-semibold ">' + mAttributes.SSID5Label + ' ' + getHelpSection("SSID5HelpMessage", mAttributes) + '</div></div>';
        mdSection += '<div class="column is-8"><div class="is-family-secondary right "><input type="text" name="' + ssids[0] + '" id="' + ssids[0] + '" value="' + ssid5Value0 + '" ';
        if(ssids.length == 2)
        mdSection += ' onBlur="jQuery(\'#' + ssids[1] + '\').val(this.value);"';
        mdSection += '></div></div>';
        if(ssids.length == 2){
            var ssid5Value1 = defaultValues ? "" : modemData[ssids[1]]['value'];
            mdSection += '<input type="hidden"  name="' + ssids[1] + '" id="' + ssids[1] + '" value="' + ssid5Value1 + '">';
        }
        mdSection += '<div id="ssid5_error"></div>';
    }
 
    if(mAttributes.showWpaPsk5){
        var wpapsks5 = mAttributes.wpaPsk5Key.split(/[,; |]/);
        var wpaPsk5Value0 = defaultValues ? "" : modemData[wpapsks5[0]]['value'];

        mdSection += '<div class="column is-4"><div class="has-text-weight-semibold ">' + mAttributes.wpaPsk5Label + ' ' + getHelpSection("wpaPsk5HelpMessage", mAttributes) + '</div></div>';
        mdSection += '<div class="column is-8"><div class="is-family-secondary right "><input type="text" name="' + wpapsks5[0] + '" id="' + wpapsks5[0] + '" value="' + wpaPsk5Value0 + '"';
        if(wpapsks5.length == 2)
        mdSection += ' onBlur="jQuery(\'#' + wpapsks5[1] + '\').val(this.value);"';
        mdSection += '></div></div>';
        if(wpapsks5.length == 2){
            var wpaPsk5Value1 = defaultValues ? "" : modemData[wpapsks5[1]]['value'];
            mdSection += '<input type="hidden"  name="' + wpapsks5[1] + '" id="' + wpapsks5[1] + '" value="' + wpaPsk5Value1 + '">';
        }
        mdSection += '<div id="wpapsk_error"></div>';
    }

    if(mAttributes.showChannel5){
        mdSection += '<div class="column is-4"><div class="has-text-weight-semibold ">' + mAttributes.channelLabel5 + ' ' + getHelpSection("channelHelpMessage5", mAttributes) + '</div></div>';
        mdSection += '<div class="column is-8"><div class="is-family-secondary right "><select name="' + mAttributes.channelKey5 + '" id="' + mAttributes.channelKey5 + '">' + getModemDataSelectRange(modemData[mAttributes.channelKey5]['parms'], modemData[mAttributes.channelKey5]['value']) + '</select></div></div>';
    }

    if(mAttributes.showChannelWidth5){
        mdSection += '<div class="column is-4"><div class="has-text-weight-semibold ">' + mAttributes.channelWidthLabel5 + ' ' + getHelpSection("channelWidthHelpMessage5", mAttributes) + '</div></div>';
        mdSection += '<div class="column is-8"><div class="is-family-secondary right "><select name="' + mAttributes.channelWidthKey5 + '" id="' + mAttributes.channelWidthKey5 + '">' + getModemDataSelectRange(modemData[mAttributes.channelWidthKey5]['parms'], modemData[mAttributes.channelWidthKey5]['value']) + '</select></div></div>';
    }
    
    mdSection += '</div></div>';
    
    mdSection += '<div class="column">';
    mdSection += '<button type="button" id="modem_data_form_submit">Gem indstillinger</button>';
    mdSection += '<button type="button" id="modem_data_form_back" onClick="history.back();">Tilbage</button>';
    mdSection += '</div>';
    mdSection += '</form>'
    jQuery("#mdcontent").append(mdSection);
    
    jQuery("#modem_data_form_submit").on("click", function(){
        return modemSettingsValidate();
    });
}

//Helper for presentModemData()
function getModemDataSelectRange(range, selected){
    var rangeElements = range.split(',');
    var selectHtml = '';
    for(var i=0; i<rangeElements.length; i++){
        var optionElements = rangeElements[i].split(':');
        var markSelected = optionElements[0] == selected ? ' selected' : '';
        selectHtml += '<option value="' + optionElements[0] + '"' + markSelected + '>' + optionElements[1] + '</option>';
    }
    
    return selectHtml;
}

function getModemDataRadioRange(id, range, selected){
    var rangeElements = range.split(',');
    var selectHtml = '';
    for(var i=0; i<rangeElements.length; i++){
        var optionElements = rangeElements[i].split(':');
        var markSelected = optionElements[0] == selected ? ' checked' : '';
        if(optionElements[1] == "yes")
            optionElements[1] = "Ja";
        else if(optionElements[1] == "no")
            optionElements[1] = "Nej";
        selectHtml += '<span class="modem_data_option"><label for="' + id + '">' + optionElements[1] + '</label><input type="radio" name="' + id + '" value="' + optionElements[0] + '"' + markSelected + '></span>';
    }
    
    return selectHtml;
}

//Validator for presentModemData()
function modemSettingsValidate(){
    var mAttributes = jQuery("#modem-data-container").data('attributes');
    
    if(mAttributes.showSSID){
        var ssids = mAttributes.SSIDKey.split(/[,; |]/);
        var ssidContent = jQuery('#' + ssids[0]).val();
        var ssidRegex = new RegExp(mAttributes.SSIDRegex);
        if(!ssidRegex.test(ssidContent)){
            jQuery('#ssid_error').html(mAttributes.SSIDErrorMessage);
            return false
        }
    }

    if(mAttributes.showSSID5){
        var ssids5 = mAttributes.SSID5Key.split(/[,; |]/);
        var ssid5Content = jQuery('#' + ssids5[0]).val();
        var ssid5Regex = new RegExp(mAttributes.SSID5Regex);
        if(!ssid5Regex.test(ssid5Content)){
            jQuery('#ssid5_error').html(mAttributes.SSID5ErrorMessage);
            return false
        }
    }
    
    if(mAttributes.showWpaPsk){
        var wpapsks = mAttributes.wpaPskKey.split(/[,; |]/);
        var wpapskContent = jQuery('#' + wpapsks[0]).val();
        var wpapskRegex = new RegExp(mAttributes.wpaPskRegex);
        if(!wpapskRegex.test(wpapskContent)){
            jQuery('#wpapsk_error').html(mAttributes.wpaPskErrorMessage);
            return false
        }
    }
    alert('Dit modem vil nu genstarte. Det tager cirka 3 minutter.');
    jQuery('#modem_data_form').submit();
}