export function init(){
    var type = jQuery(".addon_product_container").data('type');
    jQuery.ajax({
        type: 'GET',
        url: addNonce(JQUERY_VARS.restUrl+'/customer/products/type/?ident='+type+'&token='+sessionStorage.getItem('customertoken')+"&is-master="+sessionStorage.getItem('isMaster')),
        credentials: 'same-origin',
        success: function(data) {
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var result = JSON.parse(data);
            if(result.amountOfProducts > 0){
                jQuery(".addon-product-order-button").hide();
                jQuery(".addon_product_container").append("<div class=\"columns is-centered\"><button class=\"column is-2 is-hidden-touch addon-product-cancel-button\">Opsig</button></div>");
                jQuery(".addon_product_container").append("<div class=\"columns is-centered is-mobile\"><button class=\"column is-6 is-offset-2 is-hidden-desktop addon-product-cancel-button\">Opsig</button></div>");
                
                jQuery(".addon-product-cancel-button").on('click', function(data){
                    jQuery.ajax({
                        type: 'DELETE',
                        url: addNonce(JQUERY_VARS.restUrl+'/customer/products/type?ident='+type+'&token='+sessionStorage.getItem('customertoken')),
                        credentials: 'same-origin',
                        success: function(data) {
                            var result = JSON.parse(data);
                            window.parent.notifySuccess("Produkt opsagt");
                            setTimeout(function(){location.reload()}, 3000);
                        },
                        error: function(data){
                            window.parent.notifyError("Der skete en fejl, pr�v igen senere");
                        }
                    });
                });
                
            } else {
                jQuery(".addon-product-order-button").show();
                jQuery(".addon-product-order-button").on('click', function(data){
                    jQuery.post(addNonce(JQUERY_VARS.restUrl+'/customer/products/type?token='+sessionStorage.getItem('customertoken')), {'ident':type}, function(data){
                        window.parent.notifySuccess("Produkt tilf�jet");
                        setTimeout(function(){location.reload()}, 3000);
                    });
                });
            }
            
            
            
            
        },
        error: function(data){
            jQuery(".fixedip-order-button").show();
            jQuery(".fixedip-order-button").on('click', function(data){
                jQuery.post(addNonce(JQUERY_VARS.restUrl+'/customer/products/type?token='+sessionStorage.getItem('customertoken')), null, function(data){
                    window.parent.notifySuccess("Produkt tilf�jet");
                    setTimeout(function(){location.reload()}, 3000);
                });
            });
        }
    });
    
}