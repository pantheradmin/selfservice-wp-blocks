export function init(){
    jQuery("#menu-item-"+JQUERY_VARS.loginPage.menu_item_id).css('display','none');
    jQuery.ajax({
        type: "GET",
        url: addNonce(JQUERY_VARS.restUrl+"/customer?token="+sessionStorage.getItem('customertoken')),
        credentials: 'same-origin',
        success: function(data){
            sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
            var customer = JSON.parse(data);
            jQuery("#customer_name").text(customer['name']);
            jQuery("#customer_address").text(customer['address']);
            jQuery("#customer_membernumber").text(customer['memberNumber']);
            jQuery("#customer_company").text(customer['company']);
            var paymentForm = '';
            switch(customer['invoiceBy']) {
                case 'bs':
                paymentForm = 'Betalingsservice';
                break;
                case 'ean':
                paymentForm = 'EAN';
                break;
                case 'email':
                paymentForm = 'Email-faktura';
                break;
                case 'paper':
                paymentForm = 'Papir-faktura';
                break;
                case 'card':
                paymentForm = 'Kort';
                break;
            }
            
            //If paymentCyclus is selectable
            switch(customer['paymentCyclus']) {
                case 1:
                    jQuery("#billingPeriod1").attr('checked', true);
                break;
                case 3:
                    jQuery("#billingPeriod3").attr('checked', true);
                break;
                case 6:
                    jQuery("#billingPeriod6").attr('checked', true);
                break;
                case 12:
                    jQuery("#billingPeriod12").attr('checked', true);
                break;
            }

            if(customer['invoiceBy'] == "bs"){
                jQuery(".invoiceByEmail").hide();
                jQuery(".invoiceByPaper").hide();
            }else if(customer['invoiceBy'] == "email"){
                if(jQuery("#customer_invoiceByPaper").length == 0){
                    jQuery("#customer_invoiceByEmail").attr('disabled', true);
                }
                jQuery("#customer_invoiceByEmail").prop('checked', true);
            }else if(customer['invoiceBy'] == "paper"){
                if(jQuery("#customer_invoiceByEmail").length == 0){
                    jQuery("#customer_invoiceByPaper").attr('disabled', true);
                }
                jQuery("#customer_invoiceByPaper").prop('checked', true);
            }

            //If paymentCyclus is not selectable
            jQuery("#generalPeriodLabel").html(customer['paymentCyclus'] + (customer['paymentCyclus'] == 1 ? " måned" : " måneder"));
            jQuery("#customer_paymentform").text(paymentForm);
            if(customer['isMaster'] === 1){
                jQuery("#customer_contactperson").text(customer['contact']['name']);
                jQuery("div#customer_phone").text(customer['contact']['phone']);
                jQuery("div#customer_mobile").text(customer['contact']['mobile']);
                jQuery("div#customer_email").text(customer['contact']['email']);
                jQuery("input#customer_phone").val(customer['contact']['phone']);
                jQuery("input#customer_mobile").val(customer['contact']['mobile']);
                jQuery("input#customer_email").val(customer['contact']['email']);
            } else {
                jQuery("#customer_contactperson").text(customer['name']);
                jQuery("div#customer_phone").text(customer['phone']);
                jQuery("div#customer_mobile").text(customer['mobile']);
                jQuery("div#customer_email").text(customer['email']);
                jQuery("input#customer_phone").val(customer['phone']);
                jQuery("input#customer_mobile").val(customer['mobile']);
                jQuery("input#customer_email").val(customer['email']);
          
                var vatNo = customer['vatNo'];
                var companyName = customer['company'];

                jQuery("input#customer_companyname").val(companyName);
                jQuery("div#customer_companyname").text(companyName);
                jQuery("input#customer_cvr").val(vatNo);
                jQuery("div#customer_cvr").text(vatNo);


            }

            fetchChannels();

        },error: function(data){
            console.log('error');
        }
    });
 
    jQuery("#customer_invoiceByEmail").on('click', function(){
        if(jQuery("#customer_invoiceByEmail").is(':checked')){
            jQuery("#customer_invoiceByPaper").prop('checked', false);
        }
    });
    jQuery("#customer_invoiceByPaper").on('click', function(){
        if(jQuery("#customer_invoiceByPaper").is(':checked')){
            jQuery("#customer_invoiceByEmail").prop('checked', false);
        }
    });


    jQuery("#logout_button").on('click', function(){
        sessionStorage.clear();
        goToLogin();
    });
    
    jQuery("#change_pass_button").on('click', function(){
        jQuery("#change-password-modal").toggleClass('is-active');
        jQuery("input[name=token]").val(sessionStorage.getItem('customertoken'));

        var help_text = "";
        switch(JQUERY_VARS.pass_chars){
            case "none":
                help_text = "Minimum "+JQUERY_VARS.pass_length+" tegn.";
                break;
            case "high":
                help_text = ", og et special tegn ( [ ! ( ) - . ? : ; ~ ` @ # $ % ^ * + = )";
            case "medium":
                help_text = ", et tal"+help_text;
            case "low":
                help_text = "Skal indeholde et stort og et lille bogstav"+help_text;
            default:
                help_text = "Minimum "+JQUERY_VARS.pass_length+" tegn. " + help_text+". "
        }
        jQuery("#pass_tooltip1").attr("data-tooltip", help_text);
        jQuery("#pass_tooltip2").attr("data-tooltip", help_text);
    });

    jQuery("#change_password_save_button").on("click", function(){
        var old_password = jQuery("#old_password").val();
        var pass = jQuery("#new_password").val()
        var pass2 = jQuery("#new2_password").val()
        if(pass != pass2) {window.parent.notifyError("Samme adgangskode skal tastes i begge felter");} else {
            if(verifyPass(pass, pass2)){
                jQuery.ajax({
                    type:"post",
                    url:addNonce(JQUERY_VARS.restUrl+'/customer/password'),
                    data: {"new_password":pass,"new2_password":pass2,"old_password":old_password, "token": sessionStorage.getItem('customertoken')},
                    success: function(data){
                        if(data.message == 'Mismatch between new password and confirmation'){
                            window.parent.notifyError("Samme adgangskode skal tastes i begge felter");
                        } else {
                            window.parent.notifySuccess("Nyt kodeord gemt.")
                        }
                    }, error: function(data){
                        console.log(data.responseJSON.message)
                        var parsedMessage = JSON.parse(data.responseJSON.message);
                        if(parsedMessage.error_msg == 'Old password not correct.'){
                            window.parent.notifyError("Nuværende kodeord ikke tastet korrekt.");
                        } else {
                            window.parent.notifyError("Der er sket en fejl, prøv igen. Kontakt kundeservice hvis fejlen forsætter. "+parsedMessage.error_msg);
                        }
                    }
                });
            } else {
                window.parent.notifyError("Den valgte kode overholder ikke reglerne. Tjek ? ved siden af feltet.");
            }
        }
    })

    function verifyPass(pass){
        var isValid = true;
        switch(JQUERY_VARS.pass_chars){
            case "none":
                isValid = pass.length >= JQUERY_VARS.pass_length;
                break;
            case "low":
                isValid = pass.length >= JQUERY_VARS.pass_length && pass.match(/[A-ZÆØÅ]{1,}/g) && pass.match(/[a-zæøå]{1,}/g);
                break;
            case "medium":
                isValid = pass.length >= JQUERY_VARS.pass_length && pass.match(/[A-ZÆØÅ]{1,}/g) && pass.match(/[a-zæøå]{1,}/g) && pass.match(/[0-9]{1,}/g);
                break;
            case "high":
                isValid = pass.length >= JQUERY_VARS.pass_length && pass.match(/[A-ZÆØÅ]{1,}/g) && pass.match(/[a-zæøå]{1,}/g) && pass.match(/[0-9]{1,}/g) && pass.match(/[\!\(\)\-\.\?\:\;\~\`\@\#\$\%\^\*\+\=]{1,}/g);
                break;
        }
        return isValid;
    }
    
    jQuery(".modal-close").on('click', function(){
        jQuery(this).parent().parent().toggleClass('is-active')
    });
    
    jQuery(".modal-background").on('click', function(){
        jQuery("#change-password-modal").toggleClass('is-active')
    });

    jQuery("#save-info-changes").on('click', function(){
        var phone = jQuery("#customer_phone").val();
        var mobile = jQuery("#customer_mobile").val();
        var email = jQuery("#customer_email").val();
        var company = jQuery("#customer_companyname").val();
        var cvrnum = jQuery("#customer_cvr").val();
        var billingPeriod = jQuery('input[name="billingPeriod"]:checked').val();

        var customerData = {"phone":phone, "mobile":mobile, "email":email, "company":company, "cvrnum":cvrnum, "billingPeriod": billingPeriod};

        if(jQuery("#customer_invoiceByEmail").is(':checked')){
            customerData["invoiceBy"] = "email";
        }        
        if(jQuery("#customer_invoiceByPaper").is(':checked')){
            customerData["invoiceBy"] = "paper";
        }

        jQuery.ajax({
            type: "PUT",
            url: addNonce(JQUERY_VARS.restUrl+"/customer?token="+sessionStorage.getItem('customertoken')),
            data: customerData,
            credentials: 'same-origin',
            success: function(data){
                window.parent.notifySuccess("Oplysninger gemt");
                setTimeout(function() {window.location.reload()}, 3000);
            },
            error: function(data){
                window.parent.notifyError("Der skete en fejl, prøv igen senere");
            }
        });
    })
    
    
}

function goToLogin(){
    window.location.replace(JQUERY_VARS.siteUrl+'/'+JQUERY_VARS.loginPage.page+'/');
}

function fetchChannels(){
    //Get channels if they are not already present
    if(sessionStorage.getItem('pa_channels') == null){
        jQuery.ajax({
            type: 'GET',
            url: addNonce(JQUERY_VARS.restUrl+'/prchannels'),
            credentials: 'same-origin',
            success: function(data) {
                sessionStorage.setItem('pa_channels', getChannelsOrganized(data));
            },
            error: function(data){
                //Kunne ikke hente...
            }
        });
    }
}

function getChannelsOrganized(chnls){
    var providerChannels = JSON.parse(chnls);
    var channels = providerChannels[0].channels;
    var sortedChannels = new Object();
    var channelId2Product = new Object();
    
    for(const ci in channels){
        //Which conax products contain the channel
        let mIds = channels[ci].prClearPacketID;
        let listIds = [];
        if(typeof mIds !== "undefined"){
            listIds = mIds.split(',');
        }
    
        for(var i=0; i<listIds.length; i++){
            if(sortedChannels[listIds[i]] == null){
                sortedChannels[listIds[i]] = [];
            }
            //The channel is inserted into a package for that specific conax product having pcid as key
            sortedChannels[listIds[i]].push(channels[ci]);
        }

        //Which TV packet products contain the channel
        let nIds = channels[ci].clearPacket;
        let nListIds = [];
        if(typeof nIds !== "undefined"){
            nListIds = nIds.split(',');
        }
    
        for(var i=0; i<nListIds.length; i++){
            if(!Number.isInteger(parseInt(nListIds[i]))){
                continue;
            }

            if(sortedChannels["M" + nListIds[i]] == null){
                sortedChannels["M" + nListIds[i]] = [];
            }
            //The channel is inserted into a package for that specific conax product having pcid as key
            sortedChannels["M" + nListIds[i]].push(channels[ci]);
        }

    }

    for(const cj in sortedChannels){        
        if(sortedChannels[cj].length == 1){
            let slugInfo = new Object();
            slugInfo['pcid'] = cj;
            slugInfo['slug'] = sortedChannels[cj][0].slug;
            channelId2Product[sortedChannels[cj][0].id] = slugInfo;
        }
    }

    sessionStorage.setItem('pa_channelId2Product', JSON.stringify(channelId2Product));
    return JSON.stringify(sortedChannels);
}