export function init() {
	var socialConnection_block_attributes = jQuery("#socialConnection-container").data('attributes');

	jQuery.ajax({
		type: "GET",
		url: addNonce(JQUERY_VARS.restUrl+"/customer?token=" + sessionStorage.getItem('customertoken')),
		credentials: 'same-origin',
		success: function (data) {
			sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000) + 900);
			var customer = JSON.parse(data);
			if (customer.socialIdents.hasOwnProperty('google')) {
				socialConnection_block_attributes['userAttachedGoogle'] = true;
				jQuery("#googleAttachButtonText").text(socialConnection_block_attributes['googleRemoveButtonText'])
			}

			if (customer.socialIdents.hasOwnProperty('facebook')) {
				socialConnection_block_attributes['userAttachedFacebook'] = true;
				jQuery("#facebookAttachButtonText").text(socialConnection_block_attributes['facebookRemoveButtonText'])
			}

			if (customer.socialIdents.hasOwnProperty('apple')) {
				socialConnection_block_attributes['userAttachedApple'] = true;
				jQuery("#appleAttachButtonText").text(socialConnection_block_attributes['appleRemoveButtonText'])
			}

			jQuery("#googleattachbutton").removeAttr("disabled");
			jQuery("#facebookattachbutton").removeAttr("disabled");
			jQuery("#appleattachbutton").removeAttr("disabled");

		}, error: function (data) {
			console.log('error');
		}
	});

	/**                           **
	 *  Attach Facebook Account   **
	 *                            */
	if (jQuery("#facebookattachbutton").length) {
		jQuery("#facebookattachbutton").on("click", function () {

			FB.init({
				appId: JQUERY_VARS.facebookClientId,
				cookie: true,
				xfbml: true,
				version: 'v13.0'
			});

			if (socialConnection_block_attributes['userAttachedFacebook']) {
				jQuery.ajax({
					type: "delete",
					url: addNonce(JQUERY_VARS.restUrl+'/removesocial'),
					data: {"socialType": "facebook", "token": sessionStorage.getItem('customertoken')},
					success: function (data) {
						var isValidJson = true;
						try {
							var response = JSON.parse(data);
						} catch (error) {
							isValidJson = false;
						}
						if (isValidJson) {
							socialConnection_block_attributes['userAttachedFacebook'] = false;
							jQuery("#facebookAttachButtonText").text(socialConnection_block_attributes['facebookButtonText']);
							jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
								'<span class="icon has-text-success"><i class="fas fa-check-square"></i></span>' +
								'<span class="ml-3">Frakoblet Facebook konto</span>' +
								'</div>');
							setTimeout(function () {
								jQuery("#feedback").empty();
							}, 5000);
						}else{
							jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
								'<span class="icon has-text-danger"><i class="fas fa-ban"></i></span>' +
								'<span class="ml-3">Sletning mislykkedes</span>' +
								'</div>');
							setTimeout(function () {
								jQuery("#feedback").empty();
							}, 5000);
						}
					},
					error: function (){
						jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
							'<span class="icon has-text-danger"><i class="fas fa-ban"></i></span>' +
							'<span class="ml-3">Sletning mislykkedes</span>' +
							'</div>');
						setTimeout(function () {
							jQuery("#feedback").empty();
						}, 5000);
					}
				});
			} else {

				FB.login(function (response) {
					if (response.status === 'connected') {
						checkFacebookLoginState();
					} else {
						// Feedback
					}
				}, {scope: 'public_profile,email'});
			}
		});

		function facebookStatusChangeCallback(response) {
			if (response.status === "connected") {
				attachFacebookCall(response.authResponse.accessToken);
			}
		}


		function checkFacebookLoginState() {
			FB.getLoginStatus(function (response) {
				facebookStatusChangeCallback(response);
			});
		}

		function attachFacebookCall(accessToken) {
			FB.api('/me', {fields: 'name, email'}, function (response) {
				jQuery.ajax({
					type: "post",
					url: addNonce(JQUERY_VARS.restUrl+'/attachfacebook'),
					data: {"facebookident": response.email, "token": sessionStorage.getItem('customertoken')},
					success: function (data) {
						var isValidJson = true;
						try {
							var customer = JSON.parse(data);
						} catch (error) {
							isValidJson = false;
						}
						if (isValidJson) {
							socialConnection_block_attributes['userAttachedFacebook'] = true;
							jQuery("#facebookAttachButtonText").text(socialConnection_block_attributes['facebookRemoveButtonText']);
							jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
								'<span class="icon has-text-success"><i class="fas fa-check-square"></i></span>' +
								'<span class="ml-3">Forbundet med Facebook konto</span>' +
								'</div>');
							setTimeout(function () {
								jQuery("#feedback").empty();
							}, 5000);
						} else {
							jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
								'<span class="icon has-text-danger"><i class="fas fa-ban"></i></span>' +
								'<span class="ml-3">Ikke forbundet med Facebook konto</span>' +
								'</div>');
							setTimeout(function () {
								jQuery("#feedback").empty();
							}, 5000);
						}
					},
					error: function (){
						jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
							'<span class="icon has-text-danger"><i class="fas fa-ban"></i></span>' +
							'<span class="ml-3">Ikke forbundet med Facebook konto</span>' +
							'</div>');
						setTimeout(function () {
							jQuery("#feedback").empty();
						}, 5000);
					}
				});
			});
		}

	}

	/**                           **
	 *  Attach Apple Account   **
	 *                            */
	if (jQuery("#appleattachbutton").length) {
		jQuery("#appleattachbutton").on("click", async function () {
			AppleID.auth.init({
				clientId: JQUERY_VARS.appleClientId,
				scope: 'name email',
				redirectURI: JQUERY_VARS.appleRedirectUrl,
				state: 'applelogin',
				usePopup: true
			});

			if (socialConnection_block_attributes['userAttachedApple']) {
				jQuery.ajax({
					type: "delete",
					url: addNonce(JQUERY_VARS.restUrl+'/removesocial'),
					data: {"socialType": "apple", "token": sessionStorage.getItem('customertoken')},
					success: function (data) {
						var isValidJson = true;
						try {
							var response = JSON.parse(data);
						} catch (error) {
							isValidJson = false;
						}
						if (isValidJson) {
							socialConnection_block_attributes['userAttachedApple'] = false;
							jQuery("#appleAttachButtonText").text(socialConnection_block_attributes['appleButtonText']);
							jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
								'<span class="icon has-text-success"><i class="fas fa-check-square"></i></span>' +
								'<span class="ml-3">Frakoblet Apple konto</span>' +
								'</div>');
							setTimeout(function () {
								jQuery("#feedback").empty();
							}, 5000);
						}else{
							jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
								'<span class="icon has-text-danger"><i class="fas fa-ban"></i></span>' +
								'<span class="ml-3">Sletning mislykkedes</span>' +
								'</div>');
							setTimeout(function () {
								jQuery("#feedback").empty();
							}, 5000);
						}
					},
					error: function (){
						jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
							'<span class="icon has-text-danger"><i class="fas fa-ban"></i></span>' +
							'<span class="ml-3">Sletning mislykkedes</span>' +
							'</div>');
						setTimeout(function () {
							jQuery("#feedback").empty();
						}, 5000);
					}
				});
			} else {

				const appleResponse = await AppleID.auth.signIn();
				jQuery.ajax({
					type: "post",
					url: addNonce(JQUERY_VARS.resturl+'/applelogin'),
					data: {"credential": appleResponse.authorization.code, "justValidate": true},
					success: function (data) {
						var isValidJson = true;
						try {
							var email = JSON.parse(data);
						} catch (error) {
							isValidJson = false;
						}
						if (isValidJson) {

							jQuery.ajax({
								type: "post",
								url: addNonce(JQUERY_VARS.resturl+'/attachapple'),
								data: {
									"appleident": email.appleEmail,
									"token": sessionStorage.getItem('customertoken')
								},
								success: function (data) {
									var isValidJson = true;
									try {
										var customer = JSON.parse(data);
									} catch (error) {
										isValidJson = false;
									}
									if (isValidJson) {
										socialConnection_block_attributes['userAttachedApple'] = true;
										jQuery("#appleAttachButtonText").text(socialConnection_block_attributes['appleRemoveButtonText']);
										jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
											'<span class="icon has-text-success"><i class="fas fa-check-square"></i></span>' +
											'<span class="ml-3">Forbundet med Apple konto</span>' +
											'</div>');
										setTimeout(function () {
											jQuery("#feedback").empty();
										}, 5000);
									} else {
										jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
											'<span class="icon has-text-danger"><i class="fas fa-ban"></i></span>' +
											'<span class="ml-3">Ikke forbundet med Apple konto</span>' +
											'</div>');
										setTimeout(function () {
											jQuery("#feedback").empty();
										}, 5000);
									}
								},
								error: function (){
									jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
										'<span class="icon has-text-danger"><i class="fas fa-ban"></i></span>' +
										'<span class="ml-3">Ikke forbundet med Apple konto</span>' +
										'</div>');
									setTimeout(function () {
										jQuery("#feedback").empty();
									}, 5000);
								}
							});

						}

					}
				});
			}

		});
	}

	/**                         **
	 *  Attach Google Account   **
	 *                          */
	if (jQuery("#googleattachbutton").length) {

		jQuery(document).ready(function () {
			const queryString = window.location.search;
			const urlParams = new URLSearchParams(queryString);
			const state = urlParams.get('state')
			const code = urlParams.get('code');
			if (code != null && state && state === 'attachgoogle') {

				attachGoogleAccount(code);
			}
		});

		jQuery("#googleattachbutton").on("click", function () {

			if (socialConnection_block_attributes['userAttachedGoogle']) {
				jQuery.ajax({
					type: "delete",
					url: addNonce(JQUERY_VARS.resturl+'/removesocial'),
					data: {"socialType": "google", "token": sessionStorage.getItem('customertoken')},
					success: function (data) {
						var isValidJson = true;
						try {
							var response = JSON.parse(data);
						} catch (error) {
							isValidJson = false;
						}
						if (isValidJson) {
							socialConnection_block_attributes['userAttachedGoogle'] = false;
							jQuery("#googleAttachButtonText").text(socialConnection_block_attributes['googleButtonText']);
							jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
								'<span class="icon has-text-success"><i class="fas fa-check-square"></i></span>' +
								'<span class="ml-3">Frakoblet Google konto</span>' +
								'</div>');
							setTimeout(function () {
								jQuery("#feedback").empty();
							}, 5000);
						}else{
							jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
								'<span class="icon has-text-danger"><i class="fas fa-ban"></i></span>' +
								'<span class="ml-3">Sletning mislykkedes</span>' +
								'</div>');
							setTimeout(function () {
								jQuery("#feedback").empty();
							}, 5000);
						}
					},
					error: function (){
						jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
							'<span class="icon has-text-danger"><i class="fas fa-ban"></i></span>' +
							'<span class="ml-3">Sletning mislykkedes</span>' +
							'</div>');
						setTimeout(function () {
							jQuery("#feedback").empty();
						}, 5000);
					}
				});
			} else {
				if (window.location.href.split('?').length > 1) {
					oauthGoogleSignIn(window.location.href.split('?')[0], "attachgoogle")
				} else {
					oauthGoogleSignIn(window.location.href, "attachgoogle");
				}
			}
		});

		function attachGoogleAccount(code) {
			jQuery.ajax({
				type: "post",
				url: addNonce(JQUERY_VARS.resturl+'/googlelogin'),
				data: {"credential": code, "justValidate": true},
				success: function (data) {
					var isValidJson = true;
					try {
						var email = JSON.parse(data);
					} catch (error) {
						isValidJson = false;
					}
					if (isValidJson) {

						jQuery.ajax({
							type: "post",
							url: addNonce(JQUERY_VARS.resturl+'/attachgoogle'),
							data: {"googleident": email.googleEmail, "token": sessionStorage.getItem('customertoken')},
							success: function (data) {
								var isValidJson = true;
								try {
									var customer = JSON.parse(data);
								} catch (error) {
									isValidJson = false;
								}
								if (isValidJson) {
									socialConnection_block_attributes['userAttachedGoogle'] = true;
									jQuery("#googleAttachButtonText").text(socialConnection_block_attributes['googleRemoveButtonText']);
									jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
										'<span class="icon has-text-success"><i class="fas fa-check-square"></i></span>' +
										'<span class="ml-3">Forbundet med Google konto</span>' +
										'</div>');
									setTimeout(function () {
										jQuery("#feedback").empty();
									}, 5000);
								} else {
									jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
										'<span class="icon has-text-danger"><i class="fas fa-ban"></i></span>' +
										'<span class="ml-3">Ikke forbundet med Google konto</span>' +
										'</div>');
									setTimeout(function () {
										jQuery("#feedback").empty();
									}, 5000);
								}
							},
							error: function (){
								jQuery("#feedback").append('<div class="notification" style="position:fixed;bottom:10px;right:10px;z-index:1;">' +
									'<span class="icon has-text-danger"><i class="fas fa-ban"></i></span>' +
									'<span class="ml-3">Ikke forbundet med Google konto</span>' +
									'</div>');
								setTimeout(function () {
									jQuery("#feedback").empty();
								}, 5000);
							}
						});
					}

				}
			});
		}

		/**
		 * Google OAuth Form with Redirection
		 * @param redirecturl Redirection Url after user has logged in
		 * @param state State check for antiforgery
		 */
		function oauthGoogleSignIn(redirecturl,state) {
			// Google's OAuth 2.0 endpoint for requesting an access token
			var oauth2Endpoint = 'https://accounts.google.com/o/oauth2/v2/auth';

			// Create <form> element to submit parameters to OAuth 2.0 endpoint.
			var form = document.createElement('form');
			form.setAttribute('method', 'GET'); // Send as a GET request.
			form.setAttribute('action', oauth2Endpoint);

			// Generate nonce
			var nonce = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			for(var i = 0; i < 20; i++) {
				nonce += possible.charAt(Math.floor(Math.random() * possible.length));
			}

			// Parameters to pass to OAuth 2.0 endpoint.
			var params = {'client_id': JQUERY_VARS.googleClientId,
				'redirect_uri': redirecturl,
				'response_type': 'code',
				'scope': 'https://www.googleapis.com/auth/userinfo.email',
				'access_type': 'offline',
				'state': state};

			// Add form parameters as hidden input values.
			for (var p in params) {
				var input = document.createElement('input');
				input.setAttribute('type', 'hidden');
				input.setAttribute('name', p);
				input.setAttribute('value', params[p]);
				form.appendChild(input);
			}

			// Add form to page and submit it to open the OAuth 2.0 endpoint.
			document.body.appendChild(form);
			form.submit();
		}
	}

}
