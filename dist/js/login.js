export function init(){
    var urlParams = new URLSearchParams(window.location.search);
    if(urlParams.has('token')){
        var token = urlParams.get('token'); // true
        jQuery.ajax({
            type: "GET",
            url: addNonce(JQUERY_VARS.restUrl+"/customer?token="+token),
            credentials: 'same-origin',
            success: function(data){
                var customer = JSON.parse(data);
                sessionStorage.setItem('customertoken',token);
                sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
                sessionStorage.setItem('isMaster', customer.isMaster);
				createProductData();
                window.location.replace(JQUERY_VARS.siteUrl+'/'+JQUERY_VARS.customerInfoPage.page+'/');
            },
            error: function(data){
                sessionStorage.clear();
                goToLogin();
            }
        });
    }


    if(urlParams.has('prtoken')){
        var prtoken = urlParams.get('prtoken');
        sessionStorage.setItem('prtoken',prtoken);
        window.location.replace(JQUERY_VARS.siteUrl+'/'+JQUERY_VARS.loginPage.page+'/');
    }
 
	if(urlParams.has('e')){
		window.parent.notifyError(urlParams.get('e'));
	}

	if(urlParams.has('reset_msg')){
		window.parent.notifySuccess(urlParams.get('reset_msg'));
	}

    if(sessionStorage.getItem('customertoken') != null && sessionStorage.getItem('tokenExpire') > Math.floor(Date.now() / 1000)){
        window.location.replace(JQUERY_VARS.siteUrl+'/'+JQUERY_VARS.customerInfoPage.page+'/');
    }
    function login(){
        jQuery.ajax({
            type:"post",
            url:addNonce(JQUERY_VARS.restUrl+'/login'),
            data: {"username":jQuery("#pawpplugin_login_user").val(),"password":jQuery("#pawpplugin_login_pass").val()},
            success: function(data){
                sessionStorage.clear();
                var isValidJson = true;
                try {
                    var customer = JSON.parse(data);
                } catch (error) {
                    isValidJson = false;
                }
                if(isValidJson) {
                    if(customer.authtoken != null){
                        if(customer.otpActive){
                            jQuery("#login-container").hide();
                            jQuery("#otp-authtoken").val(customer.authtoken);
                            jQuery("#otp-container").show();
                            sessionStorage.removeItem('customertoken');                    
                        }else{
                            sessionStorage.setItem('customertoken',customer.authtoken);
                            sessionStorage.setItem('isMaster', customer.isMaster);
                            sessionStorage.setItem('tokenExpire', customer.tokenExpire);
                            createProductData();
                            window.location.replace(customer.redirectTo+'/');
                        }
                        
                    }
                } else {
                    sessionStorage.clear();
                    window.parent.notifyError("Forkert brugernavn eller kodeord");
                    jQuery("#login_error").text('Tjek brugernavn og kodeord.');
                }
            }
        });
    }

    function sendOtp(){
        jQuery.ajax({
            type:"post",
            url:addNonce(JQUERY_VARS.restUrl+'/otp-activate'),
            data: {"authtoken":jQuery("#otp-authtoken").val(),"otp":jQuery("#pawpplugin_otp").val()},
            success: function(data){
                
                var isValidJson = true;
                try {
                    var result = JSON.parse(data);
                } catch (error) {
                    isValidJson = false;
                }
                if(isValidJson) {
                    if(result.authtoken != null){
                        sessionStorage.setItem('customertoken',result.authtoken);
                        sessionStorage.setItem('isMaster', result.isMaster);
                        sessionStorage.setItem('tokenExpire', result.tokenExpire);
                        createProductData();
                        window.location.replace(result.redirectTo+'/');                        
                    }
                } else {
                    sessionStorage.clear();
                    window.parent.notifyError("Login fejlet");
                    jQuery("#login_error").text('Login fejlet');
                }
            }
        });
    }

    function savePassword(){
        jQuery("#pr_error").text('');
        var pass = jQuery("#pawpplugin_newpass1").val()
        if(verifyPass(pass)){
            jQuery.ajax({
                type:"post",
                url:addNonce(JQUERY_VARS.restUrl+'/change-password'),
                data: {"new_password1":jQuery("#pawpplugin_newpass1").val(),"new_password2":jQuery("#pawpplugin_newpass2").val(),"prtoken":sessionStorage.getItem('prtoken')},
                success: function(data){
                    sessionStorage.clear();
                    if(data.message == 'Mismatch between new password and confirmation'){
                        jQuery("#pr_error").text('Samme adgangskode skal tastes i begge felter');
                        window.parent.notifyError("Samme adgangskode skal tastes i begge felter");
                    }else{
                        window.location.replace(JQUERY_VARS.siteUrl+'/'+JQUERY_VARS.loginPage.page+'/');
                    }
                }
            });
        } else {
            window.parent.notifyError("Den valgte kode overholder ikke reglerne. Tjek ? ved siden af feltet.");
        }
    }

    function verifyPass(pass){
        var isValid = true;
        switch(JQUERY_VARS.pass_chars){
            case "none":
                isValid = pass.length >= JQUERY_VARS.pass_length;
                break;
            case "low":
                isValid = pass.length >= JQUERY_VARS.pass_length && pass.match(/[A-ZÆØÅ]{1,}/g) && pass.match(/[a-zæøå]{1,}/g);
                break;
            case "medium":
                isValid = pass.length >= JQUERY_VARS.pass_length && pass.match(/[A-ZÆØÅ]{1,}/g) && pass.match(/[a-zæøå]{1,}/g) && pass.match(/[0-9]{1,}/g);
                break;
            case "high":
                isValid = pass.length >= JQUERY_VARS.pass_length && pass.match(/[A-ZÆØÅ]{1,}/g) && pass.match(/[a-zæøå]{1,}/g) && pass.match(/[0-9]{1,}/g) && pass.match(/[\!\(\)\-\.\?\:\;\~\`\@\#\$\%\^\*\+\=]{1,}/g);
                break;
        }
        return isValid;
    }

    jQuery("#login_form_btn").on('click', function(){
        login();
    });

    jQuery("#pr_btn").on('click', function(){
        savePassword();
    });

    jQuery("#otp_btn").on('click', function(){
        sendOtp();
    });


    jQuery('#pawpplugin_login_user').keydown(function(event) {
        // enter has keyCode = 13, change it if you want to use another button
        if (event.keyCode == 13) {
            login();
        }
    });
    jQuery('#pawpplugin_login_pass').keydown(function(event) {
        // enter has keyCode = 13, change it if you want to use another button
        if (event.keyCode == 13) {
            login();
        }
    });

    if(sessionStorage.getItem('prtoken')){
        //Hide login
        jQuery("#login-container").hide();
        //Show set new password
        jQuery("#pr-container").show();

        console.log(JQUERY_VARS.pass_length)
        console.log(JQUERY_VARS.pass_chars)

        var help_text = "";
        switch(JQUERY_VARS.pass_chars){
            case "none":
                jQuery("#pass1_tooltip").remove();
                jQuery("#pass1_tooltip").remove();
                break;
            case "high":
                help_text = ", og et special tegn ( [ ! ( ) - . ? : ; ~ ` @ # $ % ^ * + = )";
            case "medium":
                help_text = ", et tal"+help_text;
            case "low":
                help_text = "Skal indeholde et stort og et lille bogstav"+help_text;
            default:
                help_text = "Minimum "+JQUERY_VARS.pass_length+" tegn. " + help_text+". "
        }
        jQuery("#pass1_tooltip").attr("data-tooltip", help_text);
        jQuery("#pass2_tooltip").attr("data-tooltip", help_text);
    }

    jQuery("#pawpplugin_forgot_pass_email").on("keyup", function(){
        if( jQuery("#pawpplugin_forgot_pass_email").val().length > 1){
            jQuery("#forgot-pass-send-button").prop("disabled", false);
        } else {
            jQuery("#forgot-pass-send-button").prop("disabled", true);
        }
    })

    jQuery("#forgot-pass-link").on('click', function(){
        jQuery("#login-container").toggleClass('is-hidden');
        jQuery("#forgot-pass-container").toggleClass('is-hidden');
    });

    jQuery("#magiclink-link").on('click', function(){
        jQuery("#login-container").toggleClass('is-hidden');
        jQuery("#magiclink-container").toggleClass('is-hidden');
    });

    jQuery("#forgot-pass-goback").on('click', function(){
        jQuery("#login-container").toggleClass('is-hidden');
        jQuery("#forgot-pass-container").toggleClass('is-hidden');
    });

    jQuery("#magiclink-goback").on('click', function(){
        jQuery("#login-container").toggleClass('is-hidden');
        jQuery("#magiclink-container").toggleClass('is-hidden');
    });

	//**-- Facebook login start --**//
	if(jQuery("#facebookbutton").length) {
		jQuery("#facebookbutton").on('click', function () {
			FB.init({
				appId: JQUERY_VARS.facebookClientId,
				cookie: true,
				xfbml: true,
				version: 'v13.0'
			});

			FB.login(function (response) {
				if (response.status === 'connected') {
					checkFacebookLoginState();
				}
			}, {scope: 'public_profile,email'});
		});

		function facebookStatusChangeCallback(response) {
			if (response.status === "connected") {
				facebookPantherCall(response.authResponse.accessToken);
			}
		}

		function checkFacebookLoginState() {
			FB.getLoginStatus(function (response) {
				facebookStatusChangeCallback(response);
			});
		}

		function facebookPantherCall(accessToken) {
			FB.api('/me', {fields: 'name, email'}, function (response) {
				jQuery.ajax({
					type: "post",
					url: addNonce(JQUERY_VARS.restUrl + '/facebooklogin'),
					data: {"email": response.email, "credential": accessToken},
					success: function (data) {
						sessionStorage.clear();
						var isValidJson = true;
						try {
							var customer = JSON.parse(data);
						} catch (error) {
							isValidJson = false;
						}
						if (isValidJson) {
							if (customer.authtoken != null) {
								sessionStorage.setItem('customertoken', customer.authtoken);
								sessionStorage.setItem('isMaster', customer.isMaster);
								sessionStorage.setItem('tokenExpire', customer.tokenExpire);
								window.location.replace(customer.redirectTo + '/');
							}
						} else {
							sessionStorage.clear();
							jQuery("#login_error").text('Tjek brugernavn og kodeord.');
						}
					}
				});
			});
		}
	}
	//**-- Facebook login end --**//

	//**-- Google login start --**//
	if(jQuery("#googlebutton").length) {
		jQuery("#googlebutton").on('click', function () {
			if (window.location.href.split('?').length > 1) {
				oauthGoogleSignIn(window.location.href.split('?')[0], "googlelogin")
			} else {
				oauthGoogleSignIn(window.location.href, "googlelogin");
			}
		});

		jQuery(document).ready(function () {
			const queryString = window.location.search;
			const urlParams = new URLSearchParams(queryString);
			const state = urlParams.get('state')
			const code = urlParams.get('code');
			if (code != null && state && state === 'googlelogin') {
				googlePantherCall(code);
			}
		});

		function googlePantherCall(code) {
			jQuery.ajax({
				type: "post",
				url: addNonce(JQUERY_VARS.restUrl+'/googlelogin'),
				data: {"credential": code},
				success: function (data) {
					sessionStorage.clear();
					var isValidJson = true;
					try {
						var customer = JSON.parse(data);
					} catch (error) {
						isValidJson = false;
					}
					if (isValidJson) {
						if (customer.authtoken != null) {
							sessionStorage.setItem('customertoken', customer.authtoken);
							sessionStorage.setItem('isMaster', customer.isMaster);
							sessionStorage.setItem('tokenExpire', customer.tokenExpire);
							window.location.replace(customer.redirectTo + '/');
						}
					} else {
						sessionStorage.clear();
						jQuery("#login_error").text('Tjek brugernavn og kodeord.');
					}
				}
			});
		}

		/**
		 * Google OAuth Form with Redirection
		 * @param redirecturl Redirection Url after user has logged in
		 * @param state State check for antiforgery
		 */
		function oauthGoogleSignIn(redirecturl, state) {
			// Google's OAuth 2.0 endpoint for requesting an access token
			var oauth2Endpoint = 'https://accounts.google.com/o/oauth2/v2/auth';

			// Create <form> element to submit parameters to OAuth 2.0 endpoint.
			var form = document.createElement('form');
			form.setAttribute('method', 'GET'); // Send as a GET request.
			form.setAttribute('action', oauth2Endpoint);

			// Generate nonce
			var nonce = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			for (var i = 0; i < 20; i++) {
				nonce += possible.charAt(Math.floor(Math.random() * possible.length));
			}

			// Parameters to pass to OAuth 2.0 endpoint.
			var params = {
				'client_id': JQUERY_VARS.googleClientId,
				'redirect_uri': redirecturl,
				'response_type': 'code',
				'scope': 'https://www.googleapis.com/auth/userinfo.email',
				'access_type': 'offline',
				'state': state
			};

			// Add form parameters as hidden input values.
			for (var p in params) {
				var input = document.createElement('input');
				input.setAttribute('type', 'hidden');
				input.setAttribute('name', p);
				input.setAttribute('value', params[p]);
				form.appendChild(input);
			}

			// Add form to page and submit it to open the OAuth 2.0 endpoint.
			document.body.appendChild(form);
			form.submit();
		}
	}
	//**-- Google login end --**//

	//** -- Apple login start --**//
	if(jQuery("#applebutton").length) {
		jQuery("#applebutton").on('click', async function () {
			AppleID.auth.init({
				clientId: JQUERY_VARS.appleClientId,
				scope: 'name email',
				redirectURI: JQUERY_VARS.appleRedirectUrl,
				state: 'applelogin',
				usePopup: true
			});

			try {
				const data = await AppleID.auth.signIn()
				if (data.authorization.state === 'applelogin') {
					jQuery.ajax({
						type: "post",
						url: addNonce(JQUERY_VARS.restUrl+'/applelogin'),
						data: {"credential": data.authorization.code},
						success: function (data) {
							sessionStorage.clear();
							var isValidJson = true;
							try {
								var customer = JSON.parse(data);
							} catch (error) {
								isValidJson = false;
							}
							if (isValidJson) {
								if (customer.authtoken != null) {
									sessionStorage.setItem('customertoken', customer.authtoken);
									sessionStorage.setItem('isMaster', customer.isMaster);
									sessionStorage.setItem('tokenExpire', customer.tokenExpire);
									window.location.replace(customer.redirectTo + '/');
								}
							} else {
								sessionStorage.clear();
								jQuery("#login_error").text('Tjek brugernavn og kodeord.');
							}
						}
					});

				}
				// Handle successful response.
			} catch (error) {
				jQuery("#login_error").text('Tjek brugernavn og kodeord.');
			}
		});
	}
	//** -- Apple login end --**//


}

function goToLogin(){
	window.location.replace(JQUERY_VARS.siteUrl+'/'+JQUERY_VARS.loginPage.page+'/');
}

function createProductData(){
    //Get available products if they are not already present
    if (sessionStorage.getItem('pa_availableProducts') == null) {
        jQuery.ajax({
            type: 'GET',
            url: addNonce(JQUERY_VARS.restUrl+'/customer/product-configs?token='+sessionStorage.getItem('customertoken')+"&is-master="+sessionStorage.getItem('isMaster')),
            credentials: 'same-origin',
            success: function(data) {
				let pConfs = new Object();
				let pData = JSON.parse(data);
				for(const i in pData){
					pConfs[pData[i].id] = pData[i];
				}
                sessionStorage.setItem('pa_availableProducts', JSON.stringify(pConfs));
				buildDependencies();
            },
            error: function(data){
                //Kunne ikke hente...
				console.log('Could not get available products');
            }
        });
    }else{
		buildDependencies();
    }
}

function buildDependencies(){
	if((sessionStorage.getItem('pa_requires') == null) || (sessionStorage.getItem('pa_isrequired') == null)){
		var products = JSON.parse(sessionStorage.getItem('pa_availableProducts'));
		var productsO = new Object();
        //requires contains arrays of products required by product identified by the key (pcid)
		var requires = new Object();
        //isrequired contains arrays of products that require the product identified by the key (pcid)
		var isrequired = new Object();

		for(const i in products){
			productsO[products[i].id] = products[i];
		}

        for(const id in productsO){
			if(productsO[id].accessGrantingProducts.length > 0){
				requires[id] = [];
				for(const aid in productsO[id].accessGrantingProducts){
					let tempReq = [];

                    for(const bid in productsO[id].accessGrantingProducts[aid]){
                        let requiresID = productsO[id].accessGrantingProducts[aid][bid];
                        tempReq.push({id: requiresID, name: getProductName(productsO[requiresID])});

                        if(isrequired[requiresID] == null){
                            isrequired[requiresID] = [];
                        }
                        isrequired[requiresID].push({id: id, name: getProductName(productsO[id])});
                    }
                    
                    requires[id].push(tempReq);

				}
			}
		}

        sessionStorage.setItem('pa_requires', JSON.stringify(requires));
        sessionStorage.setItem('pa_isrequired', JSON.stringify(isrequired));
	}
}

