export function init() { 
    jQuery.get(addNonce(JQUERY_VARS.restUrl+"/customer"), {"token": sessionStorage.getItem("customertoken")}, function (data) { 
        let customer = JSON.parse(data);
        let address = [customer.installationAddress.streetName, customer.installationAddress.houseNumber, customer.installationAddress.floortxt, customer.installationAddress.apartment];

        jQuery("#sbbu_name").val(customer.name);
        jQuery("#sbbu_address").val(address.join(" ") + ", " + customer.installationAddress.postcode + " " + customer.installationAddress.city);
        if (customer.vatNo) { 
            jQuery("#sbbu_cvr").val(customer.vatNo);
            jQuery("#sbbu_entitled").val(customer.contact.name);
            
        }
    });

    jQuery("input[name=customer-token]").val(sessionStorage.getItem("customertoken"));

    jQuery("#sbbu_send_button").on("click", function () {
        jQuery(this).attr("disabled", true);
        jQuery("#sbbu_name").css("outline", "");
        jQuery("#sbbu_current_provider").css("outline", "");
        jQuery("#sbbu_current_provider_no").css("outline", "");
        jQuery("#sbbu_confirm").css("outline", "");
        jQuery("#sbbu_cvr").css("outline", "");
        let payload = {};
        let noErrors = true;
        jQuery("#sbbuform").serializeArray().forEach(element => {
            payload[element.name] = element.value;
            if (element.value == "") { 
                switch (element.name) {
                    case "name":
                        jQuery("#sbbu_name").css("outline", "2px solid red");
                        noErrors = false;
                        break;
                    case "current_provider":
                        jQuery("#sbbu_current_provider").css("outline", "2px solid red");
                        noErrors = false;
                        break;
                    case "current_provider_no":
                        jQuery("#sbbu_current_provider_no").css("outline", "2px solid red");
                        noErrors = false;
                        break;
                }
            }
            if (element.name == "cvr" && element.value.length != 8 || isNaN(element.value)) {
                jQuery("#sbbu_cvr").css("outline", "2px solid red");
                notifyError("CVR skal være 8 tegn langt og kun indeholde tal");
                noErrors = false;
             }
        });

        if (payload["confirm"] != 1) {
            jQuery("#sbbu_confirm").css("outline", "2px solid red");
            noErrors = false;
        }

        if (noErrors == false) {
            notifyError("Udfyld venligst markerede felter")
            jQuery(this).attr("disabled", false);
        } else { 
            jQuery.post(addNonce(JQUERY_VARS.restUrl+"/sendsbbu"), payload, function (data) {
                notifySuccess("SBBU afsendt med succes");
                setTimeout(function () {
                    location.replace(data.redirectTo);
                }, 3000)
            })
        }
    });


}