export function init(){
    var packetIdents = [jQuery("#allente-buy-button").data('pack1ident'),jQuery("#allente-buy-button").data('pack2ident'),jQuery("#allente-buy-button").data('pack3ident')];
    var productFound = 0;
    var counter = 0;
    packetIdents.forEach(packageIdent => {
        jQuery.get(addNonce(JQUERY_VARS.restUrl+'/customer/products/type/access?ident='+packageIdent+'&token='+sessionStorage.getItem('customertoken')), function(data){
            var result = JSON.parse(data);
            if(result["hasAccess"] != true){
                jQuery(".allente_frame_step_1").addClass("is-hidden");
                jQuery(".allente_tv_container").html("<div>Du har desværre ikke adgang til Allente produkterne med dine nuværende produkter</div>");
            }
        });
        jQuery.get(addNonce(JQUERY_VARS.restUrl+'/customer/products/type/?ident='+packageIdent+'&token='+sessionStorage.getItem('customertoken')), function(data){
            var result = JSON.parse(data);
            if(result.amountOfProducts > 0){
                productFound = 1;
                var packageNum;
                jQuery.get(addNonce(JQUERY_VARS.restUrl+'/customer/products?token='+sessionStorage.getItem('customertoken')), function(data){
                    sessionStorage.setItem('tokenExpire', Math.floor(Date.now() / 1000)+900);
                    var result = JSON.parse(data);
                    result.forEach(customerProduct => {
                        if(customerProduct.productConf.ident == packageIdent){
                            if(customerProduct.customAttributes.isunderchange){
                                jQuery("#allente-current-product-change-product").remove();
                            }
                            packageNum = packetIdents.indexOf(customerProduct.productConf.ident)+1;
                            var packagesInfo = jQuery(".allente-current-product-container").data("packages");
                            switch(packageNum) {
                                case 1:
                                    jQuery("#allente-current-product-name").text("Pakkevalg: "+packagesInfo.packet1ProductName);
                                    jQuery("#allente-current-product-content").text(packagesInfo.packet1Content);
                                    jQuery("#allente-current-product-content-image>img").prop("src",packagesInfo.packet1ContentImage);
                                    jQuery("#allente-current-product-price-normal").text(packagesInfo.packet1Price);
                                    jQuery("#allente-current-product-price-normal2").text(packagesInfo.packet1Price2);
                                    jQuery("#allente-current-product-price-discount").text(packagesInfo.packet1PriceDiscountAmount);
                                    jQuery("#allente-current-product-price-discount2").text(packagesInfo.packet1PriceDiscountAmount2);
                                    jQuery("#allente-current-product-price-final").text(packagesInfo.packet1PriceWithDiscount);
                                    jQuery("#allente-current-product-price-final2").text(packagesInfo.packet1PriceWithDiscount2);

                                    break;
                                case 2:
                                    jQuery("#allente-current-product-name").text("Pakkevalg: "+packagesInfo.packet2ProductName);
                                    jQuery("#allente-current-product-content").text(packagesInfo.packet2Content);
                                    jQuery("#allente-current-product-content-image>img").prop("src",packagesInfo.packet2ContentImage);
                                    jQuery("#allente-current-product-price-normal").text(packagesInfo.packet2Price);
                                    jQuery("#allente-current-product-price-normal2").text(packagesInfo.packet2Price2);
                                    jQuery("#allente-current-product-price-discount").text(packagesInfo.packet2PriceDiscountAmount);
                                    jQuery("#allente-current-product-price-discount2").text(packagesInfo.packet2PriceDiscountAmount2);
                                    jQuery("#allente-current-product-price-final").text(packagesInfo.packet2PriceWithDiscount);
                                    jQuery("#allente-current-product-price-final2").text(packagesInfo.packet2PriceWithDiscount2);
                                    break;
                                case 3:
                                    jQuery("#allente-current-product-name").text("Pakkevalg: "+packagesInfo.packet3ProductName);
                                    jQuery("#allente-current-product-content").text(packagesInfo.packet3Content);
                                    jQuery("#allente-current-product-content-image>img").prop("src",packagesInfo.packet3ContentImage);
                                    jQuery("#allente-current-product-price-normal").text(packagesInfo.packet3Price);
                                    jQuery("#allente-current-product-price-normal2").text(packagesInfo.packet3Price2);
                                    jQuery("#allente-current-product-price-discount").text(packagesInfo.packet3PriceDiscountAmount);
                                    jQuery("#allente-current-product-price-discount2").text(packagesInfo.packet3PriceDiscountAmount2);
                                    jQuery("#allente-current-product-price-final").text(packagesInfo.packet3PriceWithDiscount);
                                    jQuery("#allente-current-product-price-final2").text(packagesInfo.packet3PriceWithDiscount2);
                                    break;
                            }
                            jQuery("#allente-current-product-infotext").text(packagesInfo.currentProductInfoText);
                       }
                    });
                });
                
                jQuery(".allente-current-product-container").removeClass("is-hidden");
                jQuery("button#allente-current-product-change-product").on("click", function(){
                    var order_button = jQuery("button[data-tvpacket="+packageNum+"]");
                    order_button.text("Nuværende");
                    order_button.prop("disabled", true);
                    jQuery(".allente-current-product-container").addClass("is-hidden");
                    jQuery(".allente_frame_step_1").removeClass("is-hidden");
                    sessionStorage.setItem("isProductchange", true)
                });
            }
            
            counter++;
            if(counter === packetIdents.length){
                if(productFound == 0 ){
                    jQuery(".allente_frame_step_1").removeClass("is-hidden");
                }
            }
        });
    });
    var packageName;
    var packagePrice;
    var packagePrice2;
    var packageDiscount;
    var packageDiscount2;
    var packageFinalprice;
    var packageFinalprice2;
    var packageCommitment;
    var packageContent;
    var packageContent2;
    var packageContentImage;
    // STEP 1
    jQuery(".product-order-button").on("click", function(){
        sessionStorage.setItem('tvpacket', jQuery(this).data('tvpacket'));
        jQuery(".allente_frame_step_1").addClass("is-hidden");
        jQuery(".allente_frame_step_2").removeClass("is-hidden");
        packageName         = jQuery(this).data('name');
        packageContent      = jQuery(this).data('content');
        packageContent2     = jQuery(this).data('content2');
        packageContentImage = jQuery(this).data('contentimage');
        packagePrice        = jQuery(this).data('tvpacketprice');
        packagePrice2        = jQuery(this).data('tvpacketprice2');
        packageDiscount     = jQuery(this).data('tvpacketdiscount');
        packageDiscount2     = jQuery(this).data('tvpacketdiscount2');
        packageFinalprice   = jQuery(this).data('tvpacketfinalprice');
        packageFinalprice2   = jQuery(this).data('tvpacketfinalprice2');
        packageCommitment   = jQuery(this).data('tvpacketcommitment');
        jQuery("#allente-chosen-product").text("Pakkevalg: "+packageName);
        jQuery("#step2-product-content").text(packageContent);
        jQuery("#step2-product-content2").text(packageContent2);
        jQuery("#step2-product-content-image>img").prop("src", packageContentImage);
        jQuery("#step2-product-price").text(packagePrice);
        jQuery("#step2-product-price2").text(packagePrice2);
        jQuery("#step2-product-discount").text(packageDiscount);
        jQuery("#step2-product-discount2").text(packageDiscount2);
        jQuery("#step2-product-finalprice").text(packageFinalprice);
        jQuery("#step2-product-finalprice2").text(packageFinalprice2);
        jQuery("#step2-product-commitment").text(packageCommitment);
        jQuery("#step2-terms-link").attr("href", JQUERY_VARS.paymentTOSUrl);
    });
    // STEP 2
    jQuery("#allente-change-product").on("click", function(){
        jQuery(".allente_frame_step_1").removeClass("is-hidden");
        jQuery(".allente_frame_step_2").addClass("is-hidden");
        jQuery("#step2-terms>input[type=checkbox]").prop("checked", false);
    })
    jQuery("#allente-go-back").on("click", function(){
        jQuery(".allente_frame_step_1").removeClass("is-hidden");
        jQuery(".allente_frame_step_2").addClass("is-hidden");
        jQuery("#step2-terms>input[type=checkbox]").prop("checked", false);
    })
    jQuery("#step2-terms>input[type=checkbox]").on("change", function(){
        if(jQuery("#step2-terms>input[type=checkbox]").is(":checked")){
            jQuery("#allente-buy-button").prop("disabled", false);
        }
        if(!jQuery("#step2-terms>input[type=checkbox]").is(":checked")){
            jQuery("#allente-buy-button").prop("disabled", true);
        }
    })
    // STEP 3 preperations
    jQuery("#allente-buy-button").on("click", function(){
        if(jQuery("input#startdate").val() == '' || jQuery("input#birthdate").val() == ''){
            // var formattedDate = null;
            jQuery("#buy-button-alert").toggleClass("is-hidden");
        } else {
            var startDate = new Date(jQuery("input#startdate").val());
            var birthDate = new Date(jQuery("input#birthdate").val());
            var startMonth = String((startDate.getMonth()+1)).padStart(2, '0');
            var formattedStartDate = startDate.getDate()+'/'+startMonth+'/'+startDate.getFullYear();
            var birthMonth = String((birthDate.getMonth()+1)).padStart(2, '0');
            var formattedBirthDate = birthDate.getDate()+'/'+birthMonth+'/'+birthDate.getFullYear();
            var wantSettopbox = jQuery("input#settopbox").prop("checked");
            orderPacket(packageName,packagePrice,packagePrice2,packageDiscount,packageDiscount2,packageFinalprice,packageFinalprice2,packageCommitment, packageContent, packageContentImage,formattedStartDate,formattedBirthDate, wantSettopbox);
            jQuery(".allente_frame_step_2").addClass("is-hidden");
            jQuery(".allente_frame_step_3").removeClass("is-hidden");
        }
    })

    jQuery("#buy-button-alert").on('click', function(){
        jQuery(this).toggleClass("is-hidden");
    });
}

function orderPacket(packageName,packagePrice,packagePrice2,packageDiscount,packageDiscount2,packageFinalprice, packageFinalprice2,packageCommitment, packageContent, packageContentImage,startDate,birthDate, wantSettopbox){
    var packetNum = sessionStorage.getItem('tvpacket');
    
    switch (packetNum) {
        case "1":
            var packetIdent = jQuery("#allente-buy-button").data('pack1ident');
            break;
        case "2":
            var packetIdent = jQuery("#allente-buy-button").data('pack2ident')
            break;
        case "3":
            var packetIdent = jQuery("#allente-buy-button").data('pack3ident')
            break;
        }
        var isChange = sessionStorage.getItem("isProductchange") ? true : false;
    jQuery.post(addNonce(JQUERY_VARS.restUrl+'/customer/products/type?token='+sessionStorage.getItem('customertoken')), {'ident':packetIdent, 'startdate':startDate, 'birthdate':birthDate, 'wantSettopbox':wantSettopbox, 'isProductChange':isChange}, function(data){
        jQuery("#allente-receipt-package").text("Pakkevalg: "+packageName);
        jQuery("#allente-receipt-content").text(packageContent);
        jQuery("#allente-receipt-content-image>img").prop("src", packageContentImage);
        startDate == null ? jQuery("#allente-receipt-startdate-date").text('Hurtigst muligt') : jQuery("#allente-receipt-startdate-date").text(startDate);
        jQuery("#allente-receipt-normal-price").text(packagePrice);
        jQuery("#allente-receipt-normal-price2").text(packagePrice2);
        jQuery("#allente-receipt-discount").text(packageDiscount);
        jQuery("#allente-receipt-discount2").text(packageDiscount2);
        jQuery("#allente-receipt-final-price").text(packageFinalprice);
        jQuery("#allente-receipt-final-price2").text(packageFinalprice2);
        jQuery("#allente-receipt-commitment").text(packageCommitment);
    });
}

