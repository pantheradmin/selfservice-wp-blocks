<?php

class CustomDB{

    private $table_name;

    public function __construct(){
        global $wpdb;
        $this->table_name = 'payment_payload';
	}

    private function checkTable(){
        global $wpdb;
        $tblData = $wpdb->get_results("SHOW TABLES LIKE '".$wpdb->prefix . $this->table_name."'", ARRAY_N );

        if(!count($tblData)){
            $this->createTable();
        }
    }

    private function createTable(){
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
    
        $sql = "CREATE TABLE ".$wpdb->prefix . $this->table_name." (
                pid int NOT NULL AUTO_INCREMENT,
                ptime datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                ppayload text NOT NULL,
                PRIMARY KEY  (pid)
            ) ".$charset_collate.";";
    
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        dbDelta( $sql );
    }

    public function savePaymentData($data): int {
        global $wpdb;

        $this->checkTable();
        $payload = json_encode($data);  
        
        $wpdb->query($wpdb->prepare("INSERT INTO ".$wpdb->prefix . $this->table_name." (ptime, ppayload) VALUES (%s, %s)", current_time( 'mysql' ), $payload));

        return $wpdb->insert_id;
    }

    public function updatePaymentData($data, $id): void {
        global $wpdb;

        if(is_numeric($id) && $id > 0){
            $payload = json_encode($data); 
            $wpdb->query($wpdb->prepare("UPDATE ".$wpdb->prefix . $this->table_name." SET ppayload = '%s' WHERE pid = '%s'", $payload, $id));
        }
    }

    public function getPaymentData($id){
        global $wpdb;
        $pload = $wpdb->get_row("SELECT ppayload FROM ".$wpdb->prefix . $this->table_name." WHERE pid = '".$id."'", ARRAY_N );
        return $pload[0];
    }

    public function deletePaymentData($id){
        global $wpdb;
        $wpdb->delete($wpdb->prefix . $this->table_name , array('pid' => $id));        
    }
}