<?php
function fetchMedia(){
    //Fetch channels
    $u = get_rest_url() . "/pantheradmin/v1/prchannels?_wpnonce=".wp_create_nonce("wp_rest");

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $u);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $fContents = curl_exec($curl);
    curl_close($curl);
    $channelPackage = json_decode(json_decode($fContents), true);
    
    $channels = array();
    
    foreach($channelPackage as $pData){
        foreach($pData["channels"] as $ch){
            if(!$channels[$ch["id"]]){
                //Fetch logo
                $w = get_rest_url() . "/pantheradmin/v1/channels/".$ch["id"]."/logo?_wpnonce=".wp_create_nonce("wp_rest");

                try{
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $w);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    $cContents = curl_exec($curl);
                    curl_close($curl);

                    $channels[$ch["id"]] = $ch["logo"];

                    if($cContents){
                        $fp = fopen(plugin_dir_path(__FILE__) . "images/channel-logos/".$ch["logo"], 'wb');
                        fwrite($fp, $cContents);
                        fclose($fp);
                    }
                }catch(Exception $e){
                    echo "Write failed: ".$ch["logo"];
                }
            }
        }
    }

    echo "Logoer er hentet.";
}