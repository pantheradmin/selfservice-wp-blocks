<?php

class WebHelper{
    static function httpCall($url, $headerArray, $data, $method = 'GET'){
		$curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_HTTPHEADER => $headerArray,
                CURLOPT_POSTFIELDS => $data
                //CURLOPT_POSTFIELDS => json_encode($data)
            ));
            $response = curl_exec($curl);
            curl_close($curl);
			return $response;
	}
}

class Reepay{

    public function getLink($amount, $subscription, $urls, $payload){
        $options = get_option('psb_plugin_options');
        $accountNumberRnd = time().rand(10,99);
        $orderId = "order-".$accountNumberRnd;

        if($amount > 0){
            $postData = array("order" => array("handle" => $orderId,
                                            "amount" => $amount,
                                            "currency" => $options["currency"],
                                            "customer" => array("handle" => "c-".$accountNumberRnd																
                                            ),
                                            "metadata" => $payload
                            ),
                            "accept_url" => $urls["acceptUrl"],
                            "cancel_url" => $urls["cancelUrl"]
                            );

            if($subscription){
                $postData["recurring"] = true;
            }

            $paymentUrl = "https://checkout-api.reepay.com/v1/session/charge";

        }else{
            $postData = array("create_customer" => array("handle" => "c-".$accountNumberRnd,
                                                        "metadata" => $payload),
                            "accept_url" => $urls["acceptUrl"],
                            "cancel_url" => $urls["cancelUrl"]	
                            );

            $paymentUrl = "https://checkout-api.reepay.com/v1/session/recurring";
        }

        $headerArray = [
            "Authorization: Basic ".base64_encode(':'.$options["payment_api_key"]),
            "Content-Type: application/json",
            "Accept: application/json",
            "Accept-version: v10"
        ];

        return WebHelper::httpCall($paymentUrl, $headerArray, json_encode($postData), "POST");
    }

    public function callbackToRemote($callbackData){
        $options = get_option('psb_plugin_options');

        $sData = array_merge($callbackData,
                             $this->fetchPaymentMethodData($callbackData["customer"], $callbackData["payment_method"]),
                             $this->fetchSubscriptionMetaData($callbackData["customer"]));

        if(is_numeric($sData["payloadId"])){
            $cdb = new CustomDB();
            $payload = json_decode($cdb->getPaymentData($sData["payloadId"]), true);    
            $sData = array_merge($sData, $payload);
            $cdb->updatePaymentData($sData, $sData["payloadId"]);
        }


        $headerArray = [
            "Authorization: Basic ".base64_encode(':'.$options["payment_api_key"]),
            "Content-Type: application/json",
            "Accept: application/json",
            "Accept-version: v10"
        ];

        $returnVal = $sData;

        if(strlen($sData["apiTarget"])){
            WebHelper::httpCall($sData["apiTarget"], [],array("data" => json_encode($sData)), "POST");
            //array("data" => json_encode($callbackData))
            $returnVal = true;
        }

        return $returnVal;
    }
	
	public function fetchPaymentMethodData($customerid, $card_id){
        if($customerid && $card_id){
            $response = WebHelper::httpCall("https://api.reepay.com/v1/customer/".$customerid."/payment_method/card/".$card_id, $this->getHeader(), [], "GET");
            return json_decode($response, true);
        }
			
		return array();
	}
	
    public function fetchSubscriptionMetaData($handle){		
		if(!$handle){
			return array();
        }
        $response = WebHelper::httpCall("https://api.reepay.com/v1/customer/".$handle."/metadata", $this->getHeader(), [], "GET");		
		return json_decode($response, true);
	}

    private function getHeader(){
        $options = get_option('psb_plugin_options');

        return [
            "Authorization: Basic ".base64_encode($options["payment_api_key"].":''"),
            "Content-Type: application/json",
            "Accept: application/json"
        ];
    }
}

class Quickpay{    

    public function getLink($amount, $subscription, $urls, $payload){
        $options = get_option('psb_plugin_options');
        $accountNumberRnd = time().rand(10,99);
		$orderId = $accountNumberRnd;

        $params = array("order_id" => $orderId,
                        "currency" => $options["currency"],
                        //Legacy requirement - may not be needed in the future
                        "variables" => (Object)$payload
                        );
        
        if($subscription)
            $params["description"] = get_site_url();
            
        
        $pLink = $this->generateLink($amount, $subscription, $urls, $params, $options);

        $replyData = array("id" => $orderId, "url" => $pLink["url"]);
        return json_encode($replyData);
    }

    private function generateLink($amount, $subscription, $urls, $params, $options){
        error_log("PARAMS 1: ".var_export($params, true));
        if($params['variables']->paymentMethods) {
            $paymentMethods = $params['variables']->paymentMethods;
        }
        $headerArray = [
            "Authorization: Basic ".base64_encode(':'.$options["payment_api_key"]),
            "Content-Type: application/json",
            "Accept: application/json",
            "Accept-version: v10"
        ];

        $subPath = $subscription ? "subscriptions" : "payments";
        $paymentUrl = "https://api.quickpay.net/".$subPath;

        $paymentResultJson = WebHelper::httpCall($paymentUrl, $headerArray, json_encode($params), "POST");
        $paymentResult = json_decode($paymentResultJson, true);

        if(isset($paymentResult["id"])){
            $params = array("amount" => $amount,
                            "continue_url"=>$urls["acceptUrl"],
                            "cancel_url"=>$urls["cancelUrl"],
                            "callback_url" =>$urls["callbackUrl"]
                            );
            if($paymentMethods){
                $params['payment_methods'] = $paymentMethods;
            }

            error_log("PARAMS 2: ".var_export($params, true));
            $paymentLinkUrl = "https://api.quickpay.net/".$subPath."/".$paymentResult["id"]."/link";
			$paymentLinkResultJson = WebHelper::httpCall($paymentLinkUrl, $headerArray, json_encode($params), "PUT");
            error_log("LINK: ".$paymentLinkResultJson);

			return json_decode($paymentLinkResultJson, true);
    
        }else{
            //Fejl
            return $paymentResult;
        }
    }
}

class Epay{

    public function getLink($amount, $subscription, $urls, $payload){
        $options = get_option('psb_plugin_options');
        $accountNumberRnd = time().rand(10,99);
		$orderId = "".$accountNumberRnd;
        $amount = $amount ? $amount : "0";
        $subscription = $subscription ? 1 : 0;

        //Legacy requirement - may not be needed in the future
        $orderData = (strpos($urls["callbackUrl"], "?") !== false) ? "&orderdata=" : "?orderdata=";
        $orderData .= urlencode(json_encode($payload));
        $orderData .= "&invoiceid=".$payload["invoiceid"];

        $params = array("merchantnumber" => $options["merchant_id"],
                        "amount" => $amount, 
                        "currency" => $options["currency"], 
                        "subscription"=> $subscription, 
                        "accepturl"=>$urls["acceptUrl"], 
                        "windowstate"=>3,
                        "language"=>"1",
                        "ownreceipt"=>1, 
                        "callbackurl"=>$urls["callbackUrl"].$orderData, 
                        "instantcallback"=>1, 
                        "description"=>"kontonr:".$accountNumberRnd, 
                        "orderid"=>$orderId);

        if($subscription == 1)
            $params["subscriptiontype"] = "cardonfile";

        $hash = md5(implode("", array_values($params)) . $options["payment_gateway_hash"]);
        $params["hash"] = $hash;
        $params["accepturl"] = urlencode($params["accepturl"]);
        $params["callbackurl"] = urlencode($params["callbackurl"]);

        $paymentLink = "https://ssl.ditonlinebetalingssystem.dk/integration/ewindow/Default.aspx?";        
        $paymentLinkGlue = "";
        foreach($params as $k => $v){
            $paymentLink .= $paymentLinkGlue.$k."=".$v;
            $paymentLinkGlue = "&";
        }

        $replyData = array("id" => $orderId, "url" => $paymentLink);
        return json_encode($replyData);
    }
}